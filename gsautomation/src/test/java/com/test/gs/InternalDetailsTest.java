package com.test.gs;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.gs.test.base.PageBase;
import com.gs.test.page.object.GsContentChainDetailsPage;
import com.gs.test.page.object.GsInternalDetailsPage;
import com.gs.test.page.object.GsLoginPage;
import com.gs.test.page.object.GsSearchPage;
import com.gs.test.page.object.GsWelcomePage;
import com.gta.fmwk.TestBase;

public class InternalDetailsTest{
	private static Logger logger = Logger.getLogger(TestBase.class);
	public WebDriver driver;
	private GsLoginPage gsLoginPage;
	

		

	@BeforeTest 
	public  void initialization()
	{
		/** Initialize driver **/
		 System.setProperty("webdriver.ie.driver","C:\\SeleniumFramework\\gsautomation\\src\\test\\resources\\IEDriverServer.exe");
			WebDriver driver= new InternetExplorerDriver();
			
		driver.get("https://hotels.test.gta-travel.com/gs/auth/securelogin");
		logger.info("Browser opened and url entered");
		gsLoginPage = PageFactory.initElements(driver, GsLoginPage.class);
		
		
		
	}
	
	@Test
	public void EditInternalDetails() throws InterruptedException
	{
		System.out.println("We are in InternalDetailsTest");
		// Code to be removed later
		GsWelcomePage gsWelcomePage =gsLoginPage.loginWith("GTA", "lcp20s", "London123");
		logger.info("Login page displayed- UN and Pwd entered");
		GsSearchPage gsSearchPage=gsWelcomePage.clickOnContent();
		logger.info("Clicked on Welcome page");
		gsSearchPage.searchFor("France", "Avig", "Auberge De");
		GsInternalDetailsPage gsInternalDetailsPage= gsSearchPage.navigateToContentScreen();
		/** To edit the details and click on 'cancel' button. Check values are not updated **/
		
		gsInternalDetailsPage.editInternalDetails("LAS", "No", "Cancel");
		
		/** To edit the details and click on 'Update' button. check Values are reflected correctly **/
		gsInternalDetailsPage.editInternalDetails("MAS", "Yes", "Update");
		//GsContentChainDetailsPage gsChainDetailsPage= gsSearchPage.selectFirstContent();
	
		
		
	}
	
	@AfterTest
	public void tearDown()
	{
		System.out.println("Logging out");
		//gsInternalDetailsPage.quit();
		//gsLoginPage.driver.quit();
		
	}
	
	
}
