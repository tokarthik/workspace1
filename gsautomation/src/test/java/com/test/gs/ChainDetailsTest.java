package com.test.gs;


import java.net.URL;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.gs.test.page.object.GsLoginPage;
import com.gs.test.page.object.GsSearchPage;
import com.gs.test.page.object.GsWelcomePage;
import com.gs.test.page.object.GsContentChainDetailsPage;
import com.gta.fmwk.TestBase;

public class ChainDetailsTest {
	


private static Logger logger = Logger.getLogger(TestBase.class);

 /** Gs Login object */
 private GsLoginPage gsLoginPage;
 public WebDriver driver;
 private GsContentChainDetailsPage gsChainDetailsPage;
 
	
 @BeforeMethod
 public void init() {
	 
	  /** Need to set the chromedriver.exe file path to run the tests in google chrome browser. */
		URL url = ClassLoader.getSystemClassLoader().getResource("chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", url.getPath());

		/** Initialize the chrome driver. */
		driver = new ChromeDriver();
		
		/** Open the gs application in firefox web browser. */
		logger.info("Login to GS application");
		driver.get("https://hotels.demo.gta-travel.com/gs");
		
		/** Get the Gs Login page object using Pagefactory to execute the user logged in. */
		gsLoginPage = PageFactory.initElements(driver, GsLoginPage.class);
  }
  
 @Test
  public void testForIndependentRecord() {
	  GsWelcomePage gsWelcomePage = gsLoginPage.loginWith("GTA","gsuser1","London1");
	  
	  GsSearchPage gsSearchPage = gsWelcomePage.clickOnContent();

	  //gsSearchPage.searchFor("France", "Avig", "Auberge De Cassagne");
	  gsSearchPage.searchFor("France", "Avig", "All Seasons");

	  GsContentChainDetailsPage gsChainDetailsPage = gsSearchPage.selectFirstContent();
	  gsChainDetailsPage.create();
  }
 
 /** Step 16 : Edit with Future date and part of chain set to yes */
 @Test(dependsOnMethods="testForIndependentRecord")
  public void testEditToPartOfChainWithFutureDate(){
	 
	 gsChainDetailsPage.selectFirstChainRecord();
	 gsChainDetailsPage.edit(true,"2","3","4","Aug","2016");

  }
 
 /** Step 17 : Edit with Future date and part of chain set to yes with different chain and brand*/
 @Test(dependsOnMethods="testEditToPartOfChainWithFutureDate")
  public void testEditToSelectDifferentChainAndBrandForFutureDate(){
	 
	 gsChainDetailsPage.selecLastChainRecord();
	 gsChainDetailsPage.edit(true,"4","5","5","Aug","2016");

  }
 
 /** Step 18 : Edit with Future date and part of chain set to No */
 @Test(dependsOnMethods="testEditToSelectDifferentChainAndBrandForFutureDate")
  public void testEditToNotPartOfChainWithFutureDate(){
	 
	 gsChainDetailsPage.selecLastChainRecord();
	 gsChainDetailsPage.edit(false,"2","3","6","Aug","2016");

  }
 
 /** Step 19 : Edit with Future date and part of chain set to Yes and click on cancel button*/
 @Test(dependsOnMethods="testEditToSelectDifferentChainAndBrandForFutureDate")
  public void testEditToPartOfChainWithFutureDateAndCancel(){
	 
	 gsChainDetailsPage.selecLastChainRecord();
	 gsChainDetailsPage.cancel(true,"2","3","6","Aug","2016");

  }
 
 /** Step 20 : Click on History and check the options */
 @Test(dependsOnMethods="testEditToSelectDifferentChainAndBrandForFutureDate")
  public void testSelectHistoryOption(){
	 
	 gsChainDetailsPage.selectHistory();
	 gsChainDetailsPage.cancelHistory();

  }
 
 /** Step 21 : Click on History and enter the past date and click on go button */
  @Test(dependsOnMethods="testSelectHistoryOption")
  public void testSearchHistoryForPastDate(){	
	  gsChainDetailsPage.selectHistory();
	  gsChainDetailsPage.searchHistory("6", "8", "2013");
	  gsChainDetailsPage.verifyHistoryRecordsWithDate(3, "6", "Aug", "13");
	  gsChainDetailsPage.cancelHistory();
  }
  
  /** Step 22 : Click on History and enter the future date and click on go button */
	@Test(dependsOnMethods="testSearchHistoryForPastDate")
	public void testSearchHistoryForFutureDate(){
		gsChainDetailsPage.selectHistory();
		gsChainDetailsPage.searchHistory("6", "8", "2013");
		gsChainDetailsPage.verifyHistoryRecordsWithDate(3, "6", "Aug", "13");
		gsChainDetailsPage.cancelHistory();
	}

	/** Step 23 : Click on History and donot enter any date and click on go button */
	@Test(dependsOnMethods="testSearchHistoryForFutureDate")
	public void testSearchHistoryForNoDate(){
		gsChainDetailsPage.selectHistory();
		gsChainDetailsPage.cancelHistory();
	}


  @AfterMethod
  public void tearDown() {
	  driver.quit();
  }

}
