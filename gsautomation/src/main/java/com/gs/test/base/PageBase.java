package com.gs.test.base;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.xmlbeans.XmlCalendar;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is the base class which holds the webdriver
 * object for the page operations.
 * 
 * @author Suvarchala Vege
 */
/**
 * @author suvarchala
 *
 */
public class PageBase {

	/** Webdriver object. */
	public final WebDriver driver;
	
	/** Parameterized constructor for this class. */
	public PageBase(WebDriver driver){
		this.driver = driver;
	}
	
	/**
	 * This method waits for the specific element to be loaded which is specified by the locator
	 * @param locator
	 */
	public void waitForElement(By locator){
		WebDriverWait wait = new WebDriverWait(driver, 15);
	    wait.until( ExpectedConditions.presenceOfElementLocated(locator));
	}
	
	/**
	 * This method returns the last row from the webtable
	 * @param divEl
	 * @return WebElement
	 */
	public WebElement getLastRowFromTable(WebElement divEl){
		List<WebElement> rowsElList = getTRList(divEl);
		if (rowsElList == null || rowsElList.size() < 2){
			return null;
		}
	    return rowsElList.get(rowsElList.size()-1);
	}

	/**
	 * This method returns the first row from the webtable 
	 * @param divEl
	 * @return WebElement
	 */
	public WebElement getFirstRowFromTable(WebElement divEl){
		return getTRElementFromTable(divEl, 1);
	}

	/**
	 * This method returns the values from a row in a list
	 * @param divEl
	 * @param rowNum
	 * @return WebElement
	 */
	public WebElement getTRElementFromTable(WebElement divEl, int rowNum){
		List<WebElement> rowsElList = getTRList(divEl);
		if (rowsElList == null || rowsElList.size() < 2 || rowNum >= rowsElList.size()){
			return null;
		}
	    return rowsElList.get(rowNum);
	}

	/**
	 * Returns all the rows from a webtable as a list
	 * @param divEl
	 * @return List
	 */
	public List<WebElement> getTRList(WebElement divEl){
	    WebElement tableEl = divEl.findElement(By.tagName("tbody"));

	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    List<WebElement> rowsElList = tableEl.findElements(By.tagName("tr"));
	    return rowsElList;
	}
	
	/**
	 * Returns all the data from a webtable as a list
	 * @param divEl
	 * @return List
	 */
	public List<WebElement> getTDList(WebElement divEl){
	    WebElement tableEl = divEl.findElement(By.tagName("tbody"));

	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    List<WebElement> dataElList = tableEl.findElements(By.tagName("td"));
	    return dataElList;
	}
	
	/**
	 * Parses the Date and returns it
	 * @param gsDate
	 * @return Calendar
	 */
	public static Date parseStartDate(String gsDate) {
		DateFormat inputDate = new SimpleDateFormat("ddMMMyy");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String gsStartDate = gsDate.trim();
		Date gsInputStartDate = null;
		try {
			gsInputStartDate = (Date)inputDate.parse(gsStartDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String startDateFor = formatter.format(gsInputStartDate);
		Date gsStartDateFor = null;
		try {
			gsStartDateFor = (Date)formatter.parse(startDateFor);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return gsStartDateFor;
	}

	/**
	 * This method converts todays date to a calendar object
	 * @param date
	 * @return Calendar
	 */
	public static Date DateToCalendar(Date date){ 
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		String date1 = format.format(date);
		Date date2 = null;
		try {
			date2 = (Date)format.parse(date1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date2;		
	}
	
	/**
	 * This methods holds the test execution for 3 seconds
	 */
	public void threadWait(){
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}


