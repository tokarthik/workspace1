package com.gs.test.page.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.gs.test.base.PageBase;
import static com.gs.test.page.object.IPageElements.welcomeMenu;
import static com.gs.test.page.object.IPageElements.content;

public class GsWelcomePage extends PageBase{

	/**
	 * Parameterized constructor for this class.
	 * @param driver
	 */
	public GsWelcomePage(WebDriver driver){
		super(driver);
	}
	
	/** This refers to the welcome Menu Screen. */
	@FindBy(how = How.ID, using = welcomeMenu)
	private WebElement gsWelcomeMenu;
	
	/** This refers to the Content tab. */
	@FindBy(how = How.LINK_TEXT, using = content)
	private WebElement gsContent;
	
	/**
	 * This method clicks on content Image 
	 * and returns the GS Search Page
	 * @return GsSearchPage
	 */
	public GsSearchPage clickOnContent() {
		
		threadWait();
		waitForElement(By.linkText("Content"));
		gsContent.click();	
		
		/** Create the object for Search page. */
		return PageFactory.initElements(driver, GsSearchPage.class);
		
	}

}
