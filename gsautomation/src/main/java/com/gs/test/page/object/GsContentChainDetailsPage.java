package com.gs.test.page.object;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.gs.test.base.PageBase;

import static com.gs.test.page.object.IPageElements.*;
//import static com.gs.test.page.object.IPageElements.chainDetCancel;
//import static com.gs.test.page.object.IPageElements.chainDetChain;
//import static com.gs.test.page.object.IPageElements.chainDetCreate;
//import static com.gs.test.page.object.IPageElements.chainDetMenu;
//import static com.gs.test.page.object.IPageElements.chainDetNo;
//import static com.gs.test.page.object.IPageElements.chainDetSave;
//import static com.gs.test.page.object.IPageElements.chainDetYes;
//import static com.gs.test.page.object.IPageElements.chainDetContent;
//import static com.gs.test.page.object.IPageElements.chainDetCreateContent;
//import static com.gs.test.page.object.IPageElements.edit;
//import static com.gs.test.page.object.IPageElements.chainDetChainId;
//import static com.gs.test.page.object.IPageElements.chainDetBrandId;
//import static com.gs.test.page.object.IPageElements.chainDetDay;
//import static com.gs.test.page.object.IPageElements.chainDetMonth;
//import static com.gs.test.page.object.IPageElements.chainDetYear;
//import static com.gs.test.page.object.IPageElements.chainUpdate;
//import static com.gs.test.page.object.IPageElements.chainCreateMessage;

public class GsContentChainDetailsPage extends PageBase{

	/**
	 * This is a constructor for this class
	 * @param driver
	 */
	public GsContentChainDetailsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	/** This is the WebElement object for Brand. */
	@FindBy(how = How.ID, using = chainDetBrand)
	private WebElement gsChainDetBrand;
	
	/** This is the WebElement object for Cancel button. */
	@FindBy(how = How.ID, using = chainDetCancel)
	private WebElement gsChainDetCancel;
	
	/** This is the WebElement object for Chain dropdown. */
	@FindBy(how = How.ID, using = chainDetChain)
	private WebElement gsChainDetChain;
	
	/** This is the WebElement object for Create Element. */
	@FindBy(how = How.LINK_TEXT, using = chainDetCreate)
	private WebElement gsChainDetCreate;
	
	/** This is the WebElement object for Chain Menu. */
	@FindBy(how = How.ID, using = chainDetMenu)
	public WebElement gsChainDetMenu;
	
	/** This is the WebElement object for Radio button No. */
	@FindBy(how = How.ID, using = chainDetNo)
	public WebElement gsChainDetNo;
	
	/** This is the WebElement object for Save button. */
	@FindBy(how = How.CLASS_NAME, using = chainDetSave)
	private WebElement gsChainDetSave;
	
	/** This is the WebElement object for Radio button Yes. */
	@FindBy(how = How.CLASS_NAME, using = chainDetYes)
	public By gsChainDetYes;
	
	/** This is the WebElement object for Radio button Yes. */
	@FindBy(how = How.ID, using = chainDetContent)
	public WebElement gsChainDetContent;
	
	/** This is the WebElement object for Radio button Yes. */
	@FindBy(how = How.ID, using = chainDetCreateContent)
	private WebElement gsChainDetCreateContent;
	
	/** This is the WebElement object for Edit Link. */
	@FindBy(how = How.LINK_TEXT, using = edit)
	public By gsEdit;
	
	/** This is the WebElement object for Chain Drop down. */
	@FindBy(how = How.ID, using = chainDetChainId)
	private WebElement gsChainDetChainId;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.ID, using = chainDetBrandId)
	private WebElement gsChainDetBrandId;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.ID, using = chainDetDay)
	private WebElement gsChainDetDay;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.ID, using = chainDetMonth)
	private WebElement gsChainDetMonth;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.ID, using = chainDetYear)
	private WebElement gsChainDetyear;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.ID, using = chainUpdate)
	private WebElement gsChainUpdate;
	
	/** This is the WebElement object for Brand Drop down. */
	@FindBy(how = How.CLASS_NAME, using = chainCreateMessage)
	public By gsChainCreateMessage;
	
	/** This is the WebElement object for Buttons Line. */
	@FindBy(how = How.CLASS_NAME, using = chainButtonsLine)
	public WebElement gsChainButtonsLine;
	
	/** This is the WebElement object for Button. */
	@FindBy(how = How.CLASS_NAME, using = chainButton)
	public By gsChainButton;
	
	/** This is the WebElement object for Button. */
	@FindBy(how = How.ID, using = chainHistory)
	public By gsChainHistory;
	
	/** This is the WebElement object for Chain Details History. */
	@FindBy(how = How.ID, using = chainDetHistory)
	public By gsChainDetHistory;
	
	/** This is the WebElement object for Chain Details History Day. */
	@FindBy(how = How.ID, using = chainDetHistoryDay)
	private WebElement gsChainDetHistoryDay;
	
	/** This is the WebElement object for Chain Details History Month. */
	@FindBy(how = How.ID, using = chainDetHistory)
	private WebElement gsChainDetHistoryMonth;
	
	/** This is the WebElement object for Chain Details History Year. */
	@FindBy(how = How.ID, using = chainDetHistory)
	private WebElement gsChainDetHistoryYear;
	
	/** This is the WebElement object for Chain Details History Search. */
	@FindBy(how = How.ID, using = chainDetHistorySearch)
	private WebElement gsChainDetHistorySearch;
	
	
	
	
	
	/**
	 * This method clicks on the create element and creates the chain details
	 * @throws ParseException 
	 */
	public void create(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String[] words = {"Independent", "Independent", dateFormat.format(date)};  
   
		List<String> wordList = Arrays.asList(words);  
		      
		/** Clicks the chain Menu */
		gsChainDetMenu.click();
		gsChainDetMenu.findElement(By.linkText("Create")).click();	    
		waitForElement(By.id(chainDetCreateContent));
		threadWait();
		
		/** Clicks on the create menu and clicks on save button  */
		gsChainDetNo.click();
		threadWait();
		List<WebElement> buttonList = gsChainDetCreateContent.findElements(By.className(chainDetSave));
		buttonList.get(1).click();
		threadWait();
		waitForElement(By.id("chainDetails_list"));

		/** Gets the number of rows from Chain Details Table  */
		List<WebElement> rowsElList = getTRList(gsChainDetContent);
		int rowsOfChainTbl = rowsElList.size();
		if (rowsOfChainTbl == 1) {
			Assert.assertTrue(true,"Record Inserted in the Chains table");
			
			/** Retrieves the data from the table and gets stored in the list */
			List<WebElement> dataElList = getTDList(gsChainDetContent);
			int dataCount = dataElList.size();
			for (int i = 0;i<dataCount-1;i++) {
				Date d1 = parseStartDate(dataElList.get(2).getText().trim().replace("-", "").trim());
				Date d2 = DateToCalendar(date);
	
				if (!d1.equals(d2)) {
					Assert.assertFalse(false,"Data inserted in the table is " + d1 + " but the actual value should be " + d2);
				}
				if (!(dataElList.get(i).getText().trim()).equals(wordList.get(i))) {
					Assert.assertFalse(false,"Data inserted in the table is " + dataElList.get(i).toString() + " but the actual value should be " + wordList.get(i));
					break;
				}
			} /** End of For Loop  */
			
		}	/** End of Create Method  */
		
	}
	
	/**
	 * This method selects the first chain record and clicks on it
	 */
	public void selectFirstChainRecord(){
	    threadWait();	    
	    
	    getFirstRowFromTable(gsChainDetContent).click();
	    waitForElement(By.id("chainDetails_tr"));
	}
	
	/**
	 * This method selects the last chain record and clicks on it
	 */
	public void selecLastChainRecord(){
	    threadWait();	    
	    
	    getLastRowFromTable(gsChainDetContent).click();
	    waitForElement(By.id("chainDetails_tr"));
	}
	
	/**
	 * This method clicks on the edit and changes the brand, chain and selects the future date and clicks on update button
	 * @param partOfChainProperty
	 * @param chainValue
	 * @param brandValue
	 * @param date
	 * @param month
	 * @param year
	 */
	public void edit(boolean partOfChainProperty, String chainValue, String brandValue, String date, String month, String year){
		change(partOfChainProperty, chainValue, brandValue, date, month, year);
		gsChainUpdate.click();
		String strCreateMessage = gsChainDetMenu.findElement(gsChainCreateMessage).getText().trim();
		if (strCreateMessage != "Record created") {
			Assert.assertFalse(false, "Record is not updated successfully");
		}
		else {
			Assert.assertTrue(true, "Record Created successfully");
		}
	}
	
	/**
	 * This method modifies the values for the first row in the chain details table if it already exists
	 * else creates a new record
	 * @param partOfChainProperty
	 * @param chainValue
	 * @param brandValue
	 * @param date
	 * @param month
	 * @param year
	 */
	private void change(boolean partOfChainProperty, String chainValue, String brandValue, String date, String month, String year){

		gsChainDetMenu.click();

		waitForElement(By.linkText("Edit"));
	    
		gsChainDetMenu.findElement(gsEdit).click();
	    waitForElement(By.id("chainDetails-partOfChain_Y"));
	    
	    /** If block will be executed if there is already an independent chain and the record gets modified  */
	    if (partOfChainProperty){
	    	driver.findElement(gsChainDetYes).click();
		    waitForElement(By.id("chainDetails-chainId"));

		    Select selectChain = new Select(gsChainDetChainId);
			selectChain.selectByValue(chainValue);
			
			threadWait();
			waitForElement(By.id("chainDetails-brandId"));
			
			Select selectBrand = new Select(gsChainDetBrandId);
			selectBrand.selectByValue(brandValue);
	    }
	    /** else block gets if there is no independent chain record  */
	    else{
	    	gsChainDetNo.click();
			threadWait();
	    }
		
		if (date != null){
			Select selectDate = new Select(gsChainDetDay);
			selectDate.selectByValue(date);
		}
		
		if (month != null){
			Select selectMonth = new Select(gsChainDetMonth);
			selectMonth.selectByValue(month);
		}
		
		if (year != null){
			Select selectYear = new Select(gsChainDetyear);
			selectYear.selectByValue(year);
		}
	}
	
	/**
	 * This method modifies the values and clicks on cancel button
	 * @param partOfChainProperty
	 * @param chainValue
	 * @param brandValue
	 * @param date
	 * @param month
	 * @param year
	 */
	public void cancel(boolean partOfChainProperty, String chainValue, String brandValue, String date, String month, String year){
		change(partOfChainProperty, chainValue, brandValue, date, month, year);
		List<WebElement> buttonList = gsChainButtonsLine.findElements(gsChainButton);
		buttonList.get(0).click();
	}
	
	/**
	 * This method selects and clicks on History link
	 */
	public void selectHistory(){
		
		gsChainDetMenu.click();
		threadWait();
		gsChainDetMenu.findElement(gsChainHistory).click();
		threadWait();
		threadWait();
	    waitForElement(By.id("chainDetailsHistoricalDate_day"));
	}
	
	/**
	 * This method clicks on the cancel button of the history
	 */
	public void cancelHistory(){
		
		List<WebElement> buttonList = gsChainDetHistory.findElements((SearchContext) gsChainButton);
		threadWait();
		buttonList.get(1).click();
	}
	
	/**
	 * This method searches the history with the specified date.
	 * @param date
	 * @param month
	 * @param year
	 */
	public void searchHistory(String date, String month, String year){
		threadWait();
		Select selectDate = new Select(gsChainDetHistoryDay);
		selectDate.selectByValue(date);
	
		Select selectMonth = new Select(gsChainDetHistoryMonth);
		selectMonth.selectByValue(month);

		Select selectYear = new Select(gsChainDetHistoryYear);
		selectYear.selectByValue(year);
		threadWait();
		gsChainDetHistorySearch.click();
		threadWait();
	}
	
	/**
	 * This method verifies the history chain records with dates
	 * @param expectedTotalCount
	 * @param date
	 * @param month
	 * @param year
	 */
	public void verifyHistoryRecordsWithDate(int expectedTotalCount, String date, String month, String year){

		List<WebElement> trList = getTRList(gsChainDetContent);
		int actualTotalCount = 0;
		if (trList != null && trList.size() > 0){
			actualTotalCount = trList.size();
		}
		Assert.assertEquals(actualTotalCount, expectedTotalCount);
		
		for (int i=0;i<trList.size();i++){
			WebElement tr = trList.get(i);
			WebElement dateTdEl = tr.findElements(By.tagName("td")).get(2);
			String validityDate = dateTdEl.getText();
			String[] hisDatesArray = validityDate.split("-");
			String hisStartDate = hisDatesArray[0];
			String hisDate = hisStartDate.substring(0, 2);
			String hisMonth = hisStartDate.substring(2,5);
			String hisYear = hisStartDate.substring(5);
			
			Date hisDateObj = formatDate(hisDate,hisMonth,hisYear);
			Date searchDateObj = formatDate(date,month,year);
			
			if (hisDateObj.compareTo(searchDateObj) <0){
				Assert.assertTrue(false);
				break;
			}
		}
	}
	
	/**
	 * This method formats the date based on the day, month and year
	 * @param date
	 * @param month
	 * @param year
	 * @return
	 */
	private Date formatDate(String date, String month, String year){
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
		Date dateObj = null;
		try {
			dateObj = format.parse(date+ "-" +month+ "-" + year);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateObj;
	}

	    

}
