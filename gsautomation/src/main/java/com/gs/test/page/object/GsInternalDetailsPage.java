package com.gs.test.page.object;

import static com.gs.test.page.object.IPageElements.*;

import java.util.List;

import javax.swing.JOptionPane;


import net.sourceforge.htmlunit.corejs.javascript.ast.SwitchCase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.selenesedriver.FindElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.WebConsole.Logger;
import com.gs.test.base.PageBase;
import com.gta.fmwk.TestBase;

public class GsInternalDetailsPage extends PageBase {
		
	public GsInternalDetailsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	private WebDriver driver;
	private String strOldServiceCity;
	public String strTimeZone;
	public String strCurrentGroupOption;
	public String strCurrentTime;
	public String strRadioBtnOption;
	public String strIsSubmit;
	public String strSubmittype;
	public int intRowIndex=0;
	public String strCellValue;
	public String[][] arrInternal=new String[4][2];
	public String[][] arrCpyInternal= new String[4][2];
	public String strGroupFriendlyoptionSelected;
	public int i=0;int j=0;
	/** This refers to the Internal Details Menu of Provider Details screen */
	
	@FindBy(how=How.ID, using=internalDetailsContent)
	private WebElement gsinternalDetailsContent;
	
	@FindBy(how= How.ID, using=internalDetailsTab)
	private WebElement gsinternalDetailsTab;
	
	@FindBy(how = How.ID, using = editOptnInternalDetails)
	private WebElement gseditOptnInternalDetails;
	
	
	@FindBy(how = How.ID, using = timeZoneStr)
	private WebElement gstimeZoneStr;
	

	@FindBy(how = How.ID, using = currentTime)
	private WebElement gscurrentTime;
	

	@FindBy(how = How.ID, using = ServiceCity)
	private WebElement gsServiceCity;
	
	@FindBy(how=How.XPATH, using=GroupFriendlyYes)
	private WebElement gsGroupFriendlyYes;
	
	@FindBy(how = How.XPATH, using=GroupFriendlyNo)
	private WebElement gsGroupFriendlyNo;
	
	@FindBy(how=How.XPATH, using=LegacyServiceCity)
	private WebElement gsLegacyServiceCity;
	
	@FindBy(how=How.ID, using=internalDetailsContenttable)
	private WebElement gsinternalDetailsContenttable;
	
	@FindBy(how=How.LINK_TEXT, using =logout)
	private WebElement gslogout;
	
	
	
	public void editInternalDetails(String strServiceCity, String strRadioBtnOption, String strSubmittype) throws InterruptedException	
	{
		arrCpyInternal=retrieveWebTableData(gsinternalDetailsContenttable, 4, 2 );
		strTimeZone=arrCpyInternal[0][1];
		System.out.println("Time Zone displayed: "+ strTimeZone);
		strCurrentTime=arrCpyInternal[1][1];
		/** To Validate Current Time displayed correctly**/
		toValidateCurrentTime(strCurrentTime);	
		strGroupFriendlyoptionSelected=arrCpyInternal[2][1];
		System.out.println("Group Friendly option displayed: "+strGroupFriendlyoptionSelected);
		strOldServiceCity=arrCpyInternal[3][1];		
		System.out.println("Service City Code displayed: "+strOldServiceCity);
		System.out.println("**************************************************************************");		
		 WebElement editLink=gseditOptnInternalDetails.findElement(By.linkText("Edit"));
		 editLink.click();						
		try{	gsServiceCity.clear();}
			catch(Exception ei)
			{
				ei.printStackTrace();
				editLink.click();
			}
		gsServiceCity.clear();
		Thread.sleep(2000L);
		gsServiceCity.sendKeys(strServiceCity);		
		System.out.println("ServiceCity Entered- "+strServiceCity);
		/** To select Radio Button OPTION **/
		clickSpecificRadioButtonOption( strRadioBtnOption);				
		/** Enter details and click on 'Cancel' or 'Update' button **/		
		clickButton(gsinternalDetailsTab, "gsButton", strSubmittype);
		
		if(strSubmittype.toLowerCase().equalsIgnoreCase("cancel")){
			System.out.println("***************************************************************************************");
			verifyInternalDetails(strOldServiceCity,strRadioBtnOption,"cancel");}
		else
			if(strSubmittype.toLowerCase().equalsIgnoreCase("update")){
				System.out.println("***************************************************************************************");
				verifyInternalDetails(strServiceCity,strRadioBtnOption,"update" );}
			else
			{	System.out.println("Button name entered is invalid- PLEASE CHECK"+strSubmittype);
				Assert.assertTrue(false, "Button name entered is invalid- PLEASE CHECK"+strSubmittype);	}		
					
	}
	
	/**
	 * Function to click on 'Yes' or 'No' Radio button
	 */
	
	public void clickSpecificRadioButtonOption(String strRadioBtnOption){
		if(!(gsGroupFriendlyNo.isSelected()) && (strRadioBtnOption.equalsIgnoreCase("No")))
		{
			gsGroupFriendlyNo.click();
			System.out.println("Group friendly Radio button 'No' selected successfully");
		}
		else
		{
			if(!(gsGroupFriendlyYes.isSelected()) && (strRadioBtnOption.equalsIgnoreCase("Yes")))
			{
				gsGroupFriendlyYes.click();
				System.out.println("Group friendly Radio button 'Yes' selected successfully");
			}
		}
	}
	
	
	/**
	 * Function to Validate Current Time displayed correctly
	 */
	public void toValidateCurrentTime(String strCurrentTime){
		if(strCurrentTime.matches(".*\\d.*")){
			System.out.println("Current time displayed: "+strCurrentTime);
			Assert.assertTrue(true, " Current time displayed Successfully: "+strCurrentTime);
		}
		else
		{
			System.out.println("Current time not displayed: "+strCurrentTime);
			Assert.assertTrue(false, " Current time not displayed: "+strCurrentTime);
		}
	}
	/** To click on Button , identified by  classname specified
	 * @param driver
	 * @param objClassName
	 * @param strButtonName
	 */
	public void clickButton(WebElement objWebElement, String objClassName, String strButtonName){
		List<WebElement> buttonList =objWebElement.findElements(By.className("gsButton"));		
		for(WebElement button: buttonList){
			if(button.getAttribute("value").equalsIgnoreCase(strButtonName)){
				button.click();
				break;}
		}
	}
	
	/**
	 * Function to retrieve all the value from a webtable and put into array
	 * @param objWebElement
	 * @param intRowCount
	 * @param intColumnCount
	 * @return
	 */
	public String[][] retrieveWebTableData(WebElement objWebElement, int intRowCount,int intColumnCount){
		String[][] arrWebtableValue=new String[intRowCount][intColumnCount];
		List<WebElement> allRows=objWebElement.findElements(By.tagName("tr"));
		i=0;
		for (WebElement row : allRows){	
				
			List<WebElement> cells =row.findElements(By.tagName("td"));
			j=0;
			for(WebElement cell:cells)
				{	//System.out.println(cell.getText());
					
					arrWebtableValue[i][j]=cell.getText();
					j=j+1;
				}
			i=i+1;				
			}
	
		return arrWebtableValue;
	}
		

	
	/**
	 * Function to retrieve value from webtable
	 * @param intRowNum
	 * @param intColNum
	 */
	/*
	public String retrieveDatatableCellValue(WebElement objWebElement, int intRowNum, int intColNum){
	 
		List<WebElement> allRows=objWebElement.findElements(By.tagName("tr"));
						for (WebElement row : allRows){
					k=k+1;
					List<WebElement> cells =row.findElements(By.tagName("td"));
						for(WebElement cell:cells)
							{	l=l+1;//System.out.println(cell.getText());
							
								if((k==intRowNum) && (l==intColNum))
								{	strCellValue=cell.getText();
									return strCellValue;}
							}
						l=0;
						}
				k=0;
				
						
		 return null;
	}
	*/
	public void verifyInternalDetails(String strCheckServiceCity,String strRadioBtnOption,String strIsUpdate )
	{
		if(strIsUpdate.equalsIgnoreCase("Update")){
			/** To verify value reflected after 'Update' button **/
			 System.out.println("To verify 'Update' button functionality");
			 arrCpyInternal=retrieveWebTableData(gsinternalDetailsContenttable, 4, 2 );
			 if(arrCpyInternal[2][1].contains(strRadioBtnOption)){
					System.out.println("Group friendly option value reflected successfully");
					Assert.assertTrue(true, "Group friendly option value reflected successfully");}
				else
				{	System.out.println("Group friendly option value not reflected--PLEASE CHECK"+"Value displayed: "+arrCpyInternal[2][1]+" does not match with "+strCheckServiceCity);
					Assert.assertTrue(false, "Group friendly option value not reflected"+"Value displayed: "+arrCpyInternal[2][1]+" does not match with "+strCheckServiceCity);}
						 
			if(strCheckServiceCity.contains(arrCpyInternal[3][1])){
				System.out.println("Updated Service city value reflected successfully");
				Assert.assertTrue(true, "Updated Service city value reflected successfully");}
			else
			{	System.out.println("Updated Service city value not reflected--PLEASE CHECK"+"Value displayed: "+strCheckServiceCity+" does not match with "+arrCpyInternal[3][1]);
				Assert.assertTrue(false, "Updated Service city value not reflected"+"Value displayed: "+strCheckServiceCity+" does not match with "+arrCpyInternal[3][1]);}
			
			}
		
		else
			if(strIsUpdate.equalsIgnoreCase("Cancel")){
				System.out.println("To verify 'Cancel' button functionality");
				arrCpyInternal=retrieveWebTableData(gsinternalDetailsContenttable, 4, 2 );
				 if(!(arrCpyInternal[2][1].contains(strRadioBtnOption))){
						System.out.println("Group friendly option value reflected successfully");
						Assert.assertTrue(true, "Group friendly option value reflected successfully");}
					else
					{	System.out.println("Group friendly option value not reflected--PLEASE CHECK"+"Value displayed: "+arrCpyInternal[2][1]+" does not match with "+strRadioBtnOption);
						Assert.assertTrue(false, "Group friendly option value not reflected"+"Value displayed: "+arrCpyInternal[2][1]+" does not match with "+strRadioBtnOption);}
							 
					
				if(strCheckServiceCity.contains(arrCpyInternal[3][1])){
				System.out.println(" Updated Service city not changed and reflected correctly: "+strCheckServiceCity);
				Assert.assertTrue(true, "Updated Service city not changed and reflected correctly: "+strCheckServiceCity);}
				else {
					System.out.println(" Updated Service city changed which is incorrect. Old Service city:"+ strCheckServiceCity+" New Service City:"+gsServiceCity.getAttribute("value"));
					Assert.assertTrue(false,"Updated Service city changed which is incorrect. Old Service city:"+strCheckServiceCity+" New Service City:"+gsServiceCity.getAttribute("value") );}
					}
			else
			{
				System.out.println("Invalid button name/functionality enteredm, PLEASE CHECK -"+ strIsUpdate);
				Assert.assertTrue(false,"Invalid button name/functionality enteredm, PLEASE CHECK -"+ strIsUpdate );
			}
			
	}
	
	public void logout()
	{
		gslogout.click();
		driver.quit();
	}
	
	public void selectRadioButton(WebDriver driver, String objName, String strOptionToSelect )
	{
		List<WebElement> radios = driver.findElements(By.name(objName));
		for(WebElement radio: radios)
		{
			if(radio.getText().equals(strOptionToSelect))
			{
				System.out.println("Radio button selected for Group Friendly option is '"+strOptionToSelect+"'");
			}
			else
			{
				System.out.println("Radio button selected for Group Friendly option is '"+strOptionToSelect+"'");
				radio.click();
			}
		}
	}

}
