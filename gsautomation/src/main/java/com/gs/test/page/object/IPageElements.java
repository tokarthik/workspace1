package com.gs.test.page.object;

/**
 * This class holds all the page element locator constants.
 * 
 
 */
public interface IPageElements {
	
	/** GS Login page element locator names. */
	public static final String qualifier = "qualifier";
	public static final String userName = "username";
	public static final String passWord = "password";
	public static final String signOn = "login";
	
	/** GS Welcome page element locator names. */
	public static final String welcomeMenu = "internalMenu";
	public static final String content = "Content"; 
	
	/** GS Search Screen page element locator names. */
    public static final String country = "country-name";
    public static final String city = "city-name";
    public static final String cityChoices = "city-choices";
    public static final String propertyName = "name";
    public static final String search = "search";
    public static final String results = "results";
    public static final String resultsRow = "row-0";  
    
    
    public static final String editOptnInternalDetails="internalDetailsContentMenuOptions" ;
    public static final String internalDetailsTab="internalDetails_edit";
    public static final String internalDetailsContenttable= "internalDetailsContent";
    public static final String timeZoneStr="timeZoneStr";
    public static final String currentTime="currentTime";
    public static final String ServiceCity="legacyServiceCity";
    public static final String GroupFriendlyYes=".//*[@id='internalDetails_edit']/table/tbody/tr[3]/td[2]/input[1]";
    public static final String GroupFriendlyNo=".//*[@id='internalDetails_edit']/table/tbody/tr[3]/td[2]/input[2]";
    public static final String LegacyServiceCity=".//*[@id='internalDetailsContent']/table/tbody/tr[4]/td[2]";
    public static final String logout="Log out";
    public static final String internalDetailsContent="internalDetailsContent";
    
//  public static final String CONTRACT_SRCH_SELECTCITY = "//a[starts-with(.,'";
//  public static final String CONTRACT_SRCHRESULTS_HEADER = "results";
//  public static final String CONTRACT_SRCHRESULTS_ROW = "row-0";
//  public static final String CONTRACT_SRCHRESULTS_OPTIONS = "options";
//  public static final String CONTRACT_SRCHRESULTS_DISPLAY_X = "//div[@id='options']/ul/li/a";


}
