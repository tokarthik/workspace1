package com.gs.test.page.object;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.gs.test.base.PageBase;

import static com.gs.test.page.object.IPageElements.content;
import static com.gs.test.page.object.IPageElements.country;
import static com.gs.test.page.object.IPageElements.city;
import static com.gs.test.page.object.IPageElements.cityChoices;
import static com.gs.test.page.object.IPageElements.propertyName;
import static com.gs.test.page.object.IPageElements.search;
import static com.gs.test.page.object.IPageElements.results;
import static com.gs.test.page.object.IPageElements.resultsRow;

public class GsSearchPage extends PageBase{

	/**
	 * Parameterized constructor for this class.
	 * @param driver
	 */
	public GsSearchPage(WebDriver driver){
		super(driver);
	}
	
	/** This is the WebElement object for Country. */
	@FindBy(how = How.ID, using = country)
	private WebElement gsCountry;
	
	/** This is the WebElement object for City. */
	@FindBy(how = How.ID, using = city)
	private WebElement gsCity;
	
	/** This is the WebElement object for City Choices. */
	@FindBy(how = How.ID, using = cityChoices)
	private WebElement gsCityChoices;
	
	/** This is the WebElement object for Property Name. */
	@FindBy(how = How.ID, using = propertyName)
	private WebElement gsPropertyName;
	
	/** This is the WebElement object for Search button. */
	@FindBy(how = How.ID, using = search)
	private WebElement gsSearch;
	
	/** This is the WebElement object for results. */
	@FindBy(how = How.ID, using = results)
	private WebElement gsResults;
	
	/** This is the WebElement object for results table. */
	@FindBy(how = How.ID, using = resultsRow)
	private WebElement gsResultsRow;
	
	/** This is the WebElement object for content Link. */
	@FindBy(how = How.LINK_TEXT, using = content)
	private WebElement gsContent;
	
	/**
	 * This method takes the country, city and propertyName and clicks on search button
	 * and returns the GS Search Page
	 * @param qualifier : qualifier
	 * @param userName : userName
	 * @param passWord : passWord
	 */
	public void searchFor(String country, String city, String propertyName) {
		
		/** enter the search text into country field. */
		gsCountry.sendKeys(country);
		
		/** enter the search text into city field. */
		gsCity.sendKeys(city);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		gsCity.sendKeys(Keys.ENTER);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		gsCityChoices.findElement(By.linkText("Avignon")).click();
		
		
				
		/** enter the search text into propertyName field. */
		gsPropertyName.sendKeys(propertyName);
		
		/** Click the search button ("Search") on the screen. */
		gsSearch.submit();

	}
	
	/**
	 * This method selects the property and clicks on the display link
	 * and returns the GsContentChainDetailsPage
	 * @return GsContentChainDetailsPage
	 */
	public GsContentChainDetailsPage selectFirstContent(){
		
		/** Wait for the results to be displayed in the result table. */
		waitForElement(By.id("results"));
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
	    //gsResults.findElement(gsResultsRow).click();
	    gsResultsRow.click();
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    //gsResults.findElement(gsContent).click();
	    gsContent.click();
	    waitForElement(By.id("propertyHeaderShowSection"));
	    
	    return PageFactory.initElements(driver, GsContentChainDetailsPage.class);
	}
	
	public GsInternalDetailsPage navigateToContentScreen()
	{
		waitForElement(By.id("results"));
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
	    //gsResults.findElement(gsResultsRow).click();
	    gsResultsRow.click();
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    //gsResults.findElement(gsContent).click();
	    gsContent.click();
	    waitForElement(By.id("propertyHeaderShowSection"));
	    
	    return PageFactory.initElements(driver, GsInternalDetailsPage.class);
	}
	
	

}
