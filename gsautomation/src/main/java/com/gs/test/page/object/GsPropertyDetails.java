package com.gs.test.page.object;

import static com.gs.test.page.object.IPageElements.hoteleditpropname;
import static com.gs.test.page.object.IPageElements.hoteleditxpath;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import static com.gs.test.page.object.IPageElements.*;

import com.gs.test.base.PageBase;
//import com.test.gs.GsContentHotelNameAddress;
//import com.test.gs.GsContentHotelNameAddress;
import com.test.gs.GsContentHotelNameAddress;

public class GsPropertyDetails extends PageBase {

	public GsPropertyDetails(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/** This is the WebElement click edit property name in property details page. */
	@FindBy(how = How.XPATH, using = hoteleditxpath)
	private WebElement gsEditHotel;
	
	/** This is the WebElement  enter property name in property details page. */
	@FindBy(how = How.ID, using = hoteleditpropname)
	private WebElement gsEditpropname;
	
	
	/** This is the WebElement  enter street1 name in property details page. */
	@FindBy(how = How.ID, using = hoteleditstreetname1)
	private WebElement gsEditpropstreet1;
	
	/** This is the WebElement  enter street2 name in property details page. */
	@FindBy(how = How.ID, using = hoteleditstreetname2)
	private WebElement gsEditpropstreet2;
	
	/** This is the WebElement  enter city name in property details page. */
	@FindBy(how = How.ID, using = hoteleditcityname)
	private WebElement gsEditpropcity;
	
	/** This is the WebElement  enter zip/county in property details page. */
	@FindBy(how = How.ID, using = hoteleditstatecounty)
	private WebElement gsEditpropstate;
	
	/** This is the WebElement  enter zipcode in property details page. */
	@FindBy(how = How.ID, using = hoteleditzipcode)
	private WebElement gsEditzipcode;
	
	/** This is the WebElement  enter telephone in property details page. */
	@FindBy(how = How.ID, using = hoteledittelephone)
	private WebElement gsEditproptelephone;
	
	/** This is the WebElement  enter email in property details page. */
	@FindBy(how = How.ID, using = hoteleditemail)
	private WebElement gsEditpropemail;
	
	/** This is the WebElement click update button in property details page. */
	@FindBy(how = How.ID, using = hoteleditupdate)
	private WebElement gsEditupdatebutton;
	
	/** This is the WebElement click postal address checkbox in property details page. */
	@FindBy(how = How.NAME, using = hoteleditpostalcheck)
	private WebElement gsEditpostaladdresscheck;
	
	/** This is the WebElement enter stree1 name in property details page. */
	@FindBy(how = How.ID, using = hoteleditpostaladdress1)
	private WebElement gsEditpostaladdress1;

	/** This is the WebElement enter stree2 name in property details page. */
	@FindBy(how = How.ID, using = hoteleditpostaladdress2)
	private WebElement gsEditpostaladdress2;
	
	
	/** This is the WebElement enter city name in property details page. */
	@FindBy(how = How.ID, using = hoteleditpostalcityname)
	private WebElement gsEditpostalcity;
	
	
	/** This is the WebElement enter state name in property details page. */
	@FindBy(how = How.ID, using = hoteleditpostalstate)
	private WebElement gsEditpostalstate;
	
	/** This is the WebElement enter postcode in property details page. */
	@FindBy(how = How.ID, using = hoteleditpostalpostcode)
	private WebElement gsEditpostalpostcode;
	
	
	public GsContentHotelNameAddress PropertDetailsEdit(String Propertyname,String Address1,String Address2,String City,String State,String Postcode,String Telephone,String Website){
		/** Wait for the results to be displayed in the result table. */
		waitForElement(By.xpath("/html/body/div/div/div[7]/div[3]/table/tbody/tr/td/div/span[2]/span/a"));
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
	    //gsResults.findElement(gsResultsRow).click();
	    gsEditHotel.click();
	    
	    waitForElement(By.id(hoteleditpropname));
	    gsEditpropname.clear();
	    gsEditpropname.click();
	    gsEditpropname.sendKeys(Propertyname);
	    
	    waitForElement(By.id(hoteleditstreetname1));
	    gsEditpropstreet1.clear();
	    gsEditpropstreet1.click();
	    gsEditpropstreet1.sendKeys(Address1);
	    
	    waitForElement(By.id(hoteleditstreetname2));
	    gsEditpropstreet2.clear();
	    gsEditpropstreet2.click();
	    gsEditpropstreet2.sendKeys(Address2);
	    
	   waitForElement(By.id(hoteleditcityname));
	    //gsEditpropcity.clear();
	    //gsEditpropcity.click();
	   
	    
	    /** enter the search text into city field. */
	   /* gsEditpropcity.sendKeys(City);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    gsEditpropcity.sendKeys(Keys.ENTER);
			    
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitForElement(By.linkText("Aso"));
		gsEditpropcity.findElement(By.linkText("Aso")).click();*/
	    
	    waitForElement(By.id(hoteleditstatecounty));
	    gsEditpropstate.clear();
	    gsEditpropstate.click();
	    gsEditpropstate.sendKeys(State);
	    
	    waitForElement(By.id(hoteledittelephone));
	    gsEditzipcode.clear();
	    gsEditzipcode.click();
	    gsEditzipcode.sendKeys(Telephone);
	    
	    waitForElement(By.id(hoteleditzipcode));
	    gsEditproptelephone.clear();
	    gsEditproptelephone.click();
	    gsEditproptelephone.sendKeys(Telephone);
	    
	    
	    waitForElement(By.id(hoteleditemail));
	    gsEditpropemail.clear();
	    gsEditpropemail.click();
	    gsEditpropemail.sendKeys(Website);
	    
	    gsEditupdatebutton.click();
	    
	    return PageFactory.initElements(driver, GsContentHotelNameAddress.class);
		}

	
	public GsContentHotelNameAddress postalPropertDetailsEdit(String Address1,String Address2,String City,String State,String Postcode){
		/** Wait for the results to be displayed in the result table. */
		//waitForElement(By.xpath("/html/body/div/div/div[7]/div[3]/table/tbody/tr/td/div/span[2]/span/a"));
	    try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    
	    //gsResults.findElement(gsResultsRow).click();
	    gsEditHotel.click();
	    
	    gsEditpostaladdresscheck.click();

	    
	    waitForElement(By.id(hoteleditpostaladdress1));
	    gsEditpostaladdress1.clear();
	    gsEditpostaladdress1.click();
	    gsEditpostaladdress1.sendKeys(Address1);
	    
	    waitForElement(By.id(hoteleditpostaladdress2));
	    gsEditpostaladdress2.clear();
	    gsEditpostaladdress2.click();
	    gsEditpostaladdress2.sendKeys(Address2);
	    
	   waitForElement(By.id(hoteleditpostalcityname));
	    //gsEditpostalcity.clear();
	    //gsEditpostalcity.click();
	   
	    
	    /** enter the search text into city field. */
	   /* gsEditpostalcity.sendKeys(City);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    gsEditpostalcity.sendKeys(Keys.ENTER);
			    
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		waitForElement(By.linkText("Aso"));
		gsEditpostalcity.findElement(By.linkText("Aso")).click();*/
	    
	    waitForElement(By.id(hoteleditpostalstate));
	    gsEditpostalstate.clear();
	    gsEditpostalstate.click();
	    gsEditpostalstate.sendKeys(State);
	    
	    waitForElement(By.id(hoteleditpostalpostcode));
	    gsEditpostalpostcode.clear();
	    gsEditpostalpostcode.click();
	    gsEditpostalpostcode.sendKeys(Postcode);
	    
	    
	    gsEditupdatebutton.click();
	    
	    return PageFactory.initElements(driver, GsContentHotelNameAddress.class);
		}
	
}
