package com.gs.test.page.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.gs.test.base.PageBase;

import static com.gs.test.page.object.IPageElements.qualifier;
import static com.gs.test.page.object.IPageElements.userName;
import static com.gs.test.page.object.IPageElements.passWord;
import static com.gs.test.page.object.IPageElements.signOn;

/**
 * This class is the page object for GS Login page.
 * It holds the all the required elements for the test.
 * 
 * @author Suvarchala Vege
 */
public class GsLoginPage extends PageBase {
	
	/**
	 * Parameterized constructor for this class.
	 * @param driver
	 */
	public GsLoginPage(WebDriver driver){
		super(driver);
	}

	/** This is the WebElement object for Qualifier. */
	@FindBy(how = How.ID, using = qualifier)
	private WebElement gsQualifier;

	/** This is the WebElement object for Username. */
	@FindBy(how = How.ID, using = userName)
	private WebElement gsUserName;
	
	/** This is the WebElement object for Password. */
	@FindBy(how = How.ID, using = passWord)
	private WebElement gsPassWord;
	
	/** This is the WebElement object for Sign On button. */
	@FindBy(how = How.ID, using = signOn)
	private WebElement gsSignOn;
	
	/**
	 * This method takes the Qualifier, username and the password and clicks on submit button 
	 * and returns the GS Welcome Page
	 * @param qualifier : qualifier
	 * @param userName : userName
	 * @param passWord : passWord
	 * @return GsWelcomePage
	 */
	public GsWelcomePage loginWith(String qualifier, String userName, String passWord) {
		
		/** enter the search text into qualifier field. */
		gsQualifier.clear();
		gsQualifier.sendKeys(qualifier);
		
		/** enter the search text into username field. */
		gsUserName.sendKeys(userName);
		
		/** enter the search text into password field. */
		gsPassWord.sendKeys(passWord);
		
		/** Click the search button ("Sign On") on the screen. */
		gsSignOn.submit();
		
		/** Create the object for Welcome page. */
		 try{
		    	driver.findElement((By) gsQualifier);
		    	return loginWith(qualifier, userName, passWord);
		 }catch(Exception e){
		    	return PageFactory.initElements(driver, GsWelcomePage.class);
		 }
    }
}
