
'########### Declarations ##################
	 Dim Testname,sLoc,testfullpath,sTestDir,oexcel,excelread,exceldata,i
	 Dim FSO
	 Dim objWMIService, colProcessList,strProcessToKill
	 Dim objRS      
    Dim strSQL      
    Dim strConnection
	Dim objConn
	dim conn
	dim functionLib
	dim strUsername
	dim Enviornment
	dim strFoldername,qtApp,testsuiteid,testdatasheetname,ResultFolderLoc
	

	
Set fso = CreateObject("Scripting.FileSystemObject")
Set listFile = fso.OpenTextFile("C:\Windows\Temp\runconfig.txt")

'Set fsoresult = CreateObject("Scripting.FileSystemObject")
'Set listFileresult = fsoresult.OpenTextFile("C:\AutomationFramework\Batch\runstatus.txt")

'do while not listFile.AtEndOfStream 
fName =  listFile.ReadLine()
sline=split(fName,",")
strTracker=sline(0)
strUsername=sline(1)
strIP=sline(2)
testsuiteid=sline(3)
''msgbox strTracker
''msgbox testsuiteid
'loop
	
'call killprocess("Excel.exe")
'call killprocess("cmd.exe")
'call killprocess("iexplore.exe")


Set oFSO = CreateObject("Scripting.FileSystemObject") 
Set wshShell = WScript.CreateObject("WSCript.shell")
On Error Resume Next

sTestDir="C:\AutomationFramework\QTP Scripts"
set qtApp = CreateObject("QuickTest.Application",strIP) 'Create the Application object 

Set oexcel = CreateObject("Excel.application")
Set excelread = oexcel.Workbooks.Open("G:\Test Automation\Public\TestAutomation\AutomationFramework\Tracker\"+strTracker)
Set exceldata = excelread.Worksheets(1)

Set fso= CreateObject("Scripting.FileSystemObject")
set logfolde=fso.createfolder("C:\AFLogs")
FilePath = "C:\AFLogs\"&replace( replace(now,":",""),"/","")&".txt"
set fso=fso.CreateTextFile(FilePath, True)
'Set FSOFile = fso.OpenTextFile(FilePath, forwriting, True)

Set wshNetwork = WScript.CreateObject( "WScript.Network" )
strComputerName = wshNetwork.ComputerName
call WriteTextFile("Machine Name on which test was run :",strIP)



call driverQTPTest()
call SendMail(strUsername,strTracker)

'call syncwithAccurev()
'call WriteTextFile("Execution Completed Successfully refer results in Shared path:","")
'call saveResultFolder()

''''msgbox "Execution Completed"
call WriteTextFile("Execution Completed Successfully refer results in Shared path:","")

Function driverQTPTest()
	rowcount=exceldata.usedrange.rows.count
	'''''msgbox rowcount
	for i=2 to rowcount
	
	Runtest=exceldata.Cells(i,5)
	if ucase(trim(Runtest))="Y" then
	Product=exceldata.Cells(i,2)
	Testname=exceldata.Cells(i,4)
	functionLib=exceldata.Cells(i,7)
	testdatasheetname=exceldata.Cells(i,9)
	''msgbox testdatasheetname
	'''''msgbox Testname
	call WriteTextFile("Test Start Time :",now)
	call WriteTextFile("Test Name :",TestName)
	call launchQTPandRunTest(Product,Testname,functionLib,testdatasheetname)
	call WriteTextFile("Test End Time :",now)
	call closeQTP()
	'call killprocess("QTpro.exe","LONWKSG05750")
	End if 
	next
	'''''msgbox "done all"
	excelread.close
	oexcel.quit
end Function 


Function launchQTPandRunTest(Product,Testname,functionLib,testdatasheetname)
 qtApp.Launch  
 qtApp.Visible = True
 qtApp.Options.Run.ImageCaptureForTestResults = "OnError" 
 qtApp.Options.Run.RunMode = "Fast" 
 qtApp.Options.Run.ViewResults = False 
 qtApp.Options.Run.ImageCaptureForTestResults = "Always"
 testfullpath=getTestLocation(Testname)
 call WriteTextFile("Test Path :",testfullpath)
 '''''msgbox testfullpath
 qtApp.Open testfullpath,False
 call loadFunctionLibrary(qtApp,functionLib)
 call loadDataSheet(qtApp,testdatasheetname)
 Set qtTest = qtApp.Test 
 strResultslocation="G:\Test Automation\Public\TestAutomation\AutomationFramework\QTPResults\"
 strFoldername=Testname &replace( replace(now,":",""),"/","")
 qtTest.Settings.Run.OnError = "NextStep"
 Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions") ' Create the Run Results Options object 
 qtResultsOpt.ResultsLocation = strResultslocation + strFoldername
 ResultFolderLoc=strResultslocation + strFoldername+"\"
 qtApp.Test.Environment("ResultFolder") = ResultFolderLoc 'Create Env Variable for Result Folder Location to store the Test Export DataSheet
 qtTest.Run qtResultsOpt 'Run the test 
 qtTest.close
 
 call readResultXML(Product,Testname,strResultslocation,strFoldername)
 ''msgbox("hello")
 'call saveResultFolder()
 ''msgbox("hello2")
 call WriteTextFile("Result Posted :",now)

end function


Function closeQTP()
	''''msgbox "Quit"
	qtApp.Quit
end function

function getTestLocation(Testname)
	'sync down accurev
	'syncwithAccurev()
	sLoc=sTestDir+"\"+Testname
	getTestLocation= sLoc
end function

Function syncwithAccurev()
 wshShell.run "%comspec% /c cd C:\AutomationFramework\ & accurev update ", 1, True
end function


Function WriteTextFile(strtexttowrite,strvalue)
	fso.write(strtexttowrite &" "&strvalue)
	fso.writeblanklines(1)
End Function


Function WriteTextFileresult(strtexttowrite,strvalue)
	fsoresult.write(strtexttowrite &" "&strvalue)
	fsoresult.writeblanklines(1)
End Function




function loadFunctionLibrary(qtApp,functionLib)

'''''msgbox functionLib
 sFuncDir="C:\AutomationFramework\Function Libraries\"
 
 'get all fuction libraries  name 
 funlibarry=Split(functionLib,",")
 
 
 
 'add the libraries to QTP Resources  
 Set qtLibraries = qtApp.Test.Settings.Resources.Libraries
 qtLibraries.RemoveAll() 
 for each x in funlibarry
 ''add the library
 FuncFullName=sFuncDir+x
 '''''msgbox FuncFullName
 qtLibraries.add FuncFullName,1	
 next
end function

function loadDataSheet(qtApp,testdatasheetname)
  'msgbox testdatasheetname
  'msgbox "in loading test data shet"
  sTestDataDir="G:\Test Automation\Public\TestAutomation\AutomationFramework\TestData\"
  sTestDataSheet=sTestDataDir+testdatasheetname+".xls"
  'msgbox sTestDataSheet 
  'qtApp.Test.Environment("TestDataSheet") = testdatasheetname  
  qtApp.Test.DataTable.ImportSheet sTestDataSheet, 1, 1
  'msgbox "loading done"
end function

'''MOVE THE RESULT FOLDER TO G DRIVE

Function saveResultFolder()
 sFuncDir="C:\AutomationFramework\Results"
 sDestFir="G:\Test Automation\Public\TestAutomation\AutomationFramework\QTPResults"
 ''''''msgbox sFuncDir
 '''''msgbox sDestFir
 '''''msgbox (oFSO.FolderExists(sFuncDir))
 If oFSO.FolderExists(sFuncDir) Then
 oFSO.CopyFolder sFuncDir, sDestFir
 'delete the folder so that it is no copied next time.
 str="%comspec% /c rmdir C:\AutomationFramework\Results /s /q & cd C:\AutomationFramework & mkdir Results "
 wshShell.run str, 1, True
 End If
End Function

Function readResultXML(Product,Testname,Resultslocation,ResultFolderName)
 ''msgbox Resultslocation
 ''msgbox ResultFolderName
 sXmlLocation=Resultslocation+ResultFolderName+"\Report\"
 ''msgbox sXmlLocation
 sFile = "Results.xml" 
 set xmlDoc=CreateObject("Microsoft.XMLDOM")
 xmlDoc.async="false"
 xmlDoc.load(sXmlLocation+sFile)
 Set oTags = xmlDoc.GetElementsByTagName("Summary") 
 Set oTag = oTags((oTags.Length)-1)
 Set statTags = xmlDoc.GetElementsByTagName("NodeArgs") 
 '''''msgbox statTags.Length
 Set statTag = statTags((statTags.Length)-1)
 '''''msgbox statTag.Length
 StepsPassed = oTag.getAttribute("passed")
 '''''msgbox StepsPassed
 StepsFailed = oTag.getAttribute("failed")
 '''''msgbox StepsFailed
 Stepswarn = oTag.getAttribute("warnings")
 '''''msgbox Stepswarn
 StartTime = Replace(oTag.getAttribute("sTime"), "-", "")
 '''''msgbox StartTime
 EndTime = Replace(oTag.getAttribute("eTime"), "-", "")
 '''''msgbox EndTime
 Status = statTag.getAttribute("status")
 '''''msgbox Status
 if(Status="Passed")then
 Status="PASS"
 elseif(Status="Warning")then
 Status="Warning"
 elseif(Status="Failed")then
 Status="Fail"
 end if
 Enviornment="UAT"
 call storeResults(Product,Testname,StepsPassed,StepsFailed,Stepswarn,StartTime,EndTime,Status,Enviornment,ResultFolderName)
 
end function

Function ConnectDatabase
 Set objConn = CreateObject("ADODB.Connection")  
'''''msgbox objConn 
 dbloc="G:\Test Automation\Public\TestAutomation\AutomationFramework\QTPResults\QTPResult.accdb"
 objConn.Open"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+dbloc
 set ConnectDatabase=objConn
End Function

Function storeResults(Product,Testname,StepsPassed,StepsFailed,Stepswarn,StartTime,EndTime,Status,Enviornment,ResultFolderName)
	'''''msgbox Enviornment
  set conn=ConnectDatabase()
  '''''msgbox conn 
  Set objRS = CreateObject("ADODB.Recordset")
   strSQL = "INSERT INTO Overallresult(Product,Testname,Passcount,Failcount,Warningcount,Starttime,Endtime,Status,UserName,Environment,ResultFolder,TestSuiteId) values('" & Product & "','" & Testname & "'," & StepsPassed & "," & StepsFailed & "," & Stepswarn & ",'" & StartTime & "','" & EndTime & "','" & Status & "','" & strUsername & "','" &Enviornment& "','" & ResultFolderName & "'," & testsuiteid & ")"
  
  ''msgbox (strSQL)
  ''msgbox (testsuiteid)
  Set objRS = conn.Execute(strSQL)
  '''''msgbox "done posting result"
  end Function
  
   '############## Kill Running process #############################
function killprocess(strparamProcessToKill,strcomputer)
	'Const strComputer = strcomputer
	''msgbox(strparamProcessToKill)
	''msgbox(strcomputer)
	strProcessToKill = strparamProcessToKill
	Set WshShell = CreateObject("WScript.Shell")
	Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery ("Select * from Win32_Process Where Name = '" & strProcessToKill & "'")
	For Each objProcess in colProcessList 
	objProcess.Terminate()
	Next
end function 

Function SendMail(Username,strTracker)
    ''''msgbox Username
  select case Username
   case "Richa" 
   sEmail="richa.walia@gta-travel.com"
   case "Rowthrinath"
   sEmail="Rowthrinath.Natarajan@gta-travel.com"
   case "Devi"
   sEmail="suvarchala.vege@gta-travel.com"
   end select 
   
	Dim objOutlook 
	Dim objOutlookMsg 
	Set objOutlook = CreateObject("Outlook.Application")
	Set objOutlookMsg = objOutlook.CreateItem(0)
	With objOutlookMsg
		'.To = "suvarchala.vege@gta-travel.com"
		.To = sEmail
		.Subject = replace(strTracker,".xls","")&" Automation Test Completed"
		'.Attachments.Add sReport
		'.Body = "Your Test Suite has Run Successfuly kindly visit http://10.241.120.226:8080/myapp  to  View Results"
		.HTMLBody = "Your Test Suite has Run Successfuly kindly visit http://10.241.120.226:8080/myapp  to  View Results"
		.Send 
	End With
	Set objOutlookMsg = Nothing
	Set objOutlook = Nothing

End Function


