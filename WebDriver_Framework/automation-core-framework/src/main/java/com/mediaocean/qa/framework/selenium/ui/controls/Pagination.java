package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Pagination operations.
 * 
 * When we click on any of the pagination links, page will be
 * reloaded. So the actual reference of this pagination object
 * will not be worked after the page reload. We need to create a 
 * new pagination object for each click operation on the pagination.
 * 
 * @author Karthikeyan
 */
public class Pagination extends PageBase {
	
	private WebElement m_pagination;
	
	/**
	 * This constructor takes Pagination element
	 * @param pagination
	 */
	public Pagination(WebElement pagination){
		super(getDriver());
		m_pagination = waitForVisibilityOf(pagination);;
		if (m_pagination == null){
			throw new RuntimeException("Pagination element is null");
		}
	}

	/**
	 * This constructor takes the locator object for Pagination.
	 * @param locator
	 */
	public Pagination(By locator){
		this(waitForElement(locator));
	}

	/**
	 * This constructor takes the parent element and pagination locator object.
	 * This can be used if your page has multiple pagination sections.
	 * 
	 * @param parentElement
	 * @param locator
	 */
	public Pagination(WebElement parentElement, By locator){
		this(parentElement.findElement(locator));
	}

	/**
	 * This method clicks the Previous link.
	 */
	public void clickPrevious(){
		if (isPreviousExists()){
			m_pagination.findElement(By.linkText("Previous")).click();
		}
	}
	
	/**
	 * This method clicks the Next link.
	 */
	public void clickNext(){
		if (isNextExists()){
			m_pagination.findElement(By.linkText("Next")).click();
		}
	}
	
	/**
	 * This method checks and returns if the previous page is exists or not.
	 * @return boolean
	 */
	public boolean isPreviousExists(){
		return isExists("Previous");
	}
	
	/**
	 * This method checks and returns if the next page is exists or not.
	 * @return boolean
	 */
	public boolean isNextExists(){
		return isExists("Next");
	}

	private boolean isExists(String linkText){
		try {
			m_pagination.findElement(By.linkText(linkText));
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * This method checks and returns if the given page number is exists or not. 
	 * @param pageNumber
	 * @return boolean
	 */
	public boolean isPageNumberExists(int pageNumber){
		return isExists(Integer.toString(pageNumber));
	}
	
	/**
	 * This method clicks the given page number link.
	 * @param pageNumber
	 */
	public void clickPageNumber(int pageNumber){
		if (isPageNumberExists(pageNumber)){
			m_pagination.findElement(By.linkText(Integer.toString(pageNumber))).click();
		}
	}
	
	/**
	 * This method returns the current page number.
	 * @return int
	 */
	public int currentPageNumber(){
		String currentStep = m_pagination.findElement(By.className("currentStep")).getText();
		return Integer.parseInt(currentStep);
	}
	
	/**
	 * This method return the total pages count.
	 * @return int
	 */
	public int totalPages(){
		List<WebElement> pageList = m_pagination.findElements(By.tagName("a"));
		if (pageList == null || pageList.size()>0){
			int size = pageList.size();
			WebElement lastPageElement = null;
			if (isNextExists()){
				lastPageElement = pageList.get(size-2);
			}else{
				lastPageElement = pageList.get(size-1);
			}
			return Integer.parseInt(lastPageElement.getText());
		}
		return 1;
	}

	/**
	 * This method returns the pagination web element object.
	 * @return WebElement
	 */
	public WebElement getPaginationElement(){
		return m_pagination;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_pagination.getAttribute(attributeName);
	}
}
