package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Check Box operations.
 * @author Karthikeyan
 */
public class CheckBox extends PageBase{

	private WebElement m_checkBox;

	/**
	 * This constructor takes Check box element
	 * @param checkbox
	 */
	public CheckBox(WebElement checkbox) {
		super(getDriver());
		this.m_checkBox = waitForVisibilityOf(checkbox);	
	}
	
	/**
	 * This constructor takes the locator object for Check box.
	 * @param checkBoxLocator
	 */
	public CheckBox(By checkBoxLocator) {
		this(waitForElement(checkBoxLocator));
	}

	/**
	 * This constructor takes the Check box label
	 * @param checkBoxLabel
	 */
	public CheckBox(String checkBoxLabel) {
		this(findByLabel(checkBoxLabel));
	}

	/**
	 * This constructor takes the Check box label
	 * @param checkBoxLabel
	 */
	public CheckBox(By locator, String checkBoxValue) {
		this(findByValue(locator, checkBoxValue));
	}
	
	private static WebElement findByValue(By locator, String value){
		List<WebElement> list = getElements(locator);
		for (WebElement element : list){
			if (element.getAttribute("value").equalsIgnoreCase(value)){
				return element;
			}
		}
		return null;
	}
	
	public static void checkAll(By locator){
		List<WebElement> list = getElements(locator);
		for (WebElement element : list){
			if (!element.isSelected()){
				element.click();
			}
		}
	}
	
	public static void uncheckAll(By locator){
		List<WebElement> list = getElements(locator);
		for (WebElement element : list){
			if (element.isSelected()){
				element.click();
			}
		}
	}

	private static WebElement findByLabel(String labelText){
		Label label = new Label(labelText);
		String textBoxId = label.getAttributeValue("for");
		if (textBoxId != null){
			return getDriver().findElement(By.id(textBoxId));
		}
		return null;
	}
	
	/**
	 * This method checks if the check box is selected or nor.
	 * @return boolean
	 */
	public boolean isChecked() {
		return m_checkBox.isSelected();
	}

	/**
	 * This method selects/check the check box.
	 */
	public void check() {
		if (!isChecked()) {
			m_checkBox.click();
		}	
	}

	/**
	 * This method uncheck the check box
	 */
	public void uncheck() {
		if (isChecked()) {
			m_checkBox.click();
		}			
	}

	public void checkByVale(String value){
		
	}
	
	/**
	 * This method returns the check box element.
	 * @return WebElement
	 */
	public WebElement getCheckBoxElement() {
		return m_checkBox;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_checkBox.getAttribute(attributeName);
	}
}
