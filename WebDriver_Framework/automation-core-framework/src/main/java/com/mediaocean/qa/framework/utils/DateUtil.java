package com.mediaocean.qa.framework.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This class provides all the utility methods 
 * for date operations. 
 * 
 * @author Karthikeyan
 */
public class DateUtil {

	public static String getCurrentUKDate() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String currentDate = formatter.format(date);
		
		return currentDate;
	}
	
	public static String getCurrentUSDate() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String currentDate = formatter.format(date);

		return currentDate;
	}
	
	public static String getCurrentDateAsString(String dateFormat) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		String currentDate = formatter.format(date);
		
		return currentDate;
	}

	public static Date getCurrentDate(String dateFormat) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		String currentDate = formatter.format(date);
		
		return convertStringToDate(currentDate,dateFormat);
	}

	public static String formatToUK(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateString = formatter.format(date);
		
		return dateString;
	}
	
	public static String formatToUS(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		String dateString = formatter.format(date);
		
		return dateString;
	}
	
	public static String convertDateToString(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateString = formatter.format(date);
		
		return dateString;
	}

	public static Date convertStringToDate(String inputDate, String inputDateFormat){
		SimpleDateFormat formatter = new SimpleDateFormat(inputDateFormat);
		Date date;
		try {
			date = formatter.parse(inputDate);
		} catch (ParseException e) { 
			throw new RuntimeException("Failed to convert string to date");
		}
		return date;
	}

	public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); 
        return cal.getTime();
    }
	
	public static Date addMonth(Date date, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months); 
        return cal.getTime();
    }

	public static Date addYears(Date date, int years) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, years); 
        return cal.getTime();
    }

	public static Date addMinutes(Date date, int minutes) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }
	
	public static Date addSeconds(Date date, int seconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, seconds);
        return cal.getTime();
    }
	
	public static Date addHours(Date date, int hours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours); 
        return cal.getTime();
    }

	public static int getDay(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd");
		return Integer.parseInt(formatter.format(date));
	}
	
	public static int getMonthNumber(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		return Integer.parseInt(formatter.format(date));
	}

	public static String getMonthName(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM");
		return formatter.format(date);
	}

	public static int getYear(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		return Integer.parseInt(formatter.format(date));
	}
	
	public static int getHour(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("hh");
		return Integer.parseInt(formatter.format(date));
	}

	public static int getMinutes(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("mm");
		return Integer.parseInt(formatter.format(date));
	}
	
	public static int getSeconds(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("ss");
		return Integer.parseInt(formatter.format(date));
	}

	public static Date getDate() {
		return new Date();
	}
	
	public static String convertDateToGTAFormat(Date date){
		int dayInt = DateUtil.getDay(date);
		String day = "";
		if (dayInt <9){
			day = "0"+dayInt;
		}else{
			day = Integer.toString(dayInt);
		}
		String month = DateUtil.getMonthName(date);
		int yearInt = DateUtil.getYear(date) % 1000;
		String year = "";
		if (yearInt <9){
			year = "0"+yearInt;
		}else{
			year = Integer.toString(yearInt);
		}
		return day + month + year;
	}
}
