package com.mediaocean.qa.framework.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.tools.ant.BuildException;

/**
 * This class provides all the utility methods 
 * for Excel operations. 
 * 
 * @author Karthikeyan
 */
public class ExcelUtil {

    private String excelFileName;
    private Workbook workbook;

    /**
     * This constructor takes the excel file path.
     * @param fName
     */
    public ExcelUtil(String fPath) {
        excelFileName = fPath;
        loadWorkbook();
    }

    private Workbook loadWorkbook() {

        File workbookFile = new File(excelFileName);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(workbookFile);
            workbook = WorkbookFactory.create(fis);
        	fis.close();
        } catch(Exception e) {
            throw new BuildException("Cannot load file " + excelFileName
                    + ". Make sure the path and file permissions are correct.", e);
        }
        return workbook ;
    }
    
    /**
     * This method returns Workbook object.
     * @return Workbook
     */
    public Workbook getWorkbook() {
        return workbook;
    }

    /**
     * This method returns file path.
     * @return String : file path
     */
    public String getFileName() {
        return excelFileName;
    }
    
    /**
     * This method returns the list of all sheet names in the excel file.
     * @return List
     */
    public ArrayList<String> getSheets() {
    	ArrayList<String> sheets = new ArrayList<String>() ;
    	int sheetCount = workbook.getNumberOfSheets() ;
    	for( int x=0; x<sheetCount; x++ ) {
    		sheets.add( workbook.getSheetName( x ) ) ;
    	}
    	return sheets ;
    }

    /**
     * This method sets the date value to the cell.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param date
     */
    public void setDateValue(String sheetName, int rowNum, String colName, Date date  ) {
        Cell cell = getCell(sheetName, rowNum, colName);
        cell.setCellValue( date ) ;
    	writeWorkbook();
    }

    /**
     * This method sets the double value to the cell.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param dValue
     */
    public void setDoubleValue(String sheetName, int rowNum, String colName, Double dValue  ) {
        Cell cell = getCell(sheetName, rowNum, colName);
        cell.setCellValue( dValue ) ;
    	writeWorkbook();
    }

    /**
     * This method sets the true/false value to the cell.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param bValue
     */
    public void setBooleanValue(String sheetName, int rowNum, String colName, boolean bValue  ) {
        Cell cell = getCell(sheetName, rowNum, colName);
        cell.setCellValue( bValue ) ;
    	writeWorkbook();
    }

    /**
     * This method sets the Calendar value to the cell.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param calValue
     */
    public void setCalendarValue(String sheetName, int rowNum, String colName, Calendar calValue  ) {
        Cell cell = getCell(sheetName, rowNum, colName);
        cell.setCellValue( calValue ) ;
    	writeWorkbook();
    }

    /**
     * This method sets the String value to the cell.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param sValue
     */
    public void setStringValue(String sheetName, int rowNum, String colName, String sValue  ) {
        Cell cell = getCell(sheetName, rowNum, colName);
        cell.setCellValue( sValue ) ;
    	writeWorkbook();
    	writeWorkbook();
    }
    
    /**
     * This method returns the sheet object.
     * @param sheetName 
     * @return Sheet
     */
    public Sheet getSheet(String sheetName){
    	return workbook.getSheet(sheetName);
    }
    
    /**
     * This method returns the Header Row object
     * @param sheetName
     * @return Row
     */
    public Row getHeaderRow(String sheetName){
    	return getRow(sheetName, 0);
    }
    
    /**
     * This method returns the Row object for the given row number.
     * @param sheetName
     * @param rowNum
     * @return Row
     */
    public Row getRow(String sheetName, int rowNum){
    	return getSheet(sheetName).getRow(rowNum);
    }
    
    /**
     * This method returns the cell object for the given row & column number.
     * @param sheetName
     * @param rowNum
     * @param colNum
     * @return Cell
     */
    public Cell getCell (String sheetName, int rowNum, int colNum){
    	Sheet sheet = getSheet(sheetName);
    	Row row = sheet.getRow(rowNum);
    	Cell cell = row.getCell(colNum);
    	return cell;
    }
    
    /**
     * This method returns the cell object for the given row & column name.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @return
     */
    public Cell getCell(String sheetName, int rowNum, String colName){
    	Row hRow = getHeaderRow(sheetName);
    	int i=hRow.getFirstCellNum();
    	for (; i<hRow.getLastCellNum(); i++){
    		Cell c = hRow.getCell(i);
    		if (c.getStringCellValue().equalsIgnoreCase(colName)){
    			break;
    		}
    	}
    	return getCell(sheetName, rowNum, i);
    }

    /**
     * This method returns the cell value as String for the given row & column number.
     * @param sheetName
     * @param rowNum
     * @param colNum
     * @return String
     */
    public String getCellAsString(String sheetName, int rowNum, int colNum){
    	return getCell(sheetName, rowNum, colNum).getStringCellValue();
    }
    
    /**
     * This method returns the cell value as String for the given row & column name.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @return String
     */
    public String getCellAsString(String sheetName, int rowNum, String colName){
    	int colNum = getColNum(sheetName, colName);
    	return getCell(sheetName, rowNum, colNum).getStringCellValue();
    }
    
    /**
     * This method creates new row in the given sheet.
     * @param sheetName
     */
    public void createNewRow(String sheetName){
            Sheet sheet = getSheet(sheetName);
        	sheet.createRow(sheet.getLastRowNum()+1);
        	writeWorkbook();
    }
    
    /**
     * This method creates new cell with value in the last row of the given column name.
     * @param sheetName
     * @param colName
     * @param value
     */
    public void createNewCell(String sheetName, String colName, String value){
        	Cell cell = getRow(sheetName,getLastRowNum(sheetName)).createCell(getColNum(sheetName, colName));
        	cell.setCellValue(value);
        	writeWorkbook();
    }
    
    /**
     * This method creates new cell with value in the last row of the given column number.
     * @param sheetName
     * @param colNum
     * @param value
     */
    public void createNewCell(String sheetName, int colNum, String value){
    	Cell cell = getRow(sheetName,getLastRowNum(sheetName)).createCell(colNum);
    	cell.setCellValue(value);
    	writeWorkbook();
    }
    
    /**
     * This method creates new cell with value for the given row number and column name.
     * @param sheetName
     * @param rowNum
     * @param colName
     * @param value
     */
    public void createNewCell(String sheetName, int rowNum, String colName, String value){
    	Cell cell = getRow(sheetName,rowNum).createCell(getColNum(sheetName, colName));
    	cell.setCellValue(value);
    	writeWorkbook();
    }


    /**
     * This method creates new cell with value for the given row number and column number.
     * @param sheetName
     * @param rowNum
     * @param colNum
     * @param value
     */
    public void createNewCell(String sheetName, int rowNum, int colNum, String value){
    	Cell cell = getRow(sheetName,rowNum).createCell(colNum);
    	cell.setCellValue(value);
    	writeWorkbook();
    }

    /**
     * This method returns last row number
     * @param sheetName
     * @return int
     */
    public int getLastRowNum(String sheetName){
    	return getSheet(sheetName).getLastRowNum();
    }
    
    /**
     * This method returns the column number for the given column name.
     * @param sheetName
     * @param colName
     * @return int
     */
    public int getColNum(String sheetName, String colName){
    	Row hRow = getHeaderRow(sheetName);
    	int i=hRow.getFirstCellNum();
    	for (; i<hRow.getLastCellNum(); i++){
    		Cell c = hRow.getCell(i);
    		if (c.getStringCellValue().equalsIgnoreCase(colName)){
    			break;
    		}
    	}
    	return i;
    }
    
    /**
     * This method writes the data to workbook and save it.
     */
    public void writeWorkbook(){
        try {
        	FileOutputStream fileOut = new FileOutputStream(excelFileName);
            workbook.write(fileOut);
        	fileOut.flush();
        	fileOut.close();
        }catch(Exception e){
        	e.printStackTrace();
        }
    }
    
    /**
     * This method takes the first column as key and return the 
     * second column as value. If the sheet contains duplicate
     * key names, it will return first key value.
     * @param sheetName
     * @param key
     * @return Value
     */
    public String getKeyValue(String sheetName, String key){
    	Sheet sheet = getSheet(sheetName);
    	if (sheet == null){
    		return null;
    	}
    	int rowCount = getSheet(sheetName).getLastRowNum();
    	String value = "";
    	for(int i=0;i<rowCount-1;i++){
    		Row row = getSheet(sheetName).getRow(i);
    		Cell cell0 = row.getCell(0);
    		if (cell0 != null && cell0.getStringCellValue().equalsIgnoreCase(key)){
    			Cell cell1 = row.getCell(1);
    			if (cell1 != null){
    				value = cell1.getStringCellValue();
    			}
    		}
    	}
    	return value;
    }
}