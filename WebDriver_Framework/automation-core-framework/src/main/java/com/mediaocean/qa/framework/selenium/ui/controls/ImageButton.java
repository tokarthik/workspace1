package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Image Button operations.
 * @author Karthikeyan
 */
public class ImageButton extends PageBase {

	private WebElement m_imageBtn;
	
	/**
	 * This constructor takes Image Button element
	 * @param imageBtn
	 */
	public ImageButton(WebElement imageBtn) {
		super(getDriver());
		this.m_imageBtn = waitForVisibilityOf(imageBtn);	

	}
	
	/**
	 * This constructor takes the locator object for Image Button.
	 * @param imageBtnLocator
	 */
	public ImageButton(By imageBtnLocator) {
		this(waitForElement(imageBtnLocator));
	}
	
	/**
	 * This constructor try to locate the element in parent element by attribute value.
	 * 
	 * @param parentElement : parent element of image button
	 * @param attributeName : attribute name of the image button
	 * @param attributeValue : attribute value of the image button
	 */
	public ImageButton(WebElement parentElement, String attributeName, String attributeValue) {
		this(findByAttributeInParentElement(parentElement, attributeName, attributeValue));
	}

	/**
	 * This constructor try to locate the element in whole page by attribute value.
	 * @param attributeName : attribute name of the image button
	 * @param attributeValue : attribute value of the image button
	 */
	public ImageButton(String attributeName, String attributeValue) {
		this(findByAttributeInParentElement(null,attributeName, attributeValue));
	}

	private static WebElement findByAttributeInParentElement(WebElement parentElement, String attributeName, String attributeValue){
		
		List<WebElement> imgButtonList = null;
		
		if (parentElement == null){
			imgButtonList = getDriver().findElements(By.tagName("img"));
		}else{
			imgButtonList = parentElement.findElements(By.tagName("img"));
		}
		
		for (WebElement imgButtonEl : imgButtonList){
			if (imgButtonEl.getAttribute(attributeName).equalsIgnoreCase(attributeValue)){
				return imgButtonEl;
			}
		}
		return null;
	}
	
	/**
	 * This method return the name of the image button.
	 * @return String : name
	 */
	public String getName() {
		String name;
		name = m_imageBtn.getAttribute("value").trim();
		return name;
	}

	/**
	 * This method returns the title of the image button
	 * @return String : title
	 */
	public String getTitle(){
		return getAttributeValue("title");
	}
	
	/**
	 * This method returns the source url of the image button.
	 * @return String : source url
	 */
	public String getSource(){
		return getAttributeValue("src");
	}

	/**
	 * This method clicks on the image button.
	 */
	public void click() {
		m_imageBtn.click();	
	}

	/**
	 * This method returns the image button element.
	 * @return WebElement
	 */
	public WebElement getImageButtonElement() {
		return m_imageBtn;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_imageBtn.getAttribute(attributeName);
	}
}
