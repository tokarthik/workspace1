package com.mediaocean.qa.framework.selenium.ui.controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Combo Box operations.
 * @author Karthikeyan
 */
public class ComboBox extends PageBase {

	private WebElement m_comboBox;

	/**
	 * This constructor takes Combo Box element
	 * @param comboBox
	 */
	public ComboBox(WebElement comboBox) {
		super(getDriver());
		this.m_comboBox = waitForVisibilityOf(comboBox);	
	}
	
	/**
	 * This constructor takes the locator object for Combo Box.
	 * @param comboBoxLocator
	 */
	public ComboBox(By comboBoxLocator) {
		this(waitForElement(comboBoxLocator));
	}

	/**
	 * This constructor takes the Combo Box label
	 * @param comboBoxLabel
	 */
	public ComboBox(String comboBoxLabel) {
		this(findByLabel(comboBoxLabel));
	}

	private static WebElement findByLabel(String labelText){
		Label label = new Label(labelText);
		String textBoxId = label.getAttributeValue("for");
		if (textBoxId != null){
			return getDriver().findElement(By.id(textBoxId));
		}
		return null;
	}
	
	/**
	 * This method selects the value in Combo Box
	 * @param valueToSelect
	 */
	public void select(String valueToSelect) {
		selectDropdownItem(m_comboBox, valueToSelect);
	}
	
	/**
	 * This method returns the selected value from Combo Box 
	 * @return
	 */
	public String getSelectedItem() {
		return m_comboBox.getText();
	}

	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_comboBox.getAttribute(attributeName);
	}
}
