package com.mediaocean.qa.framework.selenium.ui.controls;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Text Box operations.
 * @author Karthikeyan
 */
public class TextBox extends PageBase {

	private WebElement m_textBox;

	/**
	 * This constructor takes TextBox element
	 * @param textbox
	 */
	public TextBox(WebElement textbox) {
		super(getDriver());
		this.m_textBox = waitForVisibilityOf(textbox);		

	}
	
	/**
	 * This constructor takes the locator object for Text box.
	 * @param textBoxLocator
	 */
	public TextBox(By textBoxLocator) {
		this(waitForElement(textBoxLocator));
	}

	/**
	 * This constructor takes the parent element and locator objects for Text box.
	 * @param parent element
	 * @param textBoxLocator
	 */
	public TextBox(WebElement parentEl, By textBoxLocator) {
		this(waitForElement(parentEl, textBoxLocator));
	}

	/**
	 * This constructor takes the Text box label
	 * @param label
	 */
	public TextBox(String label) {
		this(findByLabel(label));
	}
	
	private static WebElement findByLabel(String labelText){
		Label label = new Label(labelText);
		String textBoxId = label.getAttributeValue("for");
		if (textBoxId != null){
			return getDriver().findElement(By.id(textBoxId));
		}
		return null;
	}
	
	/**
	 * This method sets the text box value.
	 * @param text
	 */
	public void setText(String text) {
		clearAndType(m_textBox, text);
	}
	
	/**
	 * This method sets the text box value.
	 * @param text
	 */
	public void clickTab() {
		m_textBox.sendKeys(Keys.TAB);
	}
	
	/**
	 * This method clears the text box value.
	 */
	public void clear() {
		clearAndType(m_textBox, "");
	}

	/**
	 * This method returns the text box value.
	 * @return
	 */
	public String getText() {
		String text;
		/** getText() wouldn't work for some elements with input tag and disabled elements */
		text = m_textBox.getAttribute("value");	
		return text;
	}

	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_textBox.getAttribute(attributeName);
	}
	
}