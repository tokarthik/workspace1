package com.mediaocean.qa.framework.selenium;

import java.io.File;
import java.net.URL;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.mediaocean.qa.framework.utils.ExcelUtil;

/**
 * This is the base class for all test cases. All test cases must be extended from this base class.
 * Returns interfaces to all page objects. These page object getters must be provided for all modules using it.  
 */
public abstract class TestBase extends WebDriverBase {

	private static Logger logger = Logger.getLogger(TestBase.class);
	
	private static boolean driverInitialised = false;
	private static boolean log4jInitialised = false;
	private static boolean environmentInitialised = false;
	private static boolean browserOpen = false;
	
	/** Application test environment variables */
	//public static String BROWSER;
	//public static String ENVIRONMENT;
	//public static String URL;
	
	public String defaultEnvironment = "OTHER";
	public static final String defaultBrowser = "IE";
	public String browserProfilePath;

	public static ExcelUtil excelUtil;

	/**
	 * Initialise test environment
	 */
	protected void initialiseEnvironment() {
		/** If environment value is not provided by Test itself, try to get it from the 
		 * System property. If it is still null then set the default environment. */ 
		if (ENVIRONMENT == null || ENVIRONMENT.equals("")){
			ENVIRONMENT = System.getProperty("environment");
		}
		if (ENVIRONMENT == null || ENVIRONMENT.equals("")){
			ENVIRONMENT = defaultEnvironment;
		}
		
		/** If browser value is not provided by Test itself, try to get it from the 
		 * System property. If it is still null then try to get it from the excel sheet data
		 * for the given environment. If still it is null, set the default browser. */ 
		if (BROWSER == null || BROWSER.equals("")){
			BROWSER = System.getProperty("browser");
		}
		if (BROWSER == null || BROWSER.equals("") && excelUtil != null){
			if (ENVIRONMENT != null){
				BROWSER = excelUtil.getKeyValue(ENVIRONMENT, "browser");
			}
		}
		if (BROWSER == null || BROWSER.equals("")){
			BROWSER = defaultBrowser;
		}
		
		/** If browser value is not provided by Test itself, try to get it from the 
		 * System property. If it is still null then try to get it from the excel sheet data
		 * for the given environment. If still it is null, set the default browser. */ 
		if (browserProfilePath == null || browserProfilePath.equals("")){
			browserProfilePath = System.getProperty("browserProfilePath");
		}
		if (browserProfilePath == null || browserProfilePath.equals("") && excelUtil != null){
			if (ENVIRONMENT != null){
				browserProfilePath = excelUtil.getKeyValue(ENVIRONMENT, "browserProfilePath");
			}
		}

		/** If url is not provided by Test itself, try to get it from the 
		 * System property. If it is still null then try to get it from the excel sheet data
		 * for the given environment. If still it is null, throw exception. */ 
		if (URL == null || URL.equals("")){
			URL = System.getProperty("url");
		}
		if (URL == null || URL.equals("") && excelUtil != null){
			if (ENVIRONMENT != null){
				URL = excelUtil.getKeyValue(ENVIRONMENT, "URL");
			}
		}
		if (URL == null || URL.equals("")){
			throw new RuntimeException("The URL should not be null or empty");
		}
	}
	
	/**
	 * Initialise test environment by providing the details
	 */
	protected void initialiseEnvironment(String environment, String browser, String url) {
		setEnvironment(environment);
		setBrowser(browser);
		setURL(url);
		initialiseEnvironment();
	}

	/**
	 * Initialise test environment & test by providing the details
	 */
	protected void initialiseEnvironmentAndTest(String environment, String browser, String url) {
		setEnvironment(environment);
		setBrowser(browser);
		setURL(url);
		initialiseEnvironment();
		initialiseTest();
	}

	/**
	 * Initialise test environment & test by providing the details (over loading method)
	 */
	protected void initialiseEnvironmentAndTest(String browser, String url) {
		setBrowser(browser);
		setURL(url);
		initialiseEnvironment();
		initialiseTest();
	}

	/**
	 * Initialise test environment & test by providing the details (over loading method)
	 */
	protected void initialiseEnvironmentAndTest(String url) {
		setURL(url);
		initialiseEnvironment();
		initialiseTest();
	}
	
	/**
	 * Initialise test environment & test (over loading method)
	 */
	protected void initialiseEnvironmentAndTest() {
		initialiseEnvironment();
		initialiseTest();
	}

	/**
	 * This method takes the driver sheet excel file absolute path and creates the excel object.
	 * @param driverSheetPath
	 */
	public void setDriverSheetAbsolutePath(String driverSheetPath){
		excelUtil = new ExcelUtil(driverSheetPath);
	}
	
	/**
	 * This method takes the driver sheet excel file name and gets the absolute path from 
	 * class path and creates the excel object.
	 * @param driverSheetFileName
	 */
	public void setDriverSheetFileName(String driverSheetFileName){
		excelUtil = new ExcelUtil(ClassLoader.getSystemClassLoader()
				.getResource(driverSheetFileName).getPath());
	}
	
	/**
	 * Initialises WebDriver instance with browser driver specified in ITestEnvironment
	 */
	protected void initialiseDriver() {
		logger.info("Initialising driver...");		

		if (TestBase.getBrowser().equalsIgnoreCase("Firefox")) {
			
			if (browserProfilePath != null && !browserProfilePath.equals("")){
				FirefoxProfile fireFoxProfile = new FirefoxProfile(new File(browserProfilePath));
				DesiredCapabilities fireFoxCapabilities = DesiredCapabilities.firefox();
				fireFoxCapabilities.setCapability(FirefoxDriver.PROFILE, fireFoxProfile);
				setDriver(new FirefoxDriver(fireFoxCapabilities));
			}else{
				setDriver(new FirefoxDriver());
			}
			driverInitialised = true;
			logger.info("driver initialised with Firefox driver");
		}
		else if (TestBase.getBrowser().equalsIgnoreCase("Chrome")) {
			// specify the location of chromedriver.exe path
			String driverPath = getClass().getResource(getClass().getSimpleName()+".class").getPath().split("/target")[0] + "/src/main/resources/chromedriver.exe";
			System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY , driverPath);
			
			/** Recommended way to pass capabilities to ChromeDriver */
			ChromeOptions options = new ChromeOptions();
			options.addArguments("always-authorize-plugins");
			options.addArguments("allow-outdated-plugins");
			setDriver(new ChromeDriver(options));
			
			driverInitialised = true;
			logger.info("driver initialised with chrome driver");
		}
		else if (TestBase.getBrowser().equalsIgnoreCase("InternetExplorer") 
				|| TestBase.getBrowser().equalsIgnoreCase("IE")) {
			URL url = ClassLoader.getSystemClassLoader().getResource("IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", url.getPath());
			
			/**
			 * If the below property will not work, we need to set the security domains manually in IE browser.
			 * Go to IE-->Internet Options-->Security and either turn off or turn on "Enable Protected mode"
			 * check box for all the four tabs (Internet, Local Internet, Trusted sites & Restricted sites)
			 */
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability("nativeEvents", false);
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

			
			setDriver(new InternetExplorerDriver(ieCapabilities));
			
			driverInitialised = true;
			logger.info("driver initialised with InternetExplorerDriver");
			
           /* RemoteWebDriver driver;
            Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
            String version = caps.getVersion();
            String browserName = caps.getBrowserName();
         
            System.out.println("BrowserName is :" + " " +browserName +"  "  +"with version :" +" " +version);*/

		}
		else if (TestBase.getBrowser().equalsIgnoreCase("Safari")) {
			setDriver(new SafariDriver());
			driverInitialised = true;
			logger.info("driver initialised with Safari driver");
		}
		else if (getBrowser().equalsIgnoreCase("HtmlUnitDriver")) {
			setDriver(new HtmlUnitDriver());
			driverInitialised = true;
			logger.info("driver initialised with HTMLUnitDriver");
		}
		
	}
	
	/**
	 * 
	 * Call it to initialise Log4j and driver at the beginning of the test
	 */
	protected void initialiseTest() {
		if (!log4jInitialised) {
			initialiseLog4jLogger();
		}
		if (!environmentInitialised) {
			initialiseEnvironment();
		}
		if (!driverInitialised) {
			initialiseDriver();
		}

		logger.info("Test initialised.");
	}
	
	/**
	 * 
	 * Initialise log4j allowing all levels to be logged to the console.
	 * change the logger level here
	 */
	private void initialiseLog4jLogger() {
		if(!log4jInitialised) {	
			BasicConfigurator.resetConfiguration();
			BasicConfigurator.configure();
			Logger.getRootLogger().setLevel(Level.WARN);
			log4jInitialised = true;
			logger.info("Log4j logger initialised.");
		}			
	}
	
	/**
	 *  Must be called to finalise and quit the driver 
	 */
	public void finaliseTest() {
		logger.info("Closing browser and finalising driver...");		
		//The ChromeDriver *requires* that you call close and then quit to not leak resources. 
		//However, the firefoxdriver crashes if you call close before calling quit with any time delay. 
		if (isDriverInitialised()) {
			//driver.close();
			getDriver().quit();	
			driverInitialised = false;
		}
		
		logger.info("driver finalised!");	
	}

	public void openBrowser(String url) {
		logger.info(String.format("openbrowser(%s)", url));
		
		getDriver().get(url);
		getDriver().manage().window().maximize();		
		browserOpen = true;
	}

	public void openBrowser() {
		logger.info(String.format("openbrowser(%s)", getURL()));
		
		getDriver().get(getURL());
		getDriver().manage().window().maximize();	
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		browserOpen = true;
	}
	
	/**
	 *  This will only close the browser, will NOT quit the driver 
	 */
	public void closebrowser() {
		if (browserOpen){
			getDriver().close();
		}
		browserOpen = false;

		logger.info("Browser closed.");		
	}
	
	public static boolean isDriverInitialised() {
		return driverInitialised;
	}
	
	public static String getBrowser() {
		return BROWSER;
	}

	public static void setBrowser(String browserName) {
		BROWSER = browserName;
	}

	public String getEnvironment() {
		return ENVIRONMENT;
	}

	public void setEnvironment(String environment) {
		ENVIRONMENT = environment;
	}
	
	public static String getURL() {
		return URL;
	}

	public static void setURL(String uRL) {
		URL = uRL;
	}

	public String getBrowserProfilePath() {
		return browserProfilePath;
	}

	public void setBrowserProfilePath(String browserProfilePath) {
		this.browserProfilePath = browserProfilePath;
	}	
}
