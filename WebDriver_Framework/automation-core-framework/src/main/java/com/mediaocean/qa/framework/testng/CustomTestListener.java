package com.mediaocean.qa.framework.testng;

import org.apache.log4j.Logger;
import org.testng.IInvokedMethod;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class CustomTestListener extends TestListenerAdapter {
	private static Logger logger = Logger.getLogger(CustomTestListener.class);
	
	public void afterInvocation(IInvokedMethod method, ITestResult result) {
		logger.info(String.format("afterInvocation(%s,%s)", method, result));
	}
	
}
