package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Link operations.
 * @author Karthikeyan
 */
public class Link extends PageBase {
	
	private WebElement m_link;
	
	/**
	 * This constructor takes Link element
	 * @param button
	 */
	public Link(WebElement button) {
		super(getDriver());	
		this.m_link = waitForVisibilityOf(button);
	}
	
	/**
	 * Use this constructor if you don't have id for Link but 
	 * you have id for parent element.
	 * 
	 * @param parentWebElement : parent of button element
	 * @param linkText : link text
	 */
	public Link(WebElement parentWebElement, String linkText) {
		this(identifyLink(parentWebElement, linkText));
	}
	
	/**
	 * Use this constructor if you don't have id for Link but 
	 * you have id for parent element and title.
	 * 
	 * @param parentWebElement : parent of button element
	 * @param linkText : link text
	 * @param title
	 */
	public Link(WebElement parentWebElement, String linkText, String title) {
		this(identifyLink(parentWebElement, linkText, title));
	}

	/**
	 * This constructor takes the locator object for Link.
	 * 
	 * @param linkLocator
	 */
	public Link(By linkLocator) {
		this(waitForElement(linkLocator));
	}
	
	/**
	 * This constructor takes the link text
	 * @param linkText
	 */
	public Link(String linkText) {
		this(identifyLink(null, linkText));
	}
	
	/**
	 * Use this constructor if you have multiple link with same text 
	 * and different titles.
	 * @param linkText
	 * @param title
	 */
	public Link(String linkText, String title) {
		this(identifyLink(null, linkText, title));
	}
	
	private static WebElement identifyLink(WebElement parentElement, String linkText){
		WebElement linkWebElement = null;
		if (parentElement == null){
			linkWebElement = getDriver().findElement(By.linkText(linkText));
		}else{
			linkWebElement = parentElement.findElement(By.linkText(linkText));
		}
		return linkWebElement;
	}
	
	public static boolean isLinkVisible(String linkText){
		return isLinkVisible(null, linkText);
	}
	
	public static boolean isLinkVisible(WebElement parentElement, String linkText){
		try{
			if (parentElement == null){
				getDriver().findElement(By.linkText(linkText));
			}else{
				parentElement.findElement(By.linkText(linkText));
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	private static WebElement identifyLink(WebElement parentElement, String linkText, String title){
		List<WebElement> linkWebElementList = null;
		if (parentElement == null){
			linkWebElementList = getDriver().findElements(By.linkText(linkText));
		}else{
			linkWebElementList = parentElement.findElements(By.linkText(linkText));
		}
		if (linkWebElementList != null && linkWebElementList.size() == 1){
			return linkWebElementList.get(0);
		}
		if (linkWebElementList != null){
			for (WebElement linkElement : linkWebElementList){
				if (linkElement.getAttribute("title").equalsIgnoreCase(title)){
					return linkElement;
				}
			}
		}
		return null;
	}	
	/**
	 * This method clicks the link.
	 */
	public void clickLink(){
		m_link.click();
	}
	
	/**
	 * This method returns the Link WebElement object.
	 * @return WebElement
	 */
	public WebElement getLinkElement(){
		return m_link;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_link.getAttribute(attributeName);
	}
}
