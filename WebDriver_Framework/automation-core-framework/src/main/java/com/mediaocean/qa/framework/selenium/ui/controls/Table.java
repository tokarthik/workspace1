package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Table operations.
 * @author Karthikeyan
 */
public class Table extends PageBase{
	
	private boolean tableFound = false;
	private Map<Integer, String> columnHeadres = new HashMap<Integer, String>();
	private Map<Integer, WebElement> columnHeadrElements = new HashMap<Integer, WebElement>();
	private WebElement _table;
	
	/**
	 * This constructor takes Table element
	 * @param element
	 */
	public Table(WebElement element){
		super(getDriver());
		if (element != null){
			_table = waitForVisibilityOf(element);
			tableFound = true;
			setHeaders();
		}
	}

	/**
	 * Use this constructor if you don't have id for Table but 
	 * you have id for parent element.
	 *  
	 * @param parentWebElement : parent element for the Table
	 * @param tableNum : if parent element contains multiple Tables, 
	 * 					 need to specify which table need to be considered
	 */
	public Table(WebElement parentWebElement, Integer tableNum) {
		this(locateTable(parentWebElement, tableNum));
	}
	
	/**
	 * This constructor takes the locator object for Table.
	 * @param buttonLocator
	 */
	public Table(By buttonLocator) {
		this(waitForElement(buttonLocator));
	}
	
	private static List<WebElement> identifyTablesInParentElement(WebElement parentWebElement){
		List<WebElement> tablesList = parentWebElement.findElements(By.cssSelector("table"));
		return tablesList;
	}
	
	private static WebElement locateTable(WebElement parentWebElement, Integer tableNum){
		List<WebElement> tablesList = identifyTablesInParentElement(parentWebElement);
		WebElement tableElement = null;
		if (tablesList != null && tablesList.size() >= tableNum){
			tableElement = tablesList.get(tableNum-1);
		}
		return tableElement;
	}
	
	/**
	 * This method returns the Table column headers map where key contains the 
	 * colNum and value contains colName.
	 * 
	 * @return Map<Integer, String>
	 */
	private void setHeaders() {
		List<WebElement> headerList = null;
		if (tableFound){
			waitForElement(getTable(), By.cssSelector("th"));
			headerList = getTable().findElements(By.cssSelector("th"));
			for (int i=0;i<headerList.size();i++){
				columnHeadres.put(i, headerList.get(i).getText());
				columnHeadrElements.put(i, headerList.get(i));
			}
		}
	}
	
	/**
	 * This method returns the Table column headers map where key contains the 
	 * colNum and value contains colName.
	 * 
	 * @return Map<Integer, String>
	 */
	public Map<Integer, String> getHeaders() {
		return columnHeadres;
	}

	/**
	 * This method returns the header count
	 * @return int
	 */
	public int getHeaderCount() {
		return columnHeadres.size();
	}
	
	/**
	 * This method returns the Table Row Count
	 * @return Integer
	 */
	public int getRowCount(){
		List<WebElement> rowList = getRowElements();
		if (rowList != null){
			return rowList.size();
		}
		return 0;
	}
	
	public List<WebElement> getRowElements(){
		List<WebElement> rowList = null;
		if (tableFound){
			rowList = getTable().findElements(By.cssSelector("tr"));
		}
		return rowList;
	}
	
	public List<WebElement> getColumnElements(WebElement rowElement){
		if (rowElement != null){
			return rowElement.findElements(By.cssSelector("td"));
		}
		return null;
	}

	/** 
     * This method set the Cell Data, based on row number and column number
     * @param rowNum - specify a Row Number (Starting with '1')
     * @param ColNum - specify a Column Number (Starting with '1')
     * @return cell element
     */
	public WebElement getCell(int rowNum,int colNum) {
		List<WebElement> rowList = getRowElements();
		if (rowList != null 
				&& rowList.size() >= rowNum 
				&& columnHeadres.size() >= colNum 
				&& rowNum > 0 
				&& colNum > 0){
			
			WebElement rowElement = rowList.get(rowNum);
			List<WebElement> colList = getColumnElements(rowElement);
			return colList.get(colNum-1);
		}
		return null;
	}

	/** 
     * This method set the Cell Data, based on row number and column number
     * @param rowNum - specify a Row Number (Starting with '1')
     * @param ColNum - specify a Column Number (Starting with '1')
     * @return cell element
     */
	public WebElement getCell(int rowNum, String colName) {
		int colNum = getColumnNumber(colName);
		return getCell(rowNum, colNum);
	}
	
	/** 
     * This method returns the Cell Data, based on row number and column number
     * @param rowNum - specify a Row Number (Starting with '1')
     * @param ColNum - specify a Column Number (Starting with '1')
     * @return cell data
     */
	public String getCellData(int rowNum,int colNum) {
		WebElement cell = getCell(rowNum, colNum);
		if (cell != null){
			return cell.getText();
		}
		return null;
	}
	
	/** 
     * This method returns the Cell Data,based on row number and column name
     * @param rowNum - specify a Row Number (Starting with '1')
     * @param ColName - specify a Column Name
     * @return cell data
     */
	public String getCellData(int rowNum,String colName) {
		int colNum = getColumnNumber(colName);
		return getCellData(rowNum, colNum);
	}
	
	/**
	 * This method clicks on the given row.
	 * @param rowNum : Starts from '1'
	 */
	public void clickOnRow(int rowNum){
		List<WebElement> rowList = getRowElements();
		if (rowList != null && rowList.size() > rowNum && rowNum > 0){
			rowList.get(rowNum).click();
		}
	}
	
	/**
	 * This method clicks on the given data column number
	 * @param rowNum
	 * @param colNum
	 */
	public void clickOnDataColumn(int rowNum, int colNum){
		WebElement cell = getCell(rowNum, colNum);
		if (cell != null){
			cell.click();
		}
	}
	
	/**
	 * This method clicks on the given data column name
	 * @param rowNum
	 * @param colName
	 */
	public void clickOnDataColumn(int rowNum, String colName){
		int colNum = getColumnNumber(colName);
		clickOnDataColumn(rowNum,colNum);
	}

	/**
	 * This method clicks on given header column number
	 * @param colNum : header column number
	 */
	public void clickOnHeaderColumn(int colNum){
		if (columnHeadrElements.size() >= colNum){
			columnHeadrElements.get(colNum-1).click();
		}
	}
	
	/**
	 * This method clicks on given header column name
	 * @param colName : header column name
	 */
	public void clickOnHeaderColumn(String colName){
		int colNum = getColumnNumber(colName);
		clickOnHeaderColumn(colNum);
	}

	/** 
     * This method returns the column number of a given column name
     * @param colName - specify a column name
     * @return column number
     */
	public Integer getColumnNumber(String colName){
		for (int i=0;i<columnHeadres.size();i++){
			if (columnHeadres.get(i).equalsIgnoreCase(colName)){
				return i+1;
			}
		}
		return null;
	}
	
	/**
	 * This method returns the table element
	 * @return WebElement
	 */
	public WebElement getTable() {
		return _table;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return _table.getAttribute(attributeName);
	}
	
}
	