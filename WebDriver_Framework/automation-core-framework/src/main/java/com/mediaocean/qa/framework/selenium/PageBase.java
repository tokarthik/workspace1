package com.mediaocean.qa.framework.selenium;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * <b>Description: 
 * All Page Objects must be extended from this base class
 */
public abstract class PageBase extends WebDriverBase {
	
	private static Logger logger = Logger.getLogger(PageBase.class);
	
	protected PageBase(WebDriver driver) {
		setDriver(driver);
	}
	
	public void waitForIframeAndSwitchToIt(String iframeID) {
		logger.info(String.format("waitForIframeAndSwitchToIt(%s)", iframeID));
		try {
			getDriver().switchTo().defaultContent(); 
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframeID));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}	
	
	public boolean waitForPageElementById(String elementId) {
		logger.info(String.format("waitForPageElementById(%s)", elementId));
		return waitForElementToBeClickable(By.id(elementId));
	}
	
	public boolean waitForElementToBeClickable(String elementId, long timeoutSeconds) {
		logger.info(String.format("waitForElementToBeClickable(%s)", elementId));
		return waitForElementToBeClickable(By.id(elementId), timeoutSeconds);
	}
	
	public boolean waitForPageElement(By locator) {
		logger.info(String.format("waitForPageElement(%s)", locator));

		return waitForVisibilityOfElement(locator, _TIMEOUT_SECONDS) != null ? true : false;
	}
	
	
	public boolean waitForElementToBeClickable(By locator) {
		logger.info(String.format("waitForElementToBeClickable(%s)", locator));
		return waitForElementToBeClickable(locator, _TIMEOUT_SECONDS);
	}
	
	public boolean waitForElementToBeClickable(By locator, long timeoutSeconds) {
		logger.info(String.format("waitForElementToBeClickable(%s)", locator));
		boolean clickable = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds, 200);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			clickable = true;
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}	
		return clickable;
	}
	
	public boolean waitForTextToBePresentInElement(By locator, String text, long timeoutSeconds) {
		logger.info(String.format("waitForTextToBoPresentInElement(%s)", locator));
		boolean textIsPresent = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds, 200);
//			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			textIsPresent = wait.until(ExpectedConditions.textToBePresentInElement(locator, text));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return textIsPresent;
	}
		
	public void waitAndClick(String elementID) {
		logger.info(String.format("waitAndClick(%s)", elementID));
		
		By locator = By.id(elementID);
		waitAndClick(locator);
	}
	
	public void waitAndClick(By locator) {
		logger.info(String.format("waitAndClick(%s)", locator));
	
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
			WebElement readyToClick = wait.until(ExpectedConditions.elementToBeClickable(locator));
			readyToClick.click();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
	
	/**
	 * @Description This method assumes that id attribute is present for the given element
	 * @param element
	 */
	public void waitAndClick(WebElement element) {
		logger.info(String.format("waitAndClick(%s)", element));
		waitAndClick(element.getAttribute("id"));
	}
	
	public void clearAndType(By locator, String text) {
		logger.info(String.format("clearAndType(%s)", locator, text));
	
		WebElement element = getDriver().findElement(locator);

		clearAndType(element, text);
	}
	
	public void clearAndType(WebElement element, String text) {
		logger.info(String.format("clearAndType(%s)", element, text));
		try {
			element.clear();
			element.sendKeys(text + "\t");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
	
	public void selectDropdownItem(WebElement element, String valueToSelect) {
		logger.info(String.format("setSelectedField(%s)", element, valueToSelect));
	    try {
			Select dropdown = new Select(element);
			dropdown.selectByVisibleText(valueToSelect);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
	
	public void setValueUsingJS(WebElement element, String value) {
        ((JavascriptExecutor)getDriver()).executeScript("arguments[0].value = arguments[1]", element, value);
    }
	
	public static WebElement waitForElement(By locator) {
		logger.info(String.format("waitForElement(%s)", locator));
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
//			element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			logger.warn(e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return element;
	}
	

	public static WebElement waitForElement(By locator, long timeoutSeconds) {
		logger.info(String.format("waitForElement(%s)", locator));
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds);
//			element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			logger.warn(e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return element;
	}

	public static List<WebElement> getElements(By locator) {
		logger.info(String.format("waitForElement(%s)", locator));
		List<WebElement> elements = null;
		try {
			elements = getDriver().findElements(locator);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		return elements;
	}

	public static WebElement getElement(WebElement parentElement, By locator) {
		logger.info(String.format("getElement(%s)", parentElement, locator));
		WebElement element = null;
		try {
			element = parentElement.findElement(locator);
		} catch (Exception e) {
			logger.warn(e.getMessage());
			throw new RuntimeException("Element not found in parent element "+ locator);
		}
		return element;
	}
	
	public static List<WebElement> getElements(WebElement parentElement, By locator) {
		logger.info(String.format("getElements(%s)", parentElement, locator));
		List<WebElement> elements = null;
		try {
			elements = parentElement.findElements(locator);
		} catch (Exception e) {
			logger.warn(e.getMessage());
			throw new RuntimeException("Elements not found in parent element "+ locator);
		}
		return elements;
	}
	
	
	public Boolean waitTillDisplay(final By locator){
	    WebDriverWait wait = new WebDriverWait(getDriver(), _TIMEOUT_SECONDS);
	    Boolean displayed = wait.until(new ExpectedCondition<Boolean>() {
	    	public Boolean apply(WebDriver driver) {
	    		return driver.findElement(locator).isDisplayed();      						}
        	});
        return displayed;
	}
	
	public WebElement waitForVisibilityOfElement(By locator, long timeoutSeconds) {
		logger.info(String.format("waitForVisibilityOfElement(%s)", locator));
		WebElement element = null;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds);
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return element;
	}
	
	public boolean waitForInVisibilityOfElement(By locator, long timeoutSeconds) {
		logger.info(String.format("waitForVisibilityOfElement(%s)", locator));
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds);
			return wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return false;
	}

	public boolean waitForInVisibilityOfElement(By locator) {
		return waitForInVisibilityOfElement(locator, _TIMEOUT_SECONDS);
	}

	public WebElement waitForVisibilityOf(WebElement element) {
		logger.info(String.format("waitForVisibilityOfElement(%s)", element));
		WebElement visibleElement = null;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
			visibleElement = wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return visibleElement;
	}
	
	public boolean waitUntilInvisibilityOfElement(By locator, long timeoutSeconds) {
		logger.info(String.format("waitUntilInvisibilityOfElement(%s)", locator));
		boolean invisibility = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds);
			invisibility = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}		
		return invisibility;
	}

	
	public boolean waitForStalenessOf(WebElement element) {
		logger.info(String.format("waitForStalenessOf(%s)", element));
		boolean staleness = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
			staleness = wait.until(ExpectedConditions.stalenessOf(element));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return staleness;
	}
	
	public boolean waitForDialog(String fragmentOfTitle) {
		logger.info(String.format("waitForStalenessOf(%s)", fragmentOfTitle));
		boolean is_displayed = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
			is_displayed = wait.until(ExpectedConditions.titleContains(fragmentOfTitle));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return is_displayed;
	}
	
	public String getElementText(WebElement element) {
		logger.info(String.format("getElementText(%s)", element));
		WebElement textElement = waitForVisibilityOf(element);

		return textElement.getText();	
	}
	
	public String getElementTextById(String elementId) {
		logger.info(String.format("getElementText(%s)", elementId));
		WebElement textElement = waitForVisibilityOf(waitForElement(By.id(elementId)));

		return textElement.getText();	
	}

	/**
	 * Description: confirms if it is the text of a specific tag type
	 * @return true if tagName contains expected text, false if not
	 */
	boolean isTextPresent(String tagName, String expectedText) { 
		logger.info(String.format("isTextPresent(%s)", expectedText));
		boolean result = true; 
		By by = By.xpath("//" + tagName + "[contains(text(),'" + expectedText + "')]"); 
		try { 
			getDriver().findElements(by); 
        } catch(NoSuchElementException e) { 
        	logger.warn(e.getMessage());
        	result = false; 
		} 
        return result; 
	} 
	
	public void sleepms(long milliseconds) {
		long end_time = System.currentTimeMillis() + milliseconds;
		
		while (System.currentTimeMillis() < end_time) {}

	}
	
	public void sleep(int seconds) {
		logger.info(String.format("sleep(%s)", seconds));
		Sleeper.sleepTightInSeconds(seconds);
	}
	
	public void sleep(){
		sleep(3);
	}
	public WebElement getLastRowFromTable(WebElement divEl){
		List<WebElement> rowsElList = getTableRowsListFromDiv(divEl);
		if (rowsElList == null || rowsElList.size() < 1){
			return null;
		}
	    return rowsElList.get(rowsElList.size()-1);
	}

	public WebElement getFirstRowFromTable(WebElement divEl){
		return getTRElementFromTable(divEl, 1);
	}
	
	public WebElement getTRElementFromTable(WebElement divEl, int rowNum){
		List<WebElement> rowsElList = getTableRowsListFromDiv(divEl);
		if (rowsElList == null || rowsElList.size() < 1 || rowNum > rowsElList.size()){
			return null;
		}
	    return rowsElList.get(rowNum-1);
	}
	
	public List<WebElement> getTableRowsListFromDiv(WebElement divEl){
	    List<WebElement> rowsElList = divEl.findElements(By.cssSelector("table tbody tr"));
	    return rowsElList;
	}
	
	public void clickTab(By locator){
		logger.info(String.format("clickTab(%s)", locator));
		try {
			WebElement element = getDriver().findElement(locator);
			element.sendKeys(Keys.TAB);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
	}
	
	public boolean isElementPresent(By by) {
		logger.info(String.format("isElementPresent(%s)", by));
		try{
			getDriver().findElement(by);
			return true;
		} catch(NoSuchElementException e) {
			logger.warn(e.getMessage());
			return false;
		}
	}
	
	public WebElement getElementIfItIsPresent(By by) {
		logger.info(String.format("getElementIfItIsPresent(%s)", by));
		try{
			WebElement element = getDriver().findElement(by);
			return element;
		} catch(NoSuchElementException e) {
			logger.warn(e.getMessage());
			return null;
		}
	}
	
	public WebElement getElementIfItIsPresent(WebElement parentEl, By by) {
		logger.info(String.format("getElementIfItIsPresent(%s)", by));
		try{
			WebElement element = parentEl.findElement(by);
			return element;
		} catch(NoSuchElementException e) {
			logger.warn(e.getMessage());
			return null;
		}
	}
	
	public void acceptAlert() {
	    try {
	        WebDriverWait wait = new WebDriverWait(getDriver(), 3);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = getDriver().switchTo().alert();
	        alert.accept();
	    } catch (Exception e) {
	    	logger.warn(e.getMessage());
	    }
	}	
	
	public void dismissAlert() {
	    try {
	        WebDriverWait wait = new WebDriverWait(getDriver(), 3);
	        wait.until(ExpectedConditions.alertIsPresent());
	        Alert alert = getDriver().switchTo().alert();
	        alert.dismiss();
	    } catch (Exception e) {
	    	logger.warn(e.getMessage());
	    }
	}	

	public static WebElement waitForElement(WebElement parentElement, By locator){
		long endTime = System.currentTimeMillis() + 20000l;
		WebElement element = null;
		while(endTime > System.currentTimeMillis()){
			try{
				element = parentElement.findElement(locator);
				return element;
			}catch(Exception e){
				long waitTime = System.currentTimeMillis() + 500l;
				while (waitTime <= System.currentTimeMillis()){}
			}
		}
		logger.warn("Element not found in the parent element : "+ locator);
		throw new RuntimeException("Element not found in the parent element"+ locator);
	}
	
	public WebElement waitForElement(WebElement parentElement, By locator, long timeoutSeconds){
		long endTime = System.currentTimeMillis() + (1000 * timeoutSeconds);
		WebElement element = null;
		while(endTime > System.currentTimeMillis()){
			try{
				element = parentElement.findElement(locator);
				return element;
			}catch(Exception e){
				long waitTime = System.currentTimeMillis() + 500l;
				while (waitTime <= System.currentTimeMillis()){}
			}
		}
		logger.warn("Element not found in the parent element : "+ locator);
		throw new RuntimeException("Element not found in the parent element"+ locator);
	}

	public void waitForElement(WebElement element, String value){
		long endTime = System.currentTimeMillis() + 20000l;
		while(endTime > System.currentTimeMillis()){
			try{
				if (element.getText().equalsIgnoreCase(value)){
					return;
				}
			}catch(Exception e){
			}
		}
		logger.warn("Element with given text not found" + element + "  --> " + value);
	}
	public void waitForElement(By locator, String value){
		long endTime = System.currentTimeMillis() + 20000l;
		while(endTime > System.currentTimeMillis()){
			try{
				WebElement element = getDriver().findElement(locator);
				if (element.getText().equalsIgnoreCase(value)){
					return;
				}
			}catch(Exception e){
			}
		}
		logger.warn("Element with given text not found");
	}
	public void waitForText(String value){
		long endTime = System.currentTimeMillis() + 20000l;
		while(endTime > System.currentTimeMillis()){
			try{
				if (getDriver().getPageSource().contains(value)){
					return;
				}
			}catch(Exception e){
			}
		}
		logger.warn("Element with given text not found");
	}

	public static List<WebElement> waitForElements(By locator) {
		logger.info(String.format("waitForElement(%s)", locator));
		List<WebElement> elementsList = null;
		long endTime = System.currentTimeMillis() + (1000 * _TIMEOUT_SECONDS);
		while(endTime > System.currentTimeMillis()){
			try{
				elementsList = getDriver().findElements(locator);
				return elementsList;
			}catch(Exception e){
				continue;
			}
		}
		throw new RuntimeException("Elements not found "+ locator);
	}

	public static List<WebElement> waitForElements(By locator, long timeoutSeconds) {
		logger.info(String.format("waitForElement(%s)", locator));
		List<WebElement> elementsList = null;
		long endTime = System.currentTimeMillis() + (1000 * timeoutSeconds);
		while(endTime > System.currentTimeMillis()){
			try{
				elementsList = getDriver().findElements(locator);
				return elementsList;
			}catch(Exception e){
				continue;
			}
		}
		return null;
	}
	public static List<WebElement> waitForElements(WebElement parentElement, By locator, long timeoutSeconds) {
		logger.info(String.format("waitForElement(%s)", locator));
		List<WebElement> elementsList = null;
		long endTime = System.currentTimeMillis() + (1000 * timeoutSeconds);
		while(endTime > System.currentTimeMillis()){
			try{
				elementsList = parentElement.findElements(locator);
				return elementsList;
			}catch(Exception e){
				continue;
			}
		}
		return null;
	}
	public boolean waitForInVisibilityOfElement(WebElement parentElement, By locator, long timeoutSeconds) {
		long endTime = System.currentTimeMillis() + (1000 * timeoutSeconds);
		while(endTime > System.currentTimeMillis()){
			try{
				parentElement.findElement(locator);
				continue;
			}catch(Exception e){
				return true;
			}
		}
		return false;
	}
	public boolean waitForInVisibilityOfElement(WebElement parentElement, By locator) {
		return waitForInVisibilityOfElement(parentElement, locator, 20000l);
	}
	public static WebElement waitAndGetElementIfItIsPresent(By locator, long timeoutSeconds) {
        logger.info(String.format("waitForElement(%s)", locator));
        WebElement element = null;
        try {
               WebDriverWait wait = new WebDriverWait(getDriver(),  timeoutSeconds);
               element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
               logger.warn(e.getMessage());
        }
        return element;
	}

	 public static WebElement waitAndGetElementIfItIsPresent(By locator) {
	        logger.info(String.format("waitForElement(%s)", locator));
	        WebElement element = null;
	        try {
	               WebDriverWait wait = new WebDriverWait(getDriver(),  _TIMEOUT_SECONDS);
	               element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	        } catch (Exception e) {
	               logger.warn(e.getMessage());
	        }
	        return element;
	 }
	public boolean waitForVisibilityOfElement(WebElement parentEl, By locator, long timeoutSeconds) {
		long endTime = System.currentTimeMillis() + (1000 * timeoutSeconds);
		while(endTime > System.currentTimeMillis()){
			try{
				parentEl.findElement(locator);
				return true;
			}catch(Exception e){
				continue;
			}
		}
		return false;
	}
	
	public void executeIEFireEventJS(String elementId, String eventName){
		if (BROWSER.equals("IE") || BROWSER.equals("InternetExplorer")){
			((JavascriptExecutor) getDriver()).
					executeScript("return document.getElementById('"+ elementId +"').fireEvent('"+ eventName +"');");
		}
	}
	
	public boolean waitForStalenessOf(WebElement element, int seconds) {
		logger.info(String.format("waitForStalenessOf(%s)", element));
		boolean staleness = false;
		try {
			WebDriverWait wait = new WebDriverWait(getDriver(),  seconds);
			staleness = wait.until(ExpectedConditions.stalenessOf(element));
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		
		return staleness;
	}


}