package com.mediaocean.qa.framework.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * This class provides all the utility methods 
 * for Database operations. 
 * 
 * @author Karthikeyan
 */
public class DatabaseUtility {
	
	private Connection _con;
	private String _dbUrl = "<dbURL>";
	private String _dbUsername = "<username>";
    private String _dbPassword = "<password>";

	/**
	 * This method opens a connection with database
	 * @return Connection
	 */
	private Connection connect() {
		Connection con = null;
		try {
//			Class.forName("com.mysql.jdbc.Driver");
	        con = DriverManager.getConnection(_dbUrl, _dbUsername, _dbPassword);
		
		} catch (Exception e) {
			new RuntimeException("Failed to open the connection with database");
		}
		return con;
	}
     
	/**
	 * This method execute select query
	 * @param query
	 * @return ResultSet
	 */
	public ResultSet executeSelectQuery(String query) {
		connect();
		try {
			PreparedStatement statement = _con.prepareStatement(query);
			return statement.executeQuery();
		} catch (Exception e) {
			new RuntimeException("Failed to execute the query");
	    }finally{
	    	closeConnection();
	    }
		return null;
	}
	
	/**
	 * This method executes update query.
	 * @param query
	 * @return Object
	 */
	public Object executeUpdate(String query) {
		connect();
		try {
			PreparedStatement statement = _con.prepareStatement(query);
			return statement.executeUpdate();
		} catch (Exception e) {
			new RuntimeException("Failed to execute the update query");
		}finally{
	    	closeConnection();
	    }
		return null;
	}
	
	/**
	 * This method close the connection.
	 */
	public void closeConnection() {
		
		try {
			if (!_con.isClosed()) {
				_con.close();
			}
		} catch (Exception e) {
			new RuntimeException("Failed to close the database connection");
	    }
	}
}
