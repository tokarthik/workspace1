package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Button operations.
 * @author Karthikeyan
 */
public class Button extends PageBase {

	private WebElement m_button;
	
	/**
	 * This constructor takes Button element
	 * @param button
	 */
	public Button(WebElement button) {
		super(getDriver());	
		this.m_button = waitForVisibilityOf(button);
	}
	
	/**
	 * Use this constructor if you don't have id for Button but 
	 * you have id for parent element.
	 * 
	 * @param parentWebElement : parent of button element
	 * @param lebel : button label
	 */
	public Button(WebElement parentWebElement, String label) {
		this(identifyButtonInParentElement(parentWebElement, label));
	}
	
	/**
	 * This constructor takes the locator object for Button.
	 * 
	 * @param buttonLocator
	 */
	public Button(By buttonLocator) {
		this(waitForElement(buttonLocator));
	}
	
	/**
	 * This constructor takes the button label
	 * @param label
	 */
	public Button(String label) {
		this(identifyButtonInDriver(label));
	}

	private static WebElement identifyButtonInParentElement(WebElement parentWebElement, String label){
		List<WebElement> buttonsList = parentWebElement.findElements(By.cssSelector("input[class='gsButton']"));
		return identifyButton(buttonsList, label);
	}
	private static WebElement identifyButtonInDriver(String label){
		List<WebElement> buttonsList = getDriver().findElements(By.cssSelector("input[class='gsButton']"));
		return identifyButton(buttonsList, label);
	}

	private static WebElement identifyButton(List<WebElement> buttonsList, String label){
		WebElement button = null;
		for (int i=0;i<buttonsList.size();i++){
			button = buttonsList.get(i);
			if (button.getAttribute("value").equalsIgnoreCase(label)){
				break;
			}
		}
		return button;
	}
	
	/**
	 * This method clicks the button
	 */
	public void click() { 
		m_button.click();		
	}

	/**
	 * This method returns the button name 
	 * @return string : button name
	 */
	public String getButtonName() {
		String name;
		name = m_button.getAttribute("value").toString().trim();
		return name;
	}
	
	/**
	 * This method checks whether the button is enabled or not.
	 * @return boolean
	 */
	public boolean isButtonEnabled(){
		return m_button.isEnabled();
	}

	/**
	 * This method returns the button element.
	 * @return WebElement
	 */
	public WebElement getButtonElement() {
		return m_button;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_button.getAttribute(attributeName);
	}
}
