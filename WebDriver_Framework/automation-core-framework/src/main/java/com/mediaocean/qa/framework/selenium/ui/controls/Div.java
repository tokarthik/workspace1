package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the div operations.
 * @author Karthikeyan
 */
public class Div extends PageBase{
	
	private WebElement m_div;
	
	/**
	 * This constructor takes div element
	 * @param parentDiv
	 */
	public Div(WebElement parentDiv) {
		super(getDriver());
		this.m_div = parentDiv;
	}
	
	/**
	 * This constructor takes the locator object for div.
	 * @param divLocator
	 */
	public Div(By divLocator) {
		this(waitForElement(divLocator));
	}
	
	/**
	 * This method returns the list of table elements with in the div tag.
	 * @return List<WebElement>
	 */
	public List<WebElement> getTablesFromDiv(){
		List<WebElement> tableListEl = m_div.findElements(By.cssSelector("table"));
		if (tableListEl == null || tableListEl.size()==0){
			return null;
		}
		return tableListEl;
	}

	/**
	 * This method returns the first table element with in the div tag.
	 * @return WebElement
	 */
	public WebElement getFirstTableFromDiv(){
		List<WebElement> tableListEl = m_div.findElements(By.cssSelector("table"));
		if (tableListEl == null || tableListEl.size()==0){
			return null;
		}
		return tableListEl.get(0);
	}

	/**
	 * This method returns the last table element with in the div tag.
	 * @return WebElement
	 */
	public WebElement getLastTableFromDiv(){
		List<WebElement> tableListEl = m_div.findElements(By.cssSelector("table"));
		if (tableListEl == null || tableListEl.size()==0){
			return null;
		}
		return tableListEl.get(tableListEl.size()-1);
	}
	
	/**
	 * This method returns the Nth table element with in the div tag.
	 * @param n
	 * @return WebElement
	 */
	public WebElement getNthTableFromDiv(int n){
		List<WebElement> tableListEl = m_div.findElements(By.cssSelector("table"));
		if (tableListEl == null || tableListEl.size()==0){
			return null;
		}
		if (tableListEl.size() < n){
			return null;
		}
		return tableListEl.get(n-1);
	}
	/**
	 * This method returns the div element.
	 * @return WebElement
	 */
	public WebElement getDivElement() {
		return m_div;
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_div.getAttribute(attributeName);
	}
}
