package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Radio Button operations.
 * @author Karthikeyan
 */
public class RadioButton extends PageBase {

	private WebElement m_radioButton;

	/**
	 * This constructor takes Radio Button element
	 * @param radioBtn
	 */
	public RadioButton(WebElement radioBtn) {
		super(getDriver());
		this.m_radioButton = waitForVisibilityOf(radioBtn);	
	}
	
	/**
	 * This constructor takes the locator object for Radio Button.
	 * @param radioBtnLocator
	 */
	public RadioButton(By radioBtnLocator) {
		this(waitForElement(radioBtnLocator));
	}

	/**
	 * This constructor takes the element locator and value.
	 * @param radioBtnLocator
	 */
	public RadioButton(By radioBtnLocator, String value) {
		this(findByIdAndValue(radioBtnLocator, value));
	}

	/**
	 * This constructor takes the Radio Button label
	 * @param radioBtnId
	 */
	public RadioButton(String radioBtnLabel) {
		this(findByLabel(radioBtnLabel));
	}

	private static WebElement findByIdAndValue(By locator, String value){
		List<WebElement> list = getElements(locator);
		for (WebElement el : list){
			if (el.getAttribute("value").equalsIgnoreCase(value)){
				return el;
			}
		}
		return null;
	}
	
	private static WebElement findByLabel(String labelText){
		Label label = new Label(labelText);
		String textBoxId = label.getAttributeValue("for");
		if (textBoxId != null){
			return getDriver().findElement(By.id(textBoxId));
		}
		return null;
	}
	
	/**
	 * This method checks if the radio button is selected or not.
	 * @return boolean
	 */
	public boolean isSelected() {
		return m_radioButton.isSelected();
	}

	/**
	 * This method selects the radio button
	 */
	public void select() {
		if (!isSelected()){
			m_radioButton.click();
		}
	}

	/**
	 * This method un-selects the radio button
	 */
	public void unSelect() {
		if (isSelected()){
			m_radioButton.click();
		}
	}
	
	/**
	 * This method returns the radio button web element.
	 * @return WebElement
	 */
	public WebElement getRadioButton(){
		return m_radioButton;
	}

	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_radioButton.getAttribute(attributeName);
	}
}
