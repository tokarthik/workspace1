package com.mediaocean.qa.framework.selenium.ui.controls;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;

/**
 * This Class provides all the Label operations.
 * @author Karthikeyan
 */
public class Label extends PageBase {

	private WebElement m_label;

	public Label(WebElement label) {
		super(getDriver());
		this.m_label = waitForVisibilityOf(label);			
	}
	
	public Label(By labelLocator) {
		this(waitForElement(labelLocator));
	}

	public Label(String labelText) {
		this(identifyLabelByText(labelText));
	}

	private static WebElement identifyLabelByText(String labelText){
		List<WebElement> labelList = getDriver().findElements(By.tagName("label"));
		for (WebElement labelElement : labelList){
			if (labelElement.getText().equalsIgnoreCase(labelText)){
				return labelElement;
			}
		}
		return null;
	}
	
	public String getText() {
		return m_label.getText();
	}
	
	/**
	 * This method returns the attribute value
	 * @param attributeName
	 * @return string : attribute value
	 */
	public String getAttributeValue(String attributeName){
		return m_label.getAttribute(attributeName);
	}
}
