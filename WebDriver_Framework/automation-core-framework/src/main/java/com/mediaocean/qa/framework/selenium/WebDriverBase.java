package com.mediaocean.qa.framework.selenium;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

/**
 * This is the base class of the Web driver framework, defines custom assertion 
 * methods using TestNG test framework.
 * 
 * PageBase and TestBase are extended from this base class.
 * A static WebDriver instance is stored here and used throughout the framework.
 */
public abstract class WebDriverBase {
	
	private static Logger logger = Logger.getLogger(WebDriverBase.class);	

	private static WebDriver _driver;
	
	protected final static long _TIMEOUT_SECONDS  = 20;
	
	public static String BROWSER;
	public static String ENVIRONMENT;
	public static String URL;
	
	public static WebDriver getDriver() {
		return _driver;
	}
	
	public void setDriver(WebDriver driver) {
		_driver = driver; 
	}
	
	public void logMessage(String message) {
		Reporter.log(message + "<br>");
	}
	
	public static void assertTrue(boolean condition) {
    	logger.info(String.format("assertTrue(%s)", condition));
    	Assert.assertTrue(condition);
    }
    
    public static void assertTrue(boolean condition, String message) {
    	logger.info(String.format("assertTrue(%s, %s)", condition, message));
    	Assert.assertTrue(condition, message);
    }
    
    public static void assertFalse(boolean condition) {
    	logger.info(String.format("assertFalse(%s)", condition));
    	Assert.assertFalse(condition);
    }
    
    public static void assertFalse(boolean condition, String message) {
    	logger.info(String.format("assertFalse(%s)", condition));
    	Assert.assertFalse(condition, message);
    }
    
    public static void assertEquals(boolean actual, boolean expected, String message) {
    	logger.info(String.format("assertEquals(%s, %s)", actual, expected));
    	Assert.assertEquals(actual, expected, message);
    }
    
    public static void assertEquals(Object actual, Object expected) {
    	logger.info(String.format("assertEquals(%s, %s)", actual, expected));
    	Assert.assertEquals(actual, expected);
    }
    
    public static void assertEquals(Object actual, Object expected, String message) {
    	logger.info(String.format("assertEquals(%s, %s, %s)", actual, expected, message));
    	Assert.assertEquals(actual, expected, message);
    }
    
    public static void assertEquals(Object[] actual, Object[] expected) {
    	logger.info(String.format("assertEquals(%s, %s)", actual, expected));
    	Assert.assertEquals(actual, expected);
    }
    
    public static void assertEquals(Object[] actual, Object[] expected, String message) {
    	logger.info(String.format("assertEquals(%s, %s, %s)", actual, expected, message));
    	Assert.assertEquals(actual, expected, message);
    }	
    
    public static void assertSame(Object actual, Object expected) {
    	logger.info(String.format("assertSame(%s, %s)", actual, expected));
    	Assert.assertSame(actual, expected);
    }
    
    public static void assertSame(Object actual, Object expected, String message) {
    	logger.info(String.format("assertSame(%s, %s, %s)", actual, expected, message));
    	Assert.assertSame(actual, expected, message);
    }
    
    public static void assertNotSame(Object actual, Object expected) {
    	logger.info(String.format("assertNotSame(%s, %s)", actual, expected));
    	Assert.assertNotSame(actual, expected);
    }
    
    public static void assertNotSame(Object actual, Object expected, String message) {
    	logger.info(String.format("assertNotSame(%s, %s, %s)", actual, expected, message));
    	Assert.assertNotSame(actual, expected, message);
    }
    
    public static void fail(String message) {
    	logger.info(String.format("fail(%s)", message));
    	Assert.fail(message);
    }
    
	public static void assertPageLoaded(String expectedPagetitle) {
		logger.info(String.format("assertPageLoaded(%s)", expectedPagetitle));		
		assertEquals(_driver.getTitle(), expectedPagetitle, "'" + expectedPagetitle + "' page failed to load!");	
	}
	
	public static void assertDialogIsDisplayed(By dialog) {
		logger.info(String.format("assertDialogIsDisplayed(%s)", dialog));
		assertElementIsDisplayed(dialog);	
	} 
		 	
	/**
	 * Search text in page source, throws assertion error if page source doesn't contain the text
	 */
	public static void assertTextIsPresentOnPage(String text) {
		logger.info(String.format("assertTextIsPresentOnPage(%s)", text));
		if(!WebDriverBase._driver.getPageSource().contains(text)){
			logger.error("Text '" + text + "' not in page source.");
			throw new AssertionError("Text '" + text + "' not in page source.");
		}
	}
	
	/**
	 * Search in current page URL, throws assertion error if not found
	 */
	public static void assertThatURLcontains(String URLfragment) {
		logger.info(String.format("assertThatURLcontains(%s)", URLfragment));
		assertTrue(WebDriverBase._driver.getCurrentUrl().contains(URLfragment), "FAILURE: Current page URL does not contain '" + URLfragment + "'.");
	}
	
	public static void assertElementIsDisplayed(By by) {
		logger.info(String.format("assertElementIsDisplayed(%s)", by));
		assertElementIsDisplayed(_driver.findElement(by));		
	}
	
	public static void assertElementIsDisplayed(WebElement element) {
		logger.info(String.format("assertElementIsDisplayed(%s)", element));
		assertTrue(element.isDisplayed(), "Element not displayed --> id: " + element.getAttribute("id"));
	}
	
	public static void assertElementIsNotDisplayed(By by) {
		logger.info(String.format("assertElementIsNotDisplayed(%s)", by));
		assertElementIsNotDisplayed(_driver.findElement(by));			
	}
	
	public static void assertElementIsNotDisplayed(WebElement element) {
		logger.info(String.format("assertElementIsNotDisplayed(%s)", element));
		assertFalse(element.isDisplayed(), "FAILURE: expected:<not displayed>, but was:<displayed> --> id: " + element.getAttribute("id"));
	}
	
	public static void assertElementIsEnabled(By by) {
		logger.info(String.format("assertElementIsEnabled(%s)", by));		
		assertElementIsDisplayed(_driver.findElement(by));
	}
	
	public static void assertElementIsEnabled(WebElement element) {
		logger.info(String.format("assertElementIsEnabled(%s)", element));	
		boolean status = false;
		String tagName = element.getTagName();
		/** isEnabled() method generally returns true for everything but disabled input elements, hence this approach */
		if(tagName.equals("input")) {
			status = element.isEnabled();
		} else {
			status = Boolean.parseBoolean(element.getAttribute("is_enabled"));
		}
		
		assertTrue(status, "FAILURE: expected:<enabled>, but was:<disabled> --> id: " + element.getAttribute("id"));	
	}
	
	public static void assertElementIsDisabled(By by) {
		logger.info(String.format("assertElementIsDisabled(%s)", by));		
		assertElementIsDisabled(_driver.findElement(by));
	}
	
	public static void assertElementIsDisabled(WebElement element) {
		logger.info(String.format("assertElementIsDisabled(%s)", element));
		boolean status = true;
		String tagName = element.getTagName();
		/** isEnabled() method generally returns true for everything but disabled input elements, hence this approach */
		if(tagName.equals("input")) {
			status = element.isEnabled();
		} else {
			status = Boolean.parseBoolean(element.getAttribute("is_enabled"));
		}
		
		assertFalse(status, "FAILURE: expected:<disabled>, but was:<enabled> --> id: " + element.getAttribute("id"));	
	}
	
	public static void assertText(String actual, String expected) {
		logger.info(String.format("assertText(%s)", expected));			
		assertEquals(actual, expected);
	}
			
	public static void assertBodyContainsText(String textToFind) {
		logger.info(String.format("assertPageContainsText(%s)", textToFind));
		Assert.assertTrue(_driver.findElement(By.tagName("body")).getAttribute("id").contains(textToFind), 
							"FAILURE: Text doesn't appear on the page => " + textToFind);
	}
	
	public static void assertPageContainsText(String textToFind) {
		logger.info(String.format("assertPageContainsText(%s)", textToFind));
		Assert.assertTrue(_driver.getPageSource().contains(textToFind), 
							"FAILURE: Text doesn't appear on the page => " + textToFind);
	}
	
}
