package com.gta.travel.page.base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mediaocean.qa.framework.selenium.PageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.utils.ExcelUtil;

public class GSPageBase extends PageBase{
	
	public static ExcelUtil excelUtil = new ExcelUtil(ClassLoader.getSystemClassLoader()
			.getResource("Data_Sheet.xls").getPath());
	
	public GSPageBase(WebDriver driver){
		super(driver);
	}
	
	public WebElement menuElement;
	
	/**
	 * @param webElementId
	 * @return
	 */
	public int getSectionTableRowCount(String webElementId){
		WebElement parentEl = waitForElement(By.id(webElementId));
		Table table = new Table(parentEl, 1);
		return table.getRowCount();
	}
	
	/**
	 * This method clicks on select option based on the parent Web Element Id
	 * @param webElementId
	 */
	private void clickSelectOption(String webElementOptionsId) {
		menuElement = waitForElement(By.id(webElementOptionsId));
		waitForElement(menuElement, By.linkText("Select Option"));
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}
	}

	/**
	 * This method clicks on the menu option from the select option drop down
	 * @param webElementOptionsId
	 * @param optionName
	 */
	public void chooseMenuOptionsFromSelectOption(String webElementOptionsId, String optionName) {
		clickSelectOption(webElementOptionsId);
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
	}
}
