package com.gta.travel.page.base;

import com.mediaocean.qa.framework.selenium.TestBase;

public class GSTestBase extends TestBase{

	public GSTestBase(){
		String driverSheetPath = System.getProperty("dirver-sheet-path");
		if (driverSheetPath != null){
			setDriverSheetAbsolutePath(driverSheetPath);
		}else{
			setDriverSheetFileName("Driver_Sheet.xls");
		}
	}
	
	public String getUserName(){
		return excelUtil.getKeyValue("Test", "userName");
	}
	public String getPassword(){
		return excelUtil.getKeyValue("Test", "password");
	}
	public String getWebId(){
		return excelUtil.getKeyValue("Test", "webId");
	}
	public String getUrl(){
		return excelUtil.getKeyValue("Test", "URL");
	}
}
