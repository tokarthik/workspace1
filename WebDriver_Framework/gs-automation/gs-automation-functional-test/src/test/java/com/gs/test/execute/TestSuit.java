package com.gs.test.execute;

import java.util.Locale;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.gs.test.report.plugin.GSTestNgXsltMojo;


public class TestSuit { 
	public static void main(String args[]) throws Exception{
		TestSuit t = new TestSuit();
		t.method();
	} 
	public void method() throws Exception{
		TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
        testng.setTestClasses(new Class[] { 
//        										InternalDetailsTest.class
//        										PropertyDetailsTest.class
//        										,ChainDetailsTest.class
        								  });
        testng.addListener(tla);
        testng.run();  
        
        GSTestNgXsltMojo xslt = new GSTestNgXsltMojo();
        
        String targetDir = System.getProperty("targetDir");
        String surefireReportDirectory = System.getProperty("surefireReportDirectory");
		if (targetDir == null){
	        targetDir = getClass().getResource(getClass().getSimpleName()+".class")
										.getPath().split("/test-classes/")[0];
		}
		
        if (surefireReportDirectory == null){
        	xslt.setSurefireReportDirectory(targetDir.split("/target")[0]+"\\test-output");
        }
        
        xslt.setOutputDir(targetDir);
        xslt.executeReport(new Locale("EN"));
	}
}
