package com.gta.travel.page.object.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;

public class LogoutPage extends GSPageBase {

	public LogoutPage(WebDriver driver){
		super(driver);
	}

	public static LogoutPage getInstance(){
		return PageFactory.initElements(getDriver(), LogoutPage.class);	
	}
	
	public LoginPage logOut(){
		Link logOutLink = new Link(waitForElement(By.linkText("Log out")));
		logOutLink.clickLink();
		waitForElement(By.id("login"));
		
		return PageFactory.initElements(getDriver(), LoginPage.class);
	}
}
