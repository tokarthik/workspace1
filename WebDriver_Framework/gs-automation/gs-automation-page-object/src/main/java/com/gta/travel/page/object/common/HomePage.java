package com.gta.travel.page.object.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;

public class HomePage extends GSPageBase {

	public HomePage(WebDriver driver){
		super(driver);
	}
	
	public static HomePage getInstance(){
		return PageFactory.initElements(getDriver(), HomePage.class);
	}
	
	public ContentSearchPage selectContent(){
		waitAndClick(By.linkText("Content"));
		waitForElementToBeClickable(By.id("search"));
	    return PageFactory.initElements(getDriver(), ContentSearchPage.class);
	}
	
	public ContractSearchPage selectContract(){
		Link link = new Link("Contracts");
		link.clickLink();
	    return PageFactory.initElements(getDriver(), ContractSearchPage.class);
	}
	
	public void clickHomeLink(){
		Link link = new Link("Home");
		link.clickLink();
		waitForElement(By.id("internalMenu"));
	}
}
