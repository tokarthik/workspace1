package com.gta.travel.page.object.contract.rateplans;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class RatePlanSmartFillRateSectionPage extends GSPageBase{

	public String[] dayArr = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun","All"};

	public RatePlanSmartFillRateSectionPage(){
		super(getDriver());
	}
	
	public static RatePlanSmartFillRateSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanSmartFillRateSectionPage.class);
	}
	
	public void resetData(){
		
	}

	public boolean verifyRecordUpdatedMessage(){
		WebElement parentEl = waitForElement(By.id("marginRateRule_create"));
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains("Margin room rates have been created/updated.")){
			return true;
		}
		return false;
	}
	
	public void clickCreateLink(){
		WebElement parentEl = waitForElement(By.id("marginRateRuleContentMenuOptions"));
		Link link = new Link(parentEl, "Create");
		link.clickLink();
		waitForElement(By.id("marginRateRuleCreateCancel"));
		sleepms(500l);
	}
	
	public String editRates(Map<String, String> inputMap){
		String grossValue = "";
		if (inputMap.containsKey("start day")){
			waitForElement(By.id("marginRateRule-travelStartDatePicker_day"));
			ComboBox startDay = new ComboBox(By.id("marginRateRule-travelStartDatePicker_day"));
			startDay.select(inputMap.get("start day"));
		}
		if (inputMap.containsKey("start month")){
			waitForElement(By.id("marginRateRule-travelStartDatePicker_months"));
			ComboBox startMonth = new ComboBox(By.id("marginRateRule-travelStartDatePicker_months"));
			startMonth.select(inputMap.get("start month"));
		}
		if (inputMap.containsKey("start year")){
			waitForElement(By.id("marginRateRule-travelStartDatePicker_years"));
			ComboBox startYear = new ComboBox(By.id("marginRateRule-travelStartDatePicker_years"));
			startYear.select(inputMap.get("start year"));
		}
		
		if (inputMap.containsKey("end day")){
			String endDayValue = inputMap.get("end day");
			waitForElement(By.id("marginRateRule-travelEndDatePicker_day"));
			ComboBox endDay = new ComboBox(By.id("marginRateRule-travelEndDatePicker_day"));
			endDay.select(endDayValue);
			if (!endDay.getSelectedItem().equals(endDayValue)){
				endDay.select(endDayValue);
			}
		}
		if (inputMap.containsKey("end month")){
			String endMonthValue = inputMap.get("end month");
			waitForElement(By.id("marginRateRule-travelEndDatePicker_months"));
			ComboBox endMonth = new ComboBox(By.id("marginRateRule-travelEndDatePicker_months"));
			endMonth.select(endMonthValue);
			if (!endMonth.getSelectedItem().equals(endMonthValue)){
				endMonth.select(endMonthValue);
			}
		}
		if (inputMap.containsKey("end year")){
			String endYearValue = inputMap.get("end year");
			waitForElement(By.id("marginRateRule-travelEndDatePicker_years"));
			ComboBox endYear = new ComboBox(By.id("marginRateRule-travelEndDatePicker_years"));
			endYear.select(endYearValue);
			if (!endYear.getSelectedItem().equals(endYearValue)){
				endYear.select(endYearValue);
			}
		}
		
		if (inputMap.containsKey("days")){
			String value = inputMap.get("days");
			if (value.equalsIgnoreCase("All")){
				CheckBox allCB = new CheckBox(By.id("daysOfWeekTickAll"));
				if (!allCB.isChecked()){
					allCB.check();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
					if(!dayCB.isChecked()){
						dayCB.check();
					}
				}
			}else{
				CheckBox allCB = new CheckBox(By.id("daysOfWeekTickAll"));
				if (allCB.isChecked()){
					allCB.uncheck();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
					if (dayCB.isChecked()){
						dayCB.uncheck();
					}
				}
				
				for (int i=0;i<dayArr.length-1;i++){
					String dayValue = dayArr[i];
					waitForElement(By.id("daysOfWeek"+i));
					CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
					if (value.contains(dayValue)){
						dayCB.check();
					}
				}
			}
		}
		
		if (inputMap.containsKey("minimum nights")){
			waitForElement(By.id("minNightsSelect"));
			ComboBox startDay = new ComboBox(By.id("minNightsSelect"));
			startDay.select(inputMap.get("minimum nights"));
		}

		if (inputMap.containsKey("more rules")){
			RadioButton moreRulesRB = new RadioButton(By.name("moreRules"), inputMap.get("more rules"));
			moreRulesRB.select();
		}

		if (inputMap.containsKey("stay full period")){
			RadioButton moreRulesRB = new RadioButton(By.name("fullPeriod"), inputMap.get("stay full period"));
			moreRulesRB.select();
		}

		int count = -1;
		for (int i=0;i<10;i++){
			if (isElementPresent(By.id("marginRateRuleCreate.roomCheck."+i))){
				CheckBox roomType = new CheckBox(By.id("marginRateRuleCreate.roomCheck."+i));
				roomType.uncheck();
				executeIEFireEventJS("marginRateRuleCreate.roomCheck."+i, "onclick");
				count++;
			}
		}

		CheckBox twinRoomType = new CheckBox(By.id("marginRateRuleCreate.roomCheck."+ count));
		twinRoomType.check();
		executeIEFireEventJS("marginRateRuleCreate.roomCheck."+count, "onclick");

		if (inputMap.containsKey("twin room type net")){
			TextBox grossTB = new TextBox(By.id("marginRateRuleCreate.grossDsp."+ count));
			String grossValueBefore = grossTB.getText();
			TextBox stdRmNetTB = new TextBox(By.id("marginRateRuleCreate.nett."+ count));
			stdRmNetTB.setText(inputMap.get("twin room type net"));
			stdRmNetTB.clickTab();
			executeIEFireEventJS("marginRateRuleCreate.nett."+ count, "onblur");
			
			for (int m=0;m<3;m++){
				grossValue = grossTB.getText();
				
				if (grossValueBefore.equalsIgnoreCase(grossValue)){
					TextBox mrgTB = new TextBox(By.id("marginRateRuleCreate.margin."+ count));
					mrgTB.clickTab();
					executeIEFireEventJS("marginRateRuleCreate.nett."+ count, "onblur");
					sleep(1);
					continue;
				}else{
					break;
				}
			}
			
		}
		if (inputMap.containsKey("twin room type margin")){
			TextBox stdRmNetTB = new TextBox(By.id("marginRateRuleCreate.margin."+ count));
			stdRmNetTB.setText(inputMap.get("twin room type margin"));
		}
		return grossValue;
	}
	
	public String getNetValue(){
		TextBox stdRmNetTB = new TextBox(By.id("marginRateRuleCreate.nett.4"));
		return stdRmNetTB.getText();
	}
	
	public boolean cancelEditRates(){
		WebElement element = waitForElement(By.id("marginRateRuleCreateCancel"));
		Button button = new Button(element);
		button.click();
		return waitForInVisibilityOfElement(By.id("marginRateRuleCreateCancel"));
	}
	
	public boolean addAnotherEditRates(){
		WebElement parentElement = waitForElement(By.id("marginRateRule_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		addAnotherButton.click();
		waitForElement(parentElement, By.className("message"),60);
		if (waitForElement(By.id("marginRateRuleCreateCancel"))!= null){
			return true;
		}
		return false;
	}
	
	public boolean copyEditRates(){
		WebElement parentElement = waitForElement(By.id("marginRateRule_create"));
		Button button = new Button(parentElement, "Copy");
		button.click();
		waitForElement(parentElement, By.className("message"),60);
		if (waitForElement(By.id("marginRateRuleCreateCancel"))!= null){
			return true;
		}
		return false;
	}
	
	public void saveEditRates(){
		WebElement parentElement = waitForElement(By.id("marginRateRule_create"));
		Button button = new Button(parentElement, "Save");
		button.click();
		waitForElement(parentElement, By.className("message"),60);
		waitForInVisibilityOfElement(By.id("marginRateRuleCreateCancel"));
	}
	
}
