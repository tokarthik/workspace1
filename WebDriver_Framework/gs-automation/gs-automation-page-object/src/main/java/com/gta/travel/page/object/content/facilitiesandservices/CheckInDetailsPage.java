package com.gta.travel.page.object.content.facilitiesandservices;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

/**
 * This class provides the methods for check In details section
 * 
 * @author Suvarchala Vege
 *
 */
public class CheckInDetailsPage extends GSPageBase{
	
	public String[] dayArr = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun","All"};
	
	/**
	 * Default Constructor
	 */
	public CheckInDetailsPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the page factory object for the CheckIn Details Page
	 * @return static
	 */
	public static CheckInDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  CheckInDetailsPage.class);
	}
	
	/**
	 * This method clicks on the edit link of the check in details section
	 */
	public void clickEditCheckInDetails() {
		WebElement parentElement = waitForElement(By.id("checkInDetailsContentMenuOptions"));
		Link editLink = new Link(parentElement, "Edit");
		editLink.clickLink();	
		waitForVisibilityOfElement((By.id("checkInDetailsEdit")), 40);
	}
	
	/**
	 * This method enters the values in the edit section of the check In details section
	 * @param inputMap
	 */
	public void editCheckInDetails(Map<String, String> inputMap) {
		
		if (inputMap.containsKey("CheckInTime")){
			TextBox checkInTime = new TextBox(By.id("checkInTime"));
			checkInTime.setText("");
			checkInTime.setText(inputMap.get("CheckInTime"));
		}
		if (inputMap.containsKey("CheckOutTime")){
			TextBox checkOutTime = new TextBox(By.id("checkOutTime"));
			checkOutTime.setText("");
			checkOutTime.setText(inputMap.get("CheckOutTime"));
		}
		
		if (inputMap.containsKey("CheckInDays")){
			String value = inputMap.get("CheckInDays");
			if (value.equalsIgnoreCase("All")){
				CheckBox allCB = new CheckBox(By.id("checkInDayTickAll"));
				if (!allCB.isChecked()){
					allCB.check();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("checkInDay"+i));
					if(!dayCB.isChecked()){
						dayCB.check();
					}
				}
			}else{
				CheckBox allCB = new CheckBox(By.id("checkInDayTickAll"));
				if (allCB.isChecked()){
					allCB.uncheck();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("checkInDay"+i));
					if (dayCB.isChecked()){
						dayCB.uncheck();
					}
				}
				
				for (int i=0;i<dayArr.length-1;i++){
					String dayValue = dayArr[i];
					waitForElement(By.id("checkInDay"+i));
					CheckBox dayCB = new CheckBox(By.id("checkInDay"+i));
					if (value.contains(dayValue)){
						dayCB.check();
					}
				}
			}
		}
		
		if (inputMap.containsKey("CheckOutDays")){
			String value = inputMap.get("CheckOutDays");
			if (value.equalsIgnoreCase("All")){
				CheckBox allCB = new CheckBox(By.id("checkOutDayTickAll"));
				if (!allCB.isChecked()){
					allCB.check();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("checkOutDay"+i));
					if(!dayCB.isChecked()){
						dayCB.check();
					}
				}
			}else{
				CheckBox allCB = new CheckBox(By.id("checkOutDayTickAll"));
				if (allCB.isChecked()){
					allCB.uncheck();
				}
				for (int i=0;i<dayArr.length-1;i++){
					CheckBox dayCB = new CheckBox(By.id("checkOutDay"+i));
					if (dayCB.isChecked()){
						dayCB.uncheck();
					}
				}
				
				for (int i=0;i<dayArr.length-1;i++){
					String dayValue = dayArr[i];
					waitForElement(By.id("checkOutDay"+i));
					CheckBox dayCB = new CheckBox(By.id("checkOutDay"+i));
					if (value.contains(dayValue)){
						dayCB.check();
					}
				}
			}
		}
		
	}
	
	/**
	 * This method clicks on the cancel button in the check in details section
	 */
	public void cancelCheckInDetails() {
		WebElement parentElement = waitForElement(By.id("checkInDetailsContent"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement((By.id("checkInTime")), 40);
	}
	
	/**
	 * This method clicks on the update button of the check In Details section
	 */
	public void updateCheckInDetails() {
		Button updateButton = new Button("checkInDetailsEdit");
		updateButton.click();
		waitForInVisibilityOfElement(By.id("checkInTime"),40);
	}
	
	/**
	 * This method gets the count of all the rows from the check in details table
	 * @return int
	 */
	public int getcheckInDetailsRows() {
		WebElement parentElement = waitForElement(By.id("checkInDetailsContent"));
		List<WebElement> trList = getTableRowsListFromDiv(parentElement);
		return trList.size();
	}
	
	/**
	 * This method gets the value from the row
	 * @param rowNum
	 * @return WebElement
	 */
	public WebElement getcheckInDetailsRow(int rowNum) {
		WebElement parentElement = waitForElement(By.id("checkInDetailsContent"));
		return getTRElementFromTable(parentElement, rowNum);
	}
	

}
