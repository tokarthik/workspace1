package com.gta.travel.page.object.contract.search;

import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.gta.travel.page.object.common.TopLinksPage;
import com.mediaocean.qa.framework.selenium.ui.controls.ImageButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Pagination;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.utils.DateUtil;

/**
 * This class provides the methods for search results page
 * operations.
 * 
 * @author Karthikeyan
 *
 */
public class ContractSearchResultsPage extends GSPageBase{

	public ContractSearchResultsPage(){
		super(getDriver());
	}
	
	/**
	 * This method verify the search results based on the search values,
	 * @param map
	 * @return boolean
	 */
	public boolean verifyResults(Map<String, String> map){
		
		if (!isResultsFound()){
			return true;
		}
		boolean result = true;
		Pagination pagination = new Pagination(By.className("paginateButtons"));
		int totalPages = pagination.totalPages();
		totalPages = 1;
		for (int pageNum=1;pageNum<=totalPages;pageNum++){
			pagination = new Pagination(By.className("paginateButtons"));
			if (!compareResults(map)) {
				return false;
			}
			if (totalPages > pageNum){
				pagination.clickNext();
				waitForElement(By.className("currentStep"), Integer.toString(pageNum+1));
			}
		}
		return result;
	}

	/**
	 * Selects the first row from the results table.
	 * @return Home Page object
	 */
	public TopLinksPage selectFirstRowFromTheResults(){
		WebElement parentElement = waitForElement(By.id("results"));
		Table resultsTable = new Table(parentElement, 1);
		WebElement cellElement = resultsTable.getCell(1, "Provider");
		cellElement.click();
		Link displayLink = new Link(parentElement, "Display");
		displayLink.clickLink();
		
		waitForElement(By.className("menuView"));
		return PageFactory.initElements(getDriver(), TopLinksPage.class);
	}
	
	/**
	 * Returns the search results count.
	 * @return int
	 */
	public int getSearchResultsCount(){
		WebElement parentElement = waitForElement(By.id("results"));
		Table resultsTable = new Table(parentElement, 1);
		return resultsTable.getRowCount();
	}
	
	/**
	 * Selects record from the search results table for the given column name and value.
	 * @param colName
	 * @param colValue
	 * @return Home page object
	 */
	public TopLinksPage selectRecordFromSearchResults(String colName, String colValue){
		WebElement parentElement = waitForElement(By.id("results"));
		Table resultsTable = new Table(parentElement, 1);
		for (int i=1;i<resultsTable.getRowCount();i++){
			WebElement cellElement = resultsTable.getCell(i, colName);
			if (cellElement.getText().equalsIgnoreCase(colValue)){
				cellElement.click();
				Link displayLink = new Link(parentElement, "Display");
				displayLink.clickLink();
				
				waitForElement(By.className("menuView"));
				return PageFactory.initElements(getDriver(), TopLinksPage.class);
			}
		}
		return null;
	}
	
	/**
	 * Selects record from the search results table based on the given column names and values.
	 * @param inputMap
	 * @return Home page object
	 */
	public TopLinksPage selectRecordFromSearchResults(Map<String, String> inputMap){
		WebElement parentElement = waitForElement(By.id("results"));
		Table resultsTable = new Table(parentElement, 1);
		for (int i=1;i<resultsTable.getRowCount();i++){
			boolean isRecordFound = true;
			for (String key : inputMap.keySet()){
				String value = inputMap.get(key);
				WebElement cellElement = resultsTable.getCell(i, key);
				if (!cellElement.getText().equalsIgnoreCase(value)){
					isRecordFound = false;
					break;
				}
			}
			if (isRecordFound){
				resultsTable.clickOnRow(i);
//				waitForElement(By.linkText("Display"),5);
				Link displayLink = new Link(parentElement, "Display");
				displayLink.clickLink();
				
				waitForElement(By.className("menuView"));
				return PageFactory.initElements(getDriver(), TopLinksPage.class);
			}
		}
		return null;
	}

	private boolean compareResults(Map<String, String> map){
		WebElement parentElement = waitForElement(By.id("results"));
		Table resultsTable = new Table(parentElement, 1);
		for (int i=1;i<resultsTable.getRowCount();i++){
			for (String key : map.keySet()){
				String expectedValue = map.get(key);
				
				
				if (key.equalsIgnoreCase("Channel")){
					String actualValue = resultsTable.getCellData(i, key);
					if (!actualValue.contains(expectedValue) && !actualValue.equals("All") ){
						return false;
					}
				}else if (key.equalsIgnoreCase("Status")){
					WebElement actualElement = resultsTable.getCell(i, key);
					WebElement statusElement = getElement(actualElement,By.tagName("img"));
					ImageButton imgButton = new ImageButton(statusElement);
					String actualValue = imgButton.getTitle();
					if (!expectedValue.contains(actualValue)){
						return false;
					}
				}else if (key.equalsIgnoreCase("Travel Dates")){
					String actualValue = resultsTable.getCellData(i, key);
					compareTravelDates(expectedValue, actualValue);
				}else if (key.equalsIgnoreCase("Star Rating")){
					//Nothing to do
				}
				else{
					String actualValue = resultsTable.getCellData(i, key);
					if (!expectedValue.equalsIgnoreCase(actualValue)){
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private boolean compareTravelDates(String travelDates, String actualValue){
		if (actualValue.trim().equals("-")){
			return true;
		}
		String[] dateArr = travelDates.split("-");	
		String startDateStr = dateArr[0];
		String endDateStr = dateArr[1];
		
		Date startDate = DateUtil.convertStringToDate(startDateStr, "dd/MMM/yy");
		Date endDate = DateUtil.convertStringToDate(endDateStr, "dd/MMM/yy");
		
		String[] actualDates = actualValue.split("-");
		Date actualStratDate = null;
		Date actualEndDate = null;
		
		if (actualDates != null && actualDates.length == 2){
			actualStratDate = DateUtil.convertStringToDate(formatActualDate(actualDates[0].trim()), "dd/MMM/yy");
			actualEndDate = DateUtil.convertStringToDate(formatActualDate(actualDates[1].trim()), "dd/MMM/yy");
		}else if (actualDates != null && actualDates.length == 1){
			actualStratDate = DateUtil.convertStringToDate(formatActualDate(actualDates[0].trim()), "dd/MMM/yy");
		}
		
		if (actualStratDate != null && (actualStratDate.compareTo(startDate) < 0 || actualStratDate.compareTo(endDate) > 0)){
			return false;
		}
		if (actualEndDate != null && (actualEndDate.compareTo(endDate) > 0 || actualEndDate.compareTo(startDate) < 0)){
			return false;
		}
		
		return true;
	}
	
	private static String formatActualDate(String date){
		if (date != null && date.length() > 2){
			date = date.substring(0,2) + "/" + date.substring(2,5) + "/20" + date.substring(5);
		}
		return date;
	}

	/**
	 * Verify the validation error message
	 * @param errorMessage
	 * @return boolean
	 */
	public boolean verifyErrorMessage(String errorMessage){
		waitForText(errorMessage);
		if (!getDriver().getPageSource().contains(errorMessage)) 
			return false;
		return true;
	}
	
	/**
	 * This method checks if any results found or not.
	 * @return boolean
	 */
	public boolean isResultsFound(){
		WebElement parentEl = waitForElement(By.id("results"));
		try{
			waitForElement(parentEl, By.className("message"),1);
			return false;
		}catch(Exception e){
			return true;
		}
	}
}