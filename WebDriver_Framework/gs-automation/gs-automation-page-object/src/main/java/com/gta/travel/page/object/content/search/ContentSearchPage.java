package com.gta.travel.page.object.content.search;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class ContentSearchPage extends GSPageBase{
	
	private static final Map<String, String> channelMap = new HashMap<String, String>();
	private static final Map<String, String> statusMap = new HashMap<String, String>();
	
	public ContentSearchPage(){
		
		super(getDriver());
		
		channelMap.put("Consumer (B2C)", "C");
		channelMap.put("Trade (B2B)", "T");
		channelMap.put("Trade Consumer (B2B2C)", "B");
		
		statusMap.put("New", "N");
		statusMap.put("Mandatory", "M");
		statusMap.put("Images Missing", "IM");
		statusMap.put("Content Ready", "CR");
		statusMap.put("Inactive", "CR");
	}
	
	public ContentSearchResultsPage search(Map<String, String> map){
		
		String country = null;
		String city = null;
		if (map.containsKey("Country")){
			country = map.get("Country");
			selectValueFromAjaxList("country-name", country.substring(0,3), "status", "country-choices", country, country,0);
		}
		if (country != null && map.containsKey("City")){
			city = map.get("City");
			selectValueFromAjaxList("city-name", city.substring(0,3), "status", "city-choices", city, city,0);
		}else if (map.containsKey("City")){
			TextBox textBox = new TextBox(By.id("city-name"));
			textBox.setText(map.get("City"));
		}
		if (map.containsKey("Property Name")){
			TextBox propertyNameTextBox = new TextBox(By.id("name"));
			propertyNameTextBox.setText(map.get("Property Name"));
		}
		if (map.containsKey("Property Type")){
			ComboBox propTypeComboBox = new ComboBox(By.id("type"));
			propTypeComboBox.select(map.get("Property Type"));
		}
		if (map.containsKey("Provider")){
			String provider = map.get("Provider");
			String title = provider + "(" + city + ")";
			selectValueFromAjaxList("provider-name", provider.substring(0,3), "status", "provider-choices", provider, title,0);
		}
		
		Button searchButton = new Button("search");
		searchButton.click();
		
		if (map.containsKey("validation failure")){
			String value = map.get("validation failure");
			waitForElement(By.id(value),30);
		}else{
			waitForElement(By.id("results"));
			waitForElement(By.className("paginateButtons"),30);
		}
		
		return PageFactory.initElements(getDriver(), ContentSearchResultsPage.class);
	}
	
	private void selectValueFromAjaxList(String textBoxId, String text, 
			String dummyTextBoxId, String choiceListId, String valueToSelect, String title, int noOfTimes){
		
		TextBox textBox = new TextBox(By.id(textBoxId));
		textBox.setText(text);
		TextBox dummyTextBox = new TextBox(By.id(dummyTextBoxId));
		dummyTextBox.clickTab();
		WebElement choicesEl = null;
		while(true){
			choicesEl = getElementIfItIsPresent(By.id(choiceListId));
			if (choicesEl != null){
				break;
			}
		}
//		waitForElement(By.id(choiceListId), 30);
//		WebElement choicesEl = getElementIfItIsPresent(By.id(choiceListId));
		if (choicesEl != null){
			if (waitAndGetElementIfItIsPresent(By.linkText(title),5) != null){
				Link link = new Link(choicesEl, valueToSelect, title);
				link.clickLink();
			}else if (noOfTimes == 0){
				noOfTimes++;
				selectValueFromAjaxList(textBoxId,valueToSelect,dummyTextBoxId,choiceListId,valueToSelect,title, noOfTimes);
			}
		}
	}
	
	public void clearSearchFields(){
		
		TextBox countryNameTextBox = new TextBox(By.id("country-name"));
		countryNameTextBox.setText("");
		
		TextBox cityNameTextBox = new TextBox(By.id("city-name"));
		cityNameTextBox.setText("");

		TextBox propertyNameTextBox = new TextBox(By.id("name"));
		propertyNameTextBox.setText("");
	
		ComboBox propTypeComboBox = new ComboBox(By.id("type"));
		propTypeComboBox.select("");
		
		TextBox providerTextBox = new TextBox(By.id("provider-name"));
		providerTextBox.setText("");

	}
}
