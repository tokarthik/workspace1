package com.gta.travel.page.object.content.facilitiesandservices;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class LandMarkDistanceDetailsPage extends GSPageBase{

	GSPageBase gsPageBase = new GSPageBase(getDriver());
	public LandMarkDistanceDetailsPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the page factory object for the LandMark Distance Details Page
	 * @return static
	 */
	public static LandMarkDistanceDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  LandMarkDistanceDetailsPage.class);
	}
	
	/**
	 * This method returns the row count from the Land Mark Distance List Table
	 * @return integer
	 */
	public int getLandMarkDistanceDetailsRowCount() {
		Table tbl = new Table(By.id("landmarkDistanceListTable"));
		return tbl.getRowCount();
	}
	
	/**
	 * This method clicks on create Link of LandMarkDistanceDetails and waits 
	 * till the table is displayed
	 */
	public void clickCreateLandMarkDistanceDetails(){
			WebElement parentElement = waitForElement(By.id("landmarkDistanceContentMenuOptions"));
			Link createLink = new Link(parentElement, "Create");	
			createLink.clickLink();	
			waitForPageElementById("landmarkDistanceCreateCancel");
	}
	
	/**
	 * @param textBoxId
	 * @param text
	 * @param parentElementId
	 * @param dummyTextBoxId
	 * @param choiceListId
	 * @param valueToSelect
	 * @param title
	 * @param noOfTimes
	 */
	private void selectValueFromAjaxList(String textBoxId, String text, 
			String parentElementId, String dummyTextBoxId, String choiceListId, String valueToSelect, String title, int noOfTimes){
		
		TextBox textBox = new TextBox(By.id(textBoxId));
		textBox.setText(text);
		WebElement parentElement = waitForElement(By.id(parentElementId));
		TextBox dummyTextBox = new TextBox(parentElement,By.id(dummyTextBoxId));
		dummyTextBox.clickTab();
		WebElement choicesEl = null;
		while(true){
			choicesEl = getElementIfItIsPresent(By.id(choiceListId));
			if (choicesEl != null){
				break;
			}
		}
		if (textBox.getText().length() == 3){
			if (choicesEl != null){
				if (waitAndGetElementIfItIsPresent(By.linkText(title),5) != null){
					Link link = new Link(choicesEl, valueToSelect, title);
					link.clickLink();
				}else if (noOfTimes == 0){
					noOfTimes++;
					selectValueFromAjaxList(textBoxId,valueToSelect,parentElementId,dummyTextBoxId,choiceListId,valueToSelect,title, noOfTimes);
				}
			}
		}
	}
	
	/**
	 * @param textBoxId
	 * @param text
	 * @param parentElementId
	 * @param dummyTextBoxId
	 * @param choiceListId
	 * @param valueToSelect
	 * @param title
	 * @param noOfTimes
	 */
	private void selectValueFromAjaxList1(String textBoxId, String text, 
			String parentElementId, String dummyButtonId, String choiceListId, String valueToSelect, String title, int noOfTimes){
		
		TextBox textBox = new TextBox(By.id(textBoxId));
		textBox.setText(text);
		Button dummyButton = new Button(By.id(dummyButtonId));
		dummyButton.clickTab(By.id(dummyButtonId));
		WebElement choicesEl = null;
		while(true){
			choicesEl = getElementIfItIsPresent(By.id(choiceListId));
			if (choicesEl != null){
				break;
			}
		}
		if (textBox.getText().length() == 3){
			if (choicesEl != null){
				if (waitAndGetElementIfItIsPresent(By.linkText(title),5) != null){
					Link link = new Link(choicesEl, valueToSelect, title);
					link.clickLink();
				}else if (noOfTimes == 0){
					noOfTimes++;
					selectValueFromAjaxList1(textBoxId,valueToSelect,parentElementId,dummyButtonId,choiceListId,valueToSelect,title, noOfTimes);
				}
			}
		}
	}
	
	/**
	 * This method enters the values in the Land Mark Distance Details section
	 * based on the values passed through the input map
	 * @param inputMap
	 */
	public void enterLandMarkDistanceDetailsSection(Map<String, String> inputMap) {
		
		String airportCity = null;
		String stationCity = null;
		String metroCity = null;
		String exhibitionCity = null;
		String pointOfInterestCity = null;
		if (inputMap.containsKey("Airport")){
			TextBox airportTB = new TextBox(By.id("Alandmark-name"));
			airportTB.setText("");
			airportCity = inputMap.get("Airport");
			selectValueFromAjaxList("Alandmark-name", airportCity.substring(0,3), "lmd-distanceA", "distance", "Alandmark-choices", airportCity, airportCity,0);
		}
		if (inputMap.containsKey("LandMarkType")) {
			ComboBox stationCombo = new ComboBox(By.id("landmarkType"));
			stationCombo.select(inputMap.get("LandMarkType"));
		}
		if (inputMap.containsKey("Station")){
			TextBox stationTB = new TextBox(By.id("landmark-name"));
			stationTB.setText("");
			stationCity = inputMap.get("Station");	
			selectValueFromAjaxList("landmark-name", stationCity.substring(0,3), "lmd-distance", "distance", "landmark-choices", stationCity, stationCity,0);
		}
		
		if (inputMap.containsKey("Metro")){
			TextBox metroTB = new TextBox(By.id("landmark-name"));
			metroTB.setText("");
			metroCity = inputMap.get("Metro");
			selectValueFromAjaxList1("landmark-name", metroCity.substring(0,3), "landmarkDistanceCreateCancel", "landmarkDistanceCreateCancel", "landmark-choices", metroCity, metroCity,0);
		}
		if (inputMap.containsKey("Exhibition")){
			TextBox exhibitionTB = new TextBox(By.id("landmark-name"));
			exhibitionTB.setText("");
			exhibitionCity = inputMap.get("Exhibition");
			selectValueFromAjaxList("landmark-name", exhibitionCity.substring(0,3), "lmd-distance", "distance", "landmark-choices", exhibitionCity, exhibitionCity,0);
		}
		if (inputMap.containsKey("PointOfInterest")){
			TextBox pointOfInterestTB = new TextBox(By.id("landmark-name"));
			pointOfInterestTB.setText("");
			pointOfInterestCity = inputMap.get("PointOfInterest");
			selectValueFromAjaxList("landmark-name", pointOfInterestCity.substring(0,3), "lmd-distance", "distance", "landmark-choices", pointOfInterestCity, pointOfInterestCity,0);
		}		
		if (inputMap.containsKey("AirportDistance")){
			WebElement airportDistance = waitForElement(By.id("lmd-distanceA"));
			TextBox airportDistanceTB = new TextBox(airportDistance,By.id("distance"));
			airportDistanceTB.setText("");
			airportDistanceTB.setText(inputMap.get("AirportDistance"));
		}
		if (inputMap.containsKey("StationDistance")){
			TextBox stationDistanceTB = new TextBox(By.id("distance"));
			stationDistanceTB.setText("");
			stationDistanceTB.setText(inputMap.get("StationDistance"));
		}
		if (inputMap.containsKey("ExhibitionDistance")){
			TextBox exhibitionDistanceTB = new TextBox(By.id("distance"));
			exhibitionDistanceTB.setText("");
			exhibitionDistanceTB.setText(inputMap.get("ExhibitionDistance"));
		}
		if (inputMap.containsKey("PointOfInterestDistance")){
			TextBox pointOfInterestTB = new TextBox(By.id("distance"));
			pointOfInterestTB.setText("");
			pointOfInterestTB.setText(inputMap.get("PointOfInterestDistance"));
		}
		
	}
	
	/**
	 * This method enters the values in the Land Mark Distance Details section
	 * based on the values passed through the input map for the edit functionality
	 * @param inputMap
	 */
	public void enterEditLandMarkDistanceDetailsSection(Map<String, String> inputMap) {
		
		String airportCity = null;
		String metroCity = null;
		if (inputMap.containsKey("Airport")){
			TextBox airportTB = new TextBox(By.id("Alandmark-name"));
			airportTB.setText("");
			airportCity = inputMap.get("Airport");
			selectValueFromAjaxList("Alandmark-name", airportCity.substring(0,3), "lmd-distanceA", "distance", "Alandmark-choices", airportCity, airportCity,0);
		}
		if (inputMap.containsKey("Station")){
			TextBox stationTB = new TextBox(By.id("Slandmark-name"));
			stationTB.setText(inputMap.get("Station"));	
			stationTB.clickTab();
		}
		if (inputMap.containsKey("Metro")){
			TextBox metroTB = new TextBox(By.id("Mlandmark-name"));
			metroTB.setText("");			
			metroCity = inputMap.get("Metro");
			selectValueFromAjaxList1("Mlandmark-name", metroCity.substring(0,3), "landmarkDistanceEdit", "landmarkDistanceEdit", "Mlandmark-choices", metroCity, metroCity,0);
		}
		if (inputMap.containsKey("Exhibition")){
			TextBox exhibitionTB = new TextBox(By.id("Elandmark-name"));
			exhibitionTB.setText(inputMap.get("Exhibition"));
		}
		if (inputMap.containsKey("PointOfInterest")){
			TextBox pointOfInterestTB = new TextBox(By.id("Ilandmark-name"));
			pointOfInterestTB.setText(inputMap.get("PointOfInterest"));
		}		
		if (inputMap.containsKey("AirportDistance")){
			WebElement airportDistance = waitForElement(By.id("lmd-distanceA"));
			TextBox airportDistanceTB = new TextBox(airportDistance,By.id("distance"));
			airportDistanceTB.setText("");
			airportDistanceTB.setText(inputMap.get("AirportDistance"));
		}
		if (inputMap.containsKey("StationDistance")){
			WebElement stationDistance = waitForElement(By.id("lmd-distanceS"));
			TextBox stationDistanceTB = new TextBox(stationDistance,By.id("distance"));
			stationDistanceTB.setText("");
			stationDistanceTB.setText(inputMap.get("StationDistance"));
		}
		if (inputMap.containsKey("ExhibitionDistance")){
			WebElement exhibitionDistance = waitForElement(By.id("lmd-distanceE"));
			TextBox exhibitionDistanceTB = new TextBox(exhibitionDistance,By.id("distance"));
			exhibitionDistanceTB.setText("");
			exhibitionDistanceTB.setText(inputMap.get("ExhibitionDistance"));
		}
		if (inputMap.containsKey("PointOfInterestDistance")){
			WebElement pointOfInterestDistance = waitForElement(By.id("lmd-distanceI"));
			TextBox pointOfInterestTB = new TextBox(pointOfInterestDistance,By.id("distance"));
			pointOfInterestTB.setText("");
			pointOfInterestTB.setText(inputMap.get("PointOfInterestDistance"));
		}
		
	}
	
	/**
	 * This method clicks on add another button in the land mark distance details section
	 */
	public void addAnotherLandMarkDistanceDetails(){
		WebElement parentElement = waitForElement(By.id("landmarkDistance_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		int countBefore = getLandMarkDistanceDetailsRowCount();
		addAnotherButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("landmarkDistance_list"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	/**
	 * This method saves the land mark distance details
	 */
	public void saveLandMarkDistanceDetails() {
		WebElement parentElement = null;
		parentElement = waitForElement(By.id("landmarkDistanceTable"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getLandMarkDistanceDetailsRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("landmarkDistanceCreateCancel"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	/**
	 * This method updates the land mark distance details
	 */
	public void updateLandMarkDistanceDetails() {
		WebElement parentElement = null;
		parentElement = waitForElement(By.id("landmarkDistanceTable"));
		Button saveButton = new Button(parentElement, "Update");
		int countBefore = getLandMarkDistanceDetailsRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("landmarkDistanceCreateCancel"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	/**
	 * This method saves the land mark distance details for the existing table
	 */
	public void saveLandMarkDistanceDetailsForExistingTable() {
		WebElement parentElement = null;
		parentElement = waitForElement(By.id("landmarkDistance_create"),20);
		//parentElement = waitForElement(By.id("landmarkDistanceCreateForm"),20);
		Button saveButton = new Button(parentElement, "Save");
		sleep(2);
		int countBefore = getLandMarkDistanceDetailsRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("landmarkDistanceCreateCancel"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	/**
	 * This method selects the row based on the data specified
	 * @param inputMap
	 */
	public void selectLandMarkType(Map<String, String> inputMap){
		WebElement parentEl = waitForElement(By.id("landmarkDistanceListTable"));
		Table table = new Table(parentEl);
		for (int i=1;i<table.getRowCount();i++){
			boolean flag = true;
			for (String key : inputMap.keySet()){
				String value = inputMap.get(key);
				if (!value.equalsIgnoreCase(table.getCellData(i, key))){
					flag = false;
					break;
				}
			}
			if (flag){
				for (int j=0;j<10;j++){
					try{
						table.getCell(i, "Type").click();
						waitForElement(By.id("landmarkDistanceForm"),5);
						break;
					}catch(Exception e){
						sleep(1);
						continue;
					}
				}
				break;
			}
		}
	}
	
	/**
	 * This method verifies the record created/updated message after saving the details
	 * in the Land Mark Distance Details 
	 * @param parentLocator
	 * @param message
	 * @return boolean
	 */
	public boolean verifyMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator, 20);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	/**
	 * This method clicks on cancel button on the create Land Mark Details 
	 * Section Page
	 */
	public void clickCancel() {
		Button cancelButton = new Button(By.id("landmarkDistanceCreateCancel"));
		cancelButton.click();
		waitForInVisibilityOfElement(By.id("landmarkDistance_create"));
	}
	
	/**
	 * This method clicks on the refresh link in the LandMark Distance Details Section
	 * Use this if the row count is 1
	 */
	public void refreshMessage() {
		WebElement menuElement = waitForElement(By.id("landmarkDistanceContentMenuOptions"));
		Link optionLink = new Link(menuElement, "Refresh");
		optionLink.clickLink();
		WebElement parentElement = waitForElement(By.id("landmarkDistance_list"));
		waitForInVisibilityOfElement(parentElement, By.className("message"));	
	}
	

	/**
	 * This method clicks on the row specified by the row number
	 * @param rowNum
	 */
	public void clickRowByRowNum(int rowNum) {
		Table table = new Table(By.id("landmarkDistanceListTable"));
		table.clickOnRow(rowNum);
		waitForElement(By.id("landmarkDistanceContentMenuOptions"));
	}
	
	/**
	 * This method waits for the content to be displayed based on the option name
	 * @param optionName
	 */
	public void selectLandMarkDistanceDetailsOptions(String optionName){
		
		gsPageBase.chooseMenuOptionsFromSelectOption("landmarkDistanceContentMenuOptions", optionName);
		if (optionName.equalsIgnoreCase("Create")){
			int count = getLandMarkDistanceDetailsRowCount();
			waitForElement(By.id("landmarkDistance_create"));
			WebElement parentElement = waitForElement(By.id("landmarkDistance_create"));
			if (count == 0){
				waitForElement(parentElement, By.id("landmarkDistanceTable"));
			}else{
				waitForElement(parentElement, By.id("landmarkDistanceCreateForm"));
			}
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("landmarkDistanceEdit"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getLandMarkDistanceDetailsRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
		}
		if (optionName.equalsIgnoreCase("Delete")){
			sleep();
		}
	}
	
	/**
	 * This method verifies for the record updated message and will return true 
	 * or false based on the message
	 * @return boolean
	 */
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	/**
	 * This method clicks on the alert ok button for the delete Land Mark Details
	 */
	public void deleteLandMarkDetailsWithOK(){
		acceptAlert();
	}
	
	public boolean sortOnLandMarkType(){
		return sort(0, "String");
	}
	
	public boolean sortOnLandMarkDescription(){
		return sort(1, "String");
	}
	
	public boolean sortOnLandMarkDistance(){
		return sort(2, "String");
	}
	
	private boolean sort(int colNum, String objectType){

		boolean result = true;

		WebElement tableDivEl = waitForElement(By.id("landmarkDistanceListTable"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		tableDivEl = waitForElement(By.id("landmarkDistanceListTable"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			tableDivEl = waitForElement(By.id("landmarkDistanceListTable"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("order=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}
	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = tr.findElements(By.tagName("td"));
			if (i==1){
				currentValue = tdList.get(colNum).getText();
			}else{
				prevValue = currentValue;
				currentValue = tdList.get(colNum).getText();
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * @param expectedCount
	 * @return boolean
	 */
	public boolean verifyRowcount(int expectedCount){
		for (int i=0;i<10;i++){
			try{
				int rowCount = getLandMarkDistanceDetailsRowCount();
				if (expectedCount == rowCount){
					return true;
				}else{
					sleep(1);
					continue;
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
		
	}
	
	
	

}
