package com.gta.travel.page.object.contract.rateplans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class RatePlanCancellationSectionPage  extends GSPageBase{

	public RatePlanCancellationSectionPage(){
		super(getDriver());
	}
	
	public static RatePlanCancellationSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanCancellationSectionPage.class);
	}
	
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	public void resetData(){
		deleteAllCancellations();
	}
	
	private void deleteAllCancellations(){
		int count = getCancellationTableRowCount();
		while(count > 1){
			WebElement parentEl = waitForElement(By.id("cancellation_list"));
			Table table = new Table(parentEl, 1);
			table.getCell(1, "Rate Type").click();
			waitForElement(By.id("cancellation_tr"),20);
			selectCancellationOptions("Delete");
			deleteCancellationWithOK();
			verifyRowcount(count-1);
//			waitForTextToBePresentInElement(By.className("message"), "Record deleted", 20);
			selectCancellationOptions("Refresh");
			waitForInVisibilityOfElement(By.className("message"));
			count = getCancellationTableRowCount();
		}
	}
	
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	public boolean isCreateRecordSectionDisplayed(){
		return isElementPresent(By.id("cancellation_create"));
	}
	
	public int getCancellationTableRowCount(){
		WebElement parentEl = waitForElement(By.id("cancellation_list"));
		Table table = new Table(parentEl, 1);
		return table.getRowCount();
	}
	
    public void selectCancellation(Map<String, String> inputMap){
        sleep(2);
        WebElement parentEl = waitForElement(By.id("cancellation_list"));
        Table table = new Table(parentEl, 1);
        for (int i=1;i<table.getRowCount();i++){
            boolean flag = true;
            for (String key : inputMap.keySet()){
                String value = inputMap.get(key);
                if (!value.equalsIgnoreCase(table.getCellData(i, key))){
                    flag = false;
                    break;
                }
            }
            if (flag){
                for (int j=0;j<10;j++){
                    try{
                        parentEl = waitForElement(By.id("cancellation_list"));
                        table = new Table(parentEl, 1);
                        table.getCell(i, "Rate Type").click();
                        waitForElement(By.id("cancellation_div"),5);
                        break;
                    }catch(Exception e){
                        sleep(1);
                        continue;
                    }
                }
                break;
            }
        }
    }


	
	public void createCancellation(Map<String, String> inputMap){

		if (inputMap.containsKey("override base cancellation")){
			String value = inputMap.get("override base cancellation");
			RadioButton obcRB = null;
			if (value.equalsIgnoreCase("true")){
				waitForElement(By.id("ratePlanCancellation-create-overrideBaseCancellation-Y"));
				obcRB = new RadioButton(By.id("ratePlanCancellation-create-overrideBaseCancellation-Y"));
			}else{
				waitForElement(By.id("ratePlanCancellation-create-overrideBaseCancellation-N"));
				obcRB = new RadioButton(By.id("ratePlanCancellation-create-overrideBaseCancellation-N"));
			}
			obcRB.select();
		}

		if (inputMap.containsKey("days prior")){
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanCancellation-create-daysPrior"));
			daysPriorCB.select(inputMap.get("days prior"));
		}
		
		if (inputMap.containsKey("arrival time")){
			waitForElement(By.name("arrivalTime"),40);
			TextBox arrivalAtTB = new TextBox(By.name("arrivalTime"));
			arrivalAtTB.setText(inputMap.get("arrival time"));
		}
		
		if (inputMap.containsKey("percentage")){
			waitForElement(By.name("percentage"),40);
			TextBox percentageTB = new TextBox(By.name("percentage"));
			percentageTB.setText(inputMap.get("percentage"));
		}
		
		if (inputMap.containsKey("charge duration")){
			ComboBox chargeDurationCB = new ComboBox(By.id("ratePlanCancellation-create-chargeDuration"));
			chargeDurationCB.select(inputMap.get("charge duration"));
		}
		
		if (inputMap.containsKey("travel start day")){
			ComboBox startDayCB = new ComboBox(By.id("ratePlanCancellation-create-startDate_day"));
			startDayCB.select(inputMap.get("travel start day"));
		}
		
		if (inputMap.containsKey("travel start month")){
			ComboBox startMonthCB = new ComboBox(By.id("ratePlanCancellation-create-startDate_months"));
			startMonthCB.select(inputMap.get("travel start month"));
		}
		
		if (inputMap.containsKey("travel start year")){
			ComboBox startYearCB = new ComboBox(By.id("ratePlanCancellation-create-startDate_years"));
			startYearCB.select(inputMap.get("travel start year"));
		}
		
		if (inputMap.containsKey("travel end day")){
			ComboBox endDayCB = new ComboBox(By.id("ratePlanCancellation-create-endDate_day"));
			endDayCB.select(inputMap.get("travel end day"));
		}
		
		if (inputMap.containsKey("travel end month")){
			ComboBox endMonthCB = new ComboBox(By.id("ratePlanCancellation-create-endDate_months"));
			endMonthCB.select(inputMap.get("travel end month"));
		}
		
		if (inputMap.containsKey("travel end year")){
			ComboBox endYearCB = new ComboBox(By.id("ratePlanCancellation-create-endDate_years"));
			endYearCB.select(inputMap.get("travel end year"));
		}
		
//		if (inputMap.containsKey("all rates")){
//			RadioButton allRatesRB = new RadioButton(By.id("contractCancellation-create-rateType-S"));
//			allRatesRB.select();
//		}
		
//		if (inputMap.containsKey("peak rates")){
//			RadioButton peakRatesRB = new RadioButton(By.id("contractCancellation-create-rateType-P"));
//			peakRatesRB.select();
//		}
	}
	
	public void cancelCreateCancellation(){
		waitForElementToBeClickable(By.id("cancellationCreateCancel"),3);
		Button button = new Button(By.id("cancellationCreateCancel"));
		button.click();
		waitForInVisibilityOfElement(By.id("cancellation_create"));
//		sleep(1);
	}
	
	public void addAnotherCancellation(){
		WebElement parentElement = waitForElement(By.id("cancellation_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		int countBefore = getCancellationTableRowCount();
		addAnotherButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getCancellationTableRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("cancellation_list"));
		waitForElement(parentEl, By.className("message"),60);
//		sleep(1);
	}
	
	public void saveCancellation(){
		WebElement parentElement = waitForElement(By.id("cancellation_create"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getCancellationTableRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("cancellation_create"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getCancellationTableRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
//		sleep(1);
	}
	
	public void selectCancellationOptions(String optionName){
//		sleep(1);
		WebElement menuElement = waitForElement(By.id("cancellationContentMenuOptions"));
		waitForElement(menuElement, By.linkText("Select Option"));
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}

		waitForElement(By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create")){
			int count = getCancellationTableRowCount();
			waitForElement(By.id("cancellation_create"));
			WebElement parentElement = waitForElement(By.id("cancellation_create"));
			if (count == 0){
				waitForElement(parentElement, By.id("ratePlanCancellation-create-overrideBaseCancellation-Y"));
			}else{
				waitForElement(parentElement, By.id("cancellationCreateCancel"));
			}
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("cancellation_tr"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getCancellationTableRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
//			sleep(2);
//			waitForInVisibilityOfElement(By.id("cancellation_tr"));
//			waitForInVisibilityOfElement(By.id("cancellation_historySearch"));
		}
		if (optionName.equalsIgnoreCase("History")){
			WebElement parentEl = waitForElement(By.id("cancellation_historySearch"));
			waitForElement(parentEl, By.id("cancellationSearch"));
		}
//		sleep(1);
	}
	
	public boolean verifyRowcount(int expectedCount){
		for (int i=0;i<10;i++){
			try{
				int rowCount = getCancellationTableRowCount();
				if (expectedCount == rowCount){
					return true;
				}else{
					sleep(1);
					continue;
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
		
	}
	
	public void editCancellation(Map<String, String> inputMap){
		
		if (inputMap.containsKey("days prior")){
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanCancellation-edit-daysPrior"));
			daysPriorCB.select(inputMap.get("days prior"));
		}
		
		if (inputMap.containsKey("arrival time")){
			TextBox arrivalAtTB = new TextBox(By.id("ratePlanCancellation-edit-arrivalTime"));
			arrivalAtTB.setText(inputMap.get("arrival time"));
		}
		
		if (inputMap.containsKey("percentage")){
			TextBox percentageTB = new TextBox(By.id("ratePlanCancellation-edit-percentage"));
			percentageTB.setText(inputMap.get("percentage"));
		}
		
		if (inputMap.containsKey("charge duration")){
			ComboBox chargeDurationCB = new ComboBox(By.id("ratePlanCancellation-edit-chargeDuration"));
			chargeDurationCB.select(inputMap.get("charge duration"));
		}
		
		if (inputMap.containsKey("travel start day")){
			ComboBox startDayCB = new ComboBox(By.id("ratePlanCancellation-edit-startDate_day"));
			startDayCB.select(inputMap.get("travel start day"));
		}
		
		if (inputMap.containsKey("travel start month")){
			ComboBox startMonthCB = new ComboBox(By.id("ratePlanCancellation-edit-startDate_months"));
			startMonthCB.select(inputMap.get("travel start month"));
		}
		
		if (inputMap.containsKey("travel start year")){
			ComboBox startYearCB = new ComboBox(By.id("ratePlanCancellation-edit-startDate_years"));
			startYearCB.select(inputMap.get("travel start year"));
		}
		
		if (inputMap.containsKey("travel end day")){
			ComboBox endDayCB = new ComboBox(By.id("ratePlanCancellation-edit-endDate_day"));
			endDayCB.select(inputMap.get("travel end day"));
		}
		
		if (inputMap.containsKey("travel end month")){
			ComboBox endMonthCB = new ComboBox(By.id("ratePlanCancellation-edit-endDate_months"));
			endMonthCB.select(inputMap.get("travel end month"));
		}
		
		if (inputMap.containsKey("travel end year")){
			ComboBox endYearCB = new ComboBox(By.id("ratePlanCancellation-edit-endDate_years"));
			endYearCB.select(inputMap.get("travel end year"));
		}
		
//		if (inputMap.containsKey("all rates")){
//			RadioButton allRatesRB = new RadioButton(By.id("contractCancellation-edit-rateType-S"));
//			allRatesRB.select();
//		}
//		
//		if (inputMap.containsKey("peak rates")){
//			RadioButton peakRatesRB = new RadioButton(By.id("contractCancellation-edit-rateType-P"));
//			peakRatesRB.select();
//		}
	}
	
	public void cancelEditCancellation(){
		WebElement parentElement = waitForElement(By.id("cancellation_div"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement(waitForElement(By.id("cancellation_div")), By.id("ratePlanCancellation-edit-daysPrior"));
//		sleep(1);
	}
	
	public void updateCancellation(){
		WebElement parentElement = waitForElement(By.id("cancellation_div"));
		Button cancelButton = new Button(parentElement, "Update");
		cancelButton.click();
		waitForInVisibilityOfElement(By.id("cancellation_tr"),40);
		WebElement parentEl = waitForElement(By.id("cancellation_list"));
		waitForElement(parentEl, By.className("message"),40);
//		sleep(1);
	}	
	
	public void deleteCancellationWithOK(){
		acceptAlert();
//		sleep(2);
	}
	public void deleteCancellationWithCancel(){
		dismissAlert();
	}
	
	public boolean sortOrderOnRateType(){
		return sort(0, "String");
	}
	
	public boolean sortOrderOnTravelDates(){
		return sort(1, "Date");
	}
	
	public boolean sortOrderOnDayTime(){
		return sort(2, "String");
	}
	
	public boolean sortOrderOnChargeCondition(){
		return sort(3, "String");
	}
	
	private boolean sort(int colNum, String objectType){

		boolean result = true;

		WebElement tableDivEl = waitForElement(By.id("cancellation_list"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		tableDivEl = waitForElement(By.id("cancellation_list"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			tableDivEl = waitForElement(By.id("cancellation_list"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = getElements(tr,By.tagName("td"));
			if (i==1){
				currentValue = tdList.get(colNum).getText();
			}else{
				prevValue = currentValue;
				currentValue = tdList.get(colNum).getText();
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}
			}
		}
		return result;
	}	
	private boolean compareSortOrderOnDateObjects(String prevValue, String currentValue, String sortOrder){
		String prevDateStr = prevValue.split("-")[0];
		String currentDateStr = currentValue.split("-")[0];
		
		String prevDateStrFormat = prevDateStr.substring(0,2) + "-" + prevDateStr.substring(2,5) + "-" + prevDateStr.substring(5);
		String currentDateStrFormat = currentDateStr.substring(0,2) + "-" + currentDateStr.substring(2,5) + "-" + currentDateStr.substring(5);
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
		try {
			Date prevDateObj = format.parse(prevDateStrFormat);
			Date currentDateObj = format.parse(currentDateStrFormat);
			
			if (sortOrder.equals("asc")){
				if (prevDateObj.compareTo(currentDateObj)<0){
					return false;
				}
			}else{
				if (currentDateObj.compareTo(prevDateObj)<0){
					return false;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}	
	public boolean collapseCancellation(){
		WebElement parentEl = waitForElement(By.id("cancellationListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("cancellationArrow"));
		if (element.getAttribute("src").contains("ArrowRight")){
			return true;
		}
		return false;
	}
	public boolean expandCancellation(){
		WebElement parentEl = waitForElement(By.id("cancellationListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("cancellationArrow"));
		if (element.getAttribute("src").contains("ArrowDown")){
			return true;
		}
		return false;
	}		

}
