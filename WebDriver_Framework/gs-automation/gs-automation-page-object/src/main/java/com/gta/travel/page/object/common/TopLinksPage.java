package com.gta.travel.page.object.common;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;

public class TopLinksPage extends GSPageBase{

	public TopLinksPage(){
		super(getDriver());
	}

	public static TopLinksPage getInstance(){
		return PageFactory.initElements(getDriver(), TopLinksPage.class);
	}
	
	/**
     * This method clicks on facilities and services tab
     */
    public void clickFacilitiesAndServices() {
        waitAndClick(By.linkText("Facilities & Services"));
        waitForPageElementById("categoryDetailsShowSection");
    }
    
    /**
	 * This method clicks on the Room Types tab
	 */
	public void clickRoomTypesTab() {
		waitAndClick(By.linkText("Room Types"));
		waitForPageElementById("roomListSection");
	}
	
	
	public boolean selectManageRateTab(){
		sleep(2);
		waitForElement(By.linkText("Manage Rates and Availability"));
		Link link = new Link(By.linkText("Manage Rates and Availability"));
		link.clickLink();
		if (waitForElement(By.id("manageRatesAndAvailabilitySectionHeader")) != null){
			return true;
		}
		return false;
	}

	public boolean selectRatePlansTab(){
		sleep(2);
		waitForElement(By.linkText("Rate Plans"));
		Link link = new Link(By.linkText("Rate Plans"));
		link.clickLink();
		if (waitForElement(By.id("ratePlanListSection")) != null){
			return true;
		}
		return false;
	}

	public boolean selectInventoryTab(){
		sleep(2);
		waitForElement(By.linkText("Inventory"));
		Link link = new Link(By.linkText("Inventory"));
		link.clickLink();
		if (waitForElement(By.id("allotmentListSection1")) != null){
			return true;
		}
		return false;
	}

	public boolean selectExtrasAndSupplementsTab(){
		sleep(2);
		waitForElement(By.linkText("Extras and Supplements"));
		Link link = new Link(By.linkText("Extras and Supplements"));
		link.clickLink();
		if (waitForElement(By.id("roomSupplementShowSection")) != null){
			return true;
		}
		return false;
	}
	
}
