package com.gta.travel.page.object.contract.rateplans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class RatePlanValidMarketsSectionPage extends GSPageBase{

	public RatePlanValidMarketsSectionPage(){
		super(getDriver());
	}
	
	public static RatePlanValidMarketsSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanValidMarketsSectionPage.class);
	}
	
	public void resetData(){
		List<WebElement> list = waitForElements(By.id("worldWide"));
		if (list != null && list.size() == 0){
			Map<String, String> inputMap = new HashMap<String, String>();
			inputMap.put("world wide", "Yes");
			clickEditValidMarkets();
			editValidMarkets(inputMap);
			saveEditValidMarkets("No");
			clickEditValidMarkets();
		}
	}
	
	public void clickEditValidMarkets(){
		WebElement menuElement = waitForElement(By.id("marketContentMenuOptions"),5);
		waitForElement(menuElement,By.id("marketContentMenuOptions"),5);
		if (Link.isLinkVisible(menuElement,"Edit")){
			Link slectOptionLink = new Link(menuElement,"Edit");
			slectOptionLink.clickLink();
			waitForElements(By.id("worldWide"));
			WebElement parentEL = waitForElement(By.id("marketContent"));
			waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Cancel']"));
		}
	}
	
	public boolean verifyRecordUpdatedMessage(String message1, String message2){
		WebElement parentEl = waitForElement(By.id("marketContent"),5);
		waitForElement(parentEl, By.className("message"));
		String text = getElement(parentEl, By.className("message")).getText();
		if (text.contains(message1) && text.contains(message2)){
			return true;
		}
		return false;
	}

	public boolean verifyData(Map<String, String> inputMap){
		
		try{
			if (inputMap.containsKey("world wide")){
				String value = inputMap.get("world wide");
				RadioButton workdWideRB = null;
				waitForElements(By.id("worldWide"));
				if (value.equalsIgnoreCase("Yes")){
					workdWideRB = new RadioButton(By.id("worldWide"), "true");	
				}else{
					workdWideRB = new RadioButton(By.id("worldWide"), "false");
				}
				if (!workdWideRB.isSelected()){
					return false;
				}
			}
			if (inputMap.containsKey("cb zones")){
				String value = inputMap.get("cb zones");
				String[] cbZonesList = value.split(",");
				for (int i=0;i<cbZonesList.length;i++){
					waitForElement(By.id("zone-"+cbZonesList[i]));
					CheckBox cbZoneCB = new CheckBox(By.id("zone-"+cbZonesList[i]));
					if (!cbZoneCB.isChecked()){
						return false;
					}
				}
			}
			if (inputMap.containsKey("market name")){
				WebElement trEl = getTRElementFromTable(waitForElement(By.id("marketContent")), 1);
				String text = trEl.findElement(By.className("value")).getText();
				String value = inputMap.get("market name");
				if (!text.equalsIgnoreCase(value)){
					return false;
				}
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean verifyUpdatedData(Map<String, String> inputMap){
		for (int i=0;i<2;i++){
			if (verifyData(inputMap)){
				return true;
			}else{
				continue;
			}
		}
		return false;
	}
	
	private boolean uncheckZones(){
		try{
			waitForElement(By.name("zone"));
			CheckBox.uncheckAll(By.name("zone"));
			if (BROWSER.equals("IE")){
				List<WebElement> zonesList = waitForElements(By.name("zone"));
				for (WebElement zone : zonesList){
					((JavascriptExecutor) getDriver()).executeScript("return arguments[0].fireEvent('onclick');", zone);
				}
			}
				
		}catch(Exception e){
			return false;
		}
		return true;
	}
	public void uncheckAllZones(){
		for (int i=0;i<2;i++){
			if (uncheckZones()){
				return;
			}else{
				sleep(2);
			}
		}
	}
	
	public int getZonesCount(){
		WebElement parentEl = waitForElement(By.id("marketTable"));
		waitForElement(By.className("open"));
		List<WebElement> list = getElements(parentEl, By.className("open"));
		return list.size();
	}
	
	public int getZonesTickBoxCount(){
		WebElement parentEl = waitForElement(By.id("marketTable"));
		waitForElement(By.className("cbZone"));
		List<WebElement> list = getElements(parentEl, By.className("cbZone"));
		return list.size();
	}

	public void editValidMarkets(Map<String, String> inputMap){
		for (int i=0;i<2;i++){
			if (editValidMarketsData(inputMap)){
				return;
			}
		}
		throw new RuntimeException("Failed to enter edit data");
	}
	
	private boolean editValidMarketsData(Map<String, String> inputMap){
		try{
			if (inputMap.containsKey("world wide")){
				String value = inputMap.get("world wide");
				RadioButton workdWideRB = null;
				waitForElements(By.id("worldWide"));
				if (value.equalsIgnoreCase("Yes")){
					workdWideRB = new RadioButton(By.id("worldWide"), "true");	
					workdWideRB.select();
				}else{
					workdWideRB = new RadioButton(By.id("worldWide"), "false");
					workdWideRB.select();
				}
			}
	
			if (inputMap.containsKey("market name")){
				waitForElement(By.id("marketName"));
				TextBox marketNameTB = new TextBox(By.id("marketName"));
				marketNameTB.setText(inputMap.get("market name"));
			}
	
			if (inputMap.containsKey("zone openers")){
				String value = inputMap.get("zone openers");
				String[] openersList = value.split(",");
				for (int i=0;i<openersList.length;i++){
					waitForElement(By.id("zoneOpener-"+openersList[i])).click();
					WebElement parentEl = waitForElement(By.id("zone-"+openersList[i]));
					waitForElement(parentEl, By.className("close"));
				}
			}
			if (inputMap.containsKey("cb zones")){
				String value = inputMap.get("cb zones");
				String[] cbZonesList = value.split(",");
				for (int i=0;i<cbZonesList.length;i++){
					waitForElement(By.id("cbZone-"+cbZonesList[i]));
					CheckBox cbZoneCB = new CheckBox(By.id("cbZone-"+cbZonesList[i]));
					cbZoneCB.check();
					if (BROWSER.equals("IE")){
						((JavascriptExecutor) getDriver()).executeScript("return document.getElementById('"+ "cbZone-"+cbZonesList[i] +"').fireEvent('onclick');");
					}
				}
			}
			if (inputMap.containsKey("countries")){
				String value = inputMap.get("countries");
				String[] countriesList = value.split(",");
				for (int i=0;i<countriesList.length;i++){
					waitForElement(By.id("country."+countriesList[i]));
					CheckBox countryCB = new CheckBox(By.id("country."+countriesList[i]));
					countryCB.check();
					
					
				}
			}
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	public void cancelEditValidMarkets(){
		WebElement parentEL = waitForElement(By.id("marketContent"));
		waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Cancel']"));
		Button cancelButton = new Button(parentEL,"Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Cancel']"),10);
	}
	
	public void saveEditValidMarkets(String wwValue){
		WebElement parentEL = waitForElement(By.id("marketContent"));
		if (wwValue.equalsIgnoreCase("Yes")){
			waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Save']"));
		}else{
			waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Update']"));
		}
		Button saveButton = new Button(parentEL,"Save");
		saveButton.click();

		if (wwValue.equalsIgnoreCase("Yes")){
			waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Save']"),10);
		}else{
			waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Update']"),10);
		}
		WebElement parentEl = waitForElement(By.id("marketContent"),5);
		waitForElement(parentEl, By.className("message"));
	}
}
