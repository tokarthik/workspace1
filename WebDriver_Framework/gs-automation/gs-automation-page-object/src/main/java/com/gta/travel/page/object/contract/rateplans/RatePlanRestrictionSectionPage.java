package com.gta.travel.page.object.contract.rateplans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;

public class RatePlanRestrictionSectionPage  extends GSPageBase{

	public RatePlanRestrictionSectionPage(){
		super(getDriver());
	}
	
	public static RatePlanRestrictionSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanRestrictionSectionPage.class);
	}
	
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	public void resetData(){
		deleteAllRestrictions();
	}
	
	private void deleteAllRestrictions(){
		int count = getRestrictionTableRowCount();
		while(count > 1){
			WebElement parentEl = waitForElement(By.id("restriction_list"));
			Table table = new Table(parentEl, 1);
			table.getCell(1, "Restriction Type").click();
			waitForElement(By.id("restriction_tr"),20);
			selectRestrictionOptions("Delete");
			deleteRestrictionWithOK();
			verifyRowcount(count-1);
			selectRestrictionOptions("Refresh");
			waitForInVisibilityOfElement(By.className("message"));
			count = getRestrictionTableRowCount();
		}
	}
	
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	public boolean isCreateRecordSectionDisplayed(){
		return isElementPresent(By.id("restriction_create"));
	}
	
	public int getRestrictionTableRowCount(){
		WebElement parentEl = waitForElement(By.id("restriction_list"));
		Table table = new Table(parentEl, 1);
		return table.getRowCount();
	}
	
    public void selectRestriction(Map<String, String> inputMap){
        sleep(2);
        WebElement parentEl = waitForElement(By.id("restriction_list"));
        Table table = new Table(parentEl, 1);
        for (int i=1;i<table.getRowCount();i++){
            boolean flag = true;
            for (String key : inputMap.keySet()){
                String value = inputMap.get(key);
                if (!value.equalsIgnoreCase(table.getCellData(i, key))){
                    flag = false;
                    break;
                }
            }
            if (flag){
                for (int j=0;j<10;j++){
                    try{
                        parentEl = waitForElement(By.id("restriction_list"));
                        table = new Table(parentEl, 1);
                        table.getCell(i, "Restriction Type").click();
                        waitForElement(By.id("restriction_tr"),5);
                        break;
                    }catch(Exception e){
                        sleep(1);
                        continue;
                    }
                }
                break;
            }
        }
    }

	
	public void createRestriction(Map<String, String> inputMap){

		if (inputMap.containsKey("override base restriction")){
			String value = inputMap.get("override base restriction");
			RadioButton obcRB = null;
			if (value.equalsIgnoreCase("true")){
				waitForElement(By.id("ratePlanRestriction-create-overrideBaseRestriction-Y"));
				obcRB = new RadioButton(By.id("ratePlanRestriction-create-overrideBaseRestriction-Y"));
			}else{
				waitForElement(By.id("ratePlanRestriction-create-overrideBaseRestriction-N"));
				obcRB = new RadioButton(By.id("ratePlanRestriction-create-overrideBaseRestriction-N"));
			}
			obcRB.select();
		}

		if (inputMap.containsKey("restriction type")){
			waitForElement(By.id("ratePlanRestriction-create-restrictionType"));
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanRestriction-create-restrictionType"));
			String value = inputMap.get("restriction type");
			daysPriorCB.select(value);
			if (!waitForElement(By.id("ratePlanRestriction-create-restrictionType"), 2).getText().equalsIgnoreCase(value)){
				sleepms(500l);
				daysPriorCB.select(value);
			}
		}
		
		if (inputMap.containsKey("applicable from")){
			waitForElement(By.id("ratePlanRestriction-create-daysPrior"));
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanRestriction-create-daysPrior"));
			daysPriorCB.select(inputMap.get("applicable from"));
		}
		
		
		if (inputMap.containsKey("travel start day")){
			ComboBox startDayCB = new ComboBox(By.id("ratePlanRestriction-create-startDate_day"));
			startDayCB.select(inputMap.get("travel start day"));
		}
		
		if (inputMap.containsKey("travel start month")){
			ComboBox startMonthCB = new ComboBox(By.id("ratePlanRestriction-create-startDate_months"));
			startMonthCB.select(inputMap.get("travel start month"));
		}
		
		if (inputMap.containsKey("travel start year")){
			ComboBox startYearCB = new ComboBox(By.id("ratePlanRestriction-create-startDate_years"));
			startYearCB.select(inputMap.get("travel start year"));
		}
		
		if (inputMap.containsKey("travel end day")){
			ComboBox endDayCB = new ComboBox(By.id("ratePlanRestriction-create-endDate_day"));
			endDayCB.select(inputMap.get("travel end day"));
		}
		
		if (inputMap.containsKey("travel end month")){
			ComboBox endMonthCB = new ComboBox(By.id("ratePlanRestriction-create-endDate_months"));
			endMonthCB.select(inputMap.get("travel end month"));
		}
		
		if (inputMap.containsKey("travel end year")){
			ComboBox endYearCB = new ComboBox(By.id("ratePlanRestriction-create-endDate_years"));
			endYearCB.select(inputMap.get("travel end year"));
		}
	}
	
	public void cancelCreateRestriction(){
		waitForElementToBeClickable(By.id("restrictionCreateCancel"),3);
		Button button = new Button(By.id("restrictionCreateCancel"));
		button.click();
		waitForInVisibilityOfElement(By.id("restriction_create"));
	}
	
	public void addAnotherRestriction(){
		WebElement parentElement = waitForElement(By.id("restriction_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		int countBefore = getRestrictionTableRowCount();
		addAnotherButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getRestrictionTableRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("restriction_list"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	public void saveRestriction(){
		WebElement parentElement = waitForElement(By.id("restriction_create"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getRestrictionTableRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("restriction_create"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getRestrictionTableRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	public void selectRestrictionOptions(String optionName){
		WebElement menuElement = waitForElement(By.id("restrictionContentMenuOptions"));
		waitForElement(menuElement, By.linkText("Select Option"));
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}

		waitForElement(By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create")){
			int count = getRestrictionTableRowCount();
			waitForElement(By.id("restriction_create"));
			WebElement parentElement = waitForElement(By.id("restriction_create"));
			if (count == 0){
				waitForElement(parentElement, By.id("ratePlanRestriction-create-overrideBaseRestriction-Y"));
			}else{
				waitForElement(parentElement, By.id("restrictionCreateCancel"));
			}
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("restriction_tr"));
			waitForElement(By.id("restrictionEdit"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getRestrictionTableRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
		}
		if (optionName.equalsIgnoreCase("History")){
			WebElement parentEl = waitForElement(By.id("restriction_history"));
			waitForElement(parentEl, By.id("restrictionSearch"));
		}
	}
	
	public boolean verifyRowcount(int expectedCount){
		for (int i=0;i<10;i++){
			try{
				int rowCount = getRestrictionTableRowCount();
				if (expectedCount == rowCount){
					return true;
				}else{
					sleep(1);
					continue;
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
		
	}
	
	public void editRestriction(Map<String, String> inputMap){
		
		if (inputMap.containsKey("override base restriction")){
			String value = inputMap.get("override base restriction");
			RadioButton obcRB = null;
			if (value.equalsIgnoreCase("true")){
				waitForElement(By.id("ratePlanRestriction-create-overrideBaseRestriction-Y"));
				obcRB = new RadioButton(By.id("ratePlanRestriction-create-overrideBaseRestriction-Y"));
			}else{
				waitForElement(By.id("ratePlanRestriction-create-overrideBaseRestriction-N"));
				obcRB = new RadioButton(By.id("ratePlanRestriction-create-overrideBaseRestriction-N"));
			}
			obcRB.select();
		}

		if (inputMap.containsKey("restriction type")){
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanRestriction-edit-restrictionType"));
			daysPriorCB.select(inputMap.get("restriction type"));
		}
		
		if (inputMap.containsKey("applicable form")){
			ComboBox daysPriorCB = new ComboBox(By.id("ratePlanRestriction-edit-daysPrior"));
			daysPriorCB.select(inputMap.get("applicable form"));
		}
		
		
		if (inputMap.containsKey("travel start day")){
			ComboBox startDayCB = new ComboBox(By.id("ratePlanRestriction-edit-startDate_day"));
			startDayCB.select(inputMap.get("travel start day"));
		}
		
		if (inputMap.containsKey("travel start month")){
			ComboBox startMonthCB = new ComboBox(By.id("ratePlanRestriction-edit-startDate_months"));
			startMonthCB.select(inputMap.get("travel start month"));
		}
		
		if (inputMap.containsKey("travel start year")){
			ComboBox startYearCB = new ComboBox(By.id("ratePlanRestriction-edit-startDate_years"));
			startYearCB.select(inputMap.get("travel start year"));
		}
		
		if (inputMap.containsKey("travel end day")){
			ComboBox endDayCB = new ComboBox(By.id("ratePlanRestriction-edit-endDate_day"));
			endDayCB.select(inputMap.get("travel end day"));
		}
		
		if (inputMap.containsKey("travel end month")){
			ComboBox endMonthCB = new ComboBox(By.id("ratePlanRestriction-edit-endDate_months"));
			endMonthCB.select(inputMap.get("travel end month"));
		}
		
		if (inputMap.containsKey("travel end year")){
			ComboBox endYearCB = new ComboBox(By.id("ratePlanRestriction-edit-endDate_years"));
			endYearCB.select(inputMap.get("travel end year"));
		}
	}
	
	public void cancelEditRestriction(){
		WebElement parentElement = waitForElement(By.id("restriction_div"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement(waitForElement(By.id("restriction_div")), By.id("ratePlanRestriction-edit-restrictionType"));
	}
	
	public void updateRestriction(){
		WebElement parentElement = waitForElement(By.id("restriction_div"));
		Button updateButton = new Button(parentElement, "Update");
		updateButton.click();
		waitForInVisibilityOfElement(By.id("restriction_tr"),40);
		WebElement parentEl = waitForElement(By.id("restriction_list"));
		waitForElement(parentEl, By.className("message"),40);
	}	
	
	public void deleteRestrictionWithOK(){
		acceptAlert();
	}
	public void deleteRestrictionWithCancel(){
		dismissAlert();
	}
	
	public boolean sortOrderOnRestrictionType(){
		return sort(0, "String");
	}
	
	public boolean sortOrderOnDayTime(){
		return sort(2, "String");
	}
	
	private boolean sort(int colNum, String objectType){

		boolean result = true;

		WebElement tableDivEl = waitForElement(By.id("restriction_list"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		tableDivEl = waitForElement(By.id("restriction_list"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			tableDivEl = waitForElement(By.id("restriction_list"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = getElements(tr,By.tagName("td"));
			if (i==1){
				currentValue = tdList.get(colNum).getText();
				currentValue = currentValue == null ? "" : currentValue;
			}else{
				prevValue = currentValue;
				currentValue = tdList.get(colNum).getText();
				currentValue = currentValue == null ? "" : currentValue;
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}
			}
		}
		return result;
	}	
	private boolean compareSortOrderOnDateObjects(String prevValue, String currentValue, String sortOrder){
		String prevDateStr = prevValue.split("-")[0];
		String currentDateStr = currentValue.split("-")[0];
		
		String prevDateStrFormat = prevDateStr.substring(0,2) + "-" + prevDateStr.substring(2,5) + "-" + prevDateStr.substring(5);
		String currentDateStrFormat = currentDateStr.substring(0,2) + "-" + currentDateStr.substring(2,5) + "-" + currentDateStr.substring(5);
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
		try {
			Date prevDateObj = format.parse(prevDateStrFormat);
			Date currentDateObj = format.parse(currentDateStrFormat);
			
			if (sortOrder.equals("asc")){
				if (prevDateObj.compareTo(currentDateObj)<0){
					return false;
				}
			}else{
				if (currentDateObj.compareTo(prevDateObj)<0){
					return false;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}	
	public boolean collapseRestriction(){
		WebElement parentEl = waitForElement(By.id("restrictionListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("restrictionArrow"));
		if (element.getAttribute("src").contains("ArrowRight")){
			return true;
		}
		return false;
	}
	public boolean expandRestriction(){
		WebElement parentEl = waitForElement(By.id("restrictionListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("restrictionArrow"));
		if (element.getAttribute("src").contains("ArrowDown")){
			return true;
		}
		return false;
	}		

}
