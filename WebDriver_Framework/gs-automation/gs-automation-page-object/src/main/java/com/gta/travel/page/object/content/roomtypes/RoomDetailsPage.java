package com.gta.travel.page.object.content.roomtypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

/**
 * This class provides the methods for Room Details Section
 * 
 * @author Suvarchala Vege
 *
 */
public class RoomDetailsPage extends GSPageBase{

	GSPageBase gsPageBase = new GSPageBase(getDriver());
	String[] roomTypesArr = {"Twin","Double","Single","Triple","Quad","Twin for Sole Use","Double for Sole Use"};
	
	/**
	 * Default Constructor
	 */
	public RoomDetailsPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the page factory object for the Room Details Page
	 * @return static
	 */
	public static RoomDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  RoomDetailsPage.class);
	}
	
	/**
	 * This method clicks on create Link of RoomDetails and waits 
	 * till the table is displayed
	 */
	public void clickCreateRoomDetails(){
			WebElement parentElement = waitForElement(By.id("roomContentMenuOptions"));
			Link createLink = new Link(parentElement, "Create");	
			createLink.clickLink();	
			waitForPageElementById("roomCreateCancel");
	}
	
	/**
	 * This method clicks on refresh Link of Room Details and waits 
	 * till the table is displayed
	 */
	public void clickRefreshRoomDetails(){
			WebElement parentElement = waitForElement(By.id("roomContentMenuOptions"));
			Link refreshLink = new Link(parentElement, "Refresh");	
			refreshLink.clickLink();	
			sleep(2);
	}
	
	/**
	 * This method selects the option from the select option dropdown
	 * @param optionName
	 */
	public void selectRoomDetailsOptions(String optionName){
//		sleep(1);
		WebElement menuElement = waitForElement(By.id("roomContentMenuOptions"));
		waitForElement(menuElement, By.linkText("Select Option"));
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}

		waitForElement(By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create")){
//			int count = getRoomDetailsRowCount();
			waitForElement(By.id("roomCategory"));
//			WebElement parentElement = waitForElement(By.id("roomCategory"));
//			if (count == 0){
//				waitForElement(parentElement, By.id("ratePlanCancellation-create-overrideBaseCancellation-Y"));
//			}else{
//				waitForElement(parentElement, By.id("cancellationCreateCancel"));
//			}
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("roomEdit"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getRoomDetailsRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
//			sleep(2);
//			waitForInVisibilityOfElement(By.id("cancellation_tr"));
//			waitForInVisibilityOfElement(By.id("cancellation_historySearch"));
		}
		if (optionName.equalsIgnoreCase("Copy")){
			waitForElement(By.id("roomCreateCancel"));
		}
//		sleep(1);
	}
	
	private int getRoomDetailsRowCount() {
		WebElement parentElement = waitForElement(By.id("roomContent"));
		Table tbl = new Table(parentElement, 1);
		return tbl.getRowCount();
	}
	
	/**
	 * This method enter the values in the Room Details Section
	 * @param inputMap
	 */
	public void enterRoomDetails(Map<String, String> inputMap) {
		
		if (inputMap.containsKey("RoomCategory")){
			ComboBox roomCategoryCombo = new ComboBox(By.id("roomCategory"));
			roomCategoryCombo.select(inputMap.get("RoomCategory"));
		}		
		if (inputMap.containsKey("RoomType")){
			ComboBox roomTypeCombo = new ComboBox(By.id("roomType"));
			roomTypeCombo.select(inputMap.get("RoomType"));
		}		
		if (inputMap.containsKey("RoomView")){
			ComboBox roomViewCombo = new ComboBox(By.id("roomView"));
			roomViewCombo.select(inputMap.get("RoomView"));
		}
		if (inputMap.containsKey("RoomCode")){
			TextBox roomCodeTB = new TextBox(By.id("code"));
			roomCodeTB.setText("");
			roomCodeTB.setText(inputMap.get("RoomCode"));
		}
		if (inputMap.containsKey("NumberOfBeds")){
			TextBox numberOfBedsTB = new TextBox(By.id("beds"));
			numberOfBedsTB.setText("");
			numberOfBedsTB.setText(inputMap.get("NumberOfBeds"));
		}
		if (inputMap.containsKey("MaximumOccupancy")){
			TextBox maximumOccupancyTB = new TextBox(By.id("maxOccupancy"));
			maximumOccupancyTB.setText("");
			maximumOccupancyTB.setText(inputMap.get("MaximumOccupancy"));
					
		}
		if (inputMap.containsKey("RateBasis")){
			TextBox rateBasisTB = new TextBox(By.id("rateBasis"));
			rateBasisTB.setText("");
			rateBasisTB.setText(inputMap.get("RateBasis"));
			
		}
		if (inputMap.containsKey("NumberOfExtraBedsPossible")){
			TextBox extraBedTB = new TextBox(By.id("extraBed"));
			extraBedTB.setText("");
			extraBedTB.setText(inputMap.get("NumberOfExtraBedsPossible"));
			
		}
		if (inputMap.containsKey("ExistingBed")){
			waitForVisibilityOfElement(By.name("existingBed"), 20);
			String value = inputMap.get("ExistingBed");
			if (value.equalsIgnoreCase("true")){
				RadioButton existingBedRB = new RadioButton(By.name("existingBed"), "true");
				if (!existingBedRB.isSelected())
					existingBedRB.select();
			}
			else {
				RadioButton existingBedRB = new RadioButton(By.name("existingBed"), "false");
				if (!existingBedRB.isSelected())
					existingBedRB.select();
			}
		}
		if (inputMap.containsKey("NumberOfBabyCots")){
			TextBox numberOfBabyCotsTB = new TextBox(By.id("cots"));
			numberOfBabyCotsTB.setText("");
			numberOfBabyCotsTB.setText(inputMap.get("NumberOfBabyCots"));
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("Air Conditioned", "FEATURES-48");
		map.put("CD Player", "FEATURES-60");
		map.put("DVD", "FEATURES-59");
		map.put("Hairdryer", "FEATURES-51");
		map.put("In-house Film", "FEATURES-64");
		map.put("Internet", "FEATURES-61");
		map.put("Ironing", "FEATURES-56");
		map.put("Kitchenette", "FEATURES-53");
		map.put("Laundry", "FEATURES-57");
		map.put("Microwave", "FEATURES-54");
		map.put("Mini Bar", "FEATURES-50");
		map.put("Non-Smoking", "FEATURES-58");
		map.put("Safe", "FEATURES-49");
		map.put("Satellite TV", "FEATURES-63");
		map.put("Smoking", "FEATURES-52");
		map.put("Tea & Coffee Making Facilities", "FEATURES-82");
		map.put("Television", "FEATURES-62");
		map.put("Washing Machine", "FEATURES-55");
		
		if (inputMap.containsKey("RoomFacilities")){
			sleep(2);
			waitForVisibilityOfElement(By.name("FEATURES"), 20);
			CheckBox.uncheckAll(By.name("FEATURES"));
			sleep(2);
			waitForVisibilityOfElement(By.id("FEATURES-48"), 20);
			
			String roomFacilitiesStr = inputMap.get("RoomFacilities");
			String[] roomFacilitesArr = null;
			if (roomFacilitiesStr.contains(",")) {
				roomFacilitesArr = roomFacilitiesStr.split(",");
			}
			
			for (int j=0;j<=roomFacilitesArr.length-1;j++) {
				if (roomFacilitesArr[j].equals("Air Conditioned")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Air Conditioned")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("CD Player")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("CD Player")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("DVD")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("CD Player")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Hairdryer")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Hairdryer")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("In-house Film")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("In-house Film")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Internet")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Internet")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Ironing")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Ironing")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Kitchenette")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Kitchenette")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Laundry")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Laundry")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Microwave")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Microwave")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Mini Bar")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Mini Bar")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Non-Smoking")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Non-Smoking")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Safe")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Safe")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Satellite TV")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Satellite TV")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Smoking")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Smoking")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Tea & Coffee Making Facilities")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Tea & Coffee Making Facilities")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Television")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Television")));
					featuresCB.check();
				}
				if (roomFacilitesArr[j].equals("Washing Machine")){
					CheckBox featuresCB = new CheckBox(By.id(map.get("Washing Machine")));
					featuresCB.check();
				}
			}
		
		}
		if (inputMap.containsKey("RoomTypes")){
			String[] roomTypesArr = null;
			sleep(2);
			waitForVisibilityOfElement(By.id("twinBed"), 20);
			CheckBox.uncheckAll(By.id("twinBed"));
			CheckBox.uncheckAll(By.id("doubleBed"));
			CheckBox.uncheckAll(By.id("singleBed"));
			CheckBox.uncheckAll(By.id("tripleBed"));
			CheckBox.uncheckAll(By.id("quadBed"));
			CheckBox.uncheckAll(By.id("twinSoleUse"));
			CheckBox.uncheckAll(By.id("doubleSoleUse"));
			sleep(2);
			waitForVisibilityOfElement(By.id("twinBed"), 20);
			String roomTypes = inputMap.get("RoomTypes");
			if (roomTypes.contains(",")) {
				roomTypesArr = roomTypes.split(",");
			}
			for (int i=0;i<roomTypesArr.length;i++) {
				waitForElement(By.id("twinBed"));
				if (roomTypesArr[i].trim().equals("Twin")) {
					CheckBox twinBedCB = new CheckBox(By.id("twinBed"));
					twinBedCB.check();
				}
				if (roomTypesArr[i].trim().equals("Double")) { 
					CheckBox doubleBedCB = new CheckBox(By.id("doubleBed"));
					doubleBedCB.check();
				}
				if (roomTypesArr[i].trim().equals("Single")) {
					CheckBox singleBedCB = new CheckBox(By.id("singleBed"));
					singleBedCB.check();
				}
				if (roomTypesArr[i].trim().equals("Triple")) {
					CheckBox tripleBedCB = new CheckBox(By.id("tripleBed"));
					tripleBedCB.check();
				}
				if (roomTypesArr[i].trim().equals("Quad")) {
					CheckBox quadBedCB = new CheckBox(By.id("quadBed"));
					quadBedCB.check();
				}
				if (roomTypesArr[i].trim().equals("Twin for Sole Use")) {
					CheckBox twinSoleUseCB = new CheckBox(By.id("twinSoleUse")); 
					twinSoleUseCB.check();
				}
				if (roomTypesArr[i].trim().equals("Double for Sole Use")) {
					CheckBox doubleSoleUseCB = new CheckBox(By.id("doubleSoleUse"));
					doubleSoleUseCB.check();
				}
				
			}
		}
		if (inputMap.containsKey("BathShower")){
			String bathShower = inputMap.get("BathShower");
			sleep(2);
			for (int i=1;i<=3;i++) {
				waitForVisibilityOfElement(By.name("bath"+i), 20);
				CheckBox.uncheckAll(By.name("bath"+i));
			}
			if (bathShower.contains(",")) {
				String[] bathShowerArr = bathShower.split(",");
				waitForElement(By.name("bath1"));
				for (int k=0;k<bathShowerArr.length-1;k++) {
					if (bathShowerArr[k].trim().equals("Bathtub with overhead shower")) {
						CheckBox bath1CB = new CheckBox(By.name("bath1"));
						bath1CB.check();
					}
					if (bathShowerArr[k].trim().equals("Shower cubicle")) {
						CheckBox bath2CB = new CheckBox(By.name("bath2"));
						bath2CB.check();
					}
					if (bathShowerArr[k].trim().equals("Shared Facilities")) {
						CheckBox bath3CB = new CheckBox(By.name("bath3"));
						bath3CB.check();
					}
				}
			}
			else {

				if (bathShower.trim().equals("Bathtub with overhead shower")) {
					CheckBox bath1CB = new CheckBox(By.name("bath1"));
					bath1CB.check();
				}
				else if (bathShower.trim().equals("Shower cubicle")) {
					CheckBox bath2CB = new CheckBox(By.name("bath2"));
					bath2CB.check();
				}
				else if (bathShower.trim().equals("Shared Facilities")) {
					CheckBox bath3CB = new CheckBox(By.name("bath3"));
					bath3CB.check();
				}

			}
		}
		
		if (inputMap.containsKey("AllRooms")){
			waitForVisibilityOfElement(By.name("allBathrooms"), 20);
			String value = inputMap.get("AllRooms");
			if (value.equalsIgnoreCase("true")){
				RadioButton bathRoomsRB = new RadioButton(By.name("allBathrooms"), "true");
				if (!bathRoomsRB.isSelected())
					bathRoomsRB.select();
			}
			else {
				RadioButton bathRoomsRB = new RadioButton(By.name("allBathrooms"), "false");
				if (!bathRoomsRB.isSelected())
					bathRoomsRB.select();
			}
		}
		
	}
	
	/**
	 * This method clicks on add another button in the room details section
	 */
	public void addAnotherRoomDetails(){
		WebElement parentElement = waitForElement(By.id("room_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		int countBefore = getRoomDetailsRowCount();
		addAnotherButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getRoomDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("roomContent"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	/**
	 * This method clicks on save button in the room details
	 */
	public void saveRoomDetails() {
		WebElement parentElement = null;
		parentElement = waitForElement(By.id("room_create"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getRoomDetailsRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("roomCreateCancel"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getRoomDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	/**
	 * This method clicks on save button to verify the error messages
	 */
	public void saveErrorRoomDetails() {
		WebElement parentElement = null;
		parentElement = waitForElement(By.id("room_create"));
		Button saveButton = new Button(parentElement, "Save");
		saveButton.click();
		waitForVisibilityOfElement(By.id("roomCategory-errors"),20);
		sleep(2);
	}
	
	/**
	 * This method gets the validation message as a string
	 */
	public String getValidationMessage() {
		WebElement errorElement = waitForElement(By.id("roomCategory-errors")) ;
		return errorElement.getText();
	}
	
	/**
	 * This method clicks on copy button in the room details section
	 */
	public void copyRoomDetails(){
		WebElement parentElement = waitForElement(By.id("room_create"));
		Button copyButton = new Button(parentElement, "Copy");
		int countBefore = getRoomDetailsRowCount();
		copyButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getRoomDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("roomContent"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	/**
	 * This method clicks on the edit link of the room details
	 */
	public void clickEditRoomDetails() {
		WebElement parentElement = waitForElement(By.id("roomContentMenuOptions"));
		Link editLink = new Link(parentElement, "Edit");
		editLink.clickLink();	
		waitForVisibilityOfElement((By.name("bath3")), 40);
	}
	
	/**
	 * This method clicks on the update button of the room details section
	 */
	public void updateRoomDetails() {
		Button updateButton = new Button("roomEdit");
		updateButton.click();
		WebElement parentEl = waitForElement(By.id("roomContent"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	/**
	 * This method sorts on the Room Type section
	 * @return boolean
	 */
	public boolean sortOnRoomType(){
		return sort(0, "String");
	}
	
	/**
	 * This method sorts on the Room Code section
	 * @return boolean
	 */
	public boolean sortOnRoomCode(){
		return sort(1, "String");
	}
	
	
	private boolean sort(int colNum, String objectType){

		boolean result = true;
		WebElement tableDivEl = waitForElement(By.id("roomContent"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		tableDivEl = waitForElement(By.id("roomContent"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			tableDivEl = waitForElement(By.id("roomContent"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("order=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}
	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = tr.findElements(By.tagName("td"));
			if (i==1){
				currentValue = tdList.get(colNum).getText();
			}else{
				prevValue = currentValue;
				currentValue = tdList.get(colNum).getText();
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * This method clicks on cancel button on the create Room Details page
	 */
	public void clickCancel() {
		Button cancelButton = new Button(By.id("roomCreateCancel"));
		cancelButton.click();
		waitForInVisibilityOfElement(By.id("roomContentMenuOptions"));
	}
	
	/**
	 * This method clicks on cancel button on the Edit Room Details page
	 */
	public void clickCancelInEdit() {
		WebElement parentElement = waitForElement(By.id("room_div"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement(By.id("roomContentMenuOptions"));
	}
	
	
	
	/**
	 * This method deletes all the Room details
	 */
	public void resetData(){
		deleteAllRooms();
	}
	
	private void deleteAllRooms(){
		int count = getRoomDetailsRowCount();
		while(count > 1){
			WebElement parentEl = waitForElement(By.id("roomContent"));
			Table table = new Table(parentEl, 1);
			table.getCell(1, "Room Type").click();
			waitForElement(By.id("room_div"),20);
			selectRoomDetailsOptions("Delete");
			deleteRoomDetailsWithOK();
			verifyRowcount(count-1);
			clickRefreshRoomDetails();
			waitForInVisibilityOfElement(By.className("message"));
			count = getRoomDetailsRowCount();
		}
	}
	
	/**
	 * This method clicks on the ok button for the pop up display
	 */
	public void deleteRoomDetailsWithOK(){
		acceptAlert();
//		sleep(2);
	}
	
	/**
	 * This method clicks on the cancel button of the pop up display
	 */
	public void deleteRoomDetailsWithCancel(){
		dismissAlert();
	}
	
	/**
	 * This method verifies the row count with the expected value
	 * @param expectedCount
	 * @return boolean
	 */
	public boolean verifyRowcount(int expectedCount){
		for (int i=0;i<10;i++){
			try{
				int rowCount = getRoomDetailsRowCount();
				if (expectedCount == rowCount){
					return true;
				}else{
					sleep(1);
					continue;
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
		
	}
	
	/**
	 * @param parentLocator
	 * @param message
	 * @return
	 */
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	/**
	 * This method verifies for the record updated message and will return true 
	 * or false based on the message
	 * @return boolean
	 */
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	/**
	 * @param inputMap
	 */
	public void selectRoom(Map<String, String> inputMap){
        sleep(2);
        WebElement parentEl = waitForElement(By.id("roomContent"));
        Table table = new Table(parentEl, 1);
        for (int i=1;i<table.getRowCount();i++){
            boolean flag = true;
            for (String key : inputMap.keySet()){
                String value = inputMap.get(key);
                if (!value.equalsIgnoreCase(table.getCellData(i, key))){
                    flag = false;
                    break;
                }
            }
            if (flag){
                for (int j=0;j<10;j++){
                    try{
                        parentEl = waitForElement(By.id("roomContent"));
                        table = new Table(parentEl, 1);
                        table.getCell(i, "Room Type").click();
                        waitForElement(By.id("room_div"),5);
                        break;
                    }catch(Exception e){
                        sleep(1);
                        continue;
                    }
                }
                break;
            }
        }
    }
	
}


