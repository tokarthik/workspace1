package com.gta.travel.page.object.facilitiesandservices;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class LandMarkDistanceDetailsPage extends GSPageBase{

	GSPageBase gsPageBase = new GSPageBase(getDriver());
	public LandMarkDistanceDetailsPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the page factory object for the LandMark Distance Details Page
	 * @return static
	 */
	public static LandMarkDistanceDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  LandMarkDistanceDetailsPage.class);
	}
	
	/**
	 * This method returns the row count from the Land Mark Distance List Table
	 * @return integer
	 */
	public int getLandMarkDistanceDetailsRowCount() {
		Table tbl = new Table(By.id("landmarkDistanceListTable"));
		return tbl.getRowCount();
	}
	
	/**
	 * This method clicks on create Link of LandMarkDistanceDetails and waits 
	 * till the table is displayed
	 */
	public void clickCreateLandMarkDistanceDetails(){
			WebElement parentElement = waitForElement(By.id("landmarkDistanceContentMenuOptions"));
			Link createLink = new Link(parentElement, "Create");	
			createLink.clickLink();	
			waitForPageElementById("landmarkDistanceCreateCancel");
	}
	
	/**
	 * This method enters the values in the Land Mark Distance Details section
	 * based on the values passed through the input map
	 * @param inputMap
	 */
	public void enterLandMarkDistanceDetailsSection(Map<String, String> inputMap) {
		
		if (inputMap.containsKey("Airport")){
			TextBox airportTB = new TextBox(By.id("Alandmark-name"));
			airportTB.setText("");
			airportTB.setText(inputMap.get("Airport"));
		}
		if (inputMap.containsKey("Station")){
			TextBox stationTB = new TextBox(By.id("Slandmark-name"));
			stationTB.setText("");
			stationTB.setText(inputMap.get("Station"));
		}
		if (inputMap.containsKey("Metro")){
			TextBox metroTB = new TextBox(By.id("Mlandmark-name"));
			metroTB.setText("");
			metroTB.setText(inputMap.get("Metro"));
		}
		if (inputMap.containsKey("Exhibition")){
			TextBox exhibitionTB = new TextBox(By.id("Elandmark-name"));
			exhibitionTB.setText("");
			exhibitionTB.setText(inputMap.get("Exhibition"));
		}
		if (inputMap.containsKey("PointOfInterest")){
			TextBox pointOfInterestTB = new TextBox(By.id("Ilandmark-name"));
			pointOfInterestTB.setText("");
			pointOfInterestTB.setText(inputMap.get("PointOfInterest"));
		}		
		if (inputMap.containsKey("AirportDistance")){
			WebElement airportDistance = waitForElement(By.id("lmd-distanceA"));
			TextBox airportDistanceTB = new TextBox(airportDistance,By.id("distance"));
			airportDistanceTB.setText("");
			airportDistanceTB.setText(inputMap.get("AirportDistance"));
		}
		if (inputMap.containsKey("StationDistance")){
			WebElement stationDistance = waitForElement(By.id("lmd-distanceS"));
			TextBox stationDistanceTB = new TextBox(stationDistance,By.id("distance"));
			stationDistanceTB.setText("");
			stationDistanceTB.setText(inputMap.get("StationDistance"));
		}
		if (inputMap.containsKey("ExhibitionDistance")){
			WebElement exhibitionDistance = waitForElement(By.id("lmd-distanceE"));
			TextBox exhibitionDistanceTB = new TextBox(exhibitionDistance,By.id("distance"));
			exhibitionDistanceTB.setText("");
			exhibitionDistanceTB.setText(inputMap.get("ExhibitionDistance"));
		}
		if (inputMap.containsKey("PointOfInterestDistance")){
			WebElement pointOfInterestDistance = waitForElement(By.id("lmd-distanceI"));
			TextBox pointOfInterestTB = new TextBox(pointOfInterestDistance,By.id("distance"));
			pointOfInterestTB.setText("");
			pointOfInterestTB.setText(inputMap.get("PointOfInterestDistance"));
		}
		
	}
	
	/**
	 * This method clicks on add another button in the land mark distance details section
	 */
	public void addAnotherLandMarkDistanceDetails(){
		WebElement parentElement = waitForElement(By.id("landmarkDistance_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		int countBefore = getLandMarkDistanceDetailsRowCount();
		addAnotherButton.click();
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
		WebElement parentEl = waitForElement(By.id("landmarkDistance_list"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	/**
	 * This method saves the land mark distance details
	 */
	public void saveLandMarkDistanceDetails() {
		WebElement parentElement = waitForElement(By.id("landmarkDistanceTable"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getLandMarkDistanceDetailsRowCount();
		saveButton.click();
		waitForInVisibilityOfElement(By.id("landmarkDistanceCreateCancel"));
		for (int i=0;i<3;i++){
			try{
				int countAfter = getLandMarkDistanceDetailsRowCount();
				if (countBefore+1 == countAfter){
					break;
				}else{
					sleepms(200l);
				}
			}catch(Exception e){
				sleepms(200l);
				continue;
			}
		}
	}
	
	/**
	 * This method verifies the record created/updated message after saving the details
	 * in the Land Mark Distance Details 
	 * @param parentLocator
	 * @param message
	 * @return boolean
	 */
	public boolean verifyMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	/**
	 * This method clicks on the refresh link in the LandMark Distance Details Section
	 * Use this if the row count is 1
	 */
	public void refreshMessage() {
		WebElement menuElement = waitForElement(By.id("landmarkDistanceContentMenuOptions"));
		Link optionLink = new Link(menuElement, "Refresh");
		optionLink.clickLink();
		WebElement parentElement = waitForElement(By.id("landmarkDistance_list"));
		waitForInVisibilityOfElement(parentElement, By.className("message"));	
	}
	

	/**
	 * This method clicks on the row specified by the row number
	 * @param rowNum
	 */
	public void clickRowByRowNum(int rowNum) {
		Table table = new Table(By.id("landmarkDistanceListTable"));
		table.clickOnRow(rowNum);
		waitForElement(By.id("landmarkDistanceContentMenuOptions"));
	}
	
	/**
	 * This method waits for the content to be displayed based on the option name
	 * @param optionName
	 */
	public void selectLandMarkDistanceDetailsOptions(String optionName){
		
		gsPageBase.chooseMenuOptionsFromSelectOption("landmarkDistanceContentMenuOptions", optionName);
		if (optionName.equalsIgnoreCase("Create")){
			int count = getLandMarkDistanceDetailsRowCount();
			waitForElement(By.id("landmarkDistance_create"));
			WebElement parentElement = waitForElement(By.id("landmarkDistance_create"));
			if (count == 0){
				waitForElement(parentElement, By.id("landmarkDistanceTable"));
			}else{
				waitForElement(parentElement, By.id("landmarkDistanceCreateForm"));
			}
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("landmarkDistanceEdit"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getLandMarkDistanceDetailsRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
		}
		if (optionName.equalsIgnoreCase("Delete")){
			sleep();
		}
	}
	

	

	
	

}
