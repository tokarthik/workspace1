package com.gta.travel.page.object.contract.inventory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;
import com.mediaocean.qa.framework.utils.DateUtil;

public class InventoryGeneratorSectionPage extends GSPageBase{

	public String[] dayArr = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun","All"};

	public InventoryGeneratorSectionPage(){
		super(getDriver());
	}
	
	public static InventoryGeneratorSectionPage getInstance(){
		return PageFactory.initElements(getDriver(), InventoryGeneratorSectionPage.class);
	}

	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		sleep(1);
		for (int k=0;k<2;k++){
			try{
				WebElement parentEl = waitForElement(parentLocator);
				waitForElement(parentEl, By.className("message"),40);
				for (int i=0;i<2;i++){
					if (getElement(parentEl, By.className("message")).getText().contains(message)){
						return true;
					}else{
						sleepms(500l);
						continue;
					}
				}
			}catch(Exception e){
				sleep(2);
				continue;
			}
		}
		return false;
	}
	
	public boolean deleteAllInventories(){
		Date startDate = new Date();
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		int maxYear = 2023 - DateUtil.getYear(new Date());
		Date futureDate = DateUtil.addYears(new Date(), maxYear);
		String endDay = "31";
		String endMonth = "Dec";
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		selectInventoryOptions("Delete");
		enterDeleteInventoryDates(map);
		deleteInventory();
		deleteDeleteInventory();
		return waitForInventoryProcessing();
	}
	
	public void createInventory(Map<String, String> inputMap){
		
		if (inputMap.containsKey("inventory type")){
			String invTypeValue = inputMap.get("inventory type");
			waitForElement(By.id("inventorytype"));
			ComboBox invTypeCB = new ComboBox(By.id("inventorytype"));
			invTypeCB.select(invTypeValue);
			executeIEFireEventJS("inventorytype", "onchange");
			
			if (invTypeValue.equalsIgnoreCase("freesale")){
				waitForInVisibilityOfElement(By.id("inventoryreleaseDays"));
			}else {
				waitForElement(By.id("inventoryreleaseDays"));
			}
		}

		if (inputMap.containsKey("start day")){
			ComboBox startDayCB = new ComboBox(By.id("inventory-fromDateDatePicker_day"));
			startDayCB.select(inputMap.get("start day"));
		}
		
		if (inputMap.containsKey("start month")){
			ComboBox startMonthCB = new ComboBox(By.id("inventory-fromDateDatePicker_months"));
			startMonthCB.select(inputMap.get("start month"));
		}
		
		if (inputMap.containsKey("start year")){
			ComboBox startYearCB = new ComboBox(By.id("inventory-fromDateDatePicker_years"));
			startYearCB.select(inputMap.get("start year"));
		}
		
		if (inputMap.containsKey("end day")){
			ComboBox endDayCB = new ComboBox(By.id("inventory-toDateDatePicker_day"));
			endDayCB.select(inputMap.get("end day"));
		}
		
		if (inputMap.containsKey("end month")){
			ComboBox endMonthCB = new ComboBox(By.id("inventory-toDateDatePicker_months"));
			endMonthCB.select(inputMap.get("end month"));
		}
		
		if (inputMap.containsKey("end year")){
			ComboBox endYearCB = new ComboBox(By.id("inventory-toDateDatePicker_years"));
			endYearCB.select(inputMap.get("end year"));
		}	
		
		if (inputMap.containsKey("days")){
			String value = inputMap.get("days");
			if (value.equalsIgnoreCase("All")){
				CheckBox allCB = new CheckBox(By.id("daysTickAll"));
				if (!allCB.isChecked()){
					allCB.check();
					executeIEFireEventJS("daysTickAll", "onclick");
				}
			}else{
				CheckBox allCB = new CheckBox(By.id("daysTickAll"));
				if (allCB.isChecked()){
					allCB.uncheck();
					executeIEFireEventJS("daysTickAll", "onclick");
				}
				for (int i=0;i<dayArr.length-1;i++){
					String dayValue = dayArr[i];
					waitForElement(By.id("days"+i));
					CheckBox dayCB = new CheckBox(By.id("days"+i));
					if (value.contains(dayValue)){
						dayCB.check();
					}
				}
			}
		}	
		
		if (inputMap.containsKey("release by")){
			String releaseByValue = inputMap.get("release by");
			if (releaseByValue.equalsIgnoreCase("days prior")){
				RadioButton releaseByRB = new RadioButton(By.name("releaseBy"), "days");
				releaseByRB.select();
			}else{
				RadioButton releaseByRB = new RadioButton(By.name("releaseBy"), "date");
				releaseByRB.select();
			}
		}
		
		if (inputMap.containsKey("release days")){
			TextBox releaseDaysTB = new TextBox(By.id("inventoryreleaseDays"));
			releaseDaysTB.setText(inputMap.get("release days"));
		}
		
		if (inputMap.containsKey("release date day")){
			ComboBox releaseDateDayCB = new ComboBox(By.id("inventory-releaseDatePicker_day"));
			releaseDateDayCB.select(inputMap.get("release date day"));
		}
		
		if (inputMap.containsKey("release date month")){
			ComboBox releaseDateMonthCB = new ComboBox(By.id("inventory-releaseDatePicker_months"));
			releaseDateMonthCB.select(inputMap.get("release date month"));
		}
		
		if (inputMap.containsKey("release date year")){
			ComboBox releaseDateYearCB = new ComboBox(By.id("inventory-releaseDatePicker_years"));
			releaseDateYearCB.select(inputMap.get("release date year"));
		}
		
		
		if (inputMap.containsKey("room types")){
			String[] roomTypes = inputMap.get("room types").split(",");
			for (int i=0;i<roomTypes.length;i++){
				String roomType = roomTypes[i];
				List<WebElement> rowList = getTableRowsListFromDiv(waitForElement(By.id("inventoryDetail")));
				for (int j=0;j<rowList.size();j++){
					List<WebElement> tdList = waitForElements(rowList.get(j), By.tagName("td"),5);
					for (WebElement td : tdList){
						if (td.getText().trim().equalsIgnoreCase(roomType.trim())){
							if (inputMap.get("inventory type").equalsIgnoreCase("freesale")){
								CheckBox makeFreesaleCB = new CheckBox(By.id("inventoryLine["+ (j-2) +"].freesale"));
								makeFreesaleCB.check();
							}else{
								TextBox noOfRoomsTB = new TextBox(By.id("inventoryLine["+ (j-2) + "].quantity"));
								noOfRoomsTB.setText(inputMap.get(roomType));
							}
						}
					}
				}
			}
		}
	}	
	
//	public int getInventoryTableRowCount(){
//		WebElement parentEl = waitForElement(By.id("inventoryContent"));
//		Table table = new Table(parentEl, 1);
//		return table.getRowCount();
//	}
	
	
	public boolean cancelCreateInventory(){
		waitForElementToBeClickable(By.id("inventoryCreateCancel"),3);
		Button button = new Button(By.id("inventoryCreateCancel"));
		button.click();
		return waitForInVisibilityOfElement(By.id("inventory_create"));
	}	
	
	public void addAnotherInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button addAnotherButton = new Button(parentElement, "Add Another");
		addAnotherButton.click();
		WebElement parentEl = waitForElement(By.id("inventory_create"));
		waitForElement(parentEl, By.className("message"),60);
	}

	public void copyInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Copy");
		button.click();
		WebElement parentEl = waitForElement(By.id("inventory_create"));
		waitForElement(parentEl, By.className("message"),60);
	}

	public void saveInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Save");
		button.click();
		WebElement parentEl = waitForElement(By.id("inventory_create"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	public void cancelSaveOrDeleteInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Cancel");
		button.click();
		waitForElement(By.id("inventoryCreateCancel"));
	}
	
	public void saveSaveInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Save");
		button.click();
		try{
			WebElement parentEl = waitForElement(By.id("inventory_create"));
			waitForElement(parentEl, By.className("message"),60);
		}catch(Exception e){
			waitForTextToBePresentInElement(By.className("message"), 
					"An inventory generation instruction has been created. The instruction will be processed shortly.", 20);
		}
	}

	public void saveAddAnotherOrCopyInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Save");
		button.click();
		
		try{
			waitForElement(By.id("inventoryCreateCancel"));
			WebElement parentEl = waitForElement(By.id("inventory_create"));
			waitForElement(parentEl, By.className("message"),60);
		}catch(Exception e){
			waitForTextToBePresentInElement(By.className("message"), 
					"An inventory generation instruction has been created. The instruction will be processed shortly.", 20);
		}
	}
	
	public void selectInventoryOptions(String optionName){
		WebElement menuElement = waitForElement(By.id("inventoryContentMenuOptions"));
		waitForElement(menuElement, By.linkText("Select Option"));
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}

		waitForElement(By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create")){
			waitForElement(By.id("inventory_create"));
			WebElement parentElement = waitForElement(By.id("inventory_create"));
			waitForElement(parentElement, By.id("inventoryCreateCancel"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(2);
		}
		if (optionName.equalsIgnoreCase("Delete")){
			waitForElement(By.id("inventory-toDateDatePicker_years"));
			waitForElement(By.id("inventoryCreateCancel"));
		}
	}
	
	public void selectInventoryOptionsForExternalUser(String optionName){
		WebElement menuElement = waitForElement(By.id("inventoryContentMenuOptions"));
		waitForElement(menuElement, By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create")){
			waitForElement(By.id("inventory_create"));
			WebElement parentElement = waitForElement(By.id("inventory_create"));
			waitForElement(parentElement, By.id("inventoryCreateCancel"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(2);
		}
	}

	public void enterDeleteInventoryDates(Map<String, String> inputMap){
		if (inputMap.containsKey("start day")){
			ComboBox startDayCB = new ComboBox(By.id("inventory-fromDateDatePicker_day"));
			startDayCB.select(inputMap.get("start day"));
		}
		
		if (inputMap.containsKey("start month")){
			ComboBox startMonthCB = new ComboBox(By.id("inventory-fromDateDatePicker_months"));
			startMonthCB.select(inputMap.get("start month"));
		}
		
		if (inputMap.containsKey("start year")){
			ComboBox startYearCB = new ComboBox(By.id("inventory-fromDateDatePicker_years"));
			startYearCB.select(inputMap.get("start year"));
		}
		
		if (inputMap.containsKey("end day")){
			ComboBox endDayCB = new ComboBox(By.id("inventory-toDateDatePicker_day"));
			endDayCB.select(inputMap.get("end day"));
		}
		
		if (inputMap.containsKey("end month")){
			ComboBox endMonthCB = new ComboBox(By.id("inventory-toDateDatePicker_months"));
			endMonthCB.select(inputMap.get("end month"));
		}
		
		if (inputMap.containsKey("end year")){
			ComboBox endYearCB = new ComboBox(By.id("inventory-toDateDatePicker_years"));
			endYearCB.select(inputMap.get("end year"));
		}	
	}
	
	public void deleteInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Delete");
		button.click();
		WebElement parentEl = waitForElement(By.id("inventory_create"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	public void cancelDeleteInventory(){
		waitForElementToBeClickable(By.id("inventoryCreateCancel"),3);
		Button button = new Button(By.id("inventoryCreateCancel"));
		button.click();
		waitForInVisibilityOfElement(By.id("inventoryCreateCancel"));
		waitForInVisibilityOfElement(By.id("inventory_create"));
	}
	
	public void deleteDeleteInventory(){
		WebElement parentElement = waitForElement(By.id("inventory_create"));
		Button button = new Button(parentElement, "Delete");
		button.click();
		WebElement parentEl = waitForElement(By.id("inventoryContent"));
		waitForElement(parentEl, By.className("message"),60);
	}
	
	public boolean waitForInventoryProcessing(){
		long waitTime = System.currentTimeMillis() + (1000 * 90);
		while(System.currentTimeMillis() <= waitTime){
			try{
				boolean isProcessed = true;
				selectInventoryOptions("Refresh");
				sleep(2);
				WebElement parentEl = waitForElement(By.id("inventoryContent"));
				Table table = new Table(parentEl, 1);
				for (int i=1;i<table.getRowCount();i++){
					String processedValue = table.getCellData(i, "Processed");
					if (!processedValue.equalsIgnoreCase("Yes")){
						isProcessed = false;
						break;
					}
				}
				if (isProcessed){
					return true;
				}else{
					sleep(1);
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
	}
	public boolean waitForInventoryProcessingForExternalUser(){
		long waitTime = System.currentTimeMillis() + (1000 * 90);
		while(System.currentTimeMillis() <= waitTime){
			try{
				boolean isProcessed = true;
				selectInventoryOptionsForExternalUser("Refresh");
				sleep(2);
				WebElement parentEl = waitForElement(By.id("inventoryContent"));
				Table table = new Table(parentEl, 1);
				for (int i=1;i<table.getRowCount();i++){
					String processedValue = table.getCellData(i, "Processed");
					if (!processedValue.equalsIgnoreCase("Yes")){
						isProcessed = false;
						break;
					}
				}
				if (isProcessed){
					return true;
				}else{
					sleep(1);
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
	}
}
