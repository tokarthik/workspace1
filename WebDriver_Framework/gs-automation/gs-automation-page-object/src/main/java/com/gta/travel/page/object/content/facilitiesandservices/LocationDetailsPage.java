package com.gta.travel.page.object.content.facilitiesandservices;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class LocationDetailsPage extends GSPageBase{

	public LocationDetailsPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the page factory object for the Location Details Page
	 * @return static
	 */
	public static LocationDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  LocationDetailsPage.class);
	}
	
	/**
	 * This method modifies the location details based on the values passed 
	 * through inputMap
	 * @param inputMap
	 */
	public void editLocationDetails(Map<String, String> inputMap){
		
		clickEditLocationDetails();
		if (inputMap.containsKey("Latitude")){
			TextBox geoCodeLatitudeTB = new TextBox(By.id("lat"));
			geoCodeLatitudeTB.setText("");
			geoCodeLatitudeTB.setText(inputMap.get("Latitude"));
		}
		
		if (inputMap.containsKey("Longitude")){
			TextBox geoCodeLongitudeTB = new TextBox(By.id("lon"));
			geoCodeLongitudeTB.setText("");
			geoCodeLongitudeTB.setText(inputMap.get("Longitude"));
		}
		
		if (inputMap.containsKey("Location")){
			ComboBox locationCB = new ComboBox(By.id("code"));
			locationCB.select(inputMap.get("Location"));
		}
		
	}
		
	/**
	 * This method updates the location details
	 */
	public void updateLocationDetails(){
		WebElement parentElement = waitForElement(By.id("locationContent"));
		Button updateButton = new Button(parentElement, "Update");
		updateButton.click();
		waitForInVisibilityOfElement(By.id("lon"),40);
		WebElement parentEl = waitForElement(By.id("locationForm"));
		waitForElement(parentEl, By.className("message"),40);
	}
	
	/**
	 * This method cancels the location details
	 */
	public void cancelLocationDetails(){
		WebElement parentElement = waitForElement(By.id("locationContent"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement((By.id("lon")), 40);
	}
	
	/**
	 * This method verifies for the record updated message and will return true 
	 * or false based on the message
	 * @return boolean
	 */
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	/**
	 * This method clicks on the edit link of the LocationDetails Section
	 */
	private void clickEditLocationDetails() {
		WebElement parentElement = waitForElement(By.id("locationContentMenuOptions"));
		Link editLink = new Link(parentElement, "Edit");
		editLink.clickLink();	
		waitForVisibilityOfElement((By.id("lon")), 40);
	}
	
	/**
	 * This method verifies the record updated message and returns false upon
	 * failure
	 * @param parentLocator
	 * @param message
	 * @return boolean
	 */
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
}
