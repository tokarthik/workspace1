package com.gta.travel.page.object.contract.rateplans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ImageButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class RatePlanDetailsSectionPage  extends GSPageBase{

	private static Map<String, Integer> ratePlanFieldsMap;
	
	public RatePlanDetailsSectionPage(){
		super(getDriver());
		ratePlanFieldsMap = new HashMap<String, Integer>();
		ratePlanFieldsMap.put("rate plan code", 1);
		ratePlanFieldsMap.put("rate plan name", 2);
		ratePlanFieldsMap.put("margin", 4);
		ratePlanFieldsMap.put("meal basis", 5);
		ratePlanFieldsMap.put("market", 6);
	}
	
	public static RatePlanDetailsSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanDetailsSectionPage.class);
	}
	
	public boolean isRatePlanRowCollapsed(){
		try{
			waitUntilInvisibilityOfElement(By.id("ratePlan_div"), 5);
			getDriver().findElement(By.id("ratePlan_div"));
		}catch(Exception e){
			return true;
		}
		return false;
	}
	
	public boolean verifyRecordUpdatedMessage(String message){
		WebElement parentEl = waitForElement(By.id("ratePlanContent"),5);
		sleep(1);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	public boolean verifyFieldError(By locator, String message){
		sleep(1);
		for (int i=0;i<3;i++){
			if (verifyError(locator, message)){
				return true;
			}else{
				sleep(2);
			}
		}
		return false;
	}
	
	private boolean verifyError(By locator, String message){
		waitForElement(locator, message);
		WebElement element = waitForElement(locator,5);
		if (element.getText().contains(message)){
			return true;
		}
		return false;
	}
	
	public boolean verifyDataUpdated(Map<String, String> map){
		WebElement divEl = waitForElement(By.id("ratePlan_div"));
		for (String key : map.keySet()){
			String value = map.get(key);
			int rowNum = ratePlanFieldsMap.get(key);
			WebElement row = getTRElementFromTable(divEl,rowNum);
			String colValue = getElements(row, By.tagName("td")).get(1).getText();
			if (!value.equalsIgnoreCase(colValue)){
				return false;
			}
		}
		return true;
	}

	public int getRatePlanListcount(){
		WebElement parentElement = waitForElement(By.id("ratePlanContent"),5);
		Table providerTable = new Table(parentElement, 1);
		return providerTable.getRowCount();
	}
	
	public boolean verifyRecordStatus(Map<String, String> map, String status){
		WebElement parentElement = waitForElement(By.id("ratePlanContent"),5);
		Table rpTable = new Table(parentElement, 1);
		for (int i=1;i<rpTable.getRowCount();i++){
			boolean flag = true;
			for (String key : map.keySet()){
				String value = map.get(key);
				if (rpTable.getCellData(i, key).equalsIgnoreCase(value)){
					continue;
				}else{
					flag = false;
					break;
				}
			}
			if (flag){
				sleep(1);
				WebElement statusEl = rpTable.getCell(i, "Status");
				
				ImageButton imageBut = new ImageButton(getElement(statusEl,By.tagName("img")));
				if (imageBut.getAttributeValue("title").equals(status)){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean selectRatePlan(Map<String, String> map){
		WebElement parentElement = waitForElement(By.id("ratePlanContent"),5);
		Table rpTable = new Table(parentElement, 1);
		for (int i=1;i<rpTable.getRowCount();i++){
			boolean flag = true;
			for (String key : map.keySet()){
				String value = map.get(key);
				if (rpTable.getCellData(i, key).equalsIgnoreCase(value)){
					continue;
				}else{
					flag = false;
					break;
				}
			}
			if (flag){
				sleep(1);
				rpTable.clickOnDataColumn(i, "Rate Plan Code");
				sleep(2);
				WebElement parentEl1 = null;
				try{
					parentEl1 = waitForElement(By.id("ratePlan_tr"), 1);
					WebElement parentEl2 = waitForElement(parentEl1,By.id("ratePlan_div"),1);
					WebElement parentEl3 = waitForElement(parentEl2,By.className("dialog"),1);
					waitForElement(parentEl3,By.className("inserted"),1);
					return true;
				}catch(Exception e){
					return false;
				}
			}
		}
		return false;
	}

	public void selectRatePlanFromTheList(Map<String, String> map){
		for (int i=0;i<4;i++){
			if (selectRatePlan(map)){
				break;
			}else{
				sleep(2);
			}
		}
	}
	
	public void selectRatePlanOptions(String optionName){
		WebElement menuElement = waitForElement(By.id("ratePlanContentMenuOptions"),5);
		if (Link.isLinkVisible(menuElement,"Select Option")){
			Link slectOptionLink = new Link(menuElement,"Select Option");
			slectOptionLink.clickLink();
		}

		waitForElement(menuElement,By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equals("Create") || optionName.equals("Edit")){
			waitForElement(By.id("code"),5);
		}if (optionName.equals("Refresh")){
			sleep(2);
			waitForInVisibilityOfElement(By.className("message"));
		}
		else {
			sleepms(500l);
		}
	}
	
	public void createRatePlan(Map<String, String> inputMap){
		sleepms(200l);
		if (inputMap.containsKey("rate plan code")){
			waitForElement(By.id("code"));
			TextBox releaseTimeTB = new TextBox(By.id("code"));
			releaseTimeTB.setText(inputMap.get("rate plan code"));
		}

		if (inputMap.containsKey("rate plan name")){
			waitForElement(By.id("name"));
			TextBox releaseTimeTB = new TextBox(By.id("name"));
			releaseTimeTB.setText(inputMap.get("rate plan name"));
		}

		if (inputMap.containsKey("enter rate as")){
			waitForElement(By.id("rateType"));
			ComboBox releaseTimeCB = new ComboBox(By.id("rateType"));
			releaseTimeCB.select(inputMap.get("enter rate as"));
		}
		
		if (inputMap.containsKey("margin")){
			waitForElement(By.id("margin"));
			TextBox releaseTimeTB = new TextBox(By.id("margin"));
			releaseTimeTB.setText(inputMap.get("margin"));
		}

		if (inputMap.containsKey("meal basis")){
			waitForElement(By.id("mealBasiss"));
			ComboBox releaseTimeCB = new ComboBox(By.id("mealBasiss"));
			releaseTimeCB.select(inputMap.get("meal basis"));
		}
		
		if (inputMap.containsKey("meal breakfast")){
			waitForElement(By.id("mealBreakfast"));
			ComboBox mealBFCB = new ComboBox(By.id("mealBreakfast"));
			mealBFCB.select(inputMap.get("meal breakfast"));
		}

		if (inputMap.containsKey("meal lunch")){
			waitForElement(By.id("mealLunch"));
			ComboBox mealLunchCB = new ComboBox(By.id("mealLunch"));
			mealLunchCB.select(inputMap.get("meal lunch"));
		}
		
		if (inputMap.containsKey("meal dinner")){
			waitForElement(By.id("mealDinner"));
			ComboBox mealDinnerCB = new ComboBox(By.id("mealDinner"));
			mealDinnerCB.select(inputMap.get("meal dinner"));
		}

	}
	
	public void cancelCreateRatePlan(){
		WebElement parentEL = waitForElement(By.id("ratePlan_create"));
		Button updateButton = new Button(parentEL,"Cancel");
		updateButton.click();
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Cancel']"),10);
	}
	
	public void saveCreateRatePlan(){
		WebElement parentEL = waitForElement(By.id("ratePlan_create"));
		Button updateButton = new Button(parentEL,"Save");
		updateButton.click();
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Save']"),10);
		sleep(1);
	}

	public void saveCreateRatePlanWithValidationErrors(By locatorToWait){
		WebElement parentEL = waitForElement(By.id("ratePlan_create"));
		Button updateButton = new Button(parentEL,"Save");
		updateButton.click();
		waitForElement(parentEL, locatorToWait);
	}
	
	public void editRatePlan(Map<String, String> inputMap){
		if (inputMap.containsKey("rate plan code")){
			waitForElement(By.id("code"));
			TextBox releaseTimeTB = new TextBox(By.id("code"));
			releaseTimeTB.setText(inputMap.get("rate plan code"));
		}

		if (inputMap.containsKey("rate plan name")){
			waitForElement(By.id("name"));
			TextBox releaseTimeTB = new TextBox(By.id("name"));
			releaseTimeTB.setText(inputMap.get("rate plan name"));
		}

		if (inputMap.containsKey("enter rate as")){
			waitForElement(By.id("rateType"));
			ComboBox releaseTimeCB = new ComboBox(By.id("rateType"));
			releaseTimeCB.select(inputMap.get("enter rate as"));
		}
		
		if (inputMap.containsKey("margin")){
			waitForElement(By.id("margin"));
			TextBox releaseTimeTB = new TextBox(By.id("margin"));
			releaseTimeTB.setText(inputMap.get("margin"));
		}

		if (inputMap.containsKey("meal basis")){
			waitForElement(By.id("mealBasiss"));
			ComboBox releaseTimeCB = new ComboBox(By.id("mealBasiss"));
			releaseTimeCB.select(inputMap.get("meal basis"));
		}
		
		if (inputMap.containsKey("meal breakfast")){
			waitForElement(By.id("mealBreakfast"));
			ComboBox mealBFCB = new ComboBox(By.id("mealBreakfast"));
			mealBFCB.select(inputMap.get("meal breakfast"));
		}

		if (inputMap.containsKey("meal lunch")){
			waitForElement(By.id("mealLunch"));
			ComboBox mealLunchCB = new ComboBox(By.id("mealLunch"));
			mealLunchCB.select(inputMap.get("meal lunch"));
		}
		
		if (inputMap.containsKey("meal dinner")){
			waitForElement(By.id("mealDinner"));
			ComboBox mealDinnerCB = new ComboBox(By.id("mealDinner"));
			mealDinnerCB.select(inputMap.get("meal dinner"));
		}
		
	}
	
	public void cancelEditRatePlan(){
		WebElement parentEL = waitForElement(By.id("ratePlan_div"));
		Button updateButton = new Button(parentEL,"Cancel");
		updateButton.click();
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Cancel']"),10);
	}
	
	public void updateEditRatePlan(){
		WebElement parentEL = waitForElement(By.id("ratePlan_div"));
		Button updateButton = new Button(parentEL,"Update");
		updateButton.click();
		sleep(2);
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Update']"),10);
	}
	
	public void deleteAndOKRatePlan(){
		acceptAlert();
		waitForTextToBePresentInElement(By.className("message"), "Record deleted", 20);
	}

	public void deleteAndCancelRatePlan(){
		dismissAlert();
		sleep(1);
	}

	public void deleteAllRatePlans(){
		int count = getRatePlanListcount();
		while(count > 1){
			WebElement parentElement = waitForElement(By.id("ratePlanContent"),5);
			Table providerTable = new Table(parentElement, 1);
			providerTable.getCell(1, "Rate Plan Code").click();
			waitForElement(By.id("ratePlan_div"),20);
			selectRatePlanOptions("Delete");
			deleteAndOKRatePlan();
			waitForTextToBePresentInElement(By.className("message"), "Record deleted", 20);
			selectRatePlanOptions("Refresh");
			waitForInVisibilityOfElement(By.className("message"));
			count = getRatePlanListcount();
		}
	}

	public boolean sortOnRatePlanCode(){
		return sort(0, "String");
	}
	
	public boolean sortOnStatus(){
		return sort(6, "Status");
	}

	private boolean sort(int colNum, String objectType){

		boolean result = true;

		WebElement tableDivEl = waitForElement(By.id("ratePlanContent"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		tableDivEl = waitForElement(By.id("ratePlanContent"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			tableDivEl = waitForElement(By.id("ratePlanContent"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}	
	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = getElements(tr,By.tagName("td"));
			if (i==1){
				if (objectType.equals("Status")){
					currentValue = tdList.get(colNum).findElement(By.tagName("img")).getAttribute("title");
				}else{
					currentValue = tdList.get(colNum).getText();
				}
			}else{
				
				prevValue = currentValue;
				
				if (objectType.equals("Status")){
					currentValue = tdList.get(colNum).findElement(By.tagName("img")).getAttribute("title");
				}else{
					currentValue = tdList.get(colNum).getText();
				}
				
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}
				}
			}
		}
		return result;
	}	
}
