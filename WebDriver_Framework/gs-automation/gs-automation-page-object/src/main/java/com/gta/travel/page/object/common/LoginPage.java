package com.gta.travel.page.object.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;

public class LoginPage extends GSPageBase {

	public LoginPage(WebDriver driver){
		super(driver);
	}

	public HomePage login(){
		String webId = excelUtil.getKeyValue("DEV", "webId");
		String userName = excelUtil.getKeyValue("DEV", "userName");
		String password = excelUtil.getKeyValue("DEV", "password");
		
		return login(webId,userName,password);
//		return login("GTA","lcp36s","London1")
	}
	public HomePage login(String webId, String userName, String password){
		waitForElementToBeClickable(By.id("login"));
		clearAndType(By.id("qualifier"), webId);
		clearAndType(By.id("username"), userName);
		clearAndType(By.id("password"), password);
		waitAndClick(By.id("login"));
		
	    waitForPageElement(By.linkText("Contracts"));
    	return PageFactory.initElements(getDriver(), HomePage.class);
	}
}
