package com.gta.travel.page.object.contract.manageavailability;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ImageButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;
import com.mediaocean.qa.framework.utils.DateUtil;

public class ManageAvailabilitySearchAndResultsSectionPage extends GSPageBase{

	public String[] dayArr = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun","All"};

	
	public ManageAvailabilitySearchAndResultsSectionPage(){
		super(getDriver());
	}
	
	public static ManageAvailabilitySearchAndResultsSectionPage getInstance(){
		return PageFactory.initElements(getDriver(), ManageAvailabilitySearchAndResultsSectionPage.class);
	}

	public void resetData(){
		
	}
	
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}

	public boolean verifyRecordUpdatedMessage(String message, String roomType){
		WebElement parentEl = waitForElement(By.className("maCalendarSections"));
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			for (WebElement refreshEl : waitForElements(parentEl, By.linkText("Refresh"),5)){
				Link link = new Link(refreshEl);
				link.clickLink();
				sleepms(400l);
			}
			return true;
		}
		return false;
	}

	public boolean verifyErrorMessage(String errorMessage){
		List<WebElement> list = waitForElements(waitForElement(By.className("maCalendarSection")),By.className("fieldErrors"),5);
		for (WebElement el : list){
			if (el.getText().contains(errorMessage)){
				return true;
			}
		}
		return false;
	}
	
	public void clickSearch(){
		sleep(1);
		WebElement parentEl = waitForElement(By.id("manageRatesAndAvailabilitySectionHeader"));
		Link searchLink = new Link(waitForElement(parentEl, By.linkText("Search")));
		searchLink.clickLink();
		waitForElement(By.id("manageRatesAndAvailabilitySearchCreateCancel"),40);
		sleepms(500l);
	}
	
	public void search(Map<String, String> inputMap){
		sleep(2);
		for (int n=0;n<2;n++){
			try{
				if (inputMap.containsKey("update")){
					String value = inputMap.get("update");
					if (value.equals("Availability")){
						CheckBox updateAvailCB = new CheckBox(By.id("availability"));
						updateAvailCB.check();
						CheckBox updateRatesCB = new CheckBox(By.id("rates"));
						updateRatesCB.uncheck();
						
						if (!updateAvailCB.isChecked()){
							updateAvailCB.check();
						}
					}else if (value.equals("Rates")){
						CheckBox updateRatesCB = new CheckBox(By.id("rates"));
						updateRatesCB.check();
						CheckBox updateAvailCB = new CheckBox(By.id("availability"));
						updateAvailCB.uncheck();
						if (!updateRatesCB.isChecked()){
							updateRatesCB.check();
						}
					}else{
						CheckBox updateAvailCB = new CheckBox(By.id("availability"));
						updateAvailCB.check();
						CheckBox updateRatesCB = new CheckBox(By.id("rates"));
						updateRatesCB.check();
					}
				}
				if (inputMap.containsKey("start day")){
					String value = inputMap.get("start day");
					ComboBox startDayCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-StartDatePicker_day"));
					startDayCB.select(value);
					if (!startDayCB.getSelectedItem().equalsIgnoreCase(value)){
						startDayCB.select(value);
					}
				}
				if (inputMap.containsKey("start month")){
					String value = inputMap.get("start month");
					ComboBox startMonthCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-StartDatePicker_months"));
					startMonthCB.select(value);
					if (!startMonthCB.getSelectedItem().equalsIgnoreCase(value)){
						startMonthCB.select(value);
					}
				}
				if (inputMap.containsKey("start year")){
					String value = inputMap.get("start year");
					ComboBox startYearCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-StartDatePicker_years"));
					startYearCB.select(value);
					if (!startYearCB.getSelectedItem().equalsIgnoreCase(value)){
						startYearCB.select(value);
					}
				}
				if (inputMap.containsKey("end day")){
					String value = inputMap.get("end day");
					ComboBox endDayCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-EndDatePicker_day"));
					endDayCB.select(value);
					if (!endDayCB.getSelectedItem().equalsIgnoreCase(value)){
						endDayCB.select(value);
					}
				}
				if (inputMap.containsKey("end month")){
					String value = inputMap.get("end month");
					ComboBox endMonthCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-EndDatePicker_months"));
					endMonthCB.select(value);
					if (!endMonthCB.getSelectedItem().equalsIgnoreCase(value)){
						endMonthCB.select(value);
					}
				}
				if (inputMap.containsKey("end year")){
					String value = inputMap.get("end year");
					ComboBox endYearCB = new ComboBox(By.id("manageRatesAndAvailabilitySearch-EndDatePicker_years"));
					endYearCB.select(value);
					if (!endYearCB.getSelectedItem().equalsIgnoreCase(value)){
						endYearCB.select(value);
					}
				}
				if (inputMap.containsKey("days")){
					String value = inputMap.get("days");
					if (value.equalsIgnoreCase("All")){
						CheckBox allCB = new CheckBox(By.id("daysOfWeekTickAll"));
						if (!allCB.isChecked()){
							allCB.check();
						}
						for (int i=0;i<dayArr.length-1;i++){
							CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
							if(!dayCB.isChecked()){
								dayCB.check();
							}
						}
					}else{
						CheckBox allCB = new CheckBox(By.id("daysOfWeekTickAll"));
						if (allCB.isChecked()){
							allCB.uncheck();
						}
						for (int i=0;i<dayArr.length-1;i++){
							CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
							if (dayCB.isChecked()){
								dayCB.uncheck();
							}
						}
						
						for (int i=0;i<dayArr.length-1;i++){
							String dayValue = dayArr[i];
							waitForElement(By.id("daysOfWeek"+i));
							CheckBox dayCB = new CheckBox(By.id("daysOfWeek"+i));
							if (value.contains(dayValue)){
								dayCB.check();
							}
						}
					}
				}
		
				if (inputMap.containsKey("room types")){
					waitForElement(By.id("allRoomTypes1"));
					CheckBox allRoomTypesCB = new CheckBox(By.id("allRoomTypes1"));
					allRoomTypesCB.uncheck();
					executeIEFireEventJS("allRoomTypes1", "onclick");
					if (allRoomTypesCB.isChecked()){
						sleepms(300l);
						allRoomTypesCB.uncheck();
						executeIEFireEventJS("allRoomTypes1", "onclick");
					}
					String roomTypeStr = inputMap.get("selected room type");
					String[] roomTypeArr = roomTypeStr.split(",");
					for (String value : roomTypeArr){
						String rtValue = getCheckBoxValue(value);
						waitForElements(By.id("ROOMTYPES"));
						CheckBox stdRoomTypeCB = new CheckBox(By.id("ROOMTYPES"),rtValue);
						stdRoomTypeCB.check();
						if (!stdRoomTypeCB.isChecked()){
							stdRoomTypeCB.check();
						}
					}
				}
				
		
				if (inputMap.containsKey("rate plans")){
					CheckBox allRatePlansCB = new CheckBox(By.id("allRatePlans1"));
					allRatePlansCB.uncheck();
					executeIEFireEventJS("allRatePlans1", "onclick");
					String value = inputMap.get("selected rate plan");
					if (value.equalsIgnoreCase("all")){
						allRatePlansCB.check();
						executeIEFireEventJS("allRatePlans1", "onclick");
						if (!allRatePlansCB.isChecked()){
							allRatePlansCB.check();
							executeIEFireEventJS("allRatePlans1", "onclick");
						}
					}else {
						String rpValue = getCheckBoxValue(value);
						CheckBox selectedRatePlanTypeCB = new CheckBox(By.id("RATEPLANS"),rpValue);
						selectedRatePlanTypeCB.check();
						if (!selectedRatePlanTypeCB.isChecked()){
							selectedRatePlanTypeCB.check();
						}
					}
				}
				break;
			}catch(Exception e){
				sleep(2);
				continue;
			}
		}
	}
	
	public boolean cancelSearch(){
		waitForElement(By.id("manageRatesAndAvailabilitySearchCreateCancel"));
		Button cancelButton = new Button(By.id("manageRatesAndAvailabilitySearchCreateCancel"));
		cancelButton.click();
		waitForInVisibilityOfElement(By.id("manageRatesAndAvailabilitySearchCreateCancel"));
		if (!isElementPresent(By.className("maCalendarSections"))){
			return true;
		}
		return false;
	}
	
	public void continueSearch(){
		WebElement parentEL = waitForElement(By.id("manageRatesAndAvailabilitySearch"));
		waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Continue']"));
		Button continueButton = new Button(parentEL,"Continue");
		continueButton.click();
		waitForInVisibilityOfElement(parentEL, By.cssSelector("input[class='gsButton'][value='Continue']"),10);
		WebElement parentContinueEL = waitForElement(By.className("maCalendarSections"));
		waitForElement(parentContinueEL, By.cssSelector("input[class='gsButton'][value='Update']"));
	}
	
	public void refreshRates(){
		sleep(2);
		WebElement menuElement = waitForElement(By.id("manageRatesAndAvailabilitySearchResults"));
		waitForElement(menuElement,By.linkText("Refresh"));
		WebElement refreshLinkEl = waitForElement(menuElement,By.linkText("Refresh"));
		Link refreshLink = new Link(refreshLinkEl);
		refreshLink.clickLink();
		waitForStalenessOf(refreshLinkEl,3);
		menuElement = waitForElement(By.id("manageRatesAndAvailabilitySearchResults"));
		waitForElement(menuElement,By.linkText("Refresh"));
//		sleep(3);
	}
	
	public void editRates(){
		
	}
	
	public void updateEditRates(){
		WebElement parentEL = waitForElement(By.className("maCalendarSections"));
		WebElement updateElement = waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Update']"));
		Button updateButton = new Button(parentEL,"Update");
		updateButton.click();
		waitForStalenessOf(updateElement);
		parentEL = waitForElement(By.className("maCalendarSections"));
		waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Update']"));
	}
	
	public void updateEditRates(String roomType){
		sleep(2);
		
		List<WebElement> updateElementList = waitForElements(By.className("gsButton"));
		WebElement updateElement = null;

		if (roomType.equalsIgnoreCase("Standard Triple")){
			updateElement = updateElementList.get(0);
		}else{
			updateElement = updateElementList.get(1);
		}
		
		for (int i=0;i<3;i++){
			Button updateButton = new Button(updateElement);
			updateButton.click();
			if (waitForTextToBePresentInElement(By.className("message"), "Record updated", 10)){
				break;
			}else{
				sleep(1);
				continue;
			}
		}
		sleep(2);
		verifyRecordUpdatedMessage("Record updated",roomType);
	}

	private String getCheckBoxValue(String searchStr){
		String pageSource = getDriver().getPageSource();
		String value = "";
		if (pageSource.contains(searchStr.trim())){
			int index1 = pageSource.indexOf(searchStr);
			String subStr1 = pageSource.substring(0, index1);
			int index2 =subStr1.lastIndexOf("value");
			subStr1 = subStr1.substring(index2);
			if (BROWSER.equalsIgnoreCase("IE") || BROWSER.equalsIgnoreCase("InternetExplorer")){
				String value1 = subStr1.split(" ")[0];	
				value = value1.split("=")[1].trim();
				if (value.contains("\"")){
					value.replaceAll("\"", "");
				}
			}else{
				value = subStr1.split("\"")[1];
			}
		}
		return value;
	}
	
	public boolean verifyDates(Map<String, String> inputMap){
		try{
			WebElement tableEl = waitForElement(By.className("maCalendarSection"));
			String fromDateStr = inputMap.get("start day") + "/" +inputMap.get("start month") + "/" + inputMap.get("start year"); 
			String toDateStr = inputMap.get("end day") + "/" + inputMap.get("end month") + "/" + inputMap.get("end year");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
			SimpleDateFormat weekFormat = new SimpleDateFormat("EE");
			
			Date fromDate = dateFormat.parse(fromDateStr);
			Date toDate = dateFormat.parse(toDateStr);
			
			String days = inputMap.get("days");
			List<WebElement> thDatesList = waitForElements(tableEl, By.className("inventoryDay"),5);
			int count = 0;
			while (fromDate.compareTo(toDate) <= 0){
				String weekStr = weekFormat.format(fromDate);
				if (days.equalsIgnoreCase("all") || days.contains(weekStr)){
					String tableDateStr = thDatesList.get(count++).getText();
					String[] tableDateArr = tableDateStr.split(" ");
					
					int tableDate = Integer.parseInt(tableDateArr[0]);
					String tableWeek = tableDateArr[1];
					
					if (tableDate != DateUtil.getDay(fromDate)){
						return false;
					}
					if (!tableWeek.equalsIgnoreCase(weekStr)){
						return false;
					}
				}
				fromDate = DateUtil.addDays(fromDate, 1);
			}
		
		}catch(Exception e){
			throw new RuntimeException("Failed to verify data in verifyDates()");
		}
		return true;
	}
	
	public boolean verifyData(Map<String, String> inputMap, int rowNum){
		sleep(2);
		WebElement tableEl = waitForElement(By.className("maCalendarSection"));
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		int rowCount = trList.size()-13 + 3;
		for (int i=3;i<rowCount;i++){
			if (verifyRowData(inputMap, i)){
				return true;
			}
		}
		return false;
	}
	
	private boolean verifyRowData(Map<String, String> inputMap, int rowNum){
		
		WebElement tableEl = waitForElement(By.className("maCalendarSection"));
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		WebElement rowEl = trList.get(rowNum);
		List<WebElement> tdList = waitForElements(rowEl,By.tagName("td"),5);
		WebElement colEl = tdList.get(0);
		
		List<WebElement> divList = waitForElements(colEl,By.tagName("div"),5);
		WebElement ratePlanDivEl = divList.get(0);
		WebElement occupancyDivEl = divList.get(1);
		WebElement minNightsDivEl = divList.get(2);
		WebElement moreRulesDivEl = divList.get(3);
		Link link = new Link(moreRulesDivEl, "More rules");
		link.clickLink();
		divList = waitForElements(colEl,By.tagName("div"),5);
		WebElement stayFullPeriodDivEl = divList.get(4);
		
		String ratePlan = waitForElement(ratePlanDivEl, By.tagName("label")).getText();
		String occupancy = waitForElement(occupancyDivEl, By.tagName("label")).getText();
		String minNights = waitForElement(minNightsDivEl, By.tagName("label")).getText();
		String moreRules = moreRulesDivEl.getText();
		String stayFullPeriod = stayFullPeriodDivEl.getText();
		if (!ratePlan.equalsIgnoreCase(inputMap.get("selected rate plan"))){
			return false;
		}
		if (!occupancy.equalsIgnoreCase(inputMap.get("occupancy"))){
			return false;
		}
		if (!minNights.equalsIgnoreCase(inputMap.get("min nights"))){
			return false;
		}
		if (!moreRules.equalsIgnoreCase("More rules")){
			return false;
		}
		if (!stayFullPeriod.equalsIgnoreCase("Stay Full Period? "+ inputMap.get("stay full period"))){
			return false;
		}
		return true;
	}
	
	public void setValueToTableField(String days, String value, int rowNum, Date date, String type){
		
		if (!days.equalsIgnoreCase("All")){
			SimpleDateFormat weekFormat = new SimpleDateFormat("EE");
			for (int i=1;i<100;i++){
				if (days.contains(weekFormat.format(date))){
					break;
				}else{
					date = DateUtil.addDays(date, 1);
				}
			}
		}
		int colNum = getColumnNumber(date);
		System.out.println(">>> colNum : "+colNum);
		List<WebElement> divList = getDivListFromTable(rowNum, colNum);
		System.out.println(">>> divList size : "+ divList.size());
		TextBox grossTB = null;
		if (type.equalsIgnoreCase("Gross")){
			waitForElement(divList.get(0), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(0), By.className("inputAmount"));
		}else if (type.equalsIgnoreCase("Margin")){
			waitForElement(divList.get(1), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(1), By.className("inputAmount"));
		}else{
			waitForElement(divList.get(2), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(2), By.className("inputAmount"));
		}
		grossTB.setText(value);
		grossTB.clickTab();
		executeIEFireEventJS(grossTB.getAttributeValue("id"), "onblur");
	}

	private List<WebElement> getDivListFromTable(int rowNum, int colNum){
		List<WebElement> divList = null;
		for (int k=0;k<2;k++){
			try{
				WebElement tableEl = waitForElement(By.className("maCalendarSection"));
				List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
				WebElement rowEl = trList.get(rowNum);
				
				WebElement colEl = waitForElements(rowEl, By.tagName("td"),5).get(colNum);
				divList = waitForElements(colEl, By.className("xxx"),5);
				break;
			}catch(Exception e){
				sleepms(100l);
				continue;
			}
		}
		return divList;
	}
	
	public boolean isTextBoxDisabled(int rowNum, Date date, String type){
		int colNum = getColumnNumber(date);
		List<WebElement> divList = getDivListFromTable(rowNum, colNum);
		TextBox grossTB = null;
		if (type.equalsIgnoreCase("Gross")){
			waitForElement(divList.get(0), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(0), By.className("inputAmount"));
		}else if (type.equalsIgnoreCase("Margin")){
			waitForElement(divList.get(1), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(1), By.className("inputAmount"));
		}else{
			waitForElement(divList.get(2), By.className("inputAmount"),5);
			grossTB = new TextBox(divList.get(2), By.className("inputAmount"));
		}
		String attValue = grossTB.getAttributeValue("disabled");
		if (attValue != null && attValue.equalsIgnoreCase("true")){
			return true;
		}
		return false;
	}
	
	public int getColumnNumber(Date date){
		SimpleDateFormat weekFormat = new SimpleDateFormat("EE");
		int dayNum = DateUtil.getDay(date);
		String day = Integer.toString(dayNum);
		if (dayNum < 9){
			day = "0"+dayNum;
		}
		String week = weekFormat.format(date);
		String colName = day + " " + week;
		
		WebElement tableEl = waitForElement(By.className("maCalendarSection"));
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		WebElement rowEl = trList.get(1);
		List<WebElement> thList = waitForElements(rowEl, By.tagName("th"),5);
		for (int i=0; i<thList.size(); i++){
			String thValue = thList.get(i).getText();
			if (thValue.equalsIgnoreCase(colName)){
				return i;
			}
		}
		return -1;
	}
	
	public String getValueFromTableField(int rowNum, int colNum, String type){
		List<WebElement> divList = getDivListFromTable(rowNum, colNum);
		TextBox grossTB = null;
		if (type.equalsIgnoreCase("Gross")){
			grossTB = new TextBox(divList.get(0), By.className("inputAmount"));
		}else if (type.equalsIgnoreCase("Margin")){
			grossTB = new TextBox(divList.get(1), By.className("inputAmount"));
		}else{
			grossTB = new TextBox(divList.get(2), By.className("inputAmount"));
		}
		return grossTB.getText();
	}

	public void clickCopyToRightButton(int rowNum, String type, String valueToCheck){
		List<WebElement> imgButtonList = getCopyToRightImgList();
		if (type.equalsIgnoreCase("Gross")){
			imgButtonList.get(0).click();
		}else if (type.equalsIgnoreCase("Margin")){
			imgButtonList.get(1).click();
		}else{
			imgButtonList.get(2).click();
		}
		
		for (int k=0;k<20;k++){
			WebElement tableEl = waitForElement(By.className("maCalendarSection"));
			WebElement rowEl = waitForElements(tableEl, By.tagName("tr"),5).get(rowNum);
			List<WebElement> tdList = waitForElements(rowEl, By.tagName("td"),5);
			String value = getValueFromTableField(3, tdList.size()-1, type);
			if (value.equalsIgnoreCase(valueToCheck)){
				return;
			}else{
				sleepms(400l);
				continue;
			}
		}
	}
	
	private List<WebElement> getCopyToRightImgList(){
		List<WebElement> imgButtonList = new ArrayList<WebElement>();
		for (WebElement imgButtonEl : waitForElements(By.tagName("img"))){
			if (imgButtonEl.getAttribute("title").equalsIgnoreCase("Copy to right")){
				imgButtonList.add(imgButtonEl);
			}
		}
		return imgButtonList;
	}
	
	public boolean verifyRates(String type, String value, int rowNum){
		sleep(1);
		WebElement tableEl = waitForElement(By.className("maCalendarSection"));
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		int rowCount = trList.size()-13 + 3;
		for (int i=3;i<rowCount;i++){
			if (verifyRateRows(type, value, i)){
				return true;
			}
		}
		return false;
	}
	
	private boolean verifyRateRows(String type, String value, int rowNum){
		WebElement tableEl = waitForElement(By.className("maCalendarSection"));
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		WebElement rowEl = trList.get(1);
		List<WebElement> thList = waitForElements(rowEl, By.tagName("th"),5);
		
		for (int i=3; i<thList.size();i++){
			String fieldValue = getValueFromTableField(rowNum, i, type);
			if (!value.equalsIgnoreCase(fieldValue)){
				return false;
			}
		}
		return true;
	}
	
//	______________________________________
	
	private WebElement getTableElement(String roomType){
		List<WebElement> tableList = waitForElements(By.className("maCalendarSection"));
		if (roomType.equalsIgnoreCase("Standard Triple")){
			return tableList.get(0);
		}else{
			return tableList.get(1);
		}
	}
	
	public boolean verifyDates(Map<String, String> inputMap, String roomType){
		try{
			WebElement tableEl = getTableElement(roomType);
			String fromDateStr = inputMap.get("start day") + "/" +inputMap.get("start month") + "/" + inputMap.get("start year"); 
			String toDateStr = inputMap.get("end day") + "/" + inputMap.get("end month") + "/" + inputMap.get("end year");
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
			SimpleDateFormat weekFormat = new SimpleDateFormat("EE");
			
			Date fromDate = dateFormat.parse(fromDateStr);
			Date toDate = dateFormat.parse(toDateStr);
			
			String days = inputMap.get("days");
			List<WebElement> thDatesList = waitForElements(tableEl, By.className("inventoryDay"),5);
			int count = 0;
			while (fromDate.compareTo(toDate) <= 0){
				String weekStr = weekFormat.format(fromDate);
				if (days.equalsIgnoreCase("all") || days.contains(weekStr)){
					String tableDateStr = thDatesList.get(count).getText();
					count++;
					String[] tableDateArr = tableDateStr.split(" ");
					
					int tableDate = Integer.parseInt(tableDateArr[0]);
					String tableWeek = tableDateArr[1];
					
					if (tableDate != DateUtil.getDay(fromDate)){
						return false;
					}
					if (!tableWeek.equalsIgnoreCase(weekStr)){
						return false;
					}
				}
				fromDate = DateUtil.addDays(fromDate, 1);
			}
		
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("Failed to verify data in verifyDates()");
		}
		return true;
	}
	
	public boolean verifyPriority(String value, String roomType){
		return compareSinglePropertyRowData("Priority", value, "class", roomType);
	}
	
	public boolean verifySold(String value, String roomType){
		return compareSinglePropertyRowData("Sold (excluding Extra)", value, "value", roomType);
	}
	
	public boolean verifyTotalAvailable(String value, String roomType){
		if (value.contains(".gif")){
			return compareSinglePropertyRowData("Total Available", ".gif", "lock image", roomType);
		}else{
			return compareSinglePropertyRowData("Total Available", value, "value", roomType);
		}
	}
	
	public boolean verifyFreesale(String isChecked, String roomType){
		return compareSinglePropertyRowData("Freesale", isChecked, "check box", roomType);
	}
	
	public boolean verifyFlexibileStopSell(boolean isOpened, String roomType){
		if (isOpened){
			return compareSinglePropertyRowData("Flexible Stop Sell", "lockOpen.gif", "lock image", roomType);
		}else{
			return compareSinglePropertyRowData("Flexible Stop Sell", "lockClosed.gif", "lock image", roomType);
		}
	}
	
	public boolean verifyCloseOut(boolean isLocked, String roomType){
		if (isLocked){
			return compareSinglePropertyRowData("Close Out", "lockOpen.gif", "lock image", roomType);
		}else{
			return compareSinglePropertyRowData("Close Out", "lockClosed.gif", "lock image", roomType);
		}
	}

	public boolean compareSinglePropertyRowData(String propertyToVerify, String value, String compareType, String roomType){
		
		WebElement tableEl = getTableElement(roomType);
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);

		for (WebElement rowEl : trList){
			List<WebElement> tdList = waitForElements(rowEl, By.tagName("td"),5);
			if (tdList == null || tdList.size() == 0 || !tdList.get(0).getText().equalsIgnoreCase(propertyToVerify)){
				continue;
			}
			int counter = 0;
			for (int i=2;i<tdList.size();i++){
				WebElement tdEl = tdList.get(i);
				if (tdEl.getAttribute("class").equalsIgnoreCase("hidden")){
					continue;
				}
				if (compareType.equalsIgnoreCase("value")){
					if (!tdEl.getText().equalsIgnoreCase(value)){
						return false;
					}
				}else if (compareType.equalsIgnoreCase("class")){
					if (!tdEl.getAttribute("class").equalsIgnoreCase(value)){
						return false;
					}
				}else if (compareType.equalsIgnoreCase("lock image")){
					WebElement imgEl = waitForElement(tdEl, By.tagName("img"));
					String imgSrcStr = imgEl.getAttribute("src");
					if (!imgSrcStr.contains(value)){
						return false;
					}
				}else if(compareType.equalsIgnoreCase("check box")){
					CheckBox chkBox = new CheckBox(By.name("inventory[0].inventoryDailyGlobals["+ counter +"].freesaleCheckbox"));

					String attrValue = chkBox.getAttributeValue("disabled");
					if (value.equalsIgnoreCase("disabled") && (attrValue == null || attrValue.equalsIgnoreCase("false"))){
						return false;
					}
					
					if (value.equalsIgnoreCase("true") && !chkBox.isChecked()){
						return false;
					}
					if (value.equalsIgnoreCase("false") && chkBox.isChecked()){
						return false;
					}
					counter++;
				}
			}
			return true;
		}
		return true;
	}
	
	public boolean verifyBaseInventory(String value, String roomType, int rowNum){
		int noOfDaysCount = getNoOfDaysCount(roomType);
		int counter = 0;
		WebElement tableEl = getTableElement(roomType);
		for (int l=0;l<noOfDaysCount;l++){
			WebElement element = waitForElement(tableEl, By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter +"].quantityDsp"),3);
//			TextBox tBox = new TextBox(waitForElement(tableEl,
//							By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter +"].quantityDsp")));
			if (element != null){
				if (value.equalsIgnoreCase("disabled")){
					String attrValue = element.getAttribute("disabled");
					if (attrValue !=null && attrValue.equalsIgnoreCase("true")){
						return true;
					}else{
						return false;
					}
				}
			}
			TextBox tBox = new TextBox(element);
			
			if (!tBox.getText().equalsIgnoreCase(value)){
				return false;
			}
			counter++;
		}
		return true;
	}
	
	public boolean verifyOverbook(String value, String roomType, int rowNum){
		int noOfDaysCount = getNoOfDaysCount(roomType);
		int counter = 0;
		WebElement tableEl = getTableElement(roomType);
		for (int l=0;l<noOfDaysCount;l++){
			WebElement element = waitForElement(tableEl, By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter + "].overbookQuantityDsp"),3);
//			TextBox tBox = new TextBox(waitForElement(tableEl,
//							By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter + "].overbookQuantityDsp")));
			if (element != null){
				if (value.equalsIgnoreCase("disabled")){
					String attrValue = element.getAttribute("disabled");
					if (attrValue !=null && attrValue.equalsIgnoreCase("true")){
						return true;
					}else{
						return false;
					}
				}
			}

			TextBox tBox = new TextBox(element);
			if (!tBox.getText().equalsIgnoreCase(value)){
				return false;
			}
			counter++;
		}
		return true;
	}

	public boolean verifyContractReleaseDays(String value, String roomType, int rowNum){
		int noOfDaysCount = getNoOfDaysCount(roomType);
		int counter = 0;
		WebElement tableEl = getTableElement(roomType);
		for (int l=0;l<noOfDaysCount;l++){
			WebElement element = waitForElement(tableEl, By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter +"].releaseDaysDsp"),3);
//			TextBox tBox = new TextBox(waitForElement(tableEl,
//							By.name("inventory[0].inventoryDailyDetailsContract["+ rowNum +"].inventoryDayValues["+ counter +"].releaseDaysDsp")));
			
			if (element != null){
				if (value.equalsIgnoreCase("disabled")){
					String attrValue = element.getAttribute("disabled");
					if (attrValue !=null && attrValue.equalsIgnoreCase("true")){
						return true;
					}else{
						return false;
					}
				}
			}
			
			TextBox tBox = new TextBox(element);
			if (!tBox.getText().equalsIgnoreCase(value)){
				return false;
			}
			counter++;
		}
		return true;
	}

	public boolean verifyFlexibleAvailability(String value, String roomType){
		int noOfDaysCount = getNoOfDaysCount(roomType);
		int counter = 0;
		WebElement tableEl = getTableElement(roomType);
		for (int l=0;l<noOfDaysCount;l++){
			WebElement element = null; 
			if (waitForVisibilityOfElement(tableEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues["+ counter +"].quantity"),3)){
				element = waitForElement(tableEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues["+ counter +"].quantity"),3);
			}
			
			if (value.equalsIgnoreCase("n/a") || value.equalsIgnoreCase("")){
				if (element == null || element.getText().equalsIgnoreCase("")){
					return true;
				}else{
					return false;
				}
			}

			String attrValue = element.getAttribute("disabled");
			
			if (value.equalsIgnoreCase("disabled")){
				if (attrValue !=null && attrValue.equalsIgnoreCase("true")){
					return true;
				}else{
					return false;
				}
			}
			
			TextBox tBox = new TextBox(element);
			if (!tBox.getText().equalsIgnoreCase(value)){
				return false;
			}
			counter++;
		}
		return true;
	}

	public boolean verifyFlexibleReleaseDays(String value, String roomType){
		int noOfDaysCount = getNoOfDaysCount(roomType);
		int counter = 0;
		WebElement tableEl = getTableElement(roomType);
		for (int l=0;l<noOfDaysCount;l++){
			WebElement element = null; 
			if (waitForVisibilityOfElement(tableEl,By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues["+ counter +"].releaseDays"),3)){
				element = waitForElement(tableEl,By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues["+ counter +"].releaseDays"),3);
			}
			
//			WebElement element = waitForVisibilityOfElement(By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues["+ counter +"].releaseDays"),1);
			
			if (value.equalsIgnoreCase("n/a") || value.equalsIgnoreCase("")){
				if (element == null || element.getText().equalsIgnoreCase("")){
					return true;
				}else{
					return false;
				}
			}
			
			String attrValue = element.getAttribute("disabled");
			if (value.equalsIgnoreCase("disabled")){
				if (attrValue !=null && attrValue.equalsIgnoreCase("true")){
					return true;
				}else{
					return false;
				}
			}
			
			TextBox tBox = new TextBox(element);
			if (!tBox.getText().equalsIgnoreCase(value)){
				return false;
			}
//			if (value.equalsIgnoreCase("disabled") && element.getAttribute("disabled").equalsIgnoreCase("true")){
//				return true;
//			}else if (value.equalsIgnoreCase("disabled") && !element.getAttribute("disabled").equalsIgnoreCase("true")){
//				return false;
//			}else if (!value.equalsIgnoreCase("disabled") && element.getAttribute("disabled").equalsIgnoreCase("true")){
//				return false;
//			}
//
//			if (element != null){
//				TextBox tBox = new TextBox(element);
//				if (!tBox.getText().equalsIgnoreCase(value)){
//					return false;
//				}
//			}else if (value != null && !value.equalsIgnoreCase("")){
//				return false;
//			}
			counter++;
		}
		return true;
	}

	private int getNoOfDaysCount(String roomType){
		WebElement tableEl = getTableElement(roomType);
		List<WebElement> trList = waitForElements(tableEl, By.className("topText"),5);
		List<WebElement> tdList = waitForElements(trList.get(0), By.tagName("td"),5);
		int tdCount = (tdList.size() - 2) / 2;
		return tdCount;
	}

	/*public void clickOnImage(String propertyToClick, int colNum, String roomType){
		WebElement tableEl = getTableElement(roomType);
		List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
		for (WebElement rowEl : trList){
			List<WebElement> tdList = waitForElements(rowEl, By.tagName("td"),5);
			if (tdList == null || tdList.size() == 0 || !tdList.get(0).getText().equalsIgnoreCase(propertyToClick)){
				continue;
			}
			WebElement tdEl = tdList.get(2);
			ImageButton imgButton = new ImageButton(waitForElement(tdEl, By.tagName("img")));
			String sourceBefore = imgButton.getAttributeValue("src");
			imgButton.click(); 
			sleepms(400l);
			imgButton = new ImageButton(waitForElement(tdEl, By.tagName("img")));
			String sourceAfter = imgButton.getAttributeValue("src");
			if (sourceBefore.equalsIgnoreCase(sourceAfter)){
				imgButton.click();
				sleepms(400l);
			}
			break;
		}
		sleep(4);
	}*/
	
	public void clickOnImage(String propertyToClick, int colNum, String roomType){
        sleep(2);
        WebElement tableEl = getTableElement(roomType);
        List<WebElement> trList = waitForElements(tableEl, By.tagName("tr"),5);
        for (WebElement rowEl : trList){
            List<WebElement> tdList = waitForElements(rowEl, By.tagName("td"),5);
            if (tdList == null || tdList.size() == 0 || !tdList.get(0).getText().equalsIgnoreCase(propertyToClick)){
                continue;
            }
            WebElement tdEl = tdList.get(2);
            ImageButton imgButton = new ImageButton(waitForElement(tdEl, By.tagName("img")));
            String sourceBefore = imgButton.getAttributeValue("src");
            imgButton.click(); 
            sleepms(400l);
            imgButton = new ImageButton(waitForElement(tdEl, By.tagName("img")));
            String sourceAfter = imgButton.getAttributeValue("src");
            if (sourceBefore.equalsIgnoreCase(sourceAfter)){
                imgButton.click();
                sleepms(400l);
            }
            break;
        }
        sleep(4);
    }

	public boolean continueSearchWithValidationFailure(String message){
		WebElement parentEL = waitForElement(By.id("manageRatesAndAvailabilitySearch"));
		waitForElement(parentEL, By.cssSelector("input[class='gsButton'][value='Continue']"));
		Button continueButton = new Button(parentEL,"Continue");
		continueButton.click();
		WebElement messageEl = waitForElement(By.className("message"));
		if (messageEl.getText().contains(message)){
			return true;
		}
		return false;
	}
	public void refreshRatesForExternalUser(){
		sleep(2);
		WebElement menuElement = waitForElement(By.id("manageRatesAndAvailabilitySearchResults"));
		waitForElement(menuElement,By.linkText("Refresh"));
		for (WebElement refreshLinkEl : waitForElements(menuElement,By.linkText("Refresh"),5)){
			Link refreshLink = new Link(refreshLinkEl);
			refreshLink.clickLink();
			waitForStalenessOf(refreshLinkEl,3);
		}
		menuElement = waitForElement(By.id("manageRatesAndAvailabilitySearchResults"));
	}
	public void setValueForFlexibleAvailability(String value, String roomType){
		WebElement parentEl = getTableElement(roomType);
		if (roomType.equalsIgnoreCase("Standard Triple")){
			TextBox tbEl = new TextBox(waitForElement(parentEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues[0].quantity")));
			tbEl.setText(value);
		}else if (roomType.equalsIgnoreCase("Standard Twin")){
			TextBox tbEl = new TextBox(waitForElement(parentEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues[0].quantity")));
			tbEl.setText(value);
		}
	}
	public void setValueForReleaseDays(String value, String roomType){
		WebElement parentEl = getTableElement(roomType);
		if (roomType.equalsIgnoreCase("Standard Triple")){
			TextBox tbEl = new TextBox(waitForElement(parentEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues[0].releaseDays")));
			tbEl.setText(value);
		}else if (roomType.equalsIgnoreCase("Standard Twin")){
			TextBox tbEl = new TextBox(waitForElement(parentEl, By.name("inventory[0].inventoryDailyDetailsFlexible[0].inventoryDayValues[0].releaseDays")));
			tbEl.setText(value);
		}
	}
	
	public void clickCopyToRightForFlexibleAvail(String roomType){
		WebElement parentEl = getTableElement(roomType);
		for (WebElement imgButtonEl : waitForElements(parentEl, By.tagName("img"), 5)){
			if (imgButtonEl.getAttribute("title").equalsIgnoreCase("Copy to right")){
				imgButtonEl.click();
				break;
			}
		}
		sleep(4);
	}

	public void clickCopyToRightForReleaseDays(String roomType){
		WebElement parentEl = getTableElement(roomType);
		int counter = 1;
		for (WebElement imgButtonEl : waitForElements(parentEl, By.tagName("img"), 5)){
			if (imgButtonEl.getAttribute("title").equalsIgnoreCase("Copy to right")){
				if (counter > 1){
					imgButtonEl.click();
					break;
				}else{
					counter++;
				}
			}
		}
		sleep(4);
	}
}
