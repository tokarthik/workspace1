package com.gta.travel.page.object.contract.rateplans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class RatePlanPeakRateSectionPage  extends GSPageBase{

	public String[] dayArr = {"Mon","Tue","Wed","Thu","Fri","Sat","Sun","All"};
	
	/**
	 * Default Constructor
	 */
	public RatePlanPeakRateSectionPage(){
		super(getDriver());
	}
	
	/**
	 * This method returns the instance of the current object.
	 * @return RatePlanPeakRateSectionPage
	 */
	public static RatePlanPeakRateSectionPage getInstance(){
		return PageFactory.initElements(getDriver(),  RatePlanPeakRateSectionPage.class);
	}
	
	/**
	 * This method verifies the messages displayed on the screen
	 * @param parentLocator
	 * @param message
	 * @return boolean
	 */
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement topParentEl = waitForElement(parentLocator);
		waitForElement(topParentEl,By.className("message"),40);
		if (getElement(topParentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}
	
	/**
	 * This method reset the test data for Rate Rules.
	 */
	public void resetData(){
		deleteAllPeakRateRules();
	}
	
	private void deleteAllPeakRateRules(){
		int count = getPeakRateRuleTableRowCount();
		while(count > 1){
			WebElement topParentEl = waitForElement(By.id("rateRulePeakContent"));
			WebElement parentEl = getElementIfItIsPresent(topParentEl, By.className("list"));
			if (parentEl == null){
				return;
			}
			Table table = new Table(parentEl, 1);
			try{
				table.getCell(1, "Rate Type").click();
				waitForElement(By.id("rateRulePeak_tr"),20);
			}catch(Exception e){
				table.getCell(1, "Rate Type").click();
				waitForElement(By.id("rateRulePeak_tr"),20);
			}
			selectPeakRateRuleOptions("Delete");
			deletePeakRateRuleWithOK();
			verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record created");
			refreshOptionClick();
			waitForInVisibilityOfElement(By.className("message"));
			
			count = getPeakRateRuleTableRowCount();
		}
	}
	
	/**
	 * This method verifies the row count.
	 * @param expectedCount
	 * @return boolean
	 */
	public boolean verifyRowcount(int expectedCount){
		for (int i=0;i<10;i++){
			try{
				int rowCount = getPeakRateRuleTableRowCount();
				if (expectedCount == rowCount){
					return true;
				}else{
					sleep(1);
					continue;
				}
			}catch(Exception e){
				continue;
			}
		}
		return false;
		
	}
	
	/**
	 * This method checks whether the message element is present or not.
	 * @return boolean
	 */
	public boolean isMessageDisplayed(){
		return isElementPresent(By.className("message"));
	}
	
	/**
	 * This method checks whether the create section is opened or not.
	 * @return boolean
	 */
	public boolean isCreateRecordSectionDisplayed(){
		return isElementPresent(By.id("rateRulePeak_create"));
	}
	
	/**
	 * This method verifies the data.
	 * @param inputMap
	 * @return boolean
	 */
	public boolean verifyData(Map<String, String> inputMap){
		if (inputMap.containsKey("standard room type net")){
			String value = inputMap.get("standard room type net");
			TextBox tb = new TextBox(By.id("rateRulePeakCreate.nett.3"));
			if (!value.equalsIgnoreCase(tb.getText())){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This method returns the total row count of the Peak Rate Rules table.
	 * @return int
	 */
	public int getPeakRateRuleTableRowCount(){
		WebElement topParentEl = waitForElement(By.id("rateRulePeakContent"));
		WebElement parentEl = getElementIfItIsPresent(topParentEl, By.className("list"));
		if (parentEl == null){
			return 0;
		}
		Table table = new Table(parentEl, 1);
		return table.getRowCount();
	}
	
	/**
	 * This method selects the given Peak Rate Rule record in the table
	 * @param inputMap
	 */
	public void selectPeakRateRule(Map<String, String> inputMap){
		WebElement topParentEl = waitForElement(By.id("rateRulePeakContent"));
		WebElement parentEl = waitForElement(topParentEl,By.className("list"));
		Table table = new Table(parentEl, 1);
		for (int i=1;i<table.getRowCount();i++){
			boolean flag = true;
			for (String key : inputMap.keySet()){
				String value = inputMap.get(key);
				if (!value.equalsIgnoreCase(table.getCellData(i, key))){
					flag = false;
					break;
				}
			}
			if (flag){
				table.getCell(i, "Rate Type").click();
				try{
					waitForElement(By.id("rateRulePeak_div"));
				}catch(Exception e){
					table.getCell(i, "Rate Type").click();
					waitForElement(By.id("rateRulePeak_div"));
				}
				break;
			}
		}
	}
	
	/**
	 * This method clicks the Create link.
	 */
	public void createOptionClick(){
		WebElement menuElement = waitForElement(By.id("rateRulePeakContentMenuOptions"));
		waitForElement(menuElement,By.linkText("Create"));
		selectOption(menuElement, "Create");
	}
	
	/**
	 * This method clicks the Refresh link.
	 */
	public void refreshOptionClick(){
		WebElement menuElement = waitForElement(By.id("rateRulePeakContentMenuOptions"));
		waitForElement(menuElement,By.linkText("Refresh"));
		selectOption(menuElement, "Refresh");
	}
	
	private void selectOption(WebElement menuElement, String optionName){
		waitForElement(menuElement,By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equalsIgnoreCase("Create") || optionName.equalsIgnoreCase("Copy")){
			waitForElement(By.id("rateRulePeak_create"));
			WebElement parentElement = waitForElement(By.id("rateRulePeak_create"));
			waitForElement(parentElement, By.id("rateRulePeakCreateCancel"));
		}
		if (optionName.equalsIgnoreCase("Edit")){
			waitForElement(By.id("rateRulePeak_tr"));
		}
		if (optionName.equalsIgnoreCase("Refresh")){
			sleep(1);
			for (int i=0;i<10;i++){
				try{
					getPeakRateRuleTableRowCount();
					break;
				}catch(Exception e){
					continue;
				}
			}
		}		
	}

	/**
	 * This method clicks the given option name from the menu options,
	 * @param menuElement
	 * @param optionName
	 */
	public void selectPeakRateRuleOptions(String optionName){
		WebElement menuElement = waitForElement(By.id("rateRulePeakContentMenuOptions"));
		waitForElement(menuElement,By.linkText("Select Option"));
		
		Link slectOptionLink = new Link(menuElement,"Select Option");
		slectOptionLink.clickLink();
		waitForElement(menuElement,By.linkText(optionName));
		Link optionLink = new Link(menuElement, optionName);
		optionLink.clickLink();
		if (optionName.equals("Refresh")){
			sleep(2);
		}

	}	
	
	/**
	 * This method sets the values to create the new Peak Rate Rule.
	 * @param inputMap
	 */
	public void createPeakRates(Map<String, String> inputMap){

		if (inputMap.containsKey("start day")){
			waitForElement(By.id("rateRulePeak-travelStartDatePicker_day"));
			ComboBox startDay = new ComboBox(By.id("rateRulePeak-travelStartDatePicker_day"));
			startDay.select(inputMap.get("start day"));
		}
		if (inputMap.containsKey("start month")){
			waitForElement(By.id("rateRulePeak-travelStartDatePicker_months"));
			ComboBox startMonth = new ComboBox(By.id("rateRulePeak-travelStartDatePicker_months"));
			startMonth.select(inputMap.get("start month"));
		}
		if (inputMap.containsKey("start year")){
			waitForElement(By.id("rateRulePeak-travelStartDatePicker_years"));
			ComboBox startYear = new ComboBox(By.id("rateRulePeak-travelStartDatePicker_years"));
			startYear.select(inputMap.get("start year"));
		}
		
		if (inputMap.containsKey("end day")){
			String endDayValue = inputMap.get("end day");
			waitForElement(By.id("rateRulePeak-travelEndDatePicker_day"));
			ComboBox endDay = new ComboBox(By.id("rateRulePeak-travelEndDatePicker_day"));
			endDay.select(endDayValue);
			if (!endDay.getSelectedItem().equals(endDayValue)){
				endDay.select(endDayValue);
			}
		}
		if (inputMap.containsKey("end month")){
			String endMonthValue = inputMap.get("end month");
			waitForElement(By.id("rateRulePeak-travelEndDatePicker_months"));
			ComboBox endMonth = new ComboBox(By.id("rateRulePeak-travelEndDatePicker_months"));
			endMonth.select(endMonthValue);
			if (!endMonth.getSelectedItem().equals(endMonthValue)){
				endMonth.select(endMonthValue);
			}
		}
		if (inputMap.containsKey("end year")){
			String endYearValue = inputMap.get("end year");
			waitForElement(By.id("rateRulePeak-travelEndDatePicker_years"));
			ComboBox endYear = new ComboBox(By.id("rateRulePeak-travelEndDatePicker_years"));
			endYear.select(endYearValue);
			if (!endYear.getSelectedItem().equals(endYearValue)){
				endYear.select(endYearValue);
			}
		}
		
		if (inputMap.containsKey("minimum nights")){
			waitForElement(By.id("minNightsSelect"));
			ComboBox startDay = new ComboBox(By.id("minNightsSelect"));
			startDay.select(inputMap.get("minimum nights"));
		}
		if (inputMap.containsKey("minimum passengers")){
			waitForElement(By.id("minPax"));
			ComboBox startDay = new ComboBox(By.id("minPax"));
			startDay.select(inputMap.get("minimum passengers"));
		}

		if (inputMap.containsKey("rate type")){
			String value = inputMap.get("rate type");
			if (value.equalsIgnoreCase("Nett")){
				RadioButton rateTypeRB = new RadioButton(By.name("rateType"), "N");
				rateTypeRB.select();
			}else{
				RadioButton rateTypeRB = new RadioButton(By.name("rateType"), "Q");
				rateTypeRB.select();
			}
		}
		if (inputMap.containsKey("more rules")){
			RadioButton moreRulesRB = new RadioButton(By.name("moreRules"), inputMap.get("more rules"));
			moreRulesRB.select();
		}

		if (inputMap.containsKey("stay full period")){
			RadioButton moreRulesRB = new RadioButton(By.name("fullPeriod"), inputMap.get("stay full period"));
			moreRulesRB.select();
		}

		CheckBox roomType0 = new CheckBox(By.id("rateRulePeakCreate.roomCheck.0"));
		CheckBox roomType1 = new CheckBox(By.id("rateRulePeakCreate.roomCheck.1"));
		CheckBox roomType2 = new CheckBox(By.id("rateRulePeakCreate.roomCheck.2"));
		CheckBox roomType3 = new CheckBox(By.id("rateRulePeakCreate.roomCheck.3"));
		

		if (inputMap.containsKey("executive king bay view type")){
			roomType0.check();
		}else{
			roomType0.uncheck();
		}
		if (inputMap.containsKey("executive king bay view type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakCreate.nett.0"));
			stdRmNetTB.setText(inputMap.get("executive king bay view type net"));
		}
		if (inputMap.containsKey("executive king bay view type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakCreate.rsp.0"));
			stdRmRSPTB.setText(inputMap.get("executive king bay view type rsp"));
		}

		
		if (inputMap.containsKey("executive quad bay view type")){
			roomType1.check();
		}else{
			roomType1.uncheck();
		}
		if (inputMap.containsKey("executive quad bay view type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakCreate.nett.1"));
			stdRmNetTB.setText(inputMap.get("executive quad bay view type net"));
		}
		if (inputMap.containsKey("executive quad bay view type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakCreate.rsp.1"));
			stdRmRSPTB.setText(inputMap.get("executive quad bay view type rsp"));
		}

		
		if (inputMap.containsKey("standard double type")){
			roomType2.check();
		}else{
			roomType2.uncheck();
		}
		
		if (inputMap.containsKey("standard double type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakCreate.nett.2"));
			stdRmNetTB.setText(inputMap.get("standard double type net"));
		}
		if (inputMap.containsKey("standard double type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakCreate.rsp.2"));
			stdRmRSPTB.setText(inputMap.get("standard double type rsp"));
		}

		if (inputMap.containsKey("standard room type")){
			roomType3.check();
		}else{
			roomType3.uncheck();
		}
		if (inputMap.containsKey("standard room type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakCreate.nett.3"));
			stdRmNetTB.setText(inputMap.get("standard room type net"));
		}
		if (inputMap.containsKey("standard room type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakCreate.rsp.3"));
			stdRmRSPTB.setText(inputMap.get("standard room type rsp"));
		}
	}
	
	/**
	 * This method clicks the cancel button from create rate rule section.
	 */
	public void cancelCreatePeakRateRules(){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_create"));
		waitForElement(parentElement,By.id("rateRulePeakCreateCancel"));
		Button saveButton = new Button(parentElement, "Cancel");
		saveButton.click();
	}
	
	/**
	 * This method clicks the cancel button from add another section.
	 */
	public void cancelAddAnotherCreatePeakRateRules(){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_create"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForVisibilityOfElement(By.id("rateRulePeak_create"), 20);
		waitForVisibilityOfElement(By.id("rateRulePeakCreateCancel"), 20);
		
	}

	/**
	 * This method clicks add another button.
	 */
	public void addAnotherCreatePeakRateRule(){
		savePeakRateRules("Add Another");
	}
	
	/**
	 * This method save the rate rules
	 */
	public void saveCreatePeakRateRules(){
		savePeakRateRules("Save");
	}
	
	/**
	 * This method clicks the copy button.
	 */
	public void copyCreatePeakRateRules(){
		savePeakRateRules("Copy");
	}
	
	private void savePeakRateRules(String saveType){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_create"));
		Button saveButton = new Button(parentElement, saveType);
		saveButton.click();
		waitForInVisibilityOfElement(By.id("minPax"));
		waitForElement(By.className("newRatesHeading"));
	}
	
	/**
	 * Thsi method clicks the save button to confirm the save. 
	 */
	public void saveSaveCreatePeakRateRules(){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_create"));
		Button saveButton = new Button(parentElement, "Save");
		int countBefore = getPeakRateRuleTableRowCount();
		saveButton.click();
		for (int i=0;i<25;i++){
			try{
				int countAfter = getPeakRateRuleTableRowCount();
				if (countBefore < countAfter){
					break;
				}else{
					sleepms(400l);
				}
			}catch(Exception e){
				sleepms(400l);
				continue;
			}
		}
	}

	/**
	 * This method edit the values of the rate rules. 
	 * @param inputMap
	 */
	public void editPeakRateRules(Map<String, String> inputMap){
		
		if (inputMap.containsKey("more rules")){
			waitForElements(By.name("moreRules"));
			try{
				RadioButton moreRulesRB = new RadioButton(By.name("moreRules"), inputMap.get("more rules"));
				moreRulesRB.select();
			}catch(Exception e){
				sleep(2);
				RadioButton moreRulesRB = new RadioButton(By.name("moreRules"), inputMap.get("more rules"));
				moreRulesRB.select();
			}
		}

		if (inputMap.containsKey("stay full period")){
			waitForElements(By.id("fullPeriod"));
			try{
				RadioButton stayFullPeriodRB = new RadioButton(By.id("fullPeriod"), inputMap.get("stay full period"));
				stayFullPeriodRB.select();
			}catch(Exception e){
				sleep(2);
				RadioButton stayFullPeriodRB = new RadioButton(By.id("fullPeriod"), inputMap.get("stay full period"));
				stayFullPeriodRB.select();
			}
		}

		CheckBox roomType0 = new CheckBox(By.id("rateRulePeakEdit.roomCheck.0"));
		CheckBox roomType1 = new CheckBox(By.id("rateRulePeakEdit.roomCheck.1"));
		CheckBox roomType2 = new CheckBox(By.id("rateRulePeakEdit.roomCheck.2"));
		CheckBox roomType3 = new CheckBox(By.id("rateRulePeakEdit.roomCheck.3"));
		if (inputMap.containsKey("room type edit") && inputMap.get("room type edit").equalsIgnoreCase("Yes")){
			roomType0.uncheck();
			roomType1.uncheck();
			roomType2.uncheck();
			roomType3.uncheck();
		}
		if (inputMap.containsKey("executive king bay view type")){
			roomType0.check();
		}
		if (inputMap.containsKey("executive king bay view type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakEdit.nett.0"));
			stdRmNetTB.setText(inputMap.get("executive king bay view type net"));
		}
		if (inputMap.containsKey("executive king bay view type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakEdit.rsp.0"));
			stdRmRSPTB.setText(inputMap.get("executive king bay view type rsp"));
		}

		
		if (inputMap.containsKey("executive quad bay view type")){
			roomType1.check();
		}
		if (inputMap.containsKey("executive quad bay view type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakEdit.nett.1"));
			stdRmNetTB.setText(inputMap.get("executive quad bay view type net"));
		}
		if (inputMap.containsKey("executive quad bay view type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakEdit.rsp.1"));
			stdRmRSPTB.setText(inputMap.get("executive quad bay view type rsp"));
		}

		
		if (inputMap.containsKey("standard double type")){
			roomType2.check();
		}
		
		if (inputMap.containsKey("standard double type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakEdit.nett.2"));
			stdRmNetTB.setText(inputMap.get("standard double type net"));
		}
		if (inputMap.containsKey("standard double type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakEdit.rsp.2"));
			stdRmRSPTB.setText(inputMap.get("standard double type rsp"));
		}

		if (inputMap.containsKey("standard room type")){
			roomType3.check();
		}
		if (inputMap.containsKey("standard room type net")){
			TextBox stdRmNetTB = new TextBox(By.id("rateRulePeakEdit.nett.3"));
			stdRmNetTB.setText(inputMap.get("standard room type net"));
		}
		if (inputMap.containsKey("standard room type rsp")){
			TextBox stdRmRSPTB = new TextBox(By.id("rateRulePeakEdit.rsp.3"));
			stdRmRSPTB.setText(inputMap.get("standard room type rsp"));
		}
		
	}
	
	/**
	 * This method clicks the cancel button after edited the values.
	 */
	public void cancelEditPeakRateRules(){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_div"));
		waitForElement(parentElement, By.cssSelector("input[class='gsButton'][value='Cancel']"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForInVisibilityOfElement(waitForElement(By.id("rateRulePeak_div")), By.id("rateRulePeakEdit"));
	}
	
	/**
	 * This method clicks the update button to update the changes.
	 */
	public void updateEditPeakRateRules(){
		WebElement parentElement = waitForElement(By.id("rateRulePeak_div"));
		Button updateButton = new Button(parentElement, "Update");
		updateButton.click();
		waitForInVisibilityOfElement(By.id("rateRulePeak_tr"),40);
	}
	
	/**
	 * This method clicks the OK button on the alert box.
	 */
	public void deletePeakRateRuleWithOK(){
		acceptAlert();
	}

	/**
	 * This method clicks the Cancel button on the alert box.
	 */
	public void deletePeakRateRuleWithCancel(){
		dismissAlert();
	}
	
	/**
	 * This method sort the travel dates values and verify the sorting order.
	 * @return boolean
	 */
	public boolean sortOrderOnTravelDates(){
		return sort(0, "Date");
	}

	private boolean sort(int colNum, String objectType){

		boolean result = true;

		WebElement topParentEl = waitForElement(By.id("rateRulePeakContent"));
		WebElement tableDivEl = waitForElement(topParentEl,By.className("list"));
		WebElement chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		getElement(chainEl,By.tagName("a")).click();
		
		sleep(2);
		
		// Need to load the below objects again as the above click() funtion reload the div element.
		topParentEl = waitForElement(By.id("rateRulePeakContent"));
		tableDivEl = waitForElement(topParentEl,By.className("list"));
		chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
		String href = getElement(chainEl,By.tagName("a")).getAttribute("href");
		String sortOrder = "asc";
		
		if (href.contains("=desc")){
			sortOrder = "desc";
		}
		
		result = verifySortOrder(sortOrder, tableDivEl,colNum, objectType);
		if (result){
		
			getElement(chainEl,By.tagName("a")).click();
			
			sleep(2);
			
			// Need to load the below objects again as the above click() funtion reload the div element.
			topParentEl = waitForElement(By.id("rateRulePeakContent"));
			tableDivEl = waitForElement(topParentEl,By.className("list"));
			chainEl = getElements(tableDivEl,By.tagName("th")).get(colNum);
			href = getElement(chainEl,By.tagName("a")).getAttribute("href");
			sortOrder = "asc";
			
			if (href.contains("=desc")){
				sortOrder = "desc";
			}
			result = verifySortOrder(sortOrder, tableDivEl, colNum, objectType);
		}
		return result;
	}	
	private boolean verifySortOrder(String sortOrder, WebElement tableDivEl, int colNum, String objectType){
		boolean result = true;
		List<WebElement> trList = getTableRowsListFromDiv(tableDivEl);
		String prevValue = null;
		String currentValue = null;
		for (int i=1;i<trList.size();i++){
			WebElement tr = trList.get(i);
			List<WebElement> tdList = getElements(tr,By.tagName("td"));
			if (i==1){
				currentValue = tdList.get(colNum).getText();
			}else{
				prevValue = currentValue;
				currentValue = tdList.get(colNum).getText();
				if (sortOrder.equals("asc")){
					if (objectType.equals("String") && prevValue.compareToIgnoreCase(currentValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}else{
					if (objectType.equals("String") && currentValue.compareToIgnoreCase(prevValue)<0){
						result = false;
						break;
					}else if (objectType.equals("Date")){
						if (!compareSortOrderOnDateObjects(prevValue, currentValue, sortOrder)){
							result = false;
							break;
						}
					}
				}
			}
		}
		return result;
	}	
	private boolean compareSortOrderOnDateObjects(String prevValue, String currentValue, String sortOrder){
		String prevDateStr = prevValue.split("-")[0];
		String currentDateStr = currentValue.split("-")[0];
		
		String prevDateStrFormat = prevDateStr.substring(0,2) + "-" + prevDateStr.substring(2,5) + "-" + prevDateStr.substring(5);
		String currentDateStrFormat = currentDateStr.substring(0,2) + "-" + currentDateStr.substring(2,5) + "-" + currentDateStr.substring(5);
		
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");
		try {
			Date prevDateObj = format.parse(prevDateStrFormat);
			Date currentDateObj = format.parse(currentDateStrFormat);
			
			if (sortOrder.equals("asc")){
				if (prevDateObj.compareTo(currentDateObj)<0){
					return false;
				}
			}else{
				if (currentDateObj.compareTo(prevDateObj)<0){
					return false;
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}		

	/**
	 * This method collapse the reate rules section.
	 * @return boolean.
	 */
	public boolean collapsePeakRateRules(){
		WebElement parentEl = waitForElement(By.id("rateRulePeakListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("rateRulePeakArrow"));
		if (element.getAttribute("src").contains("ArrowRight")){
			return true;
		}
		return false;
	}
	/**
	 * This method expands the rate rules.
	 * @return
	 */
	public boolean expandPeakRateRules(){
		WebElement parentEl = waitForElement(By.id("rateRulePeakListSection"));
		getElement(parentEl, By.className("clickable")).click();
		sleep(1);
		WebElement element = waitForElement(By.id("rateRulePeakArrow"));
		if (element.getAttribute("src").contains("ArrowDown")){
			return true;
		}
		return false;
	}		
}
