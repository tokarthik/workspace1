package com.gta.travel.page.object.contract.search;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.ComboBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class ContractSearchPage extends GSPageBase{
	
	private static final Map<String, String> channelMap = new HashMap<String, String>();
	private static final Map<String, String> statusMap = new HashMap<String, String>();
	
	public ContractSearchPage(){
		
		super(getDriver());
		
		channelMap.put("Consumer (B2C)", "C");
		channelMap.put("Trade (B2B)", "T");
		channelMap.put("Trade Consumer (B2B2C)", "B");
		
		statusMap.put("Pending", "P");
		statusMap.put("Live", "L");
		statusMap.put("Suspended", "S");
		statusMap.put("Cancelled", "X");
	}
	
	public ContractSearchResultsPage search(Map<String, String> map){
		
		String country = null;
		String city = null;
		if (map.containsKey("Country")){
			country = map.get("Country");
			selectValueFromAjaxList("country-name", country.substring(0,3), "status", "country-choices", country, country,0);
		}
		if (country != null && map.containsKey("City")){
			city = map.get("City");
			selectValueFromAjaxList("city-name", city.substring(0,3), "status", "city-choices", city, city,0);
		}else if (map.containsKey("City")){
			TextBox textBox = new TextBox(By.id("city-name"));
			textBox.setText(map.get("City"));
		}
		if (map.containsKey("Star Rating")){
			ComboBox starRatingComboBox = new ComboBox(By.id("ratingId"));
			starRatingComboBox.select(map.get("Star Rating"));
		}
		if (map.containsKey("Property Name")){
			TextBox propertyNameTextBox = new TextBox(By.id("name"));
			propertyNameTextBox.setText(map.get("Property Name"));
		}
		if (map.containsKey("Property Type")){
			ComboBox propTypeComboBox = new ComboBox(By.id("type"));
			propTypeComboBox.select(map.get("Property Type"));
		}
		if (map.containsKey("Provider")){
			String provider = map.get("Provider");
			String title = provider + "(" + city + ")";
			selectValueFromAjaxList("provider-name", provider.substring(0,3), "status", "provider-choices", provider, title,0);
		}
		if (map.containsKey("Model")){
			ComboBox modelComboBox = new ComboBox(By.id("model"));
			modelComboBox.select(map.get("Model"));
		}
		if (map.containsKey("Channel")){
			
			CheckBox.uncheckAll(By.id("channel"));
			String[] channelArr = map.get("Channel").split(",");
			
			for (int i=0;i<channelArr.length;i++){
				CheckBox channelCheckBox = new CheckBox(By.id("channel"), channelMap.get(channelArr[i]));
				channelCheckBox.check();
			}
		}
		if (map.containsKey("Travel Dates")){
			setTravelDates(map);
		}
		if (map.containsKey("Status")){
			
			CheckBox.uncheckAll(By.id("status"));
			String[] statuslArr = map.get("Status").split(",");
			
			for (int i=0;i<statuslArr.length;i++){
				CheckBox statusCheckBox = new CheckBox(By.id("status"), statusMap.get(statuslArr[i]));
				statusCheckBox.check();
			}
		}
		
		Button searchButton = new Button("search");
		searchButton.click();
		
		if (map.containsKey("validation failure")){
			String value = map.get("validation failure");
			waitForElement(By.id(value),30);
		}else{
			WebElement parentEl = waitForElement(By.id("results"));
			try{
				waitForElement(parentEl, By.className("message"),1);
			}catch(Exception e){
				waitForElement(By.className("paginateButtons"),30);
			}
		}
		
		return PageFactory.initElements(getDriver(), ContractSearchResultsPage.class);
	}
	
	
	private void setTravelDates(Map<String, String> map){
		
		String travelDates = map.get("Travel Dates");
		String[] dateArr = travelDates.split("-");	
		String[] startDateArr = dateArr[0].split("/");
		String[] endDateArr = dateArr[1].split("/");
		
		ComboBox startDayComboBox = new ComboBox(By.id("propertyContractSearch-startDate_day"));
		int startDay = Integer.parseInt(startDateArr[0].trim());
		startDayComboBox.select(Integer.toString(startDay));
		ComboBox startMonthComboBox = new ComboBox(By.id("propertyContractSearch-startDate_months"));
		startMonthComboBox.select(startDateArr[1].trim());
		ComboBox startYearComboBox = new ComboBox(By.id("propertyContractSearch-startDate_years"));
		startYearComboBox.select("20" + startDateArr[2].trim());
	
		ComboBox endDayComboBox = new ComboBox(By.id("propertyContractSearch-endDate_day"));
		int endDay = Integer.parseInt(startDateArr[0].trim());
		endDayComboBox.select(Integer.toString(endDay));
		ComboBox endMonthComboBox = new ComboBox(By.id("propertyContractSearch-endDate_months"));
		endMonthComboBox.select(endDateArr[1].trim());
		ComboBox endYearComboBox = new ComboBox(By.id("propertyContractSearch-endDate_years"));
		endYearComboBox.select("20" + endDateArr[2].trim());
	}
	
	private void selectValueFromAjaxList(String textBoxId, String text, 
			String dummyTextBoxId, String choiceListId, String valueToSelect, String title, int noOfTimes){
		
		TextBox textBox = new TextBox(By.id(textBoxId));
		textBox.setText(text);
		TextBox dummyTextBox = new TextBox(By.id(dummyTextBoxId));
		dummyTextBox.clickTab();
		WebElement choicesEl = null;
		while(true){
			choicesEl = getElementIfItIsPresent(By.id(choiceListId));
			if (choicesEl != null){
				break;
			}
		}
		if (textBox.getText().length() == 3){
			if (choicesEl != null){
				if (waitAndGetElementIfItIsPresent(By.linkText(title),5) != null){
					Link link = new Link(choicesEl, valueToSelect, title);
					link.clickLink();
				}else if (noOfTimes == 0){
					noOfTimes++;
					selectValueFromAjaxList(textBoxId,valueToSelect,dummyTextBoxId,choiceListId,valueToSelect,title, noOfTimes);
				}
			}
		}
	}
	
	public void clearSearchFields(){
		
		TextBox countryNameTextBox = new TextBox(By.id("country-name"));
		countryNameTextBox.setText("");
		
		TextBox cityNameTextBox = new TextBox(By.id("city-name"));
		cityNameTextBox.setText("");

		TextBox propertyNameTextBox = new TextBox(By.id("name"));
		propertyNameTextBox.setText("");
	
		ComboBox propTypeComboBox = new ComboBox(By.id("type"));
		propTypeComboBox.select("");
		
		TextBox providerTextBox = new TextBox(By.id("provider-name"));
		providerTextBox.setText("");
		
		ComboBox modelComboBox = new ComboBox(By.id("model"));
		modelComboBox.select("");
		
		CheckBox.checkAll(By.id("channel"));
		CheckBox.checkAll(By.id("status"));
	}
}