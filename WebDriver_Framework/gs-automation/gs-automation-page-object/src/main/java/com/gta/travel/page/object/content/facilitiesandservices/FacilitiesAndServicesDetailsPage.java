package com.gta.travel.page.object.content.facilitiesandservices;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.gta.travel.page.base.GSPageBase;
import com.mediaocean.qa.framework.selenium.ui.controls.Button;
import com.mediaocean.qa.framework.selenium.ui.controls.CheckBox;
import com.mediaocean.qa.framework.selenium.ui.controls.Link;
import com.mediaocean.qa.framework.selenium.ui.controls.RadioButton;
import com.mediaocean.qa.framework.selenium.ui.controls.TextBox;

public class FacilitiesAndServicesDetailsPage extends GSPageBase {

	/**
	 * Default Constructor
	 */
	public FacilitiesAndServicesDetailsPage() {
		super(getDriver());
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * This method returns the page factory object for the Facilities And Services Details Page
	 * @return static
	 */
	public static FacilitiesAndServicesDetailsPage getInstance(){
		return PageFactory.initElements(getDriver(),  FacilitiesAndServicesDetailsPage.class);
	}
	
	/**
	 * This method clicks on the edit link of the facilities and services section
	 */
	public void clickEditFacilitiesAndServicesDetails() {
		WebElement parentElement = waitForElement(By.id("propertyFacilitiesContentMenuOptions"));
		Link editLink = new Link(parentElement, "Edit");
		editLink.clickLink();	
		waitForVisibilityOfElement((By.id("FEATURES-1")), 40);
	}
	
	/**
	 * This method verifies the error message for the Total Rooms 
	 * @return String
	 */
	public String verifyValidationMessageForNumberOfRooms() {
		WebElement parentElement = waitForElement(By.id("totalRooms-errors"));
		return parentElement.getText();
	}
	

	public void editFacilitiesAndServicesDetails(Map<String, String> inputMap) {
		
		if (inputMap.containsKey("TotalRooms")){
			TextBox totalRooms = new TextBox(By.id("totalRooms"));
			totalRooms.setText("");
			totalRooms.setText(inputMap.get("TotalRooms"));
		}
		
		if (inputMap.containsKey("Lifts")){
			TextBox lifts = new TextBox(By.id("lifts"));
			lifts.setText("");
			lifts.setText(inputMap.get("Lifts"));
		}
		
		if (inputMap.containsKey("Porterage")){
			String value = inputMap.get("Porterage");
			if (value.equalsIgnoreCase("true")){
				sleep(2);
				RadioButton porterageRB = new RadioButton(By.name("porterage.radio"), "true");
				if (!porterageRB.isSelected())
					porterageRB.select();
			}else{
				RadioButton porterageRB = new RadioButton(By.name("porterage.radio"), "false");
				if (!porterageRB.isSelected())
					porterageRB.select();
			}
		}
		
		if (inputMap.containsKey("PorterageTwentyFourHour")){
			String value = inputMap.get("PorterageTwentyFourHour");
			if (value.equalsIgnoreCase("true")){
				RadioButton porterageTwentyFourRB = new RadioButton(By.name("porterage.twentyfour.radio"), "true");
				porterageTwentyFourRB.select();
			}else{
				RadioButton porterageTwentyFourRB = new RadioButton(By.name("porterage.twentyfour.radio"), "false");
				porterageTwentyFourRB.select();
				waitForVisibilityOfElement(By.id("porterageFromTime"), 20);
			}
		}
		
		if (inputMap.containsKey("PorterageTimesFrom")){
			TextBox porterageTimesFrom = new TextBox(By.id("porterageFromTime"));
			porterageTimesFrom.setText("");
			porterageTimesFrom.setText(inputMap.get("PorterageTimesFrom"));
		}
		
		if (inputMap.containsKey("PorterageTimesTo")){
			TextBox porterageTimesTo = new TextBox(By.id("porterageToTime"));
			porterageTimesTo.setText("");
			porterageTimesTo.setText(inputMap.get("PorterageTimesTo"));
		}
		
		if (inputMap.containsKey("RoomService")){
			String value = inputMap.get("RoomService");
			if (value.equalsIgnoreCase("true")){
				RadioButton roomServiceRB = new RadioButton(By.name("roomService.radio"), "true");
				roomServiceRB.select();
			}else{
				RadioButton roomServiceRB = new RadioButton(By.name("roomService.radio"), "false");
				roomServiceRB.select();
			}
		}
		
		if (inputMap.containsKey("RoomServiceTwentyFourHour")){
			String value = inputMap.get("RoomServiceTwentyFourHour");
			if (value.equalsIgnoreCase("true")){
				RadioButton roomServiceTwentyFourRB = new RadioButton(By.name("roomService.twentyfour.radio"), "true");
				roomServiceTwentyFourRB.select();
			}else{
				RadioButton roomServiceTwentyFourRB = new RadioButton(By.name("roomService.twentyfour.radio"), "false");
				roomServiceTwentyFourRB.select();
				waitForVisibilityOfElement(By.id("roomServiceFromTime"), 20);
			}
		}
		
		if (inputMap.containsKey("TimesFrom")){
			TextBox timesFrom = new TextBox(By.id("roomServiceFromTime"));
			timesFrom.setText("");
			timesFrom.setText(inputMap.get("TimesFrom"));
		}
		
		if (inputMap.containsKey("TimesTo")){
			TextBox timesTo = new TextBox(By.id("roomServiceToTime"));
			timesTo.setText("");
			timesTo.setText(inputMap.get("TimesTo"));
		}
		
		if (inputMap.containsKey("EarlierBreakfast")){
			TextBox earlierBreakfast = new TextBox(By.id("earliestBreakfast"));
			earlierBreakfast.setText("");
			earlierBreakfast.setText(inputMap.get("EarlierBreakfast"));
		}
		
		if (inputMap.containsKey("MaidService")){
			String value = inputMap.get("MaidService");
			if (value.equalsIgnoreCase("true")){
				RadioButton maidServiceRB = new RadioButton(By.name("maidService.radio"), "true");
				maidServiceRB.select();
			}else{
				RadioButton maidServiceRB = new RadioButton(By.name("maidService.radio"), "false");
				maidServiceRB.select();
			}
		}
		
		if (inputMap.containsKey("MaidServiceFrequency")){
			String value = inputMap.get("MaidServiceFrequency");
			if (value.equalsIgnoreCase("true")){
				RadioButton maidServiceFrequencyRB = new RadioButton(By.name("maidService.frequency.radio"), "true");
				maidServiceFrequencyRB.select();
			}else{
				RadioButton maidServiceFrequencyRB = new RadioButton(By.name("maidService.frequency.radio"), "false");
				maidServiceFrequencyRB.select();
			}
		}
		

		
		if (inputMap.containsKey("Airport")){
			String value = inputMap.get("Airport");
			if (value.equalsIgnoreCase("true")){
				RadioButton airportRB = new RadioButton(By.name("propertyService.1.radio"), "true");
				airportRB.select();
			}else{
				RadioButton airportRB = new RadioButton(By.name("propertyService.1.radio"), "false");
				airportRB.select();
			}
		}
		
		if (inputMap.containsKey("AirportCharge")){
			String value = inputMap.get("AirportCharge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox airportCharge = new CheckBox(By.id("propertyService.1.chargeable"));
					airportCharge.check();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Centre")){
			String value = inputMap.get("Centre");
			if (value.equalsIgnoreCase("true")){
				RadioButton centreRB = new RadioButton(By.name("propertyService.2.radio"), "true");
				centreRB.select();
			}else{
				RadioButton centreRB = new RadioButton(By.name("propertyService.2.radio"), "false");
				centreRB.select();
			}
		}
		
		if (inputMap.containsKey("CentreCharge")){
			
			String value = inputMap.get("CentreCharge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox centreCharge = new CheckBox(By.id("propertyService.2.chargeable"));
					centreCharge.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Car")){
			String value = inputMap.get("Car");
			if (value.equalsIgnoreCase("true")){
				RadioButton carRB = new RadioButton(By.name("propertyService.3.radio"), "true");
				carRB.select();
			}else{
				RadioButton carRB = new RadioButton(By.name("propertyService.3.radio"), "false");
				carRB.select();
			}
		}
		
		if (inputMap.containsKey("CarCharge")){
			
			String value = inputMap.get("CarCharge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox carCharge = new CheckBox(By.id("propertyService.3.chargeable"));
					carCharge.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("Coach")){
			String value = inputMap.get("Coach");
			if (value.equalsIgnoreCase("true")){
				RadioButton coachRB = new RadioButton(By.name("propertyService.4.radio"), "true");
				coachRB.select();
			}else{
				RadioButton coachRB = new RadioButton(By.name("propertyService.4.radio"), "false");
				coachRB.select();
			}
		}
		
		if (inputMap.containsKey("CoachCharge")){
			
			String value = inputMap.get("CoachCharge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox coachCharge = new CheckBox(By.id("propertyService.4.chargeable"));
					coachCharge.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Valet")){
			String value = inputMap.get("Valet");
			if (value.equalsIgnoreCase("true")){
				RadioButton valetRB = new RadioButton(By.name("propertyService.5.radio"), "true");
				valetRB.select();
			}else{
				RadioButton valetRB = new RadioButton(By.name("propertyService.5.radio"), "false");
				valetRB.select();
			}
		}
		
		if (inputMap.containsKey("ValetCharge")){
			
			String value = inputMap.get("ValetCharge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox valetCharge = new CheckBox(By.id("propertyService.5.chargeable"));
					valetCharge.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("CoachDropOff")){
			String value = inputMap.get("CoachDropOff");
			if (value.equalsIgnoreCase("true")){
				RadioButton coachDropOffRB = new RadioButton(By.name("coachDropOff"), "true");
				coachDropOffRB.select();
			}else{
				RadioButton coachDropOffRB = new RadioButton(By.name("coachDropOff"), "false");
				coachDropOffRB.select();
			}
		}
		
		if (inputMap.containsKey("IndoorPools")){
			TextBox indoorPools = new TextBox(By.id("indoorPools"));
			indoorPools.setText("");
			indoorPools.setText(inputMap.get("IndoorPools"));
		}
		
		if (inputMap.containsKey("OutdoorPools")){
			TextBox outdoorPools = new TextBox(By.id("outdoorPools"));
			outdoorPools.setText("");
			outdoorPools.setText(inputMap.get("OutdoorPools"));
		}
		
		if (inputMap.containsKey("ChildrensPools")){
			TextBox childrensPools = new TextBox(By.id("childrensPools"));
			childrensPools.setText("");
			childrensPools.setText(inputMap.get("ChildrensPools"));
		}
		
		if (inputMap.containsKey("PropertyFacilities")) {

			sleep(2);
			waitForVisibilityOfElement(By.name("FEATURES"), 20);
			CheckBox.uncheckAll(By.name("FEATURES"));
			sleep(2);
			waitForVisibilityOfElement(By.id("FEATURES-20"), 20);

		}
		
		if (inputMap.containsKey("BabySitting")){
			String value = inputMap.get("BabySitting");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox babySitting = new CheckBox(By.id("FEATURES-1"));
					babySitting.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("BeautyParlour")){			
			String value = inputMap.get("BeautyParlour");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox beautyParlour = new CheckBox(By.id("FEATURES-5"));
					beautyParlour.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("BusinessCentre")){
			String value = inputMap.get("BusinessCentre");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox businessCentre = new CheckBox(By.id("FEATURES-81"));
					businessCentre.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Concierge")){
			String value = inputMap.get("Concierge");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox concierge = new CheckBox(By.id("FEATURES-4"));
					concierge.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("DisabledFacilities")){
			String value = inputMap.get("DisabledFacilities");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox disabledFacilities = new CheckBox(By.id("FEATURES-2"));
					disabledFacilities.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("HairSalon")){
			String value = inputMap.get("HairSalon");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox hairSalon = new CheckBox(By.id("FEATURES-7"));
					hairSalon.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("HighSpeedInternet")){
			String value = inputMap.get("HighSpeedInternet");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox highSpeedInternet = new CheckBox(By.id("FEATURES-6"));
					highSpeedInternet.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("InternetWiredChargeable")){
			String value = inputMap.get("InternetWiredChargeable");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox internetWiredChargeable = new CheckBox(By.id("FEATURES-8"));
					internetWiredChargeable.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("InternetWiredComplimentary")){
			String value = inputMap.get("InternetWiredComplimentary");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox internetWiredComplimentary = new CheckBox(By.id("FEATURES-9"));
					internetWiredComplimentary.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("InternetWirelessChargeable")){
			String value = inputMap.get("InternetWirelessChargeable");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox internetWirelessChargeable = new CheckBox(By.id("FEATURES-10"));
					internetWirelessChargeable.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("InternetWirelessComplimentary")){
			String value = inputMap.get("InternetWirelessComplimentary");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox internetWirelessComplimentary = new CheckBox(By.id("FEATURES-11"));
					internetWirelessComplimentary.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("PrivateBeach")){
			String value = inputMap.get("PrivateBeach");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox privateBeach = new CheckBox(By.id("FEATURES-12"));
					privateBeach.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("PublicBeach")){
			String value = inputMap.get("PublicBeach");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox publicBeach = new CheckBox(By.id("FEATURES-13"));
					publicBeach.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("SuperMarket")){
			String value = inputMap.get("SuperMarket");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox superMarket = new CheckBox(By.id("FEATURES-14"));
					superMarket.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("WheelChairAccess")){
			String value = inputMap.get("WheelChairAccess");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox wheelChairAccess = new CheckBox(By.id("FEATURES-3"));
					wheelChairAccess.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("BeachFront")){
			String value = inputMap.get("BeachFront");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox beachFront = new CheckBox(By.id("FEATURES-18"));
					beachFront.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("CarEssential")){
			String value = inputMap.get("CarEssential");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox carEssential = new CheckBox(By.id("FEATURES-15"));
					carEssential.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("NoCredtiCards")){
			String value = inputMap.get("NoCredtiCards");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox noCredtiCards = new CheckBox(By.id("FEATURES-16"));
					noCredtiCards.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("NonSmokingProperty")){
			String value = inputMap.get("NonSmokingProperty");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox nonSmokingProperty = new CheckBox(By.id("FEATURES-17"));
					nonSmokingProperty.check();
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("PetsAllowed")){
			String value = inputMap.get("PetsAllowed");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox petsAllowed = new CheckBox(By.id("FEATURES-19"));
					petsAllowed.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("BicycleHire")){
			String value = inputMap.get("BicycleHire");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox bicycleHire = new CheckBox(By.id("FEATURES-21"));
					bicycleHire.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("Casino")){
			String value = inputMap.get("Casino");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox casino = new CheckBox(By.id("FEATURES-23"));
					casino.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("CookeryClasses")){
			String value = inputMap.get("CookeryClasses");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox cookeryClasses = new CheckBox(By.id("FEATURES-24"));
					cookeryClasses.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("DiscoNightclub")){
			String value = inputMap.get("DiscoNightclub");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox discoNightclub = new CheckBox(By.id("FEATURES-25"));
					discoNightclub.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("Diving")){
			String value = inputMap.get("Diving");
			
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox diving = new CheckBox(By.id("FEATURES-38"));
					diving.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		if (inputMap.containsKey("FitnessCentre")){
			String value = inputMap.get("FitnessCentre");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox fitnessCentre = new CheckBox(By.id("FEATURES-28"));
					fitnessCentre.check();
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("Golf")){
			String value = inputMap.get("Golf");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox golf = new CheckBox(By.id("FEATURES-26"));	
					golf.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("JetSkiing")){
			String value = inputMap.get("JetSkiing");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox jetSkiing = new CheckBox(By.id("FEATURES-35"));
					jetSkiing.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("KidsClub")){
			String value = inputMap.get("KidsClub");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox kidsClub = new CheckBox(By.id("FEATURES-31"));
					kidsClub.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("Massage")){
			String value = inputMap.get("Massage");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox massage = new CheckBox(By.id("FEATURES-29"));
					massage.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("MiniGolf")){
			String value = inputMap.get("MiniGolf");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox miniGolf = new CheckBox(By.id("FEATURES-32"));
					miniGolf.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("MotorisedWaterSports")){
			String value = inputMap.get("MotorisedWaterSports");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox motorisedWaterSports = new CheckBox(By.id("FEATURES-46"));
					motorisedWaterSports.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("NaturalHotSpring")){
			String value = inputMap.get("NaturalHotSpring");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox naturalHotSpring = new CheckBox(By.id("FEATURES-39"));
					naturalHotSpring.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("NightlyEntertainment")){
			String value = inputMap.get("NightlyEntertainment");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox nightlyEntertainment = new CheckBox(By.id("FEATURES-33"));
					nightlyEntertainment.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("NonMotorisedWaterSports")){
			String value = inputMap.get("NonMotorisedWaterSports");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox nonMotorisedWaterSports = new CheckBox(By.id("FEATURES-47"));
					nonMotorisedWaterSports.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Sailing")){
			String value = inputMap.get("Sailing");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox sailing = new CheckBox(By.id("FEATURES-34"));
					sailing.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("Sauna")){
			String value = inputMap.get("Sauna");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox sauna = new CheckBox(By.id("FEATURES-30"));
					sauna.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Snorkelling")){
			String value = inputMap.get("Snorkelling");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox snorkelling = new CheckBox(By.id("FEATURES-36"));
					snorkelling.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		if (inputMap.containsKey("Spa")){
			String value = inputMap.get("Spa");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox spa = new CheckBox(By.id("FEATURES-27"));
					spa.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("TableTennis")){
			String value = inputMap.get("TableTennis");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox tableTennis = new CheckBox(By.id("FEATURES-42"));
					tableTennis.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Tennis")){
			String value = inputMap.get("Tennis");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox tennis = new CheckBox(By.id("FEATURES-41"));
					tennis.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("ThemePark")){
			String value = inputMap.get("ThemePark");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox themePark = new CheckBox(By.id("FEATURES-22"));
					themePark.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("TurkishBaths")){
			String value = inputMap.get("TurkishBaths");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox turkishBaths = new CheckBox(By.id("FEATURES-40"));
					turkishBaths.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Volleyball")){
			String value = inputMap.get("Volleyball");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox volleyball = new CheckBox(By.id("FEATURES-43"));
					volleyball.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("WaterPark")){
			String value = inputMap.get("WaterPark");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox waterPark = new CheckBox(By.id("FEATURES-37"));
					waterPark.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("WaterSports")){
			String value = inputMap.get("WaterSports");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox waterSports = new CheckBox(By.id("FEATURES-45"));
					waterSports.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("Windsurfing")){
			String value = inputMap.get("Windsurfing");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox windsurfing = new CheckBox(By.id("FEATURES-44"));
					windsurfing.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (inputMap.containsKey("WineTasting")){
			String value = inputMap.get("WineTasting");
			try {
				if (value.equalsIgnoreCase("true")){
					CheckBox wineTasting = new CheckBox(By.id("FEATURES-20"));
					wineTasting.check();
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}		
		
	}
	
	/**
	 * This method clicks on the cancel button in the facilities and services section
	 */
	public void cancelFacilitiesAndServicesDetails() {
		WebElement parentElement = waitForElement(By.id("propertyFacilitiesContent"));
		Button cancelButton = new Button(parentElement, "Cancel");
		cancelButton.click();
		waitForVisibilityOfElement((By.id("facilities_totalRooms_value")), 40);
	}
	
	/**
	 * This method clicks on the update button of the facilities and services section
	 */
	public void updateFacilitiesAndServicesDetails() {
		Button updateButton = new Button("propertyFacilitiesEdit");
		updateButton.click();
		waitForVisibilityOfElement(By.id("facilities_totalRooms_value"),20);
	}
	
	/**
	 * This method clicks on the update button of the facilities and services section
	 */
	public void updateFacilitiesAndServicesDetailsForInvalidNoOfRooms() {
		Button updateButton = new Button("propertyFacilitiesEdit");
		updateButton.click();
		sleep(2);
		waitForVisibilityOfElement(By.id("totalRooms-errors"),20);
	}
	
	/**
	 * This method verifies the record updated message and returns false upon
	 * failure
	 * @param parentLocator
	 * @param message
	 * @return boolean
	 */
	public boolean verifyRecordUpdatedMessage(By parentLocator, String message){
		WebElement parentEl = waitForElement(parentLocator);
		waitForElement(parentEl, By.className("message"));
		if (getElement(parentEl, By.className("message")).getText().contains(message)){
			return true;
		}
		return false;
	}

}
