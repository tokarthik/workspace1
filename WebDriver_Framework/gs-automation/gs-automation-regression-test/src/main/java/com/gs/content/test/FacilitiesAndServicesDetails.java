package com.gs.content.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.content.facilitiesandservices.FacilitiesAndServicesDetailsPage;

@Test(groups={"facilitiesAndServicesDetailsGroup"}, dependsOnGroups={"checkInDetailsGroup"})
public class FacilitiesAndServicesDetails extends GSTestBase {
	
	private FacilitiesAndServicesDetailsPage fcFacilitiesAndServicesDetailsPage;	
	
	@Test
	public void testCancelFacilitiesAndServicesDetails() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("TotalRooms", "0");
		
		fcFacilitiesAndServicesDetailsPage = FacilitiesAndServicesDetailsPage.getInstance();
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetailsForInvalidNoOfRooms();
		Assert.assertTrue(fcFacilitiesAndServicesDetailsPage.verifyValidationMessageForNumberOfRooms().trim().equals("The number of rooms in the property must be at least 1"));
		fcFacilitiesAndServicesDetailsPage.cancelFacilitiesAndServicesDetails();
		
	}
	
	@Test(dependsOnMethods="testCancelFacilitiesAndServicesDetails")
	public void testUpdateForTotalRoomsAndLifts() {
		
		fcFacilitiesAndServicesDetailsPage = FacilitiesAndServicesDetailsPage.getInstance();
		
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("TotalRooms", "100");
		inputMap.put("Lifts", "5");
		inputMap.put("Porterage", "true");
		inputMap.put("PorterageTwentyFourHour", "true");
		inputMap.put("RoomService", "true");
		inputMap.put("RoomServiceTwentyFourHour", "true");
		inputMap.put("MaidService", "false");
		inputMap.put("Airport", "true");
		inputMap.put("AirportCharge", "false");
		inputMap.put("CoachDropOff", "true");
		inputMap.put("IndoorPools", "1");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));

	}
	
	@Test(dependsOnMethods="testUpdateForTotalRoomsAndLifts")
	public void testUpdateForCheckBoxesAndCharges() {
		
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Porterage", "true");
		inputMap.put("PorterageTwentyFourHour", "false");
		inputMap.put("PorterageTimesFrom", "07:00");
		inputMap.put("PorterageTimesTo", "21:00");
		inputMap.put("RoomService", "true");
		inputMap.put("RoomServiceTwentyFourHour", "false");
		inputMap.put("TimesFrom", "07:00");
		inputMap.put("TimesTo", "21:00");
		inputMap.put("EarlierBreakfast", "07:00");
		inputMap.put("MaidService", "true");
		inputMap.put("MaidServiceFrequency", "true");		
		inputMap.put("Airport", "true");
		inputMap.put("AirportCharge", "true");
		inputMap.put("Centre", "true");
		inputMap.put("CentreCharge", "true");
		inputMap.put("Car", "true");
		inputMap.put("CarCharge", "false");
		inputMap.put("Coach", "true");
		inputMap.put("CoachCharge", "false");
		inputMap.put("Valet", "true");
		inputMap.put("ValetCharge", "false");
		inputMap.put("IndoorPools", "0");
		inputMap.put("OutdoorPools", "1");
		inputMap.put("ChildrensPools", "1");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForCheckBoxesAndCharges")
	public void testUpdateForPropertyFacilities() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyFacilities", "true");
		inputMap.put("Concierge", "true");
		inputMap.put("DisabledFacilities", "true");
		inputMap.put("InternetWirelessComplimentary", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForPropertyFacilities")
	public void testUpdateForPropertyInformation() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyInformation", "true");
		inputMap.put("NonSmokingProperty", "true");
		inputMap.put("PetsAllowed", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForPropertyInformation")
	public void testUpdateForPropertyActivities() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyActivities", "true");
		inputMap.put("FitnessCentre", "true");
		inputMap.put("MiniGolf", "true");
		inputMap.put("TableTennis", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}

}
