package com.gs.content.test;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.content.facilitiesandservices.LandMarkDistanceDetailsPage;

@Test(groups={"landMarkDistanceDetailsGroup"}, dependsOnGroups={"locationDetailsGroup"})
public class LandMarkDistanceDetails extends GSTestBase {
	
	private LandMarkDistanceDetailsPage fcLandMarkDistanceDetailsPage;
	
	@Test
	public void testLandMarkDetailsWithNoExistingLandMarkRecord() {
		fcLandMarkDistanceDetailsPage = LandMarkDistanceDetailsPage.getInstance();
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Airport", "London");
		
		int rowCount = fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1;
		if (rowCount >= 1) {
			for (int i=1;i<=rowCount;i++) {
				fcLandMarkDistanceDetailsPage.clickRowByRowNum(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1);
				fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Delete");
				fcLandMarkDistanceDetailsPage.deleteLandMarkDetailsWithOK();
				fcLandMarkDistanceDetailsPage.refreshMessage();
			}
		}
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() == 1) {
			fcLandMarkDistanceDetailsPage.clickCreateLandMarkDistanceDetails();
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.addAnotherLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"One or more Landmark Distance entries were successfully created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickCancel();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(1);
		}
	}
	
	@Test(dependsOnMethods="testLandMarkDetailsWithNoExistingLandMarkRecord")
	public void testLandMarkDetailsAiportWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("AirportDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.updateLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(1);
		}
	}
	
	@Test(dependsOnMethods="testLandMarkDetailsAiportWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsStation() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("LandMarkType", "Station");
		inputMap.put("Station", "Euston Railway Station");
		//inputMap.put("Station", "London Bridge");
		
		Map<String, String> landMarkRecordMap = new HashMap<String, String>();
		landMarkRecordMap.put("Type", "Station");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetailsForExistingTable();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.selectLandMarkType(landMarkRecordMap);
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsStation")
	public void testEditLandMarkDetailsStationWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("StationDistance", "20");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			
			fcLandMarkDistanceDetailsPage.enterEditLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.updateLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1);
		}
	}
	
	@Test(dependsOnMethods="testEditLandMarkDetailsStationWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsMetro() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("LandMarkType", "Metro");
		inputMap.put("Metro", "Barbican");
		
		Map<String, String> landMarkRecordMap = new HashMap<String, String>();
		landMarkRecordMap.put("Type", "Metro");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetailsForExistingTable();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.selectLandMarkType(landMarkRecordMap);
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsMetro")
	public void testEditLandMarkDetailsMetroWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Metro", "Barkingside");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterEditLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.updateLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1);
		}
	}
	
	@Test(dependsOnMethods="testEditLandMarkDetailsMetroWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsExhibition() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("LandMarkType", "Exhibition");
		inputMap.put("Exhibition", "London Arena");
		
		Map<String, String> landMarkRecordMap = new HashMap<String, String>();
		landMarkRecordMap.put("Type", "Exhibition");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetailsForExistingTable();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.selectLandMarkType(landMarkRecordMap);
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsExhibition")
	public void testEditLandMarkDetailsExhibitionWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("ExhibitionDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterEditLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.updateLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1);
		}
	}
	
	@Test(dependsOnMethods="testEditLandMarkDetailsExhibitionWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsPointOfInterest() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("LandMarkType", "Point of Interest");
		inputMap.put("PointOfInterest", "London Zoo");
		
		Map<String, String> landMarkRecordMap = new HashMap<String, String>();
		landMarkRecordMap.put("Type", "Point of Interest");
		
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetailsForExistingTable();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.selectLandMarkType(landMarkRecordMap);
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsPointOfInterest")
	public void testEditLandMarkDetailsPointOfInterestWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PointOfInterestDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterEditLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.updateLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
		}
	}
	
	@Test(dependsOnMethods="testEditLandMarkDetailsPointOfInterestWithExistingLandMarkRecord")
	public void testSortOnType(){ //9.1
		assertTrue(fcLandMarkDistanceDetailsPage.sortOnLandMarkType());
	}
	
	@Test(dependsOnMethods="testSortOnType")
	public void testSortOnDescription(){ //9.1
		assertTrue(fcLandMarkDistanceDetailsPage.sortOnLandMarkDescription());
	}
	
	@Test(dependsOnMethods="testSortOnDescription")
	public void testSortOnDistance(){ //9.1
		assertTrue(fcLandMarkDistanceDetailsPage.sortOnLandMarkDistance());
	}
	
	@Test(dependsOnMethods="testSortOnDistance")
	public void testDeleteLandMarkDetails() {
		int rowCount = fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1;
		for (int i=1;i<=rowCount;i++) {
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()-1);
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Delete");
			fcLandMarkDistanceDetailsPage.deleteLandMarkDetailsWithOK();
			fcLandMarkDistanceDetailsPage.refreshMessage();
		}
		assertTrue(fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount()==1);
	}

}
