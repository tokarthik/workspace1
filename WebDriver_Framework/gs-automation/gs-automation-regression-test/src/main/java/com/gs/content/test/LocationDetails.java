package com.gs.content.test;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.content.facilitiesandservices.LocationDetailsPage;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.content.search.ContentSearchResultsPage;

@Test(groups={"locationDetailsGroup"}, dependsOnGroups={"loginGroup"})
public class LocationDetails extends GSTestBase{
	
	private HomePage homePage;
	private ContentSearchPage searchPage;
	private ContentSearchResultsPage searchResultsPage;
	private LocationDetailsPage fcLocationDetailsPage;
	private TopLinksPage topLinksPage;
	
	@Test
	public void testHomePage(){
		topLinksPage = PageFactory.initElements(getDriver(),TopLinksPage.class);
		homePage = PageFactory.initElements(getDriver(),HomePage.class);
		searchPage = homePage.selectContent();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "country");
		String city = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", "1 Lexham Gardens");
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testEditLocationDetailsWithLatitudeLongitudeAndUpdate(){ 
		fcLocationDetailsPage = LocationDetailsPage.getInstance();
		topLinksPage.clickFacilitiesAndServices();
		Map<String, String> map = new HashMap<String, String>();
		map.put("Latitude", "+041.090000000000000");
		map.put("Longitude", "+028.079000000000000");
		map.put("Location", "Outside Centre");
		fcLocationDetailsPage.editLocationDetails(map);
		fcLocationDetailsPage.updateLocationDetails();
		assertTrue(fcLocationDetailsPage.verifyRecordUpdatedMessage(By.id("locationForm"),"Record updated"));
	}

	@Test(dependsOnMethods="testEditLocationDetailsWithLatitudeLongitudeAndUpdate")
	public void testEditLocationDetailsWithLocationAsCentralAndUpdate(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("Location", "Central");
		fcLocationDetailsPage.editLocationDetails(map);
		fcLocationDetailsPage.updateLocationDetails();
		assertTrue(fcLocationDetailsPage.verifyRecordUpdatedMessage(By.id("locationForm"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testEditLocationDetailsWithLocationAsCentralAndUpdate")
	public void testCancelLocationDetails(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("Location", "Beach/Waterfront");
		fcLocationDetailsPage.editLocationDetails(map);
		fcLocationDetailsPage.cancelLocationDetails();
		assertFalse(fcLocationDetailsPage.isMessageDisplayed());
	}

}
