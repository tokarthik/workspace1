package com.gs.contract.test.marginrateplans;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanValidMarketsSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;

public class RatePlanMarginValidMarketsTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanValidMarketsSectionPage validMarketSectionPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;

	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		  setBrowserProfilePath("C:\\FirefoxProfile");
		setBrowser("IE");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetMarketData(){ 
		validMarketSectionPage = RatePlanValidMarketsSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		selectRatePlan();
		validMarketSectionPage.resetData();
	}

	@Test(dependsOnMethods="testResetMarketData")
	public void testCreateForAllMarketsYesAndSave(){ //1
		validMarketSectionPage.saveEditValidMarkets("Yes");
		assertTrue(validMarketSectionPage.verifyRecordUpdatedMessage("Market for Rate Plan id","updated"));
	}

	@Test(dependsOnMethods="testCreateForAllMarketsYesAndSave")
	public void testCreateForAsianMarketsAndSave(){ //2
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("world wide", "No");
		inputMap.put("cb zones", "FE");

		validMarketSectionPage.clickEditValidMarkets();
		validMarketSectionPage.editValidMarkets(inputMap);
		validMarketSectionPage.saveEditValidMarkets("Yes");
		
		inputMap = new HashMap<String, String>();
		inputMap.put("cb zones", "FE");
		
		assertTrue(validMarketSectionPage.verifyUpdatedData(inputMap));
		assertTrue(validMarketSectionPage.verifyRecordUpdatedMessage("Market for Rate Plan id","created"));
		validMarketSectionPage.clickEditValidMarkets();
		assertEquals(validMarketSectionPage.getZonesCount(), 10);
		assertEquals(validMarketSectionPage.getZonesTickBoxCount(), 10);
		validMarketSectionPage.cancelEditValidMarkets();
	}
	@Test(dependsOnMethods="testCreateForAsianMarketsAndSave")
	public void testEditMarketToJapanAndUpdate(){ //3
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("world wide", "No");
		inputMap.put("zone openers", "FE");
		inputMap.put("countries", "FE-JP");

		validMarketSectionPage.clickEditValidMarkets();
		validMarketSectionPage.uncheckAllZones();
		validMarketSectionPage.editValidMarkets(inputMap);
		validMarketSectionPage.saveEditValidMarkets("No");

		inputMap = new HashMap<String, String>();
		inputMap.put("zone openers", "FE");
		inputMap.put("countries", "FE-JP");
		
		assertTrue(validMarketSectionPage.verifyUpdatedData(inputMap));
		assertTrue(validMarketSectionPage.verifyRecordUpdatedMessage("Market for Rate Plan id","updated"));
	}
	@Test(dependsOnMethods="testEditMarketToJapanAndUpdate")
	public void testEditMarketAndCancel(){ //4
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("world wide", "Yes");

		validMarketSectionPage.clickEditValidMarkets();
		validMarketSectionPage.editValidMarkets(inputMap);
		validMarketSectionPage.cancelEditValidMarkets();
		inputMap.put("world wide", "No");
		validMarketSectionPage.clickEditValidMarkets();
		assertTrue(validMarketSectionPage.verifyUpdatedData(inputMap));
		validMarketSectionPage.cancelEditValidMarkets();
	}


	@Test(dependsOnMethods="testEditMarketAndCancel")
	public void testEditMarketForMultipleZonesAndUpdate(){ //5
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("world wide", "No");
		inputMap.put("cb zones", "AF,FE,CB");
		inputMap.put("market name", "TestName,");

		validMarketSectionPage.clickEditValidMarkets();
		validMarketSectionPage.uncheckAllZones();
		validMarketSectionPage.editValidMarkets(inputMap);
		validMarketSectionPage.saveEditValidMarkets("No");

		inputMap = new HashMap<String, String>();
		inputMap.put("cb zones", "AF,FE,CB,");
		inputMap.put("market name", "TestName,");
		
		assertTrue(validMarketSectionPage.verifyUpdatedData(inputMap));
		assertTrue(validMarketSectionPage.verifyRecordUpdatedMessage("Market for Rate Plan id","updated"));
	}
	@Test(dependsOnMethods="testEditMarketForMultipleZonesAndUpdate")
	public void testEditToAllMarketsAndUpdate(){ //6
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("world wide", "Yes");

		validMarketSectionPage.clickEditValidMarkets();
		validMarketSectionPage.editValidMarkets(inputMap);
		validMarketSectionPage.saveEditValidMarkets("No");
		assertTrue(validMarketSectionPage.verifyRecordUpdatedMessage("Market for Rate Plan id","updated"));
		validMarketSectionPage.clickEditValidMarkets();
		assertTrue(validMarketSectionPage.verifyUpdatedData(inputMap));
	}
	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
