package com.gs.content.test.facilitiesandservices;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.content.facilitiesandservices.CheckInDetailsPage;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.content.search.ContentSearchResultsPage;

public class CheckInDetailsTest extends GSTestBase {
	
	private LoginPage loginPage;
	private HomePage homePage;
	private ContentSearchPage searchPage;
	private ContentSearchResultsPage searchResultsPage;
	private CheckInDetailsPage fcCheckInDetailsPage;
	private TopLinksPage topLinksPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
		topLinksPage = PageFactory.initElements(getDriver(),TopLinksPage.class);
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContent();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "country");
		String city = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", "1 Lexham Gardens");
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testCancelCheckInDetails() {
		Map<String, String> inputMap = new HashMap<String, String>();
		String[] arrCheckInRowValBefore = new String[4];
		String[] arrCheckInRowValAfter = new String[4];
		int i;
		inputMap.put("CheckInTime", "14:00");
		inputMap.put("CheckOutTime", "12:00");
		inputMap.put("CheckInDays", "All");
		inputMap.put("CheckOutDays", "All");
		
		fcCheckInDetailsPage = CheckInDetailsPage.getInstance();
		topLinksPage.clickFacilitiesAndServices();
		
		/* Retrieve all the row values before updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValBefore[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		fcCheckInDetailsPage.clickEditCheckInDetails();
		fcCheckInDetailsPage.editCheckInDetails(inputMap);
		fcCheckInDetailsPage.cancelCheckInDetails();
		
		/* Retrieve all the row values after updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValAfter[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		for (int j = 1;j<=arrCheckInRowValBefore.length;j++) {
			assertTrue(arrCheckInRowValBefore[j-1].equals(arrCheckInRowValAfter[j-1]));
		}
	}
	
	@Test(dependsOnMethods="testCancelCheckInDetails")
	public void testEditCheckOutTime() {
		Map<String, String> inputMap = new HashMap<String, String>();
		String[] arrCheckInRowValBefore = new String[4];
		String[] arrCheckInRowValAfter = new String[4];
		int i;
		inputMap.put("CheckInTime", "14:00");
		inputMap.put("CheckOutTime", "11:00");
		inputMap.put("CheckInDays", "Mon,Tue,Wed,Thu,Fri");
		inputMap.put("CheckOutDays", "Mon,Tue,Wed,Thu,Fri");
		
		/* Retrieve all the row values before updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValBefore[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		fcCheckInDetailsPage.clickEditCheckInDetails();
		fcCheckInDetailsPage.editCheckInDetails(inputMap);
		fcCheckInDetailsPage.updateCheckInDetails();
		
		/* Retrieve all the row values after updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValAfter[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		for (int j = 1;j<arrCheckInRowValBefore.length;j++) {
			if (arrCheckInRowValBefore[0].equals(arrCheckInRowValAfter[0]))
				assertTrue(true, "No Change for Check In Time");
			if (!arrCheckInRowValBefore[j].equals(arrCheckInRowValAfter[j]))
				assertTrue(true, "Changes made successfully for checkout time, checkin days and checkout days");
		}
	}
	
	@Test(dependsOnMethods="testEditCheckOutTime")
	public void testEditCheckInDays() {
		Map<String, String> inputMap = new HashMap<String, String>();
		String[] arrCheckInRowValBefore = new String[4];
		String[] arrCheckInRowValAfter = new String[4];
		int i;
		inputMap.put("CheckInTime", "14:00");
		inputMap.put("CheckOutTime", "12:00");
		inputMap.put("CheckInDays", "All");
		inputMap.put("CheckOutDays", "Mon,Tue,Wed,Thu,Fri");
		
		/* Retrieve all the row values before updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValBefore[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		fcCheckInDetailsPage.clickEditCheckInDetails();
		fcCheckInDetailsPage.editCheckInDetails(inputMap);
		fcCheckInDetailsPage.updateCheckInDetails();
		
		/* Retrieve all the row values after updating the check in details */
		for (i=1;i<=fcCheckInDetailsPage.getcheckInDetailsRows();i++) {
			arrCheckInRowValAfter[i-1] = fcCheckInDetailsPage.getcheckInDetailsRow(i).getText();
		}
		
		if (arrCheckInRowValBefore[0].equals(arrCheckInRowValAfter[0]))
			assertTrue(true, "No Change for Check In Time");
		
		if (arrCheckInRowValBefore[3].equals(arrCheckInRowValAfter[3]))
			assertTrue(true, "No Change for Check Out Days");
		
		for (int j = 1;j<arrCheckInRowValBefore.length-1;j++) {
			if (!arrCheckInRowValBefore[j].equals(arrCheckInRowValAfter[j]))
				assertTrue(true, "Changes made successfully for checkout time and checkin days");
		}
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

}
