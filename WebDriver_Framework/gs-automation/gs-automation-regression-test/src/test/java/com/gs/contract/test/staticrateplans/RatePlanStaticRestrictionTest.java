package com.gs.contract.test.staticrateplans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanRestrictionSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class RatePlanStaticRestrictionTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanRestrictionSectionPage rpRestrictionSectionPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetRestrictionData(){
		rpRestrictionSectionPage = RatePlanRestrictionSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		selectRatePlan();
		rpRestrictionSectionPage.resetData();
	}

	@Test(dependsOnMethods="testResetRestrictionData")
	public void testCreateRestrictionAndCancel(){ //1
		Map<String, String> map = new HashMap<String, String>();
		map.put("override base restriction", "true");

		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestrictionOptions("Create");
		rpRestrictionSectionPage.createRestriction(map);
		rpRestrictionSectionPage.cancelCreateRestriction();

		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore));
	}

	@Test(dependsOnMethods="testCreateRestrictionAndCancel")
	public void testCreateRestrictionWithAmendmentAndAddAnother(){ //2
		Map<String, String> map = new HashMap<String, String>();
		map.put("override base restriction", "true");
		map.put("restriction type", "Amendment");
		map.put("applicable from", "Immediate");
		
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);

		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestrictionOptions("Create");
		rpRestrictionSectionPage.createRestriction(map);
		rpRestrictionSectionPage.addAnotherRestriction();

		assertTrue(rpRestrictionSectionPage.verifyRecordUpdatedMessage(By.id("restriction_list"),"Record created"));
		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore+1));
		assertTrue(rpRestrictionSectionPage.isCreateRecordSectionDisplayed());

	}

	@Test(dependsOnMethods="testCreateRestrictionWithAmendmentAndAddAnother")
	public void testCreateRestrictionWithNameChangeAndSave(){ //3
		Map<String, String> map = new HashMap<String, String>();
		map.put("restriction type", "Name change");
		map.put("applicable from", "30");
		
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);


		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestrictionOptions("Create");
		rpRestrictionSectionPage.createRestriction(map);
		rpRestrictionSectionPage.saveRestriction();

		assertTrue(rpRestrictionSectionPage.verifyRecordUpdatedMessage(By.id("restriction_list"),"Record created"));
		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore+1));
	}

	@Test(dependsOnMethods="testCreateRestrictionWithNameChangeAndSave")
	public void testCreateRestrictionWithEarlyDepartureAndSave(){ //4
		Map<String, String> map = new HashMap<String, String>();
		map.put("restriction type", "Early departure");
		
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);


		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestrictionOptions("Create");
		rpRestrictionSectionPage.createRestriction(map);
		rpRestrictionSectionPage.saveRestriction();

		assertTrue(rpRestrictionSectionPage.verifyRecordUpdatedMessage(By.id("restriction_list"),"Record created"));
		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore+1));
	}

	@Test(dependsOnMethods="testCreateRestrictionWithEarlyDepartureAndSave")
	public void testEditRestrictionWith30DaysPriorAndUpdate(){ //5
		Map<String, String> map = new HashMap<String, String>();
		map.put("applicable from", "30");
		
		Map<String, String> restrictionRecordMap = new HashMap<String, String>();
		restrictionRecordMap.put("Restriction Type", "Amendment");
		restrictionRecordMap.put("Days Prior to Arrival", "Immediate");

		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		rpRestrictionSectionPage.selectRestriction(restrictionRecordMap);
		
		rpRestrictionSectionPage.selectRestrictionOptions("Edit");
		rpRestrictionSectionPage.editRestriction(map);
		rpRestrictionSectionPage.updateRestriction();
		
		assertTrue(rpRestrictionSectionPage.verifyRecordUpdatedMessage(By.id("restriction_list"),"Record updated"));
	}

	@Test(dependsOnMethods="testEditRestrictionWith30DaysPriorAndUpdate")
	public void testEditRestrictionWith10DaysPriorAndUpdate(){ //6
		Map<String, String> map = new HashMap<String, String>();
		map.put("applicable from", "30");
		map.put("restriction type", "Name change");

		
		Map<String, String> restrictionRecordMap = new HashMap<String, String>();
		restrictionRecordMap.put("Restriction Type", "Name change");
		restrictionRecordMap.put("Days Prior to Arrival", "30 days");

		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		rpRestrictionSectionPage.selectRestriction(restrictionRecordMap);
		
		rpRestrictionSectionPage.selectRestrictionOptions("Edit");
		rpRestrictionSectionPage.editRestriction(map);
		rpRestrictionSectionPage.updateRestriction();
		
		assertTrue(rpRestrictionSectionPage.verifyRecordUpdatedMessage(By.id("restriction_list"),"Record updated"));
	}

	@Test(dependsOnMethods="testEditRestrictionWith10DaysPriorAndUpdate")
	public void testSortOnRestrictionType(){ //7.1
		assertTrue(rpRestrictionSectionPage.sortOrderOnRestrictionType());
	}

	@Test(dependsOnMethods="testSortOnRestrictionType")
	public void testSortOnDayTimePrior(){ //7.2
		assertTrue(rpRestrictionSectionPage.sortOrderOnDayTime());
	}

	@Test(dependsOnMethods="testSortOnDayTimePrior")
	public void testRefresh(){ //7.3
		/**  Already convered in above test cases */
	}

	@Test(dependsOnMethods="testRefresh")
	public void testDeleteRestrictionAndCancel(){ //8
		Map<String, String> restrictionRecordMap = new HashMap<String, String>();
		restrictionRecordMap.put("Restriction Type", "Early departure");

		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestriction(restrictionRecordMap);
		rpRestrictionSectionPage.selectRestrictionOptions("Delete");
		rpRestrictionSectionPage.deleteRestrictionWithCancel();
		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore));
	}

	@Test(dependsOnMethods="testDeleteRestrictionAndCancel")
	public void testDeleteRestrictionAndOK(){ //9
		Map<String, String> restrictionRecordMap = new HashMap<String, String>();
		restrictionRecordMap.put("Restriction Type", "Early departure");

		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		int rowCountBefore = rpRestrictionSectionPage.getRestrictionTableRowCount();
		rpRestrictionSectionPage.selectRestriction(restrictionRecordMap);
		rpRestrictionSectionPage.selectRestrictionOptions("Delete");
		rpRestrictionSectionPage.deleteRestrictionWithOK();
		rpRestrictionSectionPage.selectRestrictionOptions("Refresh");
		assertTrue(rpRestrictionSectionPage.verifyRowcount(rowCountBefore-1));
	}

	@Test(dependsOnMethods="testDeleteRestrictionAndOK")
	public void testCollapseRestrictionSection(){ //10.2
		assertTrue(rpRestrictionSectionPage.collapseRestriction());
	}

	@Test(dependsOnMethods="testCollapseRestrictionSection")
	public void testExpandRestrictionSection(){ //10.1
		assertTrue(rpRestrictionSectionPage.expandRestriction());
	}

	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
