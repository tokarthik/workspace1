package com.gs.content.test.facilitiesandservices;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.content.facilitiesandservices.FacilitiesAndServicesDetailsPage;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.content.search.ContentSearchResultsPage;

public class FacilitiesAndServicesDetailsTest extends GSTestBase {
	
	private LoginPage loginPage;
	private HomePage homePage;
	private ContentSearchPage searchPage;
	private ContentSearchResultsPage searchResultsPage;
	private FacilitiesAndServicesDetailsPage fcFacilitiesAndServicesDetailsPage;
	private TopLinksPage topLinksPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
		topLinksPage = PageFactory.initElements(getDriver(),TopLinksPage.class);
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContent();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "country");
		String city = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", "1 Lexham Gardens");
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testCancelFacilitiesAndServicesDetails() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("TotalRooms", "0");
		
		fcFacilitiesAndServicesDetailsPage = FacilitiesAndServicesDetailsPage.getInstance();
		topLinksPage.clickFacilitiesAndServices();
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetailsForInvalidNoOfRooms();
		Assert.assertTrue(fcFacilitiesAndServicesDetailsPage.verifyValidationMessageForNumberOfRooms().trim().equals("The number of rooms in the property must be at least 1"));
		fcFacilitiesAndServicesDetailsPage.cancelFacilitiesAndServicesDetails();
		
	}
	
	@Test(dependsOnMethods="testCancelFacilitiesAndServicesDetails")
	public void testUpdateForTotalRoomsAndLifts() {
		
		fcFacilitiesAndServicesDetailsPage = FacilitiesAndServicesDetailsPage.getInstance();
		topLinksPage.clickFacilitiesAndServices();
		
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("TotalRooms", "100");
		inputMap.put("Lifts", "5");
		inputMap.put("Porterage", "true");
		inputMap.put("PorterageTwentyFourHour", "true");
		inputMap.put("RoomService", "true");
		inputMap.put("RoomServiceTwentyFourHour", "true");
		inputMap.put("MaidService", "false");
		inputMap.put("Airport", "true");
		inputMap.put("AirportCharge", "false");
		inputMap.put("CoachDropOff", "true");
		inputMap.put("IndoorPools", "1");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));

	}
	
	@Test(dependsOnMethods="testUpdateForTotalRoomsAndLifts")
	public void testUpdateForCheckBoxesAndCharges() {
		
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Porterage", "true");
		inputMap.put("PorterageTwentyFourHour", "false");
		inputMap.put("PorterageTimesFrom", "07:00");
		inputMap.put("PorterageTimesTo", "21:00");
		inputMap.put("RoomService", "true");
		inputMap.put("RoomServiceTwentyFourHour", "false");
		inputMap.put("TimesFrom", "07:00");
		inputMap.put("TimesTo", "21:00");
		inputMap.put("EarlierBreakfast", "07:00");
		inputMap.put("MaidService", "true");
		inputMap.put("MaidServiceFrequency", "true");		
		inputMap.put("Airport", "true");
		inputMap.put("AirportCharge", "true");
		inputMap.put("Centre", "true");
		inputMap.put("CentreCharge", "true");
		inputMap.put("Car", "true");
		inputMap.put("CarCharge", "false");
		inputMap.put("Coach", "true");
		inputMap.put("CoachCharge", "false");
		inputMap.put("Valet", "true");
		inputMap.put("ValetCharge", "false");
		inputMap.put("IndoorPools", "0");
		inputMap.put("OutdoorPools", "1");
		inputMap.put("ChildrensPools", "1");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForCheckBoxesAndCharges")
	public void testUpdateForPropertyFacilities() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyFacilities", "true");
		inputMap.put("Concierge", "true");
		inputMap.put("DisabledFacilities", "true");
		inputMap.put("InternetWirelessComplimentary", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForPropertyFacilities")
	public void testUpdateForPropertyInformation() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyInformation", "true");
		inputMap.put("NonSmokingProperty", "true");
		inputMap.put("PetsAllowed", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateForPropertyInformation")
	public void testUpdateForPropertyActivities() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("PropertyActivities", "true");
		inputMap.put("FitnessCentre", "true");
		inputMap.put("MiniGolf", "true");
		inputMap.put("TableTennis", "true");
		
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.editFacilitiesAndServicesDetails(inputMap);
		fcFacilitiesAndServicesDetailsPage.updateFacilitiesAndServicesDetails();
		fcFacilitiesAndServicesDetailsPage.clickEditFacilitiesAndServicesDetails();
		assertTrue(fcFacilitiesAndServicesDetailsPage.verifyRecordUpdatedMessage(By.id("propertyFacilitiesContent"), "Record updated"));
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

}
