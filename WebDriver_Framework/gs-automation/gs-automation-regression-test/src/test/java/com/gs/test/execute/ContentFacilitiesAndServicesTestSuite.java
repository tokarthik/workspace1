package com.gs.test.execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.gs.common.test.LoginTest;
import com.gs.common.test.LogoutTest;
import com.gs.common.test.SetUpTest;
import com.gs.content.test.CheckInDetails;
import com.gs.content.test.FacilitiesAndServicesDetails;
import com.gs.content.test.LandMarkDistanceDetails;
import com.gs.content.test.LocationDetails;


public class ContentFacilitiesAndServicesTestSuite  { 
	public static void main(String args[]) throws Exception{
		ContentFacilitiesAndServicesTestSuite t = new ContentFacilitiesAndServicesTestSuite();
		t.method();
	} 
	public void method() throws Exception{
		
		//for (int i=0;i<20;i++){
			TestListenerAdapter tla = new TestListenerAdapter();
	        TestNG testng = new TestNG();
	        testng.setTestClasses(new Class[] { 
	        		SetUpTest.class,
					LoginTest.class,
					LocationDetails.class,
					LandMarkDistanceDetails.class,CheckInDetails.class,FacilitiesAndServicesDetails.class,
					LogoutTest.class
	        								  });
	        testng.addListener(tla);
	        testng.run();  
//	        if (tla.getFailedTests().size() > 0){
//	        	break;
//	        }
	        //System.out.println(">>>>> Count : "+ i);
		//}
	}
}