package com.gs.content.test;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.content.facilitiesandservices.LandMarkDistanceDetailsPage;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.content.search.ContentSearchResultsPage;

public class LandMarkDistanceDetailsTest extends GSTestBase {
	
	private LoginPage loginPage;
	private HomePage homePage;
	private ContentSearchPage searchPage;
	private ContentSearchResultsPage searchResultsPage;
	private LandMarkDistanceDetailsPage fcLandMarkDistanceDetailsPage;
	private TopLinksPage topLinksPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
		topLinksPage = PageFactory.initElements(getDriver(),TopLinksPage.class);
	}

	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContent();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContentSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContentSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", "1 Lexham Gardens");
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testLandMarkDetailsWithNoExistingLandMarkRecord() {
		fcLandMarkDistanceDetailsPage = LandMarkDistanceDetailsPage.getInstance();
		topLinksPage.clickFacilitiesAndServices();
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Airport", "London");
		inputMap.put("AirportDistance", "10");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() == 1) {
			fcLandMarkDistanceDetailsPage.clickCreateLandMarkDistanceDetails();
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.addAnotherLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"One or more Landmark Distance entries were successfully created"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"One or more Landmark Distance entries were successfully created"));
		}
	}
	
	@Test(dependsOnMethods="testLandMarkDetailsWithNoExistingLandMarkRecord")
	public void testLandMarkDetailsAiportWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Airport", "East London");
		inputMap.put("AirportDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
		}
	}
	
	@Test(dependsOnMethods="testLandMarkDetailsAiportWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsStation() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Station", "Central");
		inputMap.put("StationDistance", "10");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsStation")
	public void testEditLandMarkDetailsStationWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Station", "East London");
		inputMap.put("StationDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
		}
	}
	
	@Test(dependsOnMethods="testLandMarkDetailsAiportWithExistingLandMarkRecord")
	public void testCreateLandMarkDetailsMetro() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Metro", "Central");
		inputMap.put("MetroDistance", "10");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Create");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Refresh");
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record created"));
		}
	}
	
	@Test(dependsOnMethods="testCreateLandMarkDetailsMetro")
	public void testEditLandMarkDetailsMetroWithExistingLandMarkRecord() {
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Metro", "East London");
		inputMap.put("MetroDistance", "20");
		if (fcLandMarkDistanceDetailsPage.getLandMarkDistanceDetailsRowCount() > 1) {
			fcLandMarkDistanceDetailsPage.selectLandMarkDistanceDetailsOptions("Edit");
			fcLandMarkDistanceDetailsPage.enterLandMarkDistanceDetailsSection(inputMap);
			fcLandMarkDistanceDetailsPage.saveLandMarkDistanceDetails();
			assertTrue(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
			fcLandMarkDistanceDetailsPage.refreshMessage();
			fcLandMarkDistanceDetailsPage.clickRowByRowNum(2);
			assertFalse(fcLandMarkDistanceDetailsPage.verifyMessage(By.id("landmarkDistance_list"),"Record updated"));
		}
	}

}
