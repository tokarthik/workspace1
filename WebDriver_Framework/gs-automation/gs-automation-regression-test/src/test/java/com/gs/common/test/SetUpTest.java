package com.gs.common.test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeGroups;

import com.gta.travel.page.base.GSTestBase;

/**
 * This class is the set up class which is
 * responsible to initialise the environment
 * and open the browser.
 * 
 * @author Karthikeyan
 *
 */
public class SetUpTest extends GSTestBase {

	/**
	 * This method initialise the test
	 */
	@BeforeGroups(groups={"loginGroup"})
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	/**
	 * This method will finalise the test by closing the 
	 * driver object.
	 */
	@AfterTest(groups={"contractSearchGroup"})
	public void close(){
		finaliseTest();
	}		
}
