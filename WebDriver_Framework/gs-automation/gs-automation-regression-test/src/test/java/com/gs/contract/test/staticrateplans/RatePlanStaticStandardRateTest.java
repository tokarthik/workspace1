package com.gs.contract.test.staticrateplans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanStandardRateSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;
/**
 * This is the test class for Rate Plan standard rate section for static hotels.
 * @author Karthikeyan
 *
 */
public class RatePlanStaticStandardRateTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	private RatePlanStandardRateSectionPage rpStandardRateSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetStandardRateData(){ 
		rpStandardRateSectionPage = RatePlanStandardRateSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		selectRatePlan();
		rpStandardRateSectionPage.resetData();
	}

	@Test(dependsOnMethods="testResetStandardRateData")
	public void testCreateStdRoomAndSaveAndSave(){ //1
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));
		Date futureDate = DateUtil.addDays(new Date(), 20);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "All");
		map.put("standard room type", "yes");
		map.put("standard room type net", "100");

		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.saveCreateRateRules();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record created"));
	}
	
	@Test(dependsOnMethods="testCreateStdRoomAndSaveAndSave")
	public void testEditStdRoomAndCancel(){ //2
		Map<String, String> map = new HashMap<String, String>();
		map.put("standard room type net", "110");
		map.put("room type edit", "no");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Days", "Mon, Tue, Wed, Thu, Fri, Sat, Sun");
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.cancelEditRateRules();
		rpStandardRateSectionPage.selectRateRuleOptions("Refresh");
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditStdRoomAndCancel")
	public void testEditStdRoomAndUpdate(){ //3
		Map<String, String> map = new HashMap<String, String>();
		map.put("standard room type net", "110");
		map.put("room type edit", "no");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Days", "Mon, Tue, Wed, Thu, Fri, Sat, Sun");
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.updateEditRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record updated"));
		rpStandardRateSectionPage.refreshOptionClick();
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditStdRoomAndUpdate")
	public void testCreateStdRoomAndAddAnotherAndCancel(){ //4
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 40);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Mon,Tue,Wed,Thu,Fri");
		map.put("standard room type", "yes");
		map.put("standard room type net", "100");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.addAnotherCreateRateRule();
		rpStandardRateSectionPage.cancelAddAnotherCreateRateRules();
		rpStandardRateSectionPage.cancelCreateRateRules();
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomAndAddAnotherAndCancel")
	public void testCreateStdRoomAndAddAnotherAndSave(){ //5
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 40);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Mon,Tue,Wed,Thu,Fri");
		map.put("standard room type", "yes");
		map.put("standard room type net", "100");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.addAnotherCreateRateRule();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRule_create"),"Record created"));
		rpStandardRateSectionPage.refreshOptionClick();
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomAndAddAnotherAndSave")
	public void testCreateStdRoomWith3MinNightsAndSaveAndSave(){ //6
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 40);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Mon,Tue,Wed,Thu,Fri");
		map.put("minimum nights", "3");
		map.put("minimum passengers", "2");
		map.put("standard room type", "yes");
		map.put("standard room type net", "95");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.saveCreateRateRules();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record created"));
		rpStandardRateSectionPage.refreshOptionClick();
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomWith3MinNightsAndSaveAndSave")
	public void testCreateStdRoomAndCopyAndSave(){ //7
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 40);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Sat,Sun");
		map.put("standard room type", "yes");
		map.put("standard room type net", "90.00");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.copyCreateRateRules();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		assertTrue(rpStandardRateSectionPage.verifyData(map));
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRule_create"),"Record created"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomAndCopyAndSave")
	public void testCreateStdRoomWith2PassengersAndSaveAndSave(){ //8
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 40);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Sat,Sun");
		map.put("minimum nights", "3");
		map.put("minimum passengers", "2");
		map.put("standard room type", "yes");
		map.put("standard room type net", "85.00");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.saveCreateRateRules();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record created"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomWith2PassengersAndSaveAndSave")
	public void testCreateStdRoomWith3NightsAndSaveAndSave(){ //9
		/** This test case is same as the above test case */
	}
	
	@Test(dependsOnMethods="testCreateStdRoomWith3NightsAndSaveAndSave")
	public void testCreateStdRoomWithRate150AndSaveAndSave(){ //10
		Date startDate = DateUtil.addDays(new Date(), 41);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("minimum passengers", "1");
		map.put("standard room type", "yes");
		map.put("standard room type net", "150.00");


		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.createOptionClick();
		rpStandardRateSectionPage.createStandardRates(map);
		rpStandardRateSectionPage.saveCreateRateRules();
		rpStandardRateSectionPage.saveSaveCreateRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record created"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateStdRoomWithRate150AndSaveAndSave")
	public void testEditWithMoreRulesYesAndUpdate(){ //11
		
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("more rules", "yes");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.updateEditRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record updated"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);

	}
	
	@Test(dependsOnMethods="testEditWithMoreRulesYesAndUpdate")
	public void testEditWithFullPeriodyesAndUpdate(){ //12
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("stay full period", "yes");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "Yes");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.updateEditRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record updated"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditWithFullPeriodyesAndUpdate")
	public void testEditWithMoreRulesNoAndUpdate(){ //13
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("more rules", "no");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "Yes");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.updateEditRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record updated"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);

	}
	
	@Test(dependsOnMethods="testEditWithMoreRulesNoAndUpdate")
	public void testEditDaysChangeAndUpdate(){ //14
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("days", "Mon, Tue, Wed, Thu, Fri");
		map.put("standard room type net", "180.00");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Edit");
		rpStandardRateSectionPage.editRateRules(map);
		rpStandardRateSectionPage.updateEditRateRules();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record updated"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditDaysChangeAndUpdate")
	public void testDeleteAndCancel(){ //15
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Delete");
		rpStandardRateSectionPage.deleteRateRuleWithCancel();
		rpStandardRateSectionPage.selectRateRuleOptions("Refresh");
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testDeleteAndCancel")
	public void testDeleteAndOK(){ //16
		Date startDate = DateUtil.addDays(new Date(), 41);
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpStandardRateSectionPage.refreshOptionClick();
		int countBefore = rpStandardRateSectionPage.getRateRuleTableRowCount();
		rpStandardRateSectionPage.selectRateRule(selectMap);
		rpStandardRateSectionPage.selectRateRuleOptions("Delete");
		rpStandardRateSectionPage.deleteRateRuleWithOK();
		assertTrue(rpStandardRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRuleContent"),"Record deleted"));
		int countAfter = rpStandardRateSectionPage.getRateRuleTableRowCount();
		assertTrue(countBefore == countAfter+1);
	}
	
	@Test(dependsOnMethods="testDeleteAndCancel")
	public void testSortOnTravelDates(){ //17.1
		assertTrue(rpStandardRateSectionPage.sortOrderOnTravelDates());
	}
	
	@Test(dependsOnMethods="testSortOnTravelDates")
	public void testRefresh(){ //17.2
		/** This functionality already tested in the above test cases. */
	}
	
	@Test(dependsOnMethods="testRefresh")
	public void testCollapseStandardRateSection(){ //18.1
		assertTrue(rpStandardRateSectionPage.collapseRateRules());
	}
	
	@Test(dependsOnMethods="testCollapseStandardRateSection")
	public void testExpandStandardRateSection(){ //18.2
		assertTrue(rpStandardRateSectionPage.expandRateRules());
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
