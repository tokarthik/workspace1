package com.gs.contract.test.manageavailability;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.contract.manageavailability.ManageAvailabilitySearchAndResultsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanSmartFillRateSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class ManageRatesAvailabilityTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private ManageAvailabilitySearchAndResultsSectionPage manageSearchResultsSectionPage;
	private TopLinksPage topLinksPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	private RatePlanSmartFillRateSectionPage rpSmartRateSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "channel"));
		topLinksPage = searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetData(){ 
		manageSearchResultsSectionPage = ManageAvailabilitySearchAndResultsSectionPage.getInstance();
		manageSearchResultsSectionPage.resetData();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		rpSmartRateSectionPage = RatePlanSmartFillRateSectionPage.getInstance();
	}

	@Test(dependsOnMethods="testResetData")
	public void testDeleteRatePlanAndOK(){ 
		selectRatePlan();
		rpDetailsSectionPage.selectRatePlanOptions("Delete");
		rpDetailsSectionPage.deleteAndOKRatePlan();
		rpDetailsSectionPage.verifyRecordUpdatedMessage("Record deleted");
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}

	@Test(dependsOnMethods="testDeleteRatePlanAndOK")
	public void testCreateRatePlanAndSave(){ 
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RO");
		inputMap.put("rate plan name", "Standard");
		inputMap.put("enter rate as", "Nett");
		inputMap.put("margin", "35");
		inputMap.put("meal basis", "Room Only");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
	}	
	
	@Test(dependsOnMethods="testCreateRatePlanAndSave")
	public void testCreateRatesWith100USD(){ 
		Date startDate = new Date();
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 20);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "100.00");
		
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
	}

	@Test(dependsOnMethods="testCreateRatesWith100USD")
	public void testNavigateToMangeRateTab(){  //1
		assertTrue(topLinksPage.selectManageRateTab());
	}
	
	@Test(dependsOnMethods="testNavigateToMangeRateTab")
	public void testSearch(){ //2
		Date startDate = DateUtil.addDays(new Date(), 1);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), 7);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("update", "Rates");
		map.put("days", "All");
		map.put("room types", "yes");
		map.put("rate plans", "yes");
		map.put("selected room type", "Standard Twin");
		map.put("selected rate plan", "RO: Standard Worldwide");
		
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		assertTrue(manageSearchResultsSectionPage.verifyDates(map));
		map.put("occupancy", "Occupancy 2");
		map.put("min nights", "Min Nights 1");
		map.put("stay full period", "false");
		assertTrue(manageSearchResultsSectionPage.verifyData(map,3));
	}
	
	@Test(dependsOnMethods="testSearch")
	public void testGrossFieldGreyed(){ //3
		Date startDate = DateUtil.addDays(new Date(), 1);
		assertTrue(manageSearchResultsSectionPage.isTextBoxDisabled(3, startDate, "Gross"));
	}

	@Test(dependsOnMethods="testGrossFieldGreyed")
	public void testEditMarginRatesTo50(){ //4
		Date startDate = DateUtil.addDays(new Date(), 1);
		manageSearchResultsSectionPage.setValueToTableField("All", "50", 3, startDate, "Margin");
		manageSearchResultsSectionPage.clickCopyToRightButton(3, "Margin", "50");
		manageSearchResultsSectionPage.updateEditRates();
		assertTrue(manageSearchResultsSectionPage.verifyRecordUpdatedMessage(By.className("maCalendarSections"),"Record updated"));
	}

	@Test(dependsOnMethods="testEditMarginRatesTo50")
	public void testEditMarginRatesTo20(){ //5
		Date startDate = DateUtil.addDays(new Date(), 1);
		manageSearchResultsSectionPage.setValueToTableField("All", "20", 3, startDate, "Margin");
		manageSearchResultsSectionPage.clickCopyToRightButton(3, "Margin", "20");
		manageSearchResultsSectionPage.updateEditRates();
		String errorMessage = "Margin value cannot be lower than the one specified at rate plan level";
		assertTrue(manageSearchResultsSectionPage.verifyErrorMessage(errorMessage));
		assertTrue(manageSearchResultsSectionPage.verifyRecordUpdatedMessage(By.className("maCalendarSections"),"The update failed"));
	}

	@Test(dependsOnMethods="testEditMarginRatesTo20")
	public void testEditNettRatesTo333(){ //6
		Date startDate = DateUtil.addDays(new Date(), 1);
		manageSearchResultsSectionPage.refreshRates();
		manageSearchResultsSectionPage.setValueToTableField("All", "333", 3, startDate, "Nett");
		manageSearchResultsSectionPage.clickCopyToRightButton(3, "Nett", "333");
		manageSearchResultsSectionPage.updateEditRates();
		assertTrue(manageSearchResultsSectionPage.verifyRecordUpdatedMessage(By.className("maCalendarSections"),"Record updated"));
	}

	@Test(dependsOnMethods="testEditNettRatesTo333")
	public void testSearchWithDays(){ //7
		Date startDate = DateUtil.addDays(new Date(), 1);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), 14);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("update", "Rates");
		map.put("days", "Mon,Tue,Wed,Thu");
		map.put("room types", "yes");
		map.put("rate plans", "yes");
		map.put("selected room type", "Standard Twin");
		map.put("selected rate plan", "RO: Standard Worldwide");
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		assertTrue(manageSearchResultsSectionPage.verifyDates(map));
		map.put("occupancy", "Occupancy 2");
		map.put("min nights", "Min Nights 1");
		map.put("stay full period", "false");
		assertTrue(manageSearchResultsSectionPage.verifyData(map,3));
	}

	@Test(dependsOnMethods="testSearchWithDays")
	public void testEditNettRatesTo500(){ //8
		Date startDate = DateUtil.addDays(new Date(), 1);
		manageSearchResultsSectionPage.refreshRates();
		manageSearchResultsSectionPage.setValueToTableField("Mon,Tue,Wed,Thu", "500", 3, startDate, "Nett");
		manageSearchResultsSectionPage.clickCopyToRightButton(3, "Nett", "500");
		manageSearchResultsSectionPage.updateEditRates();
		assertTrue(manageSearchResultsSectionPage.verifyRecordUpdatedMessage(By.className("maCalendarSections"),"Record updated"));
	}	
	
	@Test(dependsOnMethods="testEditNettRatesTo500")
	public void testSearchWithAllDays(){ //9
		Date startDate = DateUtil.addDays(new Date(), 1);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), 14);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("update", "Rates");
		map.put("days", "Mon,Tue,Wed,Thu,Fri,Sat,Sun");
		map.put("room types", "yes");
		map.put("rate plans", "yes");
		map.put("selected room type", "Standard Twin");
		map.put("selected rate plan", "RO: Standard Worldwide");
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		assertTrue(manageSearchResultsSectionPage.verifyDates(map));
		map.put("occupancy", "Occupancy 2");
		map.put("min nights", "Min Nights 1");
		map.put("stay full period", "false");
		assertTrue(manageSearchResultsSectionPage.verifyData(map,3));
	}
	
	@Test(dependsOnMethods="testSearchWithAllDays")
	public void testEditMarginRate40NettRateTo500(){ //10
		Date startDate = DateUtil.addDays(new Date(), 1);
		manageSearchResultsSectionPage.refreshRates();
		manageSearchResultsSectionPage.setValueToTableField("Mon,Tue,Wed,Thu,Fri,Sat,Sun", "40", 3, startDate, "Margin");
		manageSearchResultsSectionPage.setValueToTableField("Mon,Tue,Wed,Thu,Fri,Sat,Sun","500", 3, startDate, "Nett");
		manageSearchResultsSectionPage.updateEditRates();
		assertTrue(manageSearchResultsSectionPage.verifyRecordUpdatedMessage(By.className("maCalendarSections"),"Record updated"));
	}	
	
	@Test(dependsOnMethods="testEditMarginRate40NettRateTo500")
	public void testSearchAndCancel(){ //11
		Date startDate = DateUtil.addDays(new Date(), 1);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), 14);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("update", "Rates");
		map.put("days", "Mon,Tue,Wed,Thu,Fri,Sat,Sun");
		map.put("room types", "yes");
		map.put("rate plans", "yes");
		map.put("selected room type", "Standard Twin");
		map.put("selected rate plan", "RO: Standard Worldwide");
		map.put("stay full period", "false");
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		assertTrue(manageSearchResultsSectionPage.cancelSearch());
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
