package com.gs.test.execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.gs.contract.test.staticrateplans.RatePlanStaticCancellationTest;


public class RatePlanStaticTestSuite  { 
	public static void main(String args[]) throws Exception{
		RatePlanStaticTestSuite t = new RatePlanStaticTestSuite();
		t.method();
	} 
	public void method() throws Exception{
		
		for (int i=0;i<20;i++){
			TestListenerAdapter tla = new TestListenerAdapter();
	        TestNG testng = new TestNG();
	        testng.setTestClasses(new Class[] { 
	        		RatePlanStaticCancellationTest.class,

	        								  });
	        testng.addListener(tla);
	        testng.run();  
	        if (tla.getFailedTests().size() > 0){
	        	break;
	        }
	        System.out.println(">>>>> Count : "+ i);
		}
	}
}