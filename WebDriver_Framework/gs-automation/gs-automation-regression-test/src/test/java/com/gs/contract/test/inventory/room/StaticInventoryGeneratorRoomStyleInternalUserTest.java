package com.gs.contract.test.inventory.room;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.contract.inventory.InventoryGeneratorSectionPage;
import com.gta.travel.page.object.contract.manageavailability.ManageAvailabilitySearchAndResultsSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class StaticInventoryGeneratorRoomStyleInternalUserTest extends GSTestBase{
	
	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private ManageAvailabilitySearchAndResultsSectionPage manageSearchResultsSectionPage;
	private TopLinksPage topLinksPage;
	private InventoryGeneratorSectionPage inventoryGeneratorSectionPage;

	@BeforeClass
	public void init(){
//		setURL("http://hotels.demo.gta-travel.com/gs");
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "channel"));
		topLinksPage = searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetData(){ 
		assertTrue(topLinksPage.selectInventoryTab());
		manageSearchResultsSectionPage = ManageAvailabilitySearchAndResultsSectionPage.getInstance();
		inventoryGeneratorSectionPage = InventoryGeneratorSectionPage.getInstance();
		assertTrue(inventoryGeneratorSectionPage.deleteAllInventories());
	}

	@Test(dependsOnMethods="testResetData")
	public void testCreateBaseInventoryWith3Rooms(){ //1,2
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Base");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "1");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "3");
		map.put("Standard Twin", "3");
		
		
		executeCreateTestCase(map);
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "3", "3", "0", "1", null, null, null, "", "", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "3", "3", "0", "1", null, null, null, "", "", "false",true, true, 0);
	}
	
	@Test(dependsOnMethods="testCreateBaseInventoryWith3Rooms")
	public void testCreateFlexibleInventoryWith5Rooms(){ //3
		
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "0");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "5");
		map.put("Standard Twin", "5");
		
		executeCreateTestCase(map);
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "8", "3", "0", "1", null, null, null, "5", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "8", "3", "0", "1", null, null, null, "5", "0", "false",true, true, 0);
	}	
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith5Rooms")
	public void testCreateBaseInventoryWith1Room(){ //4
		
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Base");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "1");
		map.put("room types", "Standard Triple");
		map.put("Standard Triple", "2");
		
		executeCreateTestCase(map);
		
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "10", "5", "1", "1", null, null, null, "5", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "8", "3", "0", "1", null, null, null, "5", "0", "false",true, true, 0);
	}		
	
	@Test(dependsOnMethods="testCreateBaseInventoryWith1Room")
	public void testCreateBaseInventoryWith10Rooms(){ //5
		
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Base");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "3");
		map.put("room types", "Standard Twin");
		map.put("Standard Twin", "10");
		
		executeCreateTestCase(map);
		
		map = getInputMap(3, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,baseInv1, overBook1, conRelDays1,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "10", "5", "1", "1", null, null, null, "5", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "18", "3", "0", "1", "10", "1", "3", "5", "0", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testCreateBaseInventoryWith10Rooms")
	public void testCreateBaseInventoryWith2Rooms(){ //6
		
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Base");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "1");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "2");
		map.put("Standard Twin", "2");
		
		executeCreateTestCase(map);
		
		map = getInputMap(3, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "12", "7", "1", "1", null, null, null, "5", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "20", "5", "1", "1", "10", "1", "3", "5", "0", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testCreateBaseInventoryWith2Rooms")
	public void testCreateFlexibleInventoryWith2Rooms(){ //7
		
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "3");
		map.put("Standard Twin", "3");
		
		executeCreateTestCase(map);
		
		map = getInputMap(3, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "15", "7", "1", "1", null, null, null, "8", "2", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "23", "5", "1", "1", "10", "1", "3", "8", "2", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith2Rooms")
	public void testCreateFreesaleInventory(){ //8
		
		Map<String, String> map = getInputMap(7, 8);
		map.put("inventory type", "Freesale");
		map.put("days", "All");
		map.put("room types", "Standard Triple,Standard Twin");
		
		executeCreateTestCase(map);
//		assertTrue(topLinksPage.selectManageRateTab());
		
		map = getInputMap(7, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "freesale.gif", "7", "1", "1", null, null, null, "n/a", "n/a", "true",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "freesale.gif", "5", "1", "1", "10", "1", "3", "n/a", "n/a", "true",true, true, 1);
	}		

	@Test(dependsOnMethods="testCreateFreesaleInventory")
	public void testEnableFlexibleStopSell(){ //9
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Twin");
		manageSearchResultsSectionPage.updateEditRates("Standard Twin");
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut, rowNum);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "7", "7", "1", "1", null, null, null, "disabled", "disabled", "disabled",false, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "15", "5", "1", "1", "10", "1", "3", "disabled", "disabled", "disabled",false, true, 1);
	}		
	
	@Test(dependsOnMethods="testEnableFlexibleStopSell")
	public void testRemoveFlexibleStopSell(){ //10
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
//		assertTrue(topLinksPage.selectManageRateTab());
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Twin");
		manageSearchResultsSectionPage.updateEditRates("Standard Twin");
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,baseInv1, overBook1, conRelDays1,flexAvail, flexRelDays, freesale,flexStopSell, colseOut, rowNum);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "7", "7", "1", "1", null, null, null, "0", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "15", "5", "1", "1", "10", "1", "3", "0", "0", "false",true, true, 1);
	}		
	
	
	@Test(dependsOnMethods="testRemoveFlexibleStopSell")
	public void testFlexibleInventoryWith0ReleaseDays(){ //11
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "0");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "3");
		map.put("Standard Twin", "3");
		
//		assertTrue(topLinksPage.selectManageRateTab());
		executeCreateTestCase(map);
		
		map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "10", "7", "1", "1", null, null, null, "3", "0", "false",true, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "18", "5", "1", "1", "10", "1", "3", "3", "0", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testFlexibleInventoryWith0ReleaseDays")
	public void testActivateCloseOut1stRoom(){ //12
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

//		assertTrue(topLinksPage.selectManageRateTab());
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Close Out", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut, rowNum);
		executeAsserts(map, "Standard Triple", "closeOut", "0", "0", "disabled", "disabled", "disabled", null, null, null, "disabled", "disabled", "disabled",false, false, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "18", "5", "1", "1", "10", "1", "3", "3", "0", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testActivateCloseOut1stRoom")
	public void testRemoveCloseOut1stRoom(){ //13
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Close Out", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		
		//executeAsserts(map, roomType, priority, sold, totAvil, baseInv, overBook, conRelDays,flexAvail, flexRelDays, freesale,flexStopSell, colseOut, rowNum);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "0", "7", "7", "1", "1", null, null, null, "disabled", "disabled", "false",false, true, 0);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "0", "18", "5", "1", "1", "10", "1", "3", "3", "0", "false",true, true, 1);
	}		
	
	@Test(dependsOnMethods="testRemoveCloseOut1stRoom")
	public void testManageAvailabilitySearchAndCancel(){ //14
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		assertTrue(manageSearchResultsSectionPage.cancelSearch());
	}		
	
	@Test(dependsOnMethods="testManageAvailabilitySearchAndCancel")
	public void testManageAvailabilitySearchAndContinue(){ //15
		Map<String, String> map = getInputMap(5, 40);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		assertTrue(manageSearchResultsSectionPage.continueSearchWithValidationFailure("Select a minimum of 1 day and a maximum of 31 days."));
	}		
	
	@Test(dependsOnMethods="testManageAvailabilitySearchAndContinue")
	public void testSearchWithAllDays(){ //16
		Map<String, String> map = getInputMap(5, 36);
		map.put("days", "Mon,Tue,Wed,Thu,Fri");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Triple"));
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Twin"));
	}
	
	@Test(dependsOnMethods="testSearchWithAllDays")
	public void testUpdateManageAvailabilityValues(){ //17
		/** 
		 * we can't update flexible availability and flexible release days values as they
		 * are grayed out.
		 * 
		 * More over, the functionality is covered in ManageAndAvailability test cases. I 
		 * think its not required now.
		 */
	}
	
	@Test(dependsOnMethods="testUpdateManageAvailabilityValues")
	public void testInventoryAddAnother(){ //18
		Map<String, String> map = getInputMap(21, 30);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "7");
		map.put("Standard Twin", "7");
		
		assertTrue(topLinksPage.selectInventoryTab());
		inventoryGeneratorSectionPage.selectInventoryOptions("Refresh");
		inventoryGeneratorSectionPage.selectInventoryOptions("Create");
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.addAnotherInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveAddAnotherOrCopyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessing());
	}

	@Test(dependsOnMethods="testInventoryAddAnother")
	public void testInventoryCopy(){ //19
		Map<String, String> map = getInputMap(31, 35);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "2");
		map.put("Standard Twin", "2");
		
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.copyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveAddAnotherOrCopyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessing());
	}

	@Test(dependsOnMethods="testInventoryCopy")
	public void testInventoryCreateAndCancel(){ //20
		assertTrue(inventoryGeneratorSectionPage.cancelCreateInventory());
	}

	@Test(dependsOnMethods="testInventoryCreateAndCancel")
	public void testInventoryDelete(){ //21
		Date startDate = new Date();
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));

		Date futureDate = DateUtil.addDays(new Date(), 60);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		inventoryGeneratorSectionPage.selectInventoryOptions("Delete");
		inventoryGeneratorSectionPage.enterDeleteInventoryDates(map);
		inventoryGeneratorSectionPage.deleteInventory();
		inventoryGeneratorSectionPage.deleteDeleteInventory();
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessing());
	}

	@AfterClass
	public void close(){
//		finaliseTest();
	}
	
	private void executeAsserts(Map<String, String> map, String roomType, String priority, String sold, String totAvil, 
			String baseInv, String overBook, String conRelDays,
			String baseInv1, String overBook1, String conRelDays1,
			String flexAvail, String flexRelDays, String freesale, 
			boolean flexStopSell, boolean colseOut, int rowNum){
		
		assertTrue(manageSearchResultsSectionPage.verifyDates(map, roomType));
		
		assertTrue(manageSearchResultsSectionPage.verifyPriority(priority, roomType));
		assertTrue(manageSearchResultsSectionPage.verifySold(sold, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyTotalAvailable(totAvil, roomType));
		

		assertTrue(manageSearchResultsSectionPage.verifyBaseInventory(baseInv, roomType, 0));
		assertTrue(manageSearchResultsSectionPage.verifyOverbook(overBook, roomType, 0));
		assertTrue(manageSearchResultsSectionPage.verifyContractReleaseDays(conRelDays, roomType, 0));
		
		if (rowNum == 1){
			assertTrue(manageSearchResultsSectionPage.verifyBaseInventory(baseInv1, roomType, 1));
			assertTrue(manageSearchResultsSectionPage.verifyOverbook(overBook1, roomType, 1));
			assertTrue(manageSearchResultsSectionPage.verifyContractReleaseDays(conRelDays1, roomType, 1));
		}
		
		assertTrue(manageSearchResultsSectionPage.verifyFlexibleAvailability(flexAvail, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyFlexibleReleaseDays(flexRelDays, roomType));
		
		assertTrue(manageSearchResultsSectionPage.verifyFreesale(freesale, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyFlexibileStopSell(flexStopSell, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyCloseOut(colseOut, roomType));
	}
	
	private Map<String, String> getInputMap(int addStartDay, int addEndDay){
		Date startDate = DateUtil.addDays(new Date(), addStartDay);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), addEndDay);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		return map;
	}
	
	private void executeCreateTestCase(Map<String, String> map){ 

		assertTrue(topLinksPage.selectInventoryTab());
		inventoryGeneratorSectionPage.selectInventoryOptions("Refresh");
		inventoryGeneratorSectionPage.selectInventoryOptions("Create");
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.saveInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveSaveInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventoryContent"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessing());
		assertTrue(topLinksPage.selectManageRateTab());
	}	
}
