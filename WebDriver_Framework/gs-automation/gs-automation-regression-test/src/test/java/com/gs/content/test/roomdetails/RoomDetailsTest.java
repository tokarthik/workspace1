package com.gs.content.test.roomdetails;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.content.roomtypes.RoomDetailsPage;
import com.gta.travel.page.object.content.search.ContentSearchPage;
import com.gta.travel.page.object.content.search.ContentSearchResultsPage;

public class RoomDetailsTest extends GSTestBase {
	
	private LoginPage loginPage;
	private HomePage homePage;
	private ContentSearchPage searchPage;
	private ContentSearchResultsPage searchResultsPage;
	private TopLinksPage topLinksPage;
	private RoomDetailsPage roomDetailsPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
		topLinksPage = PageFactory.initElements(getDriver(),TopLinksPage.class);
	}

	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContent();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String propertyName = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "property_name");
		String country = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "country");
		String city = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "city");
		String status = "Images Missing";
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Property Name", propertyName);
		map.put("Status", status);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		String propertyName = ContentSearchPage.excelUtil.getCellAsString("Content_Search", 1, "property_name");
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", propertyName);
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetRoomDetails(){ 
		roomDetailsPage = RoomDetailsPage.getInstance();
		topLinksPage.clickRoomTypesTab();
		roomDetailsPage.resetData();
		roomDetailsPage.clickRefreshRoomDetails();
	}

	@Test(dependsOnMethods="testResetRoomDetails")
	public void testCreateRoomDetailsWithStandardDoubleAndAddAnother(){ //1
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Standard");
		map.put("RoomType", "Double");
		map.put("RoomCode", "DB1");
		map.put("NumberOfBeds", "1");
		map.put("MaximumOccupancy", "3");
		map.put("RateBasis", "2");
		map.put("NumberOfExtraBedsPossible", "1");
		map.put("ExtraBeds", "false");
		map.put("RoomFacilities", "Air Conditioned,Tea & Coffee Making Facilities,Television");
		map.put("RoomTypes", "Double,Single,Triple,Double for Sole Use");
		map.put("BathShower", "Bathtub with overhead shower");
		map.put("AllRooms", "true");
		
		roomDetailsPage.clickCreateRoomDetails();
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.addAnotherRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testCreateRoomDetailsWithStandardDoubleAndAddAnother")
	public void testSaveForValidations(){
		roomDetailsPage.clickRefreshRoomDetails();
		roomDetailsPage.saveErrorRoomDetails();
		assertTrue(roomDetailsPage.getValidationMessage().trim().equals("A room category must be selected"));		
	}
	
	@Test(dependsOnMethods="testSaveForValidations")
	public void testCreateRoomDetailsWithBusinessTwinAndCopy(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Business");
		map.put("RoomType", "Twin");
		map.put("RoomCode", "TB1");
		map.put("NumberOfBeds", "1");
		map.put("MaximumOccupancy", "3");
		map.put("RateBasis", "2");
		map.put("NumberOfExtraBedsPossible", "1");
		map.put("ExtraBeds", "false");
		map.put("RoomFacilities", "Air Conditioned,Tea & Coffee Making Facilities,Television");
		map.put("RoomTypes", "Twin,Single,Triple,Twin for Sole Use");
		map.put("BathShower", "Bathtub with overhead shower");
		map.put("AllRooms", "true");
		
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.copyRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testCreateRoomDetailsWithBusinessTwinAndCopy")
	public void testCreateRoomDetailsWithBusinessQueenAndCopy(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Business");
		map.put("RoomType", "Queen");
		map.put("RoomCode", "Queen1");
		map.put("RoomTypes", "Double,Single,Triple,Double for Sole Use");
		map.put("BathShower", "Bathtub with overhead shower");
		map.put("AllRooms", "true");
		
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.copyRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testCreateRoomDetailsWithBusinessQueenAndCopy")
	public void testCreateRoomDetailsWithStandardTwinAndSave(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Standard");
		map.put("RoomType", "Twin");
		map.put("RoomView", "City View");
		map.put("RoomCode", "TB1");
		map.put("RoomTypes", "Twin,Single,Triple,Twin for Sole Use");
		
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.saveRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testCreateRoomDetailsWithStandardTwinAndSave")
	public void testCancelRoomDetailsWithStandardTriple(){
		roomDetailsPage.clickRefreshRoomDetails();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Standard");
		map.put("RoomType", "Triple");
		map.put("RoomCode", "TRP1");
		map.put("RoomTypes", "Twin,Single,Triple,Twin for Sole Use");
		
		roomDetailsPage.clickCreateRoomDetails();
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.clickCancel();

		assertFalse(roomDetailsPage.isMessageDisplayed());
	}
	
	@Test(dependsOnMethods="testCancelRoomDetailsWithStandardTriple")
	public void testUpdateRoomDetailsWithBusinessKing(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomCategory", "Business");
		map.put("RoomType", "King");
		map.put("RoomCode", "King1");
		
		Map<String, String> roomRecordMap = new HashMap<String, String>();
		roomRecordMap.put("Room Type", "Business Twin");

		roomDetailsPage.clickRefreshRoomDetails();
		roomDetailsPage.selectRoom(roomRecordMap);
		
		roomDetailsPage.selectRoomDetailsOptions("Edit");
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.updateRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateRoomDetailsWithBusinessKing")
	public void testUpdateRoomDetailsForBusinessKingWithSeaView(){
		roomDetailsPage.clickRefreshRoomDetails();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomView", "Sea View");
		
		Map<String, String> roomRecordMap = new HashMap<String, String>();
		roomRecordMap.put("Room Type", "Business King");

		roomDetailsPage.selectRoom(roomRecordMap);
		
		roomDetailsPage.selectRoomDetailsOptions("Edit");
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.clickCancelInEdit();

		assertFalse(roomDetailsPage.isMessageDisplayed());
	}
	
	@Test(dependsOnMethods="testUpdateRoomDetailsForBusinessKingWithSeaView")
	public void testUpdateRoomDetailsForBusinessKingWithRoomFacilities(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("RoomFacilities", "Satellite TV,In-house Film");
		
		Map<String, String> roomRecordMap = new HashMap<String, String>();
		roomRecordMap.put("Room Type", "Business King");

		roomDetailsPage.selectRoomDetailsOptions("Refresh");
		roomDetailsPage.selectRoom(roomRecordMap);
		
		roomDetailsPage.selectRoomDetailsOptions("Edit");
		roomDetailsPage.enterRoomDetails(map);
		roomDetailsPage.updateRoomDetails();

		assertTrue(roomDetailsPage.verifyRecordUpdatedMessage(By.id("roomContent"),"Record updated"));
	}
	
	@Test(dependsOnMethods="testUpdateRoomDetailsForBusinessKingWithRoomFacilities")
	public void testSortRoomType(){		
		roomDetailsPage.clickRefreshRoomDetails();
		assertTrue(roomDetailsPage.sortOnRoomType());		
	}
	
	@Test(dependsOnMethods="testSortRoomType")
	public void testSortRoomCode(){
		assertTrue(roomDetailsPage.sortOnRoomCode());		
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}
	
	
}

