package com.gs.contract.test.marginrateplans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.contract.manageavailability.ManageAvailabilitySearchAndResultsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanSmartFillRateSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class RatePlanMarginSmartFillRateTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanSmartFillRateSectionPage rpSmartRateSectionPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	private ManageAvailabilitySearchAndResultsSectionPage manageSectionPage;
	private TopLinksPage topLinksPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		  setBrowserProfilePath("C:\\FirefoxProfile");
		setBrowser("IE");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 6, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetData(){ 
		rpSmartRateSectionPage = RatePlanSmartFillRateSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
//		selectRatePlan();
		rpSmartRateSectionPage.resetData();
		manageSectionPage = ManageAvailabilitySearchAndResultsSectionPage.getInstance();
		topLinksPage = TopLinksPage.getInstance();
	}
	
	@Test(dependsOnMethods="testResetData")
	public void testDeleteRatePlanAndOK(){ 
		selectRatePlan();
		rpDetailsSectionPage.selectRatePlanOptions("Delete");
		rpDetailsSectionPage.deleteAndOKRatePlan();
		rpDetailsSectionPage.verifyRecordUpdatedMessage("Record deleted");
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}

	@Test(dependsOnMethods="testDeleteRatePlanAndOK")
	public void testCreateRatePlanAndSave(){ 
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RO");
		inputMap.put("rate plan name", "Standard");
		inputMap.put("enter rate as", "Nett");
		inputMap.put("margin", "35");
		inputMap.put("meal basis", "Room Only");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
		selectRatePlan();
	}	
	

	
	@Test(dependsOnMethods="testCreateRatePlanAndSave")
	public void testCreateRatesWith100USD(){ //1
		Date startDate = new Date();
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 20);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "100.00");
		
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(0, 3, "All");
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,3));
		assertTrue(manageSectionPage.verifyRates("Margin", "35", 3));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"), 3));
		System.out.println(">>>>> Gros value : "+grossValue);
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 3));
	}
	
	@Test(dependsOnMethods="testCreateRatesWith100USD")
	public void testCreateRatesWith180USD(){ //2
		Date startDate = new Date();
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 3);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "180.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(0, 3, "All");
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,3));
		assertTrue(manageSectionPage.verifyRates("Margin", "35", 3));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"), 3));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 3));
	}	
	
	@Test(dependsOnMethods="testCreateRatesWith180USD")
	public void testCreateRatesWith110USD(){ //3
		Date startDate = DateUtil.addDays(new Date(), 21);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 24);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "110.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(21, 24, "All");
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,3));
		assertTrue(manageSectionPage.verifyRates("Margin", "35", 3));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"), 3));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 3));
	}		
	
	@Test(dependsOnMethods="testCreateRatesWith110USD")
	public void testCreateRatesWith150USD(){ //4
		Date startDate = DateUtil.addDays(new Date(), 41);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 60);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "150.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(41, 60, "All");
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,3));
		assertTrue(manageSectionPage.verifyRates("Margin", "35", 3));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"), 3));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 3));
	}			
	
	@Test(dependsOnMethods="testCreateRatesWith150USD")
	public void testCreateRatesWith250USD(){ //5
		Date startDate = DateUtil.addDays(new Date(), 41);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 60);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("more rules", "no");
		
		map.put("days", "Fri,Sat,Sun");
		map.put("twin room type net", "250.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(41, 60, map.get("days"));
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,3));
		assertTrue(manageSectionPage.verifyRates("Margin", "35", 3));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"), 3));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 3));
	}				
	
	@Test(dependsOnMethods="testCreateRatesWith250USD")
	public void testCreateRatesWith210USD(){ //6
		Date startDate = DateUtil.addDays(new Date(), 41);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 60);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "3");
		map.put("more rules", "no");
		
		map.put("days", "All");
		map.put("twin room type net", "210.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(41, 60, map.get("days"));
		manageMap.put("stay full period", "false");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 3");
		assertTrue(manageSectionPage.verifyData(manageMap,4));
		assertTrue(manageSectionPage.verifyRates("Margin", "35",4));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"),4));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue,4));
	}		
	
	@Test(dependsOnMethods="testCreateRatesWith210USD")
	public void testCreateRatesWith190USD(){ //7
		Date startDate = DateUtil.addDays(new Date(), 41);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 45);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		//map.put("minimum nights", "3"); // Need to discuss : When we select full period, min nights value become disabled with value 1
		map.put("more rules", "yes");
		map.put("stay full period", "yes");
		
		
		map.put("days", "All");
		map.put("twin room type net", "190.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		String grossValue = rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		assertTrue(topLinksPage.selectManageRateTab());
		
		
		Map<String, String> manageMap = getManageMap(41, 45, map.get("days"));
		manageMap.put("stay full period", "true");
		manageSectionPage.clickSearch();
		manageSectionPage.search(manageMap);
		manageSectionPage.continueSearch();
		assertTrue(manageSectionPage.verifyDates(manageMap));
		manageMap.put("occupancy", "Occupancy 2");
		manageMap.put("min nights", "Min Nights 1");
		assertTrue(manageSectionPage.verifyData(manageMap,5));
		assertTrue(manageSectionPage.verifyRates("Margin", "35",5));
		assertTrue(manageSectionPage.verifyRates("Nett", map.get("twin room type net"),5));
		assertTrue(manageSectionPage.verifyRates("Gross", grossValue, 5));
	}			
	
	@Test(dependsOnMethods="testCreateRatesWith190USD")
	public void testCreateRatesAndCancel(){ //8
		Date startDate = DateUtil.addDays(new Date(), 61);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 68);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1"); 
		map.put("more rules", "no");
		
		
		map.put("days", "All");
		map.put("twin room type net", "105.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		rpSmartRateSectionPage.editRates(map);
		assertTrue(rpSmartRateSectionPage.cancelEditRates());
	}
	
	@Test(dependsOnMethods="testCreateRatesAndCancel")
	public void testCreateRatesAndAddAnother(){ //9
		Date startDate = DateUtil.addDays(new Date(), 69);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 76);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1"); 
		map.put("more rules", "no");
		
		
		map.put("days", "All");
		map.put("twin room type net", "105.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		rpSmartRateSectionPage.editRates(map);
		assertTrue(rpSmartRateSectionPage.addAnotherEditRates());
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
	}
	
	@Test(dependsOnMethods="testCreateRatesAndAddAnother")
	public void testCreateRatesAndCopy(){ //10
		Date startDate = DateUtil.addDays(new Date(), 77);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 83);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1"); 
		map.put("more rules", "no");
		
		
		map.put("days", "All");
		map.put("twin room type net", "108.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		rpSmartRateSectionPage.editRates(map);
		assertTrue(rpSmartRateSectionPage.copyEditRates());
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
		String value = rpSmartRateSectionPage.getNetValue();
		assertTrue(value.equalsIgnoreCase(map.get("twin room type net")));
	}
	
	@Test(dependsOnMethods="testCreateRatesAndCopy")
	public void testCreateRatesWith116USD(){ //11
		Date startDate = DateUtil.addDays(new Date(), 84);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 90);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1"); 
		map.put("more rules", "no");
		
		
		map.put("days", "All");
		map.put("twin room type net", "116.00");
		
		assertTrue(topLinksPage.selectRatePlansTab());
		selectRatePlan();
		rpSmartRateSectionPage.clickCreateLink();
		rpSmartRateSectionPage.editRates(map);
		rpSmartRateSectionPage.saveEditRates();
		assertTrue(rpSmartRateSectionPage.verifyRecordUpdatedMessage());
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

	private Map<String, String> getManageMap(int startDateAdd, int endDateAdd, String days){
		Date startDate = DateUtil.addDays(new Date(), startDateAdd);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), endDateAdd);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", days);
		map.put("room types", "yes");
		map.put("rate plans", "yes");
		map.put("selected room type", "Standard Twin");
		map.put("selected rate plan", "RO: Standard Worldwide");

		return map;
	}
	
	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 2, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
