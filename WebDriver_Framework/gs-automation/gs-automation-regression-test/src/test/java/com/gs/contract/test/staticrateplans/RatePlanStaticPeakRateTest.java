package com.gs.contract.test.staticrateplans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanPeakRateSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class RatePlanStaticPeakRateTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanPeakRateSectionPage rpPeakRateSectionPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetPeakRateData(){ 
		rpPeakRateSectionPage = RatePlanPeakRateSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		selectRatePlan();
		rpPeakRateSectionPage.resetData();
	}
	
	@Test(dependsOnMethods="testResetPeakRateData")
	public void testCreatePeakStdRoomAndSaveAndSave(){ //1
		Date startDate = DateUtil.addDays(new Date(), 91);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 95);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "All");
		map.put("standard room type", "yes");
		map.put("standard room type net", "300");

		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.saveCreatePeakRateRules();
		rpPeakRateSectionPage.saveSaveCreatePeakRateRules();
		
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record created"));
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomAndSaveAndSave")
	public void testEditPeakStdRoomAndCancel(){ //2
		Map<String, String> map = new HashMap<String, String>();
		map.put("standard room type net", "350");
		map.put("room type edit", "no");

		Map<String, String> selectMap = new HashMap<String, String>();

		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.cancelEditPeakRateRules();
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Refresh");
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditPeakStdRoomAndCancel")
	public void testEditPeakStdRoomAndUpdate(){ //3
		Map<String, String> map = new HashMap<String, String>();
		map.put("standard room type net", "350");
		map.put("room type edit", "no");

		Map<String, String> selectMap = new HashMap<String, String>();

		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.updateEditPeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record updated"));
		rpPeakRateSectionPage.refreshOptionClick();
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditPeakStdRoomAndUpdate")
	public void testCreatePeakStdRoomAndAddAnotherAndCancel(){ //4
		Date startDate = DateUtil.addDays(new Date(), 91);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 95);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("standard room type", "yes");
		map.put("standard room type net", "320");
		map.put("minimum nights", "3");
		map.put("minimum passengers", "2");



		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.addAnotherCreatePeakRateRule();
		rpPeakRateSectionPage.cancelAddAnotherCreatePeakRateRules();
		rpPeakRateSectionPage.cancelCreatePeakRateRules();
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomAndAddAnotherAndCancel")
	public void testCreatePeakStdRoomAndAddAnotherAndSave(){ //5
		Date startDate = DateUtil.addDays(new Date(), 91);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 95);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("standard room type", "yes");
		map.put("standard room type net", "320");
		map.put("minimum nights", "3");
		map.put("minimum passengers", "2");


		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.addAnotherCreatePeakRateRule();
		rpPeakRateSectionPage.saveSaveCreatePeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeak_create"),"Record created"));
		rpPeakRateSectionPage.refreshOptionClick();
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomAndAddAnotherAndSave")
	public void testCreatePeakStdRoomWith1MinNightsAndSaveAndSave(){ //6
		Date startDate = DateUtil.addDays(new Date(), 101);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "1");
		map.put("minimum passengers", "1");
		map.put("standard room type", "yes");
		map.put("standard room type net", "450");


		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.saveCreatePeakRateRules();
		rpPeakRateSectionPage.saveSaveCreatePeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record created"));
		rpPeakRateSectionPage.refreshOptionClick();
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomWith1MinNightsAndSaveAndSave")
	public void testCreatePeakStdRoomAndCopyAndSave(){ //7
		Date startDate = DateUtil.addDays(new Date(), 101);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("minimum nights", "3");
		map.put("minimum passengers", "2");
		map.put("standard room type", "yes");
		map.put("standard room type net", "420.00");


		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.copyCreatePeakRateRules();
		rpPeakRateSectionPage.saveSaveCreatePeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyData(map));
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeak_create"),"Record created"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter-1);
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomAndCopyAndSave")
	public void testCreatePeakStdRoomWith4PassengersAndSaveAndSave(){ //8
		Date startDate = DateUtil.addDays(new Date(), 101);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		map.put("days", "Sat,Sun");
		map.put("minimum nights", "3");
		map.put("minimum passengers", "4");
		map.put("standard room type", "yes");
		map.put("standard room type net", "85.00");


		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.createOptionClick();
		rpPeakRateSectionPage.createPeakRates(map);
		rpPeakRateSectionPage.saveCreatePeakRateRules();
		rpPeakRateSectionPage.cancelAddAnotherCreatePeakRateRules();
		rpPeakRateSectionPage.cancelCreatePeakRateRules();
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testCreatePeakStdRoomWith4PassengersAndSaveAndSave")
	public void testEditPeakRatesWithMoreRulesYesAndUpdate(){ //9
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("more rules", "yes");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.updateEditPeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record updated"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditPeakRatesWithMoreRulesYesAndUpdate")
	public void testEditPeakRaesWithFullPeriodyesAndUpdate(){ //10
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("stay full period", "yes");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "Yes");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.updateEditPeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record updated"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);

	}
	
	@Test(dependsOnMethods="testEditPeakRaesWithFullPeriodyesAndUpdate")
	public void testEditPeakRaesWithMoreRulesNoAndUpdate(){ //11
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("more rules", "no");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "Yes");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.updateEditPeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record updated"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);

	}
	
	@Test(dependsOnMethods="testEditPeakRaesWithMoreRulesNoAndUpdate")
	public void testEditPeakRatesDaysChangeAndUpdate(){ //12
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("days", "Mon, Tue, Wed, Thu, Fri");
		map.put("standard room type net", "180.00");

		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Edit");
		rpPeakRateSectionPage.editPeakRateRules(map);
		rpPeakRateSectionPage.updateEditPeakRateRules();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record updated"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testEditPeakRatesDaysChangeAndUpdate")
	public void testDeletePeakRaesAndCancel(){ //13
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Delete");
		rpPeakRateSectionPage.deletePeakRateRuleWithCancel();
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Refresh");
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter);
	}
	
	@Test(dependsOnMethods="testDeletePeakRaesAndCancel")
	public void testDeletePeakRaesAndOK(){ //14
		Date startDate = DateUtil.addDays(new Date(), 101);
		Date futureDate = DateUtil.addDays(new Date(), 105);
		String startDateStr = DateUtil.convertDateToGTAFormat(startDate);
		String endDateStr = DateUtil.convertDateToGTAFormat(futureDate);
		
		String travelDates = startDateStr + " - " + endDateStr;
		
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("Rate Type", "Nett");
		selectMap.put("Min Nights", "1");
		selectMap.put("Min Pax", "1");
		selectMap.put("Rules", "No");
		selectMap.put("Travel Dates", travelDates);
		
		rpPeakRateSectionPage.refreshOptionClick();
		int countBefore = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		rpPeakRateSectionPage.selectPeakRateRule(selectMap);
		rpPeakRateSectionPage.selectPeakRateRuleOptions("Delete");
		rpPeakRateSectionPage.deletePeakRateRuleWithOK();
		assertTrue(rpPeakRateSectionPage.verifyRecordUpdatedMessage(By.id("rateRulePeakContent"),"Record deleted"));
		int countAfter = rpPeakRateSectionPage.getPeakRateRuleTableRowCount();
		assertTrue(countBefore == countAfter+1);
	}
	
	@Test(dependsOnMethods="testDeletePeakRaesAndOK")
	public void testPeakRaesSortOnTravelDates(){ //15.1
		/** Sorting order is not working for Travel Dates. The below code need to be
		 * commented until this issue resolved. */
		//assertTrue(rpPeakRateSectionPage.sortOrderOnTravelDates());
	}
	
	@Test(dependsOnMethods="testPeakRaesSortOnTravelDates")
	public void testPeakRaesRefresh(){ //15.2
		/** This functionality already tested in the above test cases. */
	}
	
	@Test(dependsOnMethods="testPeakRaesRefresh")
	public void testCollapsePeakRaesSection(){ //16.1
		assertTrue(rpPeakRateSectionPage.collapsePeakRateRules());
	}
	
	@Test(dependsOnMethods="testCollapsePeakRaesSection")
	public void testExpandPeakRaesSection(){ //16.2
		assertTrue(rpPeakRateSectionPage.expandPeakRateRules());
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}

}
