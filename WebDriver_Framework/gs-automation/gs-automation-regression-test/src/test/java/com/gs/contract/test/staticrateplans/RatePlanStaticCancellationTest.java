package com.gs.contract.test.staticrateplans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanCancellationSectionPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

public class RatePlanStaticCancellationTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanCancellationSectionPage rpCancellationSectionPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetCancellationData(){ 
		rpCancellationSectionPage = RatePlanCancellationSectionPage.getInstance();
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		selectRatePlan();
		rpCancellationSectionPage.resetData();
	}

	@Test(dependsOnMethods="testResetCancellationData")
	public void testCreateCancellationWithZeroDaysPriorAndSave(){ //1
		Map<String, String> map = new HashMap<String, String>();
		map.put("override base cancellation", "true");
		map.put("days prior", "0");
		map.put("arrival time", "18:00");
		map.put("percentage", "100%");
		map.put("charge duration", "1");
		
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);


		rpCancellationSectionPage.selectCancellationOptions("Create");
		rpCancellationSectionPage.createCancellation(map);
		rpCancellationSectionPage.saveCancellation();

		assertTrue(rpCancellationSectionPage.verifyRecordUpdatedMessage(By.id("cancellation_list"),"Record created"));
	}

	@Test(dependsOnMethods="testCreateCancellationWithZeroDaysPriorAndSave")
	public void testCreateCancellationWith20DaysPriorAndAddAnother(){ //2
		Map<String, String> map = new HashMap<String, String>();
		map.put("days prior", "20");
		map.put("percentage", "20%");
		map.put("charge duration", "1");
		
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		int rowCountBefore = rpCancellationSectionPage.getCancellationTableRowCount();
		rpCancellationSectionPage.selectCancellationOptions("Create");
		rpCancellationSectionPage.createCancellation(map);
		rpCancellationSectionPage.addAnotherCancellation();

		assertTrue(rpCancellationSectionPage.verifyRecordUpdatedMessage(By.id("cancellation_list"),"Record created"));
		assertTrue(rpCancellationSectionPage.verifyRowcount(rowCountBefore+1));
		assertTrue(rpCancellationSectionPage.isCreateRecordSectionDisplayed());
	}

	@Test(dependsOnMethods="testCreateCancellationWith20DaysPriorAndAddAnother")
	public void testCreateCancellationWithNoShowAndAddAnother(){ //3
		Map<String, String> map = new HashMap<String, String>();
		map.put("days prior", "No Show");
		map.put("percentage", "100%");
		map.put("charge duration", "Full Stay");
	
		String todayDay = Integer.toString(DateUtil.getDay(new Date()));
		String todayMonth = DateUtil.getMonthName(new Date());
		String todayYear = Integer.toString(DateUtil.getYear(new Date()));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", "31");
		map.put("travel end month", "Dec");
		map.put("travel end year", todayYear);

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		int rowCountBefore = rpCancellationSectionPage.getCancellationTableRowCount();
		rpCancellationSectionPage.selectCancellationOptions("Create");
		rpCancellationSectionPage.createCancellation(map);
		rpCancellationSectionPage.addAnotherCancellation();

		assertTrue(rpCancellationSectionPage.verifyRecordUpdatedMessage(By.id("cancellation_list"),"Record created"));
		assertTrue(rpCancellationSectionPage.verifyRowcount(rowCountBefore+1));
		assertTrue(rpCancellationSectionPage.isCreateRecordSectionDisplayed());
	}

	@Test(dependsOnMethods="testCreateCancellationWithNoShowAndAddAnother")
	public void testCreateCancellationWithPeakRatesAndSave(){ //4
		Map<String, String> map = new HashMap<String, String>();
		map.put("days prior", "Any");
		map.put("percentage", "100%");
		map.put("charge duration", "Full Stay");
	
		Date futureStartDate = DateUtil.addDays(new Date(), 60);
		
		String todayDay = Integer.toString(DateUtil.getDay(futureStartDate));
		String todayMonth = DateUtil.getMonthName(futureStartDate);
		String todayYear = Integer.toString(DateUtil.getYear(futureStartDate));

		futureStartDate = DateUtil.addDays(new Date(), 65);
		String endDay = Integer.toString(DateUtil.getDay(futureStartDate));
		String endMonth = DateUtil.getMonthName(futureStartDate);
		String endYear = Integer.toString(DateUtil.getYear(futureStartDate));

		map.put("travel start day", todayDay);
		map.put("travel start month", todayMonth);
		map.put("travel start year", todayYear);
		
		map.put("travel end day", endDay);
		map.put("travel end month", endMonth);
		map.put("travel end year", endYear);

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		int rowCountBefore = rpCancellationSectionPage.getCancellationTableRowCount();
		rpCancellationSectionPage.selectCancellationOptions("Create");
		rpCancellationSectionPage.createCancellation(map);
		rpCancellationSectionPage.saveCancellation();

		assertTrue(rpCancellationSectionPage.verifyRecordUpdatedMessage(By.id("cancellation_list"),"Record created"));
		assertTrue(rpCancellationSectionPage.verifyRowcount(rowCountBefore+1));
	}

	@Test(dependsOnMethods="testCreateCancellationWithPeakRatesAndSave")
	public void testEditCancellationWithFullStayAndUpdate(){ //5
		Map<String, String> map = new HashMap<String, String>();
		map.put("days prior", "0");
		map.put("arrival time", "18:00");
		map.put("percentage", "100%");
		map.put("charge duration", "Full Stay");

		Map<String, String> cancellationRecordMap = new HashMap<String, String>();
		cancellationRecordMap.put("Rate Type", "Standard");
		cancellationRecordMap.put("Day/Time Prior", "18:00 Day of Arrival");
		cancellationRecordMap.put("Charge Condition", "100% of 1st Night");

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		rpCancellationSectionPage.selectCancellation(cancellationRecordMap);
		
		rpCancellationSectionPage.selectCancellationOptions("Edit");
		rpCancellationSectionPage.editCancellation(map);
		rpCancellationSectionPage.updateCancellation();
		
		assertTrue(rpCancellationSectionPage.verifyRecordUpdatedMessage(By.id("cancellation_list"),"Record updated"));
	}

//	@Test(dependsOnMethods="testEditCancellationWithFullStayAndUpdate")
//	public void testEditCancellationWithZeroDaysPriorAndUpdate(){ //6
//		/**  Above test case (tc#11) and this test case are same. */
//	}

	@Test(dependsOnMethods="testEditCancellationWithFullStayAndUpdate")
	public void testEditCancellationAndCancel(){ //13
		Map<String, String> map = new HashMap<String, String>();
		map.put("arrival time", "15:00");

		Map<String, String> cancellationRecordMap = new HashMap<String, String>();
		cancellationRecordMap.put("Rate Type", "Standard");
		cancellationRecordMap.put("Day/Time Prior", "18:00 Day of Arrival");
		cancellationRecordMap.put("Charge Condition", "100% of Full Stay");

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		rpCancellationSectionPage.selectCancellation(cancellationRecordMap);
		
		rpCancellationSectionPage.selectCancellationOptions("Edit");
		rpCancellationSectionPage.editCancellation(map);
		rpCancellationSectionPage.cancelEditCancellation();
		
		assertFalse(rpCancellationSectionPage.isMessageDisplayed());
	}

	@Test(dependsOnMethods="testEditCancellationAndCancel")
	public void testDeleteCancellationAndCancel(){ //7
		Map<String, String> cancellationRecordMap = new HashMap<String, String>();
		cancellationRecordMap.put("Rate Type", "Standard");
		cancellationRecordMap.put("Day/Time Prior", "18:00 Day of Arrival");
		cancellationRecordMap.put("Charge Condition", "100% of Full Stay");

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		int rowCountBefore = rpCancellationSectionPage.getCancellationTableRowCount();
		rpCancellationSectionPage.selectCancellation(cancellationRecordMap);
		rpCancellationSectionPage.selectCancellationOptions("Delete");
		rpCancellationSectionPage.deleteCancellationWithCancel();
		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		assertTrue(rpCancellationSectionPage.verifyRowcount(rowCountBefore));
	}

	@Test(dependsOnMethods="testDeleteCancellationAndCancel")
	public void testDeleteCancellationAndOK(){ //8
		Map<String, String> cancellationRecordMap = new HashMap<String, String>();
		cancellationRecordMap.put("Rate Type", "Standard");
		cancellationRecordMap.put("Day/Time Prior", "18:00 Day of Arrival");
		cancellationRecordMap.put("Charge Condition", "100% of Full Stay");

		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		int rowCountBefore = rpCancellationSectionPage.getCancellationTableRowCount();
		rpCancellationSectionPage.selectCancellation(cancellationRecordMap);
		rpCancellationSectionPage.selectCancellationOptions("Delete");
		rpCancellationSectionPage.deleteCancellationWithOK();
		rpCancellationSectionPage.selectCancellationOptions("Refresh");
		assertTrue(rpCancellationSectionPage.verifyRowcount(rowCountBefore-1));
	}

	@Test(dependsOnMethods="testDeleteCancellationAndOK")
	public void testSortOnRateType(){ //9.1
		assertTrue(rpCancellationSectionPage.sortOrderOnRateType());
	}

	@Test(dependsOnMethods="testSortOnRateType")
	public void testSortOnTravelDates(){ //9.2
		assertTrue(rpCancellationSectionPage.sortOrderOnTravelDates());
	}

	@Test(dependsOnMethods="testSortOnTravelDates")
	public void testSortOnDaytimePrior(){ //9.3
		/** Need to ask the sort order on this column */
//		assertTrue(rpCancellationSectionPage.sortOrderOnDayTime());
	}

	@Test(dependsOnMethods="testSortOnDaytimePrior")
	public void testSortOnChargeCondition(){ //9.4
		/** Need to ask the sort order on this column */
//		assertTrue(rpCancellationSectionPage.sortOrderOnChargeCondition());
	}

//	@Test(dependsOnMethods="testSortOnChargeCondition")
//	public void testRefresh(){ //9.5
//		/**  Already convered in above test cases */
//	}

	@Test(dependsOnMethods="testSortOnChargeCondition")
	public void testCollapseCancellationSection(){ //10.1
		assertTrue(rpCancellationSectionPage.collapseCancellation());
	}

	@Test(dependsOnMethods="testCollapseCancellationSection")
	public void testExpandCancellationSection(){ //10.2
		assertTrue(rpCancellationSectionPage.expandCancellation());
	}

	@AfterClass
	public void close(){
		finaliseTest();
	}

	private void selectRatePlan(){
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Rate Plan code"));
		selectMap.put("Description", ContractSearchPage.excelUtil.getCellAsString("RatePlans", 1, "Description"));
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
	}
}
