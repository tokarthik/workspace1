package com.gs.contract.test.inventory.room;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSPageBase;
import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.common.LogoutPage;
import com.gta.travel.page.object.common.TopLinksPage;
import com.gta.travel.page.object.contract.inventory.InventoryGeneratorSectionPage;
import com.gta.travel.page.object.contract.manageavailability.ManageAvailabilitySearchAndResultsSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.selenium.ui.controls.Table;
import com.mediaocean.qa.framework.utils.DateUtil;

public class StaticInventoryGeneratorRoomStyleExternalUserTest extends GSTestBase{
	
	private LoginPage loginPage;
	private LogoutPage logOutPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private ManageAvailabilitySearchAndResultsSectionPage manageSearchResultsSectionPage;
	private TopLinksPage topLinksPage;
	private InventoryGeneratorSectionPage inventoryGeneratorSectionPage;

	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("Chrome");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "channel"));
		topLinksPage = searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetData(){ 
		assertTrue(topLinksPage.selectInventoryTab());
		manageSearchResultsSectionPage = ManageAvailabilitySearchAndResultsSectionPage.getInstance();
		inventoryGeneratorSectionPage = InventoryGeneratorSectionPage.getInstance();
		topLinksPage = TopLinksPage.getInstance();
		assertTrue(inventoryGeneratorSectionPage.deleteAllInventories());

		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Base");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "1");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "2");
		map.put("Standard Twin", "2");
		
		executeCreateTestCase(map, "internal");
	}
	
	@Test(dependsOnMethods="testResetData")
	public void testLogout(){
		logOutPage = PageFactory.initElements(getDriver(),LogoutPage.class);
		loginPage = logOutPage.logOut();
	}
	
	@Test(dependsOnMethods="testLogout")
	public void testExternalLogin(){
		String webId = GSPageBase.excelUtil.getCellAsString("External User Details", 2, "webId");
		String userName = GSPageBase.excelUtil.getCellAsString("External User Details", 2, "userName");
		String password = GSPageBase.excelUtil.getCellAsString("External User Details", 2, "password");
		homePage = loginPage.login(webId, userName, password);
		homePage.selectContract();
		selectProviderFromTheList();
	}
	
	@Test(dependsOnMethods="testExternalLogin")
	public void testCreateFlexibleInventoryWith5Rooms(){ //1
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "0");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "5");
		map.put("Standard Twin", "5");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "7",  "5", "0", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "7", "5", "0", "false",true);
	}
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith5Rooms")
	public void testCreateFlexibleInventoryWith4Rooms(){ //2
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "1");
		map.put("room types", "Standard Triple");
		map.put("Standard Triple", "4");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "11",  "9", "1", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "7", "5", "0", "false",true);
	}
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith4Rooms")
	public void testCreateFlexibleInventoryWith2Rooms(){ //3
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Twin");
		map.put("Standard Twin", "2");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "11",  "9", "1", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "9", "7", "2", "false",true);
	}
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith2Rooms")
	public void testCreateFlexibleInventoryWith3DaysReleaseDate(){ //4
		Map<String, String> map = getInputMap(3, 8);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "3");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "2");
		map.put("Standard Twin", "3");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(3, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "13",  "11", "3", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "12", "10", "3", "false",true);
	}
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith3DaysReleaseDate")
	public void testCreateFreesaleInventory(){ //5
		Map<String, String> map = getInputMap(7, 8);
		map.put("inventory type", "Freesale");
		map.put("days", "All");
		map.put("room types", "Standard Triple,Standard Twin");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(7, 8);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "freesale.gif",  "n/a", "n/a", "true",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "freesale.gif", "n/a", "n/a", "true",true);
	}
	
	@Test(dependsOnMethods="testCreateFreesaleInventory")
	public void testEnableFlexibleStopSell(){ //6
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Twin");
		manageSearchResultsSectionPage.updateEditRates("Standard Twin");
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "2",  "disabled", "disabled", "disabled",false);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "2", "disabled", "disabled", "disabled",false);
	}
	
	@Test(dependsOnMethods="testEnableFlexibleStopSell")
	public void testRemoveFlexibleStopSell(){ //7
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
//		assertTrue(topLinksPage.selectManageRateTab());
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Triple");
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		
		manageSearchResultsSectionPage.clickOnImage("Flexible Stop Sell", 3, "Standard Twin");
		manageSearchResultsSectionPage.updateEditRates("Standard Twin");
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "2",  "0", "0", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "2", "0", "0", "false",true);
	}		
	
	@Test(dependsOnMethods="testRemoveFlexibleStopSell")
	public void testCreateFlexibleInventoryWith2DaysReleaseDate(){ //8
		Map<String, String> map = getInputMap(3, 10);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "3");
		map.put("Standard Twin", "3");
		
		executeCreateTestCase(map, "external");
		
		map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		//executeAsserts(map, roomType, priority, totAvil, flexAvail, flexRelDays, freesale, flexStopSell);
		executeAsserts(map, "Standard Triple", "roomsAvailable", "5",  "3", "2", "false",true);
		executeAsserts(map, "Standard Twin", "roomsAvailable", "5", "3", "2", "false",true);
	}
	
	@Test(dependsOnMethods="testCreateFlexibleInventoryWith2DaysReleaseDate")
	public void testManageAvailabilitySearchAndCancel(){ //9
		Map<String, String> map = getInputMap(5, 5);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		assertTrue(manageSearchResultsSectionPage.cancelSearch());
	}		
	
	@Test(dependsOnMethods="testManageAvailabilitySearchAndCancel")
	public void testManageAvailabilitySearchAndContinue(){ //10
		Map<String, String> map = getInputMap(5, 40);
		map.put("days", "All");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");

		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		assertTrue(manageSearchResultsSectionPage.continueSearchWithValidationFailure("Select a minimum of 1 day and a maximum of 31 days."));
	}		
	
	@Test(dependsOnMethods="testManageAvailabilitySearchAndContinue")
	public void testSearchWithAllDays(){ //11
		Map<String, String> map = getInputMap(5, 36);
		map.put("days", "Mon,Tue,Wed,Thu,Fri");
//		map.put("update", "Availability");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();
		
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Triple"));
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Twin"));
	}	
	
	@Test(dependsOnMethods="testSearchWithAllDays")
	public void testEditManageRates(){ //12

	}
	
	@Test(dependsOnMethods="testEditManageRates")
	public void testInventoryAddAnother(){ //13
		Map<String, String> map = getInputMap(21, 30);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "7");
		map.put("Standard Twin", "7");
		
		assertTrue(topLinksPage.selectInventoryTab());
		inventoryGeneratorSectionPage.selectInventoryOptionsForExternalUser("Refresh");
		inventoryGeneratorSectionPage.selectInventoryOptionsForExternalUser("Create");
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.addAnotherInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveAddAnotherOrCopyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessingForExternalUser());
	}

	@Test(dependsOnMethods="testInventoryAddAnother")
	public void testInventoryCopy(){ //14
		Map<String, String> map = getInputMap(31, 35);
		map.put("inventory type", "Flexible");
		map.put("days", "All");
		map.put("release by", "days prior");
		map.put("release days", "2");
		map.put("room types", "Standard Triple,Standard Twin");
		map.put("Standard Triple", "2");
		map.put("Standard Twin", "2");
		
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.copyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveAddAnotherOrCopyInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessingForExternalUser());
	}

	@Test(dependsOnMethods="testInventoryCopy")
	public void testInventoryCreateAndCancel(){ //15
		assertTrue(inventoryGeneratorSectionPage.cancelCreateInventory());
	}
	public void testSearchWithDays(){ //7
		Date startDate = DateUtil.addDays(new Date(), 5);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), 20);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);

//		map.put("update", "Availability");
		map.put("days", "All");
		map.put("room types", "yes");
		map.put("selected room type", "Standard Triple,Standard Twin");
		
		assertTrue(topLinksPage.selectManageRateTab());
		
		manageSearchResultsSectionPage.clickSearch();
		manageSearchResultsSectionPage.search(map);
		manageSearchResultsSectionPage.continueSearch();

		startDate = DateUtil.addDays(new Date(), 5);
		manageSearchResultsSectionPage.refreshRatesForExternalUser();
		manageSearchResultsSectionPage.setValueForFlexibleAvailability("5", "Standard Triple");
		manageSearchResultsSectionPage.setValueForFlexibleAvailability("5", "Standard Twin");
		
		manageSearchResultsSectionPage.setValueForReleaseDays("2", "Standard Triple");
		manageSearchResultsSectionPage.setValueForReleaseDays("2", "Standard Twin");
		
		manageSearchResultsSectionPage.clickCopyToRightForFlexibleAvail("Standard Triple");
		manageSearchResultsSectionPage.clickCopyToRightForFlexibleAvail("Standard Twin");
		
		manageSearchResultsSectionPage.clickCopyToRightForReleaseDays("Standard Triple");
		manageSearchResultsSectionPage.clickCopyToRightForReleaseDays("Standard Twin");
		
		manageSearchResultsSectionPage.updateEditRates("Standard Triple");
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Triple"));
		
		manageSearchResultsSectionPage.updateEditRates("Standard Twin");
		assertTrue(manageSearchResultsSectionPage.verifyDates(map,"Standard Twin"));
	}	
	@AfterClass
	public void close(){
//		finaliseTest();
	}
	
	private void executeAsserts(Map<String, String> map, String roomType, String priority, String totAvil, 
			String flexAvail, String flexRelDays, String freesale, 
			boolean flexStopSell){
		
		assertTrue(manageSearchResultsSectionPage.verifyDates(map, roomType));
		
		assertTrue(manageSearchResultsSectionPage.verifyPriority(priority, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyTotalAvailable(totAvil, roomType));
		
		assertTrue(manageSearchResultsSectionPage.verifyFlexibleAvailability(flexAvail, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyFlexibleReleaseDays(flexRelDays, roomType));
		
		assertTrue(manageSearchResultsSectionPage.verifyFreesale(freesale, roomType));
		assertTrue(manageSearchResultsSectionPage.verifyFlexibileStopSell(flexStopSell, roomType));
	}
	
	private Map<String, String> getInputMap(int addStartDay, int addEndDay){
		Date startDate = DateUtil.addDays(new Date(), addStartDay);
		String todayDay = Integer.toString(DateUtil.getDay(startDate));
		String todayMonth = DateUtil.getMonthName(startDate);
		String todayYear = Integer.toString(DateUtil.getYear(startDate));
		
		Date futureDate = DateUtil.addDays(new Date(), addEndDay);
		String endDay = Integer.toString(DateUtil.getDay(futureDate));
		String endMonth = DateUtil.getMonthName(futureDate);
		String endYear = Integer.toString(DateUtil.getYear(futureDate));

		Map<String, String> map = new HashMap<String, String>();
		map.put("start day", todayDay);
		map.put("start month", todayMonth);
		map.put("start year", todayYear);
		map.put("end day", endDay);
		map.put("end month", endMonth);
		map.put("end year", endYear);
		
		return map;
	}
	
	private void executeCreateTestCase(Map<String, String> map, String userType){ 
		assertTrue(topLinksPage.selectInventoryTab());
		if (userType.equalsIgnoreCase("external")){
			inventoryGeneratorSectionPage.selectInventoryOptionsForExternalUser("Refresh");
			inventoryGeneratorSectionPage.selectInventoryOptionsForExternalUser("Create");
		}else{
			inventoryGeneratorSectionPage.selectInventoryOptions("Refresh");
			inventoryGeneratorSectionPage.selectInventoryOptions("Create");
		}
		inventoryGeneratorSectionPage.createInventory(map);
		inventoryGeneratorSectionPage.saveInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventory_create"),"Please review the data before saving."));
		inventoryGeneratorSectionPage.saveSaveInventory();
		assertTrue(inventoryGeneratorSectionPage.verifyRecordUpdatedMessage(By.id("inventoryContent"),"An inventory generation instruction has been created. The instruction will be processed shortly."));
		if (userType.equalsIgnoreCase("external")){
			assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessingForExternalUser());
		}else{
			assertTrue(inventoryGeneratorSectionPage.waitForInventoryProcessing());
		}
	}	

	public void selectProviderFromTheList(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "provider"));
		map.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "model"));
		map.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 7, "channel"));
		
		inventoryGeneratorSectionPage.sleep(1);
		WebElement parentElement = GSPageBase.waitForElement(By.id("propertyContractContent"));
		Table providerTable = new Table(parentElement, 1);
		for (int i=1;i<providerTable.getRowCount();i++){
			boolean flag = true;
			for (String key : map.keySet()){
				String value = map.get(key);
				if (providerTable.getCellData(i, key).equalsIgnoreCase(value)){
					continue;
				}else{
					flag = false;
					break;
				}
			}
			if (flag){
				providerTable.getCell(i, "Provider").click();
				inventoryGeneratorSectionPage.sleep();
				return;
			}
		}
	}
}
