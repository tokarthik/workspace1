package com.gs.contract.test.staticrateplans;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.common.LoginPage;
import com.gta.travel.page.object.contract.rateplans.RatePlanDetailsSectionPage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;

public class RatePlanStaticDetailsTest extends GSTestBase {

	private LoginPage loginPage;
	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	private RatePlanDetailsSectionPage rpDetailsSectionPage;
	
	@BeforeClass
	public void init(){
		setEnvironment("TEST");
		setBrowser("firefox");
		initialiseEnvironmentAndTest();
		openBrowser();
	}
	
	@Test
	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		homePage = loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
	
	@Test(dependsOnMethods="testLogin")
	public void testHomePage(){
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountryAndCity(){
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model");

		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", propertyName);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		assertTrue(searchResultsPage.getSearchResultsCount()>1);
	}

	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSelectRecordFromSearchResults(){
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("Property Name", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "property_name"));
		inputMap.put("Provider", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "provider"));
		inputMap.put("Model", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "model"));
		inputMap.put("Channel", ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 1, "channel"));
		searchResultsPage.selectRecordFromSearchResults(inputMap);
	}
	
	@Test(dependsOnMethods="testSelectRecordFromSearchResults")
	public void testResetRatePlanData(){
		rpDetailsSectionPage = RatePlanDetailsSectionPage.getInstance();
		rpDetailsSectionPage.deleteAllRatePlans();
	}
	
	@Test(dependsOnMethods="testResetRatePlanData")
	public void testCreateRatePlanWithValidationError(){ //2
		Map<String, String> inputMap = new HashMap<String, String>();
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlanWithValidationErrors(By.id("code-errors"));
		assertTrue(rpDetailsSectionPage.verifyFieldError(By.id("code-errors"), "Please specify a code for the rate plan"));
		assertTrue(rpDetailsSectionPage.verifyFieldError(By.id("name-errors"), "Please specify a name for the rate plan"));
	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithValidationError")
	public void testCreateRatePlanAndCancel(){ //3
		Map<String, String> inputMap = new HashMap<String, String>();
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int countBefore = rpDetailsSectionPage.getRatePlanListcount(); 
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.cancelCreateRatePlan();
		int countAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertTrue(countAfter == countBefore);
	}
	
	@Test(dependsOnMethods="testCreateRatePlanAndCancel")
	public void testCreateRatePlanWithCodeROAndSave(){ //4
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RO");
		inputMap.put("rate plan name", "Standard");
//		inputMap.put("enter rate as", "Nett");
//		inputMap.put("margin", "99");
		inputMap.put("meal basis", "Room Only");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithCodeROAndSave")
	public void testCreateRatePlanWithCodeRBAndSave(){ //5
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RB");
		inputMap.put("rate plan name", "Business");
//		inputMap.put("enter rate as", "Gross");
//		inputMap.put("margin", "99");
		inputMap.put("meal basis", "Room and Breakfast");
		inputMap.put("meal breakfast", "Full Breakfast");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithCodeRBAndSave")
	public void testCreateRatePlanWithCodeRDAndSave(){ //6
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RD");
		inputMap.put("rate plan name", "Deluxe");
//		inputMap.put("enter rate as", "Nett");
//		inputMap.put("margin", "99");
		inputMap.put("meal basis", "Room and Dinner");
		inputMap.put("meal breakfast", "Full Breakfast");
		inputMap.put("meal dinner", "3 course Dinner");

		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithCodeRDAndSave")
	public void testCreateRatePlanWithCodeRFAndSave(){ //7
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "RF");
		inputMap.put("rate plan name", "Grand Deluxe");
//		inputMap.put("enter rate as", "Nett");
//		inputMap.put("margin", "99");
		inputMap.put("meal basis", "Full Board");
		inputMap.put("meal breakfast", "Buffet Breakfast");
		inputMap.put("meal lunch", "Buffet Lunch");
		inputMap.put("meal dinner", "Buffet Dinner");

		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);

	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithCodeRFAndSave")
	public void testCreateRatePlanWithCodeAIAndSave(){ //8
		Map<String, String> inputMap = new HashMap<String, String>();
		inputMap.put("rate plan code", "AI");
		inputMap.put("rate plan name", "Superior Deluxe");
//		inputMap.put("enter rate as", "Nett");
//		inputMap.put("margin", "99");
		inputMap.put("meal basis", "All Inclusive");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanOptions("Create");
		rpDetailsSectionPage.createRatePlan(inputMap);
		rpDetailsSectionPage.saveCreateRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record created"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter-1);
	}
	
	@Test(dependsOnMethods="testCreateRatePlanWithCodeAIAndSave")
	public void testEditRFRatePlanAndSave(){ //9
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "RF");
		selectMap.put("Description", "Grand Deluxe");
		
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Edit");
		
		Map<String, String> editMap = new HashMap<String, String>();
		editMap.put("rate plan code", "RF1");
		editMap.put("rate plan name", "Grand Deluxe_1");
//		editMap.put("margin", "98");
		editMap.put("meal basis", "Full Board");
		editMap.put("meal breakfast", "Full Breakfast");
		editMap.put("meal lunch", "2 course Lunch");
		editMap.put("meal dinner", "4 course Dinner");

		rpDetailsSectionPage.editRatePlan(editMap);
		rpDetailsSectionPage.updateEditRatePlan();
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Record updated"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}
	
	@Test(dependsOnMethods="testEditRFRatePlanAndSave")
	public void testEditRatePlanAndCancel(){ //10
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "RF1");
		selectMap.put("Description", "Grand Deluxe_1");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Edit");
		
		Map<String, String> editMap = new HashMap<String, String>();
		editMap.put("meal dinner", "3 course Dinner");
		
		rpDetailsSectionPage.editRatePlan(editMap);
		rpDetailsSectionPage.cancelEditRatePlan();
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");

		Map<String, String> verifyMap = new HashMap<String, String>();
		verifyMap.put("rate plan code", "RF1");
		verifyMap.put("rate plan name", "Grand Deluxe_1");

		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		assertTrue(rpDetailsSectionPage.verifyDataUpdated(verifyMap));
	}
	
	@Test(dependsOnMethods="testEditRatePlanAndCancel")
	public void testEnableRatePlan(){ //11
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "RF1");
		selectMap.put("Description", "Grand Deluxe_1");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Enable");
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Rate plan enabled"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}
	
	@Test(dependsOnMethods="testEnableRatePlan")
	public void testHoldRatePlan(){ //12
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "RF1");
		selectMap.put("Description", "Grand Deluxe_1");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Hold");
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Rate plan held"));
		assertTrue(rpDetailsSectionPage.verifyRecordStatus(selectMap, "Suspended"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}
	
	@Test(dependsOnMethods="testHoldRatePlan")
	public void testCancelRatePlanAndClickCancel(){ //13
		/** This functionality not available in the current application */	
	}
	
	@Test(dependsOnMethods="testCancelRatePlanAndClickCancel")
	public void testCancelRatePlanAndClickOK(){ //14
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "RF1");
		selectMap.put("Description", "Grand Deluxe_1");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Cancel");
		assertTrue(rpDetailsSectionPage.verifyRecordUpdatedMessage("Rate plan cancelled"));
		assertTrue(rpDetailsSectionPage.verifyRecordStatus(selectMap, "Cancelled"));
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}
	
	@Test(dependsOnMethods="testCancelRatePlanAndClickOK")
	public void testDeleteRatePlanAndClickCancel(){ //15
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "AI");
		selectMap.put("Description", "Superior Deluxe");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountBefore = rpDetailsSectionPage.getRatePlanListcount();
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Delete");
		rpDetailsSectionPage.deleteAndCancelRatePlan();
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		int rpRowCountAfter = rpDetailsSectionPage.getRatePlanListcount();
		assertEquals(rpRowCountBefore, rpRowCountAfter);
	}
	
	@Test(dependsOnMethods="testDeleteRatePlanAndClickCancel")
	public void testDeleteRatePlanAndClickOK(){ //16
		Map<String, String> selectMap = new HashMap<String, String>();
		selectMap.put("rate plan code", "AI");
		selectMap.put("Description", "Superior Deluxe");
		
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
		rpDetailsSectionPage.selectRatePlanFromTheList(selectMap);
		rpDetailsSectionPage.selectRatePlanOptions("Delete");
		rpDetailsSectionPage.deleteAndOKRatePlan();
		rpDetailsSectionPage.verifyRecordUpdatedMessage("Record deleted");
		rpDetailsSectionPage.selectRatePlanOptions("Refresh");
	}
	
	@Test(dependsOnMethods="testDeleteRatePlanAndClickOK")
	public void testSortOnRatePlanCode(){ //17.1
		assertTrue(rpDetailsSectionPage.sortOnRatePlanCode());
	}
	
	@Test(dependsOnMethods="testSortOnRatePlanCode")
	public void testSortOnStatus(){ //17.2
		assertTrue(rpDetailsSectionPage.sortOnStatus());
	}
	
	@Test(dependsOnMethods="testSortOnStatus")
	public void testRefresh(){ //17.3
		/** This functionality tested in above test cases  */
	}
	
	@AfterClass
	public void close(){
		finaliseTest();
	}
}
