package com.gs.common.test;


import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.LoginPage;

/**
 * This is a test class for Login page
 * @author Karthikeyan
 *
 */
@Test(groups={"loginGroup"})
public class LoginTest extends GSTestBase {

	private LoginPage loginPage;

	public void testLogin(){
		loginPage = PageFactory.initElements(getDriver(),LoginPage.class);
		loginPage.login(getWebId().trim(), getUserName().trim(), getPassword().trim());
	}
}
