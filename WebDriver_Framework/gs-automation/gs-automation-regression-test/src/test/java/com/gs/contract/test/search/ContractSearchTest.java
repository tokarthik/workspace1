package com.gs.contract.test.search;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.gta.travel.page.base.GSTestBase;
import com.gta.travel.page.object.common.HomePage;
import com.gta.travel.page.object.contract.search.ContractSearchPage;
import com.gta.travel.page.object.contract.search.ContractSearchResultsPage;
import com.mediaocean.qa.framework.utils.DateUtil;

/**
 * Test class for Contracts Search.
 * @author Karthikeyan
 *
 */
@Test(groups={"contractSearchGroup"}, dependsOnGroups={"loginGroup"})
public class ContractSearchTest extends GSTestBase {

	private HomePage homePage;
	private ContractSearchPage searchPage;
	private ContractSearchResultsPage searchResultsPage;
	
	@Test
	public void testHomePage(){
		homePage = PageFactory.initElements(getDriver(),HomePage.class);
		searchPage = homePage.selectContract();
	}
	
	@Test(dependsOnMethods="testHomePage")
	public void testSearchWithCountry(){
		
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithCountry")
	public void testSearchWithCity(){
		
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("City", country);
		map.put("validation failure", "country-errors");
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyErrorMessage("Please select a country");
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithCity")
	public void testSearchWithCountryAndCity(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithCountryAndCity")
	public void testSearchWithPropertyType(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String propertyType = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "property_type");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Property Type", propertyType);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithPropertyType")
	public void testSearchWithPropertyName(){
		homePage.clickHomeLink();
		homePage.selectContract();
		
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String propertyName = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "property_name");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Property Name", propertyName);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithPropertyName")
	public void testSearchWithProvider(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String provider = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "provider");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Provider", provider);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}	
	@Test(dependsOnMethods="testSearchWithProvider")
	public void testSearchModel(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String model = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "model");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Model", model);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchModel")
	public void testSearchWithChannelB2C(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String channel = "Consumer (B2C)";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Channel", channel);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);
	}
	
	@Test(dependsOnMethods="testSearchWithChannelB2C")
	public void testSearchWithChannelB2B(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String channel = "Trade (B2B)";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Channel", channel);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);	
	}
	
	@Test(dependsOnMethods="testSearchWithChannelB2B")
	public void testSearchWithChannelB2B2C(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String city = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "city");
		String channel = "Trade Consumer (B2B2C)";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("City", city);
		map.put("Channel", channel);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);	
	}
	
	@Test(dependsOnMethods="testSearchWithChannelB2B2C")
	public void testSearchWithStatusLive(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String status = "Live";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Status", status);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);	
	}
	
	@Test(dependsOnMethods="testSearchWithStatusLive")
	public void testSearchWithStatusSuspended(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String status = "Suspended";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Status", status);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);		
	}
	
	@Test(dependsOnMethods="testSearchWithStatusSuspended")
	public void testSearchWithAllStatusesExceptCancelled(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String status = "Pending,Live,Suspended";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Status", status);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		status = "Ready-to-go-live,Pending,Live,Suspended";
		map.put("Status", status);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);	
	}
	
	@Test(dependsOnMethods="testSearchWithAllStatusesExceptCancelled")
	public void testSearchWithStatusCancelled(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String status = "Cancelled";
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Status", status);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);		
	}
	
	@Test(dependsOnMethods="testSearchWithStatusCancelled")
	public void testSearchWithTravelDateOneMonth(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		Date todayDate = new Date();
		Date futureDate = DateUtil.addMonth(todayDate, 1);
		String startDate = DateUtil.convertDateToString(todayDate, "dd/MMM/yy");
		String endDate = DateUtil.convertDateToString(futureDate, "dd/MMM/yy");
		String travelDate = startDate + "-" + endDate;
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Travel Dates", travelDate);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);		
	}

	@Test(dependsOnMethods="testSearchWithTravelDateOneMonth")
	public void testSearchWithTravelDateOneYear(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		Date todayDate = new Date();
		Date futureDate = DateUtil.addYears(todayDate, 1);
		String startDate = DateUtil.convertDateToString(todayDate, "dd/MMM/yy");
		String endDate = DateUtil.convertDateToString(futureDate, "dd/MMM/yy");
		String travelDate = startDate + "-" + endDate;
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Travel Dates", travelDate);
		
		map.put("validation failure", "propertyContractSearch-endDate-error-msg");
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyErrorMessage("The end date cannot be more than one year beyond the start date");
		assertTrue(result);	
	}
	
	@Test(dependsOnMethods="testSearchWithTravelDateOneYear")
	public void testSearchWithStarRating(){
		homePage.clickHomeLink();
		homePage.selectContract();
		String country = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "country");
		String starRating = ContractSearchPage.excelUtil.getCellAsString("Contract_Search", 5, "star_rating");
		Map<String, String> map = new HashMap<String, String>();
		map.put("Country", country);
		map.put("Star Rating", starRating);
		
		searchPage.clearSearchFields();
		searchResultsPage = searchPage.search(map);
		boolean result = searchResultsPage.verifyResults(map);
		assertTrue(result);	
	}	
}
