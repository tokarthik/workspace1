package com.gs.test.execute;

import java.util.Locale;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.gs.common.test.LoginTest;
import com.gs.common.test.SetUpTest;
import com.gs.contract.test.search.ContractSearchTest;
import com.gs.test.report.plugin.GSTestNgXsltMojo;

/**
 * This is the test suite class for contract search
 * @author Karthikeyan
 *
 */
public class ContractSearchTestSuit { 
	
	public static void main(String args[]) throws Exception{
		ContractSearchTestSuit contractSearchTestSuit = new ContractSearchTestSuit();
		contractSearchTestSuit.method();
	} 
	public void method() throws Exception{
		
		TestListenerAdapter tla = new TestListenerAdapter();
        TestNG testng = new TestNG();
        testng.setTestClasses(new Class[] {
        									SetUpTest.class,
        									LoginTest.class,
		        							ContractSearchTest.class
        								  });
        testng.addListener(tla);
        testng.setGroups("setUpGroup,loginGroup,contractSearchGroup");
        testng.run();  
        
        if (tla.getFailedTests().size() > 0){
        	tla.getFailedTests().get(0).getThrowable().printStackTrace();
        }
        
        GSTestNgXsltMojo gsTestNgXsltMojo = new GSTestNgXsltMojo();

        String targetDir = System.getProperty("targetDir");
        String surefireReportDirectory = System.getProperty("surefireReportDirectory");
		
        if (targetDir == null){
	        targetDir = getClass().getResource(getClass().getSimpleName()+".class")
										.getPath().split("/test-classes/")[0];
		}
		
        if (surefireReportDirectory == null){
        	gsTestNgXsltMojo.setSurefireReportDirectory(targetDir.split("/target")[0]+"\\test-output");
        }
        
        gsTestNgXsltMojo.setOutputDir(targetDir);
        
        // Need to mention the sure fire report path here.
        gsTestNgXsltMojo.executeReport(new Locale("EN"));
	}
}
